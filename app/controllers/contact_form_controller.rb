class ContactFormController < ApplicationController
   before_filter :authenticate
  before_filter :has_roles  
  before_filter :get_product
  before_filter :get_product_view
  before_filter :get_communities  
  before_filter :get_communities_view    
  before_filter :get_parcels    
  before_filter :get_parcels_view

  def index
  	@contact_info = ContactForm.all
  end
  def edit
  	@contact_info = ContactForm.find(params[:id])
  end
  def update
  	contact_info_details = ContactForm.find(params[:id]) unless params[:id].nil?    
    contact_info_details.update_attributes({:Name => params[:name], :Email_Address => params[:email_address], :Contact_Num => params[:contact_num]}) unless contact_info_details.nil?
    redirect_to "/contact_form"
  end
end