class ImageGalleryController < ApplicationController  
	before_filter :authenticate
	before_filter :has_roles 
	before_filter :get_product
  	before_filter :get_product_view
  	before_filter :get_comm_city_view
  	before_filter :get_community_city
  	before_filter :get_communities  
  	before_filter :get_communities_view    
  	before_filter :get_parcels    
  	before_filter :get_parcels_view
	before_filter :set_res_header, :only => [:save_images]
	

	def index
		@opted_community = params[:community]
        @opted_product = params[:project]
        @opted_parcel = params[:parcel]
        @key_image_flag = false        
        # @images = ImageGallery.where("product_type=? and community=?", @opted_product, @opted_community) 
        @images = ImageGallery.where("product_type LIKE ? and community=?", "%#{@opted_product}%", @opted_community) 
        @houses = ImageGallery.get_lots_based_on_product_type(@opted_product, @opted_community, @opted_parcel)
        render "image_gallery/index.js.erb"
	end  

	def upload_view  

	end


	def save_images	
		
		all_images_array = Array.new
		images_exists_array = Array.new
		yo_url_wout_http = params[:youtube_url].sub(/^https?\:\/\//, '')
		yo_url_video_id = yo_url_wout_http.split("/").last.split("=").last  unless yo_url_wout_http.blank?
		yo_image_exists = ImageGallery.where("ytube_video_id = ? and product_type = ? and community = ?", yo_url_video_id, params[:product_type], params[:community]).exists?
		if params[:image_gallery].present? && params[:image_gallery][0][:pics].present? || params[:youtube_url].present?
			if params[:image_gallery].present? && params[:image_gallery][0][:pics].present?
				params[:image_gallery].each do |img| 
				all_files = (img[:pics].original_filename.include?(" ")) ? img[:pics].original_filename.tr!(" ", "_") : img[:pics].original_filename
				all_images_array << all_files
				end

				all_images_array.each do |each_image|          
				image_exists = ImageGallery.where("pics_file_name = ? and product_type LIKE ? and community = ?", each_image, "%#{params[:product_type]}%", params[:community]).exists?
					if image_exists
					images_exists_array << each_image+" already exists"
					end
				end

				unless images_exists_array.empty?
					render :json=> {:status=>"error", :upload_image_errors=>images_exists_array}
				else  
					image_obj = params["image_gallery"]
					if yo_image_exists
						render :json=> {:status=>"error", :msg=>"Youtube url already exists"}
					else	
						unless params[:youtube_url].blank?
							y_url=Tbldatum.validate_ytubeurl(params[:youtube_url])
							if ( (y_url =~ /^http[s]?:\/\/www\.youtube\.com\/watch\?v=([a-zA-Z0-9_-]*)/) || (y_url =~ /^http[s]?:\/\/youtu\.be([a-zA-Z0-9_-]*)/) )
								ImageGallery.create(:youtube_url => yo_url_wout_http, :ytube_video_id => yo_url_video_id, :product_type => params[:product_type], :community => params[:community]) unless params[:youtube_url].blank?			
								(0...image_obj.count).each do |i|				
									ImageGallery.create( image_obj[i].merge(:product_type => params[:product_type], :community => params[:community]) )
								end	
								render :json=> {:status=>"success"}  
							else
								render :json=> {:status=>"error", :msg=>"Please ener valid youtube url"}	
							end
						else
							(0...image_obj.count).each do |i|				
									ImageGallery.create( image_obj[i].merge(:product_type => params[:product_type], :community => params[:community]) )
							end
							render :json=> {:status=>"success"} 
						end																
					end

				end  
			else	
										
				if yo_image_exists
					render :json=> {:status=>"error", :msg=>"Youtube url already exists"}
				else	
					y_url=Tbldatum.validate_ytubeurl(params[:youtube_url])
					if ( (y_url =~ /^http[s]?:\/\/www\.youtube\.com\/watch\?v=([a-zA-Z0-9_-]*)/) || (y_url =~ /^http[s]?:\/\/youtu\.be([a-zA-Z0-9_-]*)/) )

						#saving the url without http or https bcz if we are saving the entire url it contains
						#http or https. while validating the uniqueness it will be checking for entire url, if we are 
						#giving as http and gallery already contains with https, it is saving as other link.

						ImageGallery.create(:youtube_url => yo_url_wout_http, :ytube_video_id => yo_url_video_id,:product_type => params[:product_type], :community => params[:community]) unless params[:youtube_url].blank?			
						render :json=> {:status=>"success"}  
					else
						render :json=> {:status=>"error", :msg=>"Please ener valid youtube url"}	
					end
				end
			end
		else
			render :json=> {:status=>"error",:msg=> "Please Upload an image or Youtube Url"}
		end
	end

	def delete_images
	@msg = ""			
		 params["image_gallery"].each do |gallery_item|
		 	img = ImageGallery.find(gallery_item["id"])		 	
		 	unless gallery_item["_destroy"].nil?
		 		img.destroy
		 		@msg+= "Deleted"
		 	else
		 		link_name = ImageGallery.validate_url(gallery_item["navigation_link"]) unless gallery_item["navigation_link"].blank?
		 		img.update_attributes(:navigation_link=>link_name, :title => gallery_item["title"], :description => gallery_item["description"] )		 		
		 	end
		 end

		 unless params["key_image"].blank?
 		params["key_image"].each do |key_image|
		 unless key_image["_key_image"].nil?		 	
		 	key_image_rec = GalleryImagesToLot.find_by_tbldatum_id_and_key_image(params[:lots_dropdown],1 )
		 	key_image_rec.update_attributes(:key_image => 0) unless key_image_rec.blank?
		 	@lot_number = params[:lots_dropdown]
		 	lot_rec = GalleryImagesToLot.find_by_tbldatum_id_and_image_gallery_id(params[:lots_dropdown], key_image["id"])
		 	lot_rec.update_attributes(:key_image => key_image["_key_image"]) unless lot_rec.blank?
		 	@msg+= "Saved"
		 end
	end	
	end	 
		  respond_to do |format|
        msg = { :status => "success", :message => @msg}
        format.json  { render :json => msg }        
      end
end

	def gallery_items	
		images =[]	
		lot_images =[]
		plan_images = []
		images_list =[]
		lot_images_list =[]
		@opted_product = params[:project]
        
        lot = Tbldatum.find(params[:lot_id])
        lot_images = ImageGallery.where("product_type LIKE ? and community = ? and plan_id IS NULL","%#{@opted_product}%", params[:community])              
        plan_images = ImageGallery.find_all_by_plan_id_and_product_type(lot.Floor_Plan_Id, @opted_product) unless lot.Floor_Plan_Id.blank?
        images = lot_images + plan_images

        lot_images_list = Tbldatum.find(params[:lot_id]).image_galleries.where("gallery_images_to_lots.product_type LIKE ? and gallery_images_to_lots.community=?", "%#{@opted_product}%", params[:community])       
        images_list = lot_images_list + plan_images
        selected_image_ids = images_list.map{|img| img.id}
		render :partial => "image_gallery/gallery_items", :locals => {:images => images, :lot_id => params[:lot_id], :selected_image_ids => selected_image_ids,
			:user_role => params[:user_role], :access_permission => params[:access_permission],:parent_tab_id => params[:parent_tab_id], :community => params[:community],
			:product_type => params[:project], :phase_parcel => params[:phase_parcel],  :lot_number => lot.Lot_Number}	
		end

	def view_images
		lot = LotInfo.find(params[:lot_id])
	    images=lot.image_galleries.all
	    render :partial => '/image_gallery/view_images.html.erb', :locals => {:images => images, :images_count => images.count}
  end

   def show_images_based_on_product_type
  	@access_permission = params[:access_permission]
  	@opted_community = params[:community]
  	@opted_product = params[:project]
  	@opted_parcel = params[:parcel]
  	if params[:lot_number].blank?
        # @images = ImageGallery.where("product_type=? and community=?", @opted_product, @opted_community) 
        @images = ImageGallery.where("product_type LIKE ? and community=?", "%#{@opted_product}%", @opted_community) 
  		@key_image_flag = false
  	else
  		@key_image_flag = true
  		@images= []    
    	images1= []    
    	images2= []   
  		
  		if @opted_parcel.blank?
		   @houses = ImageGallery.get_lots_based_on_product_type(@opted_product, @opted_community, @opted_parcel)
		   @opted_parcel = @houses.find_all{|k,v| k["lot_nbr"]==params[:lot_number]}.map{|x|x["phase_name"]} unless @houses.blank?	
  		end  	
		tbldatm_rec = Tbldatum.find_by_Lot_Number_and_Community_and_Phase_Name(params[:lot_number], @opted_community, @opted_parcel)  	  		
  		images1 = tbldatm_rec.image_galleries.where("gallery_images_to_lots.product_type like ?",  "%#{params[:project]}%")  
  		floor_plans_count = tbldatm_rec.Floor_Plan_Id.split(",").count unless tbldatm_rec.Floor_Plan_Id.blank?
      	if floor_plans_count == 1
        	images2 =ImageGallery.find_all_by_plan_id_and_product_type(tbldatm_rec.Floor_Plan_Id,@opted_product)
    	end
     	@images = images1 + images2
     	@selected_key_image_id = tbldatm_rec.Key_Image_Id
    end    
  		render :partial => "show_images"
  end

end
