class Tblusergroupaccess < ActiveRecord::Base  
  attr_protected
  self.table_name = "tbldatafrmfields"
  self.primary_key ="ID"
  belongs_to :group, :class_name => 'Tblusergroup', :primary_key => 'ID', :foreign_key => 'Group_id'
  belongs_to :menu_option, :class_name => 'MenuOption', :primary_key => 'MenuOptionName', :foreign_key => 'Menu_Option_Name'
  belongs_to :access, :class_name => 'Access', :primary_key => 'ID', :foreign_key => 'Access_id'

end