class AgentMasterOds < ActiveRecord::Base
	attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_AGENTMASTER"

  def self.load_ods_agent_master(windows_db_nm)

  	truncate_ods_agent_master

    puts "********** 1A) Started Loading AgentMasterOds Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from "+windows_db_nm+".dbo.AGENTMASTER");
      	puts "............................................."
      	puts "............................................."

        batch=[]
        batch_size = 1000

        result.each do |row|
          batch << AgentMasterOds.new(:AGENTCODE => row['AGENTCODE'],
                                      :AGENTNAME => row['AGENTNAME'],
                                      :BROKERCODE => row['BROKERCODE'],
                                      :ADDRESS2 => row['ADDRESS2'],
                                      :CITY => row['CITY'],
                                      :DISTHOUSE => row['DISTHOUSE'],
                                      :ZIP => row['ZIP'],
                                      :PHONENUMBER => row['PHONENUMBER'],
                                      :EXTENSION => row['EXTENSION'],
                                      :FAXNUMBER => row['FAXNUMBER'],
                                      :CELLNUMBER => row['CELLNUMBER'],
                                      :PAGERNUMBER => row['PAGERNUMBER'],
                                      :EMAILADDRESS => row['EMAILADDRESS'],
                                      :FIELD1 => row['FIELD1'],
                                      :FIELD2 => row['FIELD2'],
                                      :FIELD3 => row['FIELD3'],
                                      :FIELD4 => row['FIELD4'],
                                      :FIELD5 => row['FIELD5'],
                                      :FIELD6 => row['FIELD6']
								)
            if batch.size >= batch_size 
              AgentMasterOds.import batch
              batch = [] 
            end
        end
        AgentMasterOds.import batch
    end

    puts "Total no. of Loaded rows.........#{AgentMasterOds.count}"
    puts "********** 1B) Finished Loading AgentMasterOds Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_agent_master
		ActiveRecord::Base.connection.execute("TRUNCATE ODS_AGENTMASTER")
	end

 end