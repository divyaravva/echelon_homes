class ProspectFinanceOds < ActiveRecord::Base
	attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_PROSPECTFINANCE"
  has_one :lender_master, :class_name => "LenderMasterOds", :foreign_key => [:LENDERCODE],:primary_key => [:LENDERCODE] 

  def self.load_ods_prospect_finance(windows_db_nm)

  	truncate_ods_prospect_finance

    puts "********** 1A) Started Loading ProspectFinanceOds Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from "+windows_db_nm+".dbo.PROSPECTFINANCE");
      	puts "............................................."
      	puts "............................................."

        batch=[]
        batch_size = 1000

        result.each do |row|
          batch << ProspectFinanceOds.new(:CASENUMBER => row['CASENUMBER'],
                                      :LENDERCODE => row['LENDERCODE'],
                                      :LOANOFFICER => row['LOANOFFICER'],
                                      :PROCESSOR => row['PROCESSOR'],
                                      :LOANTYPE => row['LOANTYPE'],
                                      :LENDERRATING => row['LENDERRATING'],
                                      :OTHERRATING => row['OTHERRATING'],
                                      :MORTAPPLYDATE => row['MORTAPPLYDATE'],
                                      :MORTSUBMITDATE => row['MORTSUBMITDATE'],
                                      :MORTAPPROVEDATE => row['MORTAPPROVEDATE'],
                                      :CONTRACTEXPDATE => row['CONTRACTEXPDATE'],
                                      :OPTIONSCOMPDATE => row['OPTIONSCOMPDATE'],
                                      :COLORCOMPDATE => row['COLORCOMPDATE'],
                                      :CONSTCLOSEDATE => row['CONSTCLOSEDATE'],
                                      :MORTCLOSEDATE => row['MORTCLOSEDATE'],
                                      :LOTTOPERMITDATE => row['LOTTOPERMITDATE'],
                                      :DOCSENTDATE => row['DOCSENTDATE'],
                                      :MORTCONTRACTDATE => row['MORTCONTRACTDATE'],
                                      :PROJECTCLOSEDATE => row['PROJECTCLOSEDATE'],
                                      :TERMINATOR => row['TERMINATOR'],
                                      :CONTRACTAMOUNT => row['CONTRACTAMOUNT'],
                                      :QUOTEDRATE => row['QUOTEDRATE'],
                                      :QUOTEDPAYMENT => row['QUOTEDPAYMENT'],
                                      :POINTS => row['POINTS'],
                                      :CLOSINGCOSTS => row['CLOSINGCOSTS'],
                                      :BUILDERBUYDOWN => row['BUILDERBUYDOWN']
								)
            if batch.size >= batch_size 
              ProspectFinanceOds.import batch
              batch = [] 
            end
        end
        ProspectFinanceOds.import batch
    end

    puts "Total no. of Loaded rows.........#{ProspectFinanceOds.count}"
    puts "********** 1B) Finished Loading ProspectFinanceOds Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_prospect_finance
		ActiveRecord::Base.connection.execute("TRUNCATE ODS_PROSPECTFINANCE")
	end

 end