class PlanFieldDisplayNameController < ApplicationController

  before_filter :authenticate
  before_filter :has_roles  
  before_filter :get_product
  before_filter :get_product_view
  before_filter :get_communities  
  before_filter :get_communities_view    
  before_filter :get_parcels    
  before_filter :get_parcels_view

  def display_names
    display_names = PlanFieldDisplayName.find(1)
    params[:plan_name] = "Plan Name"  unless params[:plan_name].blank? == false
    params[:link_name] = "Plan Url"  unless params[:link_name].blank? == false
    params[:plan_description] = "Plan Description"  unless params[:plan_description].blank? == false
    params[:sq_feet] = "Sq Feet"  unless params[:sq_feet].blank? == false
    params[:floors] = "Floors"  unless params[:floors].blank? == false
    params[:bedrooms] = "Bedrooms"  unless params[:bedrooms].blank? == false
    params[:bathrooms] = "Bathrooms"  unless params[:bathrooms].blank? == false
    params[:garages] = "Garages"  unless params[:garages].blank? == false

    display_names.update_attributes(:Plan_Name => params[:plan_name], :Plan_Url => params[:link_name], :Plan_Description => params[:plan_description], :Sq_Feet => params[:sq_feet], :Floors => params[:floors], :Bedrooms => params[:bedrooms], :Bathrooms => params[:bathrooms], :Garages => params[:garages])
    redirect_to "/status_key"
  end
end