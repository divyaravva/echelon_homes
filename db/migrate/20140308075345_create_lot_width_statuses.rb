class CreateLotWidthStatuses < ActiveRecord::Migration
  def change
    create_table :lot_width_statuses ,:id=>false do |t|
      t.primary_key :ID
      t.string :Lot_Width_Data
      t.string :Color

      t.timestamps
    end
  end
end
