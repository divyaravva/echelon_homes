class StatusKey < ActiveRecord::Base
	self.table_name = "GIS_LotStatusKey"
	attr_protected
  self.primary_key = :id, :MenuOptionName
     # has_many :tbldatums, :dependent => :destroy, :foreign_key => "Floor_Plan_Id"  

    def self.find_by_menu_option(menu_opt)
        Rails.cache.fetch("echelon_legend_keys_of_#{menu_opt}") do
            StatusDetail.find :all, :order => 'LegendOrder ASC',:conditions => ['LegendType =?', menu_opt]
        end
    end

  def self.get_all_statuses    
    StatusKey.order("StatusKey").where("LegendType = ?", "Floor Plan").map{|g| [g.StatusKey]}
  end
   
   def self.get_all_menu_options
      @product_id=ProductType.get_ids_of_product
      find_by_sql("select Distinct(MenuOptionName) from GIS_MenuOption where MenuName !='Gallery'").map{|g| g.MenuOptionName}
   end

  def self.get_distinct_legendtypes
    self.find_by_sql("select Distinct(LegendType) from GIS_LotStatusKey where MenuOptionName = 'Sales Status'")
  end
  def self.insert_legendtypes_fr_all_comm(community)
    self.where("Community = ?", "Grey Fox").each do |rec|
      sts_key_rec = StatusKey.find_or_initialize_by_MenuOptionName_and_LegendType_and_Community(rec.MenuOptionName, rec.LegendType, community)
      # StatusKey.create(:MenuOptionName => rec.MenuOptionName, :LegendType => rec.LegendType, :Community => community, :Product_Type => "Lot Insight", :legend_order => rec.legend_order) 
      sts_key_rec.update_attributes(:Product_Type => rec.Product_Type, :legend_order => rec.legend_order)
    end
  end

end