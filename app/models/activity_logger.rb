class ActivityLogger < ActiveRecord::Base	
	self.table_name="activity_logger"
	attr_protected
	serialize :event_info 
	serialize :event_info_previous
	serialize :event_info_changes

	def self.log_activity_on_lots(event_name, event_on_type, lot, user, before_save_lot, attrs_changes)
		user_id = user.id unless user.blank?
		self.create({
			:event => event_name, 
			:event_on_id => lot.id, 
			:event_on_type => event_on_type, 
			:event_date => DateTime.now, 
			:event_info	 => ActiveSupport::JSON.encode(lot), 
			:event_info_previous => ActiveSupport::JSON.encode(before_save_lot), 
			:event_info_changes => ActiveSupport::JSON.encode(attrs_changes),
			:event_by => user_id
			})	
	end

	def self.log_closed_lots_based_on_expected_closing_date_automated(lot)	
		self.create({:event => "Lot Closed Based On Expected Closing Date Automated", :event_on_id => lot.id, :event_on_type => "Lot", :event_date => DateTime.now, :event_info	 => ActiveSupport::JSON.encode(lot)})
	end

	def self.get_logs_based_on_type(log_type)		
		evts=[]
		 ActivityLogger.where(:event => log_type).each do|log|
		 	log_data = ActiveSupport::JSON.decode(log.event_info)
		 	if log_type == "Home Sale Cancellation"			 	
			 	unless log.event_info_previous.nil?
			 		log_data_previous_obj = ActiveSupport::JSON.decode(log.event_info_previous)
			 		log_data["Purchasers"] = log_data_previous_obj["Purchasers"]
			 		log_data["Buyer_Address"]= log_data_previous_obj["Buyer_Address"]
			 		log_data["Buyer_City"] = log_data_previous_obj["Buyer_City"]
			 		log_data["Buyer_State"] = log_data_previous_obj["Buyer_State"]
			 		log_data["Buyer_Zip"] = log_data_previous_obj["Buyer_Zip"]
			 		log_data["sales_agent"] = log_data_previous_obj["sales_agent"]			 		
			 	end
		 	end		 	
		 	referral_by = nil
		 	cancellation_reason_id = nil
		 	cancellation_reason_text = nil
		 	sales_representative = nil


		 	begin
		 		unless log_data["sales_agent"].blank? || SalesRep.find(log_data["sales_agent"]).blank?
		 			sales_representative = SalesRep.find(log_data["sales_agent"]).Sales_Rep_Name
		 		end
		 	rescue
		 		sales_representative = nil
		 	end

		 	begin
		 		event_by = log.event_by 
		 		unless log.event_by.blank? || Tblusers.find(log.event_by).blank?
		 			event_by = Tblusers.find(log.event_by).First_Name
		 		end
		 	rescue
		 		event_by = nil
		 	end

		 	begin
		 		unless log_data["Referral_By"].blank? || Referral.find(log_data["Referral_By"]).blank?
		 			referral_by = Referral.find(log_data["Referral_By"]).Type_Of_Referral
		 		end
		 	rescue
		 	    referral_by = nil		 
		 	end

		 	begin
		 		unless log_data["Cancellation_Reason"].blank? || CancellationReason.find(log_data["Cancellation_Reason"]).blank?
		 			cancel_reason = CancellationReason.find(log_data["Cancellation_Reason"])
		 			cancellation_reason_id = cancel_reason.id
		 			cancellation_reason_text = cancel_reason.cancel_reason
		 		end	
		 	rescue
		 		cancellation_reason_id = nil
		 		cancellation_reason_text = nil
		 	end	 	

		 	evts<<log_data.merge({
		 		"Event_Date" => log.event_date.strftime("%m/%d/%Y"), 		 		
		 		"sales_agent"=> sales_representative,
		 		"Event"    => log.event,
		 		"Event_By" => event_by,
		 		"Referral_By" => referral_by,
		 		"Cancellation_Reason_Id" => cancellation_reason_id ,
		 		"Cancellation_Reason" => cancellation_reason_text
		 		})
		 end
		evts
	end	

	def self.get_logs_based_on_type_dt(log_type,start_date, end_date, builders)
		 evts=[]
		 ActivityLogger.where(:event => log_type,:Event_Date => start_date..end_date+1).each do|log|
		 	log_data = ActiveSupport::JSON.decode(log.event_info)
		 	if builders.include?log_data["Builder"]
			 	if log_type == "Home Sale Cancellation"			 	
				 	unless log.event_info_previous.nil?
				 		log_data_previous_obj = ActiveSupport::JSON.decode(log.event_info_previous)
				 		log_data["Purchasers"] = log_data_previous_obj["Purchasers"]
				 		log_data["Buyer_Address"]= log_data_previous_obj["Buyer_Address"]
				 		log_data["Buyer_City"] = log_data_previous_obj["Buyer_City"]
				 		log_data["Buyer_State"] = log_data_previous_obj["Buyer_State"]
				 		log_data["Buyer_Zip"] = log_data_previous_obj["Buyer_Zip"]
				 		log_data["sales_agent"] = log_data_previous_obj["sales_agent"]				 		
				 	end
			 	end
			 	
			 	referral_by=nil
			 	cancellation_reason_id = nil
		 		cancellation_reason_text = nil
		 		sales_representative = nil
		 		
		 		
			 	begin
			 		unless log_data["sales_agent"].blank? || SalesRep.find(log_data["sales_agent"]).blank?
		 				sales_representative = SalesRep.find(log_data["sales_agent"]).Sales_Rep_Name
		 			end
		 		rescue
		 			sales_representative=nil
		 		end
		 		begin
				 	event_by = log.event_by 
				 	unless log.event_by.blank? || Tblusers.find(log.event_by).blank?
				 		event_by = Tblusers.find(log.event_by).First_Name
				 	end
				rescue
					event_by = nil
				end

				begin
			 		unless log_data["Referral_By"].blank? || Referral.find(log_data["Referral_By"]).blank?
			 			referral_by = Referral.find(log_data["Referral_By"]).Type_Of_Referral
			 		end
			 	rescue
					referral_by = nil		 		
			 	end

			 	begin
				 	unless log_data["Cancellation_Reason"].blank? || CancellationReason.find(log_data["Cancellation_Reason"]).blank?
			 			cancel_reason = CancellationReason.find(log_data["Cancellation_Reason"])
			 			cancellation_reason_id = cancel_reason.id
			 			cancellation_reason_text = cancel_reason.cancel_reason
			 	    end
			 	rescue
			 		cancellation_reason_id = nil
			 		cancellation_reason_text = nil
			 	end

			 	evts<<log_data.merge({
			 		"Event_Date" => log.event_date.strftime("%m/%d/%Y"), 			 		
			 		"sales_agent"=> sales_representative,
			 		"Event"    => log.event,
			 		"Event_By" => event_by,
			 		"Referral_By" => referral_by,			 		
			 		})
		 	end
		 end
		evts
	end


	def self.get_logs_based_on_type_to_test(log_type)		
		 ActivityLogger.where(:event => log_type).each do|log|
		 	log_data = ActiveSupport::JSON.decode(log.event_info)
		 	puts "--start--"
		 	puts "Log ID :"+log.id.to_s
		 	puts log.event_by
		 	puts log.event_date
		 	puts log_data["Unique_ID"]
		 	puts "--end--"
		 	puts ""
		 	puts ""
		 end	
	end

	def self.get_objs_event(log_id)		
		 activity=ActivityLogger.find log_id
		 log_data_pre = ActiveSupport::JSON.decode(activity.event_info_previous)
		 log_data_post = ActiveSupport::JSON.decode(activity.event_info)
		 log_data_changes = ActiveSupport::JSON.decode(activity.event_info_changes)


		 puts "******PRE******"
		 puts ""		
		 puts log_data_pre
		 puts ""		 
		 puts "*******PRE*****"
		 puts ""
		 puts ""		 
		 puts "*******POST*****"
		 puts ""		 
		 	puts log_data_post
		 puts ""		 
		 puts "*******POST*****"
		 puts ""
		 puts ""
		 puts "*******CHANGES*****"
		 puts ""
		 	puts log_data_changes
		 puts ""
		 puts "*******CHANGES*****"

		 puts ""
		 puts ""
		 puts "*******EVENT*****"
		 puts ""
		 	puts activity.event
		 puts ""
		 puts "*******EVENT*****"

		 puts ""
		 puts ""
		 puts "*******EVENT DATE*****"
		 puts ""
		 	puts puts activity.event_date
		 puts ""
		 puts "*******EVENT DATE*****"
	end


	def self.get_obj_post_event(log_id)		
		 ActivityLogger.where(:event => log_type).each do|log|
		 	log_data = ActiveSupport::JSON.decode(log.event_info)
		 	puts "--start--"
		 	puts "Log ID :"+log.id.to_s
		 	puts log.event_by
		 	puts log.event_date
		 	puts log_data["Unique_ID"]
		 	puts "--end--"
		 	puts ""
		 	puts ""
		 end	
	end
	
end
