class GlTransMasterOds < ActiveRecord::Base
	attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_GLTRANSMASTER"

  def self.load_ods_gltransmaster

  	truncate_ods_gltransmaster

    puts "********** 1A) Started Loading GLTRANSMASTER Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
      #message = "Inside the Lots info Load Method"
      #Mailer.notify_user_on_client_failure(message).deliver
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from Vantage_Homes_ODS.dbo.GLTRANSMASTER");
      	puts "Total no. of ODS rows.........#{result.count}"
      	puts "............................................."
      	puts "............................................."

        batch=[]
        batch_size = 1000

        result.each do |row|
          #GLTRANSMASTER.find_or_initialize_by_COMPANYCODE_and_DEVELOPMENTCODE_and_LOTNUMBER(row['COMPANYCODE'], row['DEVELOPMENTCODE'], row['LOTNUMBER'])
          batch << GlTransMasterOds.new(:COMPANYCODE => row['COMPANYCODE'],
                                        :DEVELOPMENTCODE => row['DEVELOPMENTCODE'],
                                        :HOUSENUMBER => row['HOUSENUMBER'],
                                        :GLACCOUNT => row['GLACCOUNT'],
                                        :TRANSREMARK => row['TRANSREMARK'],
                                        :DRCRCODE => row['DRCRCODE'],
                                        :AMOUNT => row['AMOUNT']
                                      )
            if batch.size >= batch_size
              GlTransMasterOds.import batch
              batch = [] 
            end
        end
        # GlTransMasterOds.mass_insert(batch, :per_batch => 1000)
        GlTransMasterOds.import batch
    end

    puts "Total no. of Loaded rows.........#{GlTransMasterOds.count}"
    puts "********** 1B) Finished Loading GLTRANSMASTER Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_gltransmaster
		ActiveRecord::Base.connection.execute("TRUNCATE ODS_GLTRANSMASTER")
	end


end