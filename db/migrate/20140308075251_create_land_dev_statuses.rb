class CreateLandDevStatuses < ActiveRecord::Migration
  def change
    create_table :land_dev_statuses ,:id=>false do |t|
      t.primary_key :ID
      t.string :Land_Dev_Status
      t.string :Color

      t.timestamps
    end
  end
end
