class CategoryCostCode < ActiveRecord::Base
	attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_CATEGORYCOSTCODES"
  self.primary_key = "id"

  def self.load_ods_uddev_master

  	truncate_ods_uddev_master

    puts "********** 1A) Started Loading UddevMasterOds Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from Vantage_Homes_ODS.dbo.UDDEVMASTER where DEVELOPMENTCODE IN ('ND', 'NF', 'NH', 'CB', 'CP', 'FH', 'TF')");
      	puts "Total no. of ODS rows.........#{result.count}"
      	puts "............................................."
      	puts "............................................."

        batch=[]
        batch_size = 1000

        result.each do |row|
          batch << UddevMasterOds.new(:COMPANYCODE => row['COMPANYCODE'],
                                      :DEVELOPMENTCODE => row['DEVELOPMENTCODE'],
                                      :DEVELOPCITY => row['DEVELOPCITY'],
                                      :DEVELOPZIP => row['DEVELOPZIP'],
                                      :DEVLEGALNAME => row['DEVLEGALNAME'],
                                      :TERMS_ESCALATOR => row['TERMS_ESCALATOR'],
                                      :TERMS_PARTICIPAT => row['TERMS_PARTICIPAT'],
                                      :TERMS_TAKEDOWNS => row['TERMS_TAKEDOWNS']
								)
        end
        UddevMasterOds.mass_insert(batch, :per_batch => 1000)
    end

    puts "Total no. of Loaded rows.........#{UddevMasterOds.count}"
    puts "********** 1B) Finished Loading UddevMasterOds Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_uddev_master
		ActiveRecord::Base.connection.execute("TRUNCATE ODS_UDDEVMASTER")
	end


end