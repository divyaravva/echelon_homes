
require "rvm/capistrano"
require "bundler/capistrano"
require "whenever/capistrano"
set :application, "ghm.pointmatter.net"
set :repository,  "git@visper.scrumweb.com:GHM.git"
set :branch, "staging"
set :rvm_type, :system

set :scm, :git # You can set :scm explicitly or Capistrano will make an intelligent guess based on known version control directory names
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

role :web, "#{application}"                           # Your HTTP server, Apache/etc
role :app, "#{application}"                         # This may be the same as your `Web` server
role :db,  "#{application}", :primary => true # This is where Rails migrations will run
role :db,  "#{application}"

set :user, "ubuntu"
set :use_sudo, false
set :deploy_to, "/var/www/#{application}"
set :deploy_via, :remote_cache
ssh_options[:keys] = ["/var/www/LMI.pem"]
set :whenever_identifier, "#{application}"
set :whenever_command, "bundle exec whenever"
# if you want to clean up old releases on each deploy uncomment this:
 after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
namespace :deploy do
  task :start do 
    run "sudo service apache2 start"  
  end
  task :stop do 
    run "sudo service apache2 stop"
  end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end
  task :common_symlink do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
    run "ln -nfs #{shared_path}/public/tehaleh.svg #{release_path}/public/tehaleh.svg"
    run "ln -nfs #{shared_path}/public/images #{release_path}/public/images"
  end
  # desc "Update the crontab file"
  # task :update_crontab do
  #   run "cd #{release_path} && whenever --update-crontab #{application}"
  # end
  desc "restart apache"
  task :apache_restart do
    run "sudo service apache2 restart"
  end
end
namespace :gems do
  task :install do 
    run "cd #{release_path} && RAILS_ENV=production bundle install"   
  end
 end
 namespace :db do
  task :migrate do
    run "cd #{release_path} && RAILS_ENV=production rake db:migrate"  
  end
 end
 before "deploy:restart", "gems:install"
 after "gems:install", "deploy:common_symlink"
 after "deploy:common_symlink", "deploy:migrate"
 after "deploy:migrate", "whenever:update_crontab"
 after "deploy:restart", "deploy:apache_restart"