class JlHousesOds < ActiveRecord::Base
  attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_JLHOUSES"
  has_one :jlloan_master, :class_name => "JlloanMasterOds", :foreign_key => [:LOANNUMBER],:primary_key => [:LOANNUMBER], :conditions => {:COMPANYCODE => '001'}    
  def self.load_ods_jlhouses(windows_db_nm)

    truncate_ods_jlhouses

    puts "********** 1A) Started Loading JlHousesOds Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from "+windows_db_nm+".dbo.JLHOUSES  where HOUSENUMBER != '00000000'");
        puts "............................................."
        puts "............................................."

        batch=[]
        batch_size = 1000

        result.each do |row|
          #HouseCostDetail.find_or_initialize_by_COMPANYCODE_and_DEVELOPMENTCODE_and_LOTNUMBER(row['COMPANYCODE'], row['DEVELOPMENTCODE'], row['LOTNUMBER'])
          batch << JlHousesOds.new(:COMPANYCODE => row['COMPANYCODE'],
                                          :LOANNUMBER => row['LOANNUMBER'],
                                          :DEVELOPMENTCODE => row['DEVELOPMENTCODE'],
                                          :HOUSENUMBER => row['HOUSENUMBER'],
                                          :UNPACKEDHOUSENUM => row['UNPACKEDHOUSENUM'],
                                          :LOANAMOUNT => row['LOANAMOUNT'],
                                          :DRAWREQUEST => row['DRAWREQUEST']
                                    )
            if batch.size >= batch_size
              JlHousesOds.import batch
              batch = [] 
            end  
        end
        JlHousesOds.import batch
        # HouseCostDetailOds.mass_insert(batch, :per_batch => 1000)
    end

    puts "Total no. of Loaded rows.........#{JlHousesOds.count}"
    puts "********** 1B) Finished Loading ODS_JLHOUSES Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_jlhouses
    ActiveRecord::Base.connection.execute("TRUNCATE ODS_JLHOUSES")
  end


end