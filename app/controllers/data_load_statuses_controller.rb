class DataLoadStatusesController < ApplicationController
require 'tiny_tds'
	  before_filter :authenticate
  before_filter :has_roles  
  before_filter :get_product
  before_filter :get_product_view
  before_filter :get_comm_city_view
  before_filter :get_community_city
  before_filter :get_communities  
  before_filter :get_communities_view    
  before_filter :get_parcels    
  before_filter :get_parcels_view

  def fetch_date_and_status
    if SITE_ROOT_DOMAIN_NAME == "http://localhost:3000"
      @environment = "Development"
    elsif SITE_ROOT_DOMAIN_NAME == "http://echelonhomes.stg.lotvue.com"
      @environment = "Staging"
    elsif SITE_ROOT_DOMAIN_NAME == "http://echelonhomes.lotvue.com"
      @environment = "Production"
    end
    @data_loads = DataLoadStatus.find(:all, :order => "StartDate desc").select{|rec| (rec.Environment == @environment || rec.Environment == nil) && rec.Status == "Succeeded"}.first(7)
    successful_load_rec = @data_loads.first
    if Rails.env == "development"
      @successful_data_load_date_view = (successful_load_rec.StartDate-330.minutes).utc.in_time_zone(SetTimeZone.first.TimeZone).
                strftime("%m/%d/%Y %H:%M:%S") + " " +SetTimeZone.first.Abbrevation unless successful_load_rec.nil?       	  
    elsif Rails.env == "production"
      @successful_data_load_date = successful_load_rec.StartDate.utc.in_time_zone(SetTimeZone.first.TimeZone) unless successful_load_rec.nil?
      @successful_data_load_date_view = (@successful_data_load_date).strftime("%m/%d/%Y %H:%M:%S") + " " +SetTimeZone.first.Abbrevation unless @successful_data_load_date.nil?
    end  
  end

  def data_refresh
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if(client.nil? == false && client.active? == true)
      result = client.execute("select * from [Westport].[dbo].[DataLoadStatus] order by id desc").first

      if result.nil? == true || result["Total_Job_Status"] != "Running"      
        system "rake run_jar_file[Manual]"
        render :json => {:success => "Completed"}
      else
        render :json => {:success => "Already running"}
      end
    end
  end

  def check_new_record_inserted
    if !DataLoadStatus.all.empty? && DataLoadStatus.last.Status == 'Running'
      DataRefreshStatus.first.update_attributes(:refresh_type => 'Manual', :secondary_status => 'Running')
      flash[:notice] = "Manual Data Refresh process initiated."
      render :json => {:success => "Running"}
    else
      render :json => {:success => "Not Running"}
    end
  end

  def check_refresh_completed    
    last_dataload_rec = DataLoadStatus.last
    unless last_dataload_rec.blank?
        if last_dataload_rec.Status == 'Running'
          render :json => {:success => "Started"}
        elsif last_dataload_rec.Status != 'Running'
          flash[:notice] = "Manual Data Refresh process Completed."
          render :json => {:success => "Not Started"}      
        end    
    end
  end

    
end
