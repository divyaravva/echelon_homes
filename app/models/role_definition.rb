class RoleDefinition < ActiveRecord::Base
  attr_protected
  self.table_name = "Role_Definitions"  
  has_many :rds_lot_collections, :dependent => :destroy, :foreign_key => "Role_Definition_Id"  
  has_many :lot_collections, :through => :rds_lot_collections, :foreign_key => "Lot_Collection_Id"
  has_many :user_role_definitions,:foreign_key => "Role_Definition_Id", :dependent => :destroy
  has_many :users, :through=> :user_role_definitions, :class_name => "Tblusers"
  has_many :rds_lot_collections_report_role_accesses, :foreign_key => "Role_Id", :conditions => {:Role_Access_Id => "2" , :Report_Type => "Uploaded"}   
  # has_many :rds_lot_collections_report_role_accesses_for_geneated_reps, :class_name =>"RdsLotCollectionsReportRoleAccess", :foreign_key => "Role_Id", :conditions => ["Role_Access_Id IN (1,2) AND Report_Type='Generated' "]  

  validates_presence_of :Role_Definition, :message => 'Role Definition name is blank'
  validates_uniqueness_of :Role_Definition, :message => 'Role Definition name is already registered'
  

  def self.get_rules
    includes(
  		[
	  		{
	  			:rds_lot_collections=>
	  			[
	  				{
	  					:rds_lot_collections_menu_options=>
	  					[
	  						:rds_lot_collections_menu_options_role_access,
	  						:role_access
	  					]
	  				},
	  				:menu_options
	  			]
	  		},
	  		:lot_collections
  		]
  	)
  end

  def self.get_roles
  	select("Role_Definition,id").where("Is_Active=1").map{|g| [g.Role_Definition, g.id]}
  end

  def is_admin?
    self.Role_Definition == "Admin"
  end  

  def self.get_roles_access_to_uploaded_reports
    role_ids = []
    self.where("Role_Definition !=?","Admin").each do |each_role|
        each_role.rds_lot_collections.each do |each_lc|
          if (each_lc.rds_lot_collections_menu_options.flatten.map{|x|x.Menu_Option_Id}.include?(7) == true)         
            role_ids << each_role.id
          end
        end
    end
    role_ids.uniq
  end

  def self.add_new_comm_to_roles(lc)
    RoleDefinition.all.each do |r|
      rc = RdsLotCollection.find_by_Role_Definition_Id_and_Lot_Collection_Id(r.id,lc.id)
      if rc.blank?
        @role_comm_per = RoleCommonPermissions.where("role_id = ?", r.id)
        if @role_comm_per.blank?
          comm_prdt = "1,2"
          can_upload = "1"
        else
          comm_prdt = @role_comm_per.pluck(:product_id).uniq.join(",")
          can_upload = @role_comm_per.where("report_name is not null").pluck(:can_upload).compact.uniq[0]
        end

        rds_lot_collection = lc.rds_lot_collections.create(:Role_Definition_Id => r.id,
             :Lot_Collection_Id => lc.id, :Product_Type_Id => comm_prdt, :Can_Upload_Report => can_upload)
        menu_ids = @role_comm_per.pluck(:menu_id)
        MenuOption.all.each do |menu_option|     
          if menu_ids.uniq.include?(menu_option.Id)
            rds_lot_collection_menu_option = RdsLotCollectionsMenuOption.create(:Rds_Lot_Collection_Id => rds_lot_collection.id, 
                  :Menu_Option_Id => menu_option.Id)   
            if menu_ids.count(menu_option.Id) == 1 
              role_access = RdsLotCollectionsMenuOptionRoleAccess.create(:Rds_Lot_Collections_Menu_Options_Id => rds_lot_collection_menu_option.id,
                  :Role_Access_Id =>  @role_comm_per.find_by_menu_id(menu_option.Id).permission)
              
              if !@role_comm_per.find_by_menu_id(menu_option.Id).report_name.blank?
                @role_comm_per.where("menu_id = #{menu_option.id}").each do |x|
                  report_access_rec = RdsLotCollectionsReportRoleAccess.find_or_create_by_Role_Id_and_Report_Id_and_Report_Type_and_Lot_Collection_Id(r.id, x.report_id, menu_option.MenuOptionName.split(" ")[0], lc.id)
                  report_access_rec.update_attributes(:Role_Access_Id => @role_comm_per.find_by_menu_id(menu_option.Id).permission)
                end
              end

            else
              role_access = RdsLotCollectionsMenuOptionRoleAccess.create(:Rds_Lot_Collections_Menu_Options_Id => rds_lot_collection_menu_option.id,
                  :Role_Access_Id => "2")
              @role_comm_per.where("menu_id = #{menu_option.id}").each do |x|
                report_access_rec = RdsLotCollectionsReportRoleAccess.find_or_create_by_Role_Id_and_Report_Id_and_Report_Type_and_Lot_Collection_Id(r.id, x.report_id, menu_option.MenuOptionName.split(" ")[0], lc.id)
                report_access_rec.update_attributes(:Role_Access_Id => @role_comm_per.find_by_menu_id_and_report_id(menu_option.Id,x.report_id).permission)
              end
            end
          else
            if @role_comm_per.blank?
              rds_lot_collection_menu_option = RdsLotCollectionsMenuOption.create(:Rds_Lot_Collection_Id => rds_lot_collection.id, 
                    :Menu_Option_Id => menu_option.Id)   
              role_access = RdsLotCollectionsMenuOptionRoleAccess.create(:Rds_Lot_Collections_Menu_Options_Id => rds_lot_collection_menu_option.id,
                    :Role_Access_Id =>  "2")
                
              if menu_option.MenuName == "Reports"
                Report.all.each do |x|
                  report_access_rec = RdsLotCollectionsReportRoleAccess.find_or_create_by_Role_Id_and_Report_Id_and_Report_Type_and_Lot_Collection_Id(r.id, x.id, x.report_type, lc.id)
                  report_access_rec.update_attributes(:Role_Access_Id => "2")
                end
              end
            end 
          end
        end
      end
    end
  end


end
