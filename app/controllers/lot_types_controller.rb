class LotTypesController < ApplicationController

  before_filter :authenticate
  before_filter :is_admin
  before_filter :get_product
  before_filter :get_product_view   
  before_filter :get_comm_city_view
  before_filter :get_community_city    
  before_filter :get_communities   
  before_filter :get_communities_view
  before_filter :get_parcels
  before_filter :get_parcels_view

  def index
    @lot_types=LotType.order("Lot_Type").all
  end

  def show
    @lot_type_details = LotType.find(params[:id]) unless params[:id].nil?
  end

  def destroy    
    lot_types = LotType.find(params[:id]) unless params[:id].nil?
    lot_types.destroy unless lot_types.nil?
    Tbldatum.invalidate_cache
    redirect_to "/lot_types"
  end

  def new     
     @lot_types = LotType.all
  end

  def create   	
    lot_type=LotType.create(:Lot_Type => params[:lot_type_name])    
    redirect_to "/lot_types"
  end  

  def edit       
    @lot_type_details = LotType.find(params[:id]) unless params[:id].nil?
    @lot_type = @lot_type_details.Lot_Type unless @lot_type_details.nil?   
    @lot_types = LotType.all.select{|x| x.Lot_Type!=@lot_type}       
  end

  def update
    lot_type_details = LotType.find(params[:id]) unless params[:id].nil?    
    lot_type_details.update_attributes({:Lot_Type => params[:lot_type]}) unless lot_type_details.nil?
    redirect_to "/lot_types"
  end

   def change_status
    lot_type_details = LotType.find(params[:id]) unless params[:id].nil?   
      if params[:status] == "archive" 
        lot_type_details.update_attributes({:Is_Active => 0}) unless lot_type_details.nil?
      else
         lot_type_details.update_attributes({:Is_Active => 1}) unless lot_type_details.nil?
      end
       @lot_types=LotType.order("Lot_Type").all
       render :partial => "lot_type"
   end

  def destroy    
    lot_type = LotType.find(params[:id]) unless params[:id].nil?
    lot_type.destroy unless lot_type.nil?
    @lot_types=LotType.order("Lot_Type").all
    render :partial => "lot_type"
  end
end