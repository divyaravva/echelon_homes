class SalesPersonMasterOds < ActiveRecord::Base
	attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_SALESPERSONMASTER"

  def self.load_ods_sales_person_master(windows_db_nm)

  	truncate_ods_sales_person_master

    puts "********** 1A) Started Loading SalesPersonMasterOds Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from "+windows_db_nm+".dbo.SALESPERSONMAST");
      	puts "............................................."
      	puts "............................................."

        batch=[]
        batch_size = 1000

        result.each do |row|
          batch << SalesPersonMasterOds.new(:COMPANYCODE => row['COMPANYCODE'],
                                      :SALESPERSONCODE => row['SALESPERSONCODE'],
                                      :FIRSTNAME => row['FIRSTNAME'],
                                      :MIDDLEINITIAL => row['MIDDLEINITIAL'],
                                      :LASTNAME => row['LASTNAME'],
                                      :CELLPHONE => row['CELLPHONE'],
                                      :HOMEPHONE => row['HOMEPHONE'],
                                      :SALESMANAGER => row['SALESMANAGER'],
                                      :EMAILADDRESS => row['EMAILADDRESS'],
                                      :USERID => row['USERID'],
                                      :TERMINATOR => row['TERMINATOR'],
								)
            if batch.size >= batch_size 
              SalesPersonMasterOds.import batch
              batch = [] 
            end
        end
        SalesPersonMasterOds.import batch
    end

    puts "Total no. of Loaded rows.........#{SalesPersonMasterOds.count}"
    puts "********** 1B) Finished Loading SalesPersonMasterOds Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_sales_person_master
		ActiveRecord::Base.connection.execute("TRUNCATE ODS_SALESPERSONMASTER")
	end

 end