class GlTrans2015Ods < ActiveRecord::Base
	attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_GLTRANS2015"

  def self.load_ods_gltrans2015(dev_codes,windows_db_nm)

  	truncate_ods_gltrans2015

    puts "********** 1A) Started Loading GlTrans2015 Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
      #message = "Inside the Lots info Load Method"
      #Mailer.notify_user_on_client_failure(message).deliver
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from "+windows_db_nm+".dbo.GLTRANS2015 where DEVELOPMENTCODE in ("+dev_codes+")  and HOUSENUMBER != '00000000'");
      	puts "............................................."
      	puts "............................................."

        batch=[]
        batch_size = 1000

        result.each do |row|
          #GlTrans2015.find_or_initialize_by_COMPANYCODE_and_DEVELOPMENTCODE_and_LOTNUMBER(row['COMPANYCODE'], row['DEVELOPMENTCODE'], row['LOTNUMBER'])
          batch << GlTrans2015Ods.new(:COMPANYCODE => row['COMPANYCODE'],
                                      :DEVELOPMENTCODE => row['DEVELOPMENTCODE'],
                                      :HOUSENUMBER => row['HOUSENUMBER'],
                                      :GLACCOUNT => row['GLACCOUNT'],
                                      :TRANSREMARK => row['TRANSREMARK'],
                                      :AMOUNT => row['AMOUNT']
                                      )
            
            if batch.size >= batch_size
              GlTrans2015Ods.import batch
              batch = [] 
            end
        end
        # GlTrans2015Ods.mass_insert(batch, :per_batch => 1000)
        GlTrans2015Ods.import batch
    end

    puts "Total no. of Loaded rows.........#{GlTrans2015Ods.count}"
    puts "********** 1B) Finished Loading GlTrans2015 Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_gltrans2015
		ActiveRecord::Base.connection.execute("TRUNCATE ODS_GLTRANS2015")
	end


end