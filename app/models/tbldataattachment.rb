class Tbldataattachment < ActiveRecord::Base
  attr_protected
  validates_attachment_presence :image
  belongs_to :imageable, :polymorphic => true
  before_create :increment_upload_counter
  before_destroy :decrement_upload_counter
  has_attached_file :image, :styles => lambda { |a|
      if a.instance.is_image?
        {
      :large => {
          :geometry => "800x600!",
          :quality => "95",
          :sharpen => "0x1.2"
      },
      :small => {
          :geometry => "130x130!",
          :quality => "95",
          :sharpen => "0x1.2"
      },
      :thumb => Proc.new { |instance| instance.resize },
      :thumb_edit_images => Proc.new { |instance| instance.resize_edit_images },
  }
      else
        Hash.new
      end
},
                    :url => "/pictures/:class/:id/:style/:basename.:extension",
                    :path => ATTACHMENT_PATH+"shared/pictures/:class/:id/:style/:basename.:extension"



def is_image?   
  return false unless image.content_type
    ['image/jpeg', 'image/pjpeg', 'image/gif', 'image/png', 'image/x-png', 'image/jpg'].include?(image.content_type)
end

def resize
  geo = Paperclip::Geometry.from_file(image.to_file(:original))
  if geo.width > 800 || geo.height > 600
    new_width = geo.width/800
    new_height = geo.height/600

    if new_height > new_width
      save_height = geo.width/new_height
      save_width = geo.height/new_height
    else
      save_height = geo.height/new_width
      save_width = geo.width/new_width
    end       
    
    "#{save_width.round}x#{save_height.round}!"
  else
    save_height = geo.height
    save_width = geo.width
    "#{save_width.round}x#{save_height.round}!"
  end
end

def resize_edit_images
  geo = Paperclip::Geometry.from_file(image.to_file(:original))
  if geo.width > 130 || geo.height > 130
    new_width = geo.width/130
    new_height = geo.height/130

    if new_height > new_width
      save_height = geo.width/new_height
      save_width = geo.height/new_height
    else
      save_height = geo.height/new_width
      save_width = geo.width/new_width
    end       
    
    "#{save_width.round}x#{save_height.round}!"
  else
    save_height = geo.height
    save_width = geo.width
    "#{save_width.round}x#{save_height.round}!"
  end
end


private 

def increment_upload_counter  
  if product_type == "Lot Buzz"
    Tbldatum.increment_counter(:doc_count_lotbuzz, self.imageable_id)
  else
    Tbldatum.increment_counter(:doc_count_lotinsight, self.imageable_id)
   end
end


def decrement_upload_counter
  if product_type == "Lot Buzz"
    Tbldatum.decrement_counter(:doc_count_lotbuzz, self.imageable_id)
  else
    Tbldatum.decrement_counter(:doc_count_lotinsight, self.imageable_id)
  end
end

end
