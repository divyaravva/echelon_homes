class HomeController < ApplicationController
  def index
    @udata=Tblusers.find(session[:current_user]) if session[:current_user].present?
  end

  def sign_in
    redirect_to products_path if session[:current_user].present?
  end

  def sign_in_process
    @email = params[:Email_Address]
    @password = params[:Password]
    @user = Tblusers.find(:all, :conditions => {:Email_Address => @email, :Password => @password}).first
    if @user.nil?
      ActiveRecord::Base.logger.error "Password : #{params[:Password]}"
      flash[:msg] = 'Please enter valid email address / password'
      render :sign_in
    else
      session[:current_user] = @user.id
      redirect_to products_path
    end
  end

  def mainpage

    if session[:current_user].nil?
      redirect_to root_url, :notice => "Logged out!"
    else
      @user = Tblusers.find(:all, :conditions => {:ID => session[:current_user]})
    end
  end

  def sign_out
    Tblusers.invalidate_user session[:current_user]
    session[:current_user] = nil
    redirect_to root_url, :notice => "Logged out!"
  end

def reset_pwd    
  end

  def registration
    @user = Tblusers.where(:token=> params[:token]).first
    if @user && @user.Is_Active==1
    flash[:notice] = "You are already registered.Please confirm to Sign In"
      render "home/sign_in"
    elsif @user == nil
      render 'home/link_expired'
    else
      render 'home/registration'
    end
  end

  def forgotpwd     
  end  

  def update_forgotpwd
    @user = Tblusers.where(:Email_Address=> params[:email_address],:Is_Active=>'1').first
    unless @user.nil?      
      ActiveRecord::Base.logger.error "User Details : #{@user.inspect}"
      ActiveRecord::Base.logger.error "User Details : #{params[:user]}"
      @user.update_attributes(params[:user])
      Mailer.forgotpwd(@user).deliver      
      render "home/reset_pwd_msg"
    else
      flash[:notice] = "The email address you entered is not registered, Please enter a registered email address"
      render "home/forgotpwd"
    end 
  end

  def confirm_forgotpwd
  end  
  def confirm
  end

  def confirm_forgotpwd_signin
    @user = Tblusers.where(:token=> params[:token],:Is_Active=>'1').first
    unless @user.nil?
      ActiveRecord::Base.logger.error "User Details : #{params[:user]}"
      ActiveRecord::Base.logger.error "Password : #{params[:Password]}"
      @user.update_attributes(params[:user])
      params[:Email_Address] = @user.Email_Address
      params[:Password] = @user.Password
      flash[:notice] = "Welcome! Password Changed Successfully"
      sign_in_process      
    else
      flash[:notice] = "Sorry..You are not a registered User"
      render "home/sign_in"
    end 
  end

  def updateactive
      @user = Tblusers.where(:token=> params[:token],:Is_Confirmed=>'0').first
    unless @user.nil?
      @user.update_attributes(:Is_Confirmed=>'1')
      flash[:notice] = "Confirmation completed Successfully.Please Sign In"
      render "home/sign_in"
    else
      flash[:notice] = "Your confirmation is already completed.Please Sign In."
      render "home/sign_in"

    end

  end  
  def update_registration

     @user = Tblusers.where(:token=> params[:token],:Is_Active=>'0').first
    unless @user.nil?
      ActiveRecord::Base.logger.error "User Details : #{params[:user]}"      
      @user.update_attributes(params[:user])
      @user.update_attributes(:Is_Active=>'1')
      @user.update_attributes(:Is_Confirmed=>'1')
      @user.update_attributes(:Invitation_Date => Time.now,
               :Invitation_Action => '', :Invitation_Status => 'Registered')    
      Mailer.confirmation(@user).deliver
      flash[:notice] = "Registration completed Successfully. Please Sign In to continue."
      render "home/sign_in"
    else
      flash[:notice] = "Sorry.You are already registered"
      render "home/sign_in"
    end     
     
  end


  def update_password
    @udata=Tblusers.find(session[:current_user]) if session[:current_user].present?
    @user = Tblusers.where(:Email_Address=>@udata.Email_Address,:Password => params[:old_password]).first
    unless @user.nil?
      ActiveRecord::Base.logger.error "Password : #{params[:password]}"
      @udata.Password=params[:password]
      @udata.save
      flash[:notice] = "Password changed successfully"      
      redirect_to users_path
    else    
      flash.now[:error] = 'Please enter valid Current Password'
      render "home/reset_pwd"      
    end
  end


end
