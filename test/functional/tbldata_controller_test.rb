require 'test_helper'

class TbldataControllerTest < ActionController::TestCase
  setup do
    @tbldatum = tbldata(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tbldata)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tbldatum" do
    assert_difference('Tbldatum.count') do
      post :create, tbldatum: { Held_by: @tbldatum.Held_by, Lot_Builder: @tbldatum.Lot_Builder, SVG_ID: @tbldatum.SVG_ID }
    end

    assert_redirected_to tbldatum_path(assigns(:tbldatum))
  end

  test "should show tbldatum" do
    get :show, id: @tbldatum
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tbldatum
    assert_response :success
  end

  test "should update tbldatum" do
    put :update, id: @tbldatum, tbldatum: { Held_by: @tbldatum.Held_by, Lot_Builder: @tbldatum.Lot_Builder, SVG_ID: @tbldatum.SVG_ID }
    assert_redirected_to tbldatum_path(assigns(:tbldatum))
  end

  test "should destroy tbldatum" do
    assert_difference('Tbldatum.count', -1) do
      delete :destroy, id: @tbldatum
    end

    assert_redirected_to tbldata_path
  end
end
