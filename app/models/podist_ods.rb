class PodistOds < ActiveRecord::Base
	attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_PODIST"
  self.primary_key = "DISTHOUSE"
  def self.load_ods_podist(windows_db_nm)

  	truncate_ods_podist

    puts "********** 1A) Started Loading PodistOds Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from "+windows_db_nm+".dbo.PODIST  where UPHOUSENUMBER != '00000000'");
      	puts "............................................."
      	puts "............................................."

      	batch=[]
        batch_size = 1000
        
        result.each do |row|
          #PodistOds.find_or_initialize_by_COMPANYCODE_and_DEVELOPMENTCODE_and_LOTNUMBER(row['COMPANYCODE'], row['DEVELOPMENTCODE'], row['LOTNUMBER'])
          batch << PodistOds.new(:COMPANYCODE => row['COMPANYCODE'],
                                      :PONUMBER => row['PONUMBER'],
                    									:SEQUENCENUMBER => row['SEQUENCENUMBER'],
                    									:HOUSENUMBER => row['UPHOUSENUMBER'],
                    									:GLACCOUNT => row['GLACCOUNT'],
                    									:DISTDEV => row['DISTDEV'],
                    									:DISTHOUSE => row['DISTHOUSE'],
                    									:COSTCODE => row['COSTCODE'],
                    									:OPTIONCODE => row['OPTIONCODE'],
                    									:UPHOUSENUMBER => row['UPHOUSENUMBER'],
                    									:GLDESCRIPTION => row['GLDESCRIPTION'],
                    									:LOANNUMBER => row['LOANNUMBER'],
                    									:LOANDRAW => row['LOANDRAW'],
                    									:VARIANCECODE => row['VARIANCECODE'],
                    									:TERMINATOR => row['TERMINATOR'],
                    									:AMOUNT => row['AMOUNT'],
								                    )
								if batch.size >= batch_size
                  PodistOds.import batch
                  batch = [] 
                end
        end
        # PodistOds.mass_insert(batch, :per_batch => 1000)
        PodistOds.import batch
    end

    puts "Total no. of Loaded rows.........#{PodistOds.count}"
    puts "********** 1B) Finished Loading PodistOds Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_podist
		ActiveRecord::Base.connection.execute("TRUNCATE ODS_PODIST")
	end


end