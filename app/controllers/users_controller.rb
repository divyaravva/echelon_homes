class UsersController < ApplicationController
  before_filter :authenticate
  before_filter :is_admin

   before_filter :get_product
   before_filter :get_product_view 
   before_filter :get_comm_city_view
   before_filter :get_community_city     
   before_filter :get_communities   
   before_filter :get_communities_view
   before_filter :get_parcels
   before_filter :get_parcels_view
  
  def index
    @role = params[:role]
    if params[:role].present?
        @role_name = RoleDefinition.find(params[:role]).Role_Definition
        @users_for_role = RoleDefinition.get_rules.find(params[:role]).users
    end
    @udata=Tblusers.find(session[:current_user]) if session[:current_user].present?
    @users=Tblusers.order("First_Name").all
  end

  def update_info   
    @user = Tblusers.find(params[:user][:ID])    
    unless @user.nil?
      @user.role_ids= params[:Roles]
      update_access_group(@user)
      @user.update_attributes(params[:user].reject{|user_info| user_info == "ID"})
    end
    Tbldatum.invalidate_cache     
    redirect_to users_url
  end
  
  def create_info
    Tbldatum.invalidate_cache
    if params[:save_only] == "save"
      @user = Tblusers.create(params[:user])      
      @user.role_ids= params[:Roles]
      @user.update_attributes(:Invitation_Date => "",
               :Invitation_Action => 'Invite', :Invitation_Status => 'Not Invited')
        update_access_group(@user)
        render_users_list(params)
    elsif params[:save_and_invite] == "invite"
      @user = Tblusers.create(params[:user])
      if @user.save
        @user.role_ids= params[:Roles]
        @user.token = Tblusers.create_token
        @user.update_attributes(:Invitation_Date => Time.now,
               :Invitation_Action => 'Reinvite', :Invitation_Status => 'Pending')
        update_access_group(@user)
        Mailer.invitation(@user).deliver
        render_users_list(params)
      else
          respond_to do |format|
            format.js
          end
      end
    end
  end

  def reinvite_info    
    @user = Tblusers.find(params[:user][:ID])
    unless @user.nil?
      @user.role_ids= params[:Roles]
      update_access_group(@user)
      @user.update_attributes(params[:user].reject{|user_info| user_info == "ID"})
      @user.token = Tblusers.create_token    
      @user.update_attributes(:Invitation_Date => Time.now,
               :Invitation_Action => 'Reinvite', :Invitation_Status => 'Pending')
      Mailer.invitation(@user).deliver
      render_users_list({})
    end
  end  

  def delete_info
    @user = Tblusers.find(params[:user][:ID])
    reports_for_user = @user.reports
    id_of_admin = Tblusers.find_by_First_Name("Admin").ID
    reports_for_user.map{|each_rep| each_rep.update_attributes(:user_id => id_of_admin)} unless reports_for_user.blank?
    @user.destroy
    Tbldatum.invalidate_cache
    redirect_to users_url
  end

  def update_access_group(user)
    admin_group_id = Tblusergroup.find_by_Group_Name("Admin").id
    standard_group_id = Tblusergroup.find_by_Group_Name("Standard").id
    if user.roles.map(&:Role_Definition).include?("Admin") == true
        user.update_attributes(:Access_Group => admin_group_id)
    else 
        user.update_attributes(:Access_Group => standard_group_id)   
    end
    Tbldatum.invalidate_cache    
  end    

  def render_users_list(params)
    if params.empty? == true
      flash[:notice] = "Thank you, invitation sent successfully."
      redirect_to users_url      
    elsif  params.present? == true
      flash[:notice] =  if (params[:save_only] == "save") 
                           "User saved successfully." 
                        elsif(params[:save_and_invite] == "invite")
                          "Thank you, invitation sent successfully."
                        end
      render js: %(window.location.href='#{users_path}')
    end
  end

  def change_status_and_date
    @user = Tblusers.find_by_Email_Address(params[:email])
    if (@user.Invitation_Action == "Invite")
      send_mail_and_update(@user)
    elsif (@user.Invitation_Action == "Reinvite")
      send_mail_and_update(@user)
    end
  end

  def send_mail_and_update(user)
    user.token = Tblusers.create_token
    Mailer.invitation(user).deliver
      user.update_attributes(:Invitation_Action => "Reinvite", :Invitation_Status => "Pending",
        :Invitation_Date => Time.now)
  end
end
