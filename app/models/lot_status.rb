class LotStatus < ActiveRecord::Base
  attr_protected

  def self.upload_data
	  book = Spreadsheet.open '/var/www/ElliotHomes/DB/elliott-homes-lotbuzz-data.xls'
      sheet1 = book.worksheet 1   
      sheet1.each do |row|
        rec=self.find_or_initialize_by_id(row[0])
        rec.update_attributes(
            :status =>   row[0],
            :color => row[1]
          )
      end
	end

  def self.lot_status
  lot_status=LotStatus.all
  lot_status.map{|x| [x.status,x.id] }
end

end
