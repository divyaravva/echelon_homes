$(document).ready(function(){
	$('form#login_form').validate({
    rules: {
        email_address: {            
            required: true,
            email: true
        },
        password: {            
            required: true
        }
    },
    messages: {
    	email_address: {            
            required: "Please Enter Email",
            email: "Please Enter Email in valid format"
        },
        password: {
        	required: "Please Enter password"
        }
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
    	$("#login_error_msg").remove();
        error.insertAfter($(element).closest('.form-group')); //So i putted it after the .form-group so it will not include to your append/prepend group.
    }, 
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    }
});

  

   $("form#edit_user_form").validate({    
   rules: {          
          "user[First_Name]": {            
            required: true
        }
      },
       messages: {
        "user[First_Name]": {
            required: "Please Enter First Name"
        }
    },
    errorElement: 'span',
    errorClass: 'help-block',
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    }
});

   $("form#create_user_group_form").validate({    
   rules: {
          "user_group[Group_Name]": {            
              required: true
          },
          "user_group[Description]": {            
            required: true
        }
      },
       messages: {
        "user_group[Group_Name]": {            
            required: "Please Enter Group Name"
        },
        "user_group[Description]": {
            required: "Please Enter Description"
        }
    },
    errorElement: 'span',
    errorClass: 'help-block',
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    }
});

   $("form#edit_user_group_form").validate({    
   rules: {
          "user_group[Group_Name]": {            
              required: true
          },
          "user_group[Description]": {            
            required: true
        }
      },
       messages: {
        "user_group[Group_Name]": {            
            required: "Please Enter Group Name"
        },
        "user_group[Description]": {
            required: "Please Enter Description"
        }
    },
    errorElement: 'span',
    errorClass: 'help-block',
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    }
});



   
});

function toggleDiv(table_tr) {
    if($("."+table_tr).is(':visible'))
    {
     $("."+table_tr+"_image_rotate").addClass("image_rotate_oncollapse").removeClass("image_rotate_onexpand");
    }
    else{
      $("."+table_tr+"_image_rotate").addClass("image_rotate_onexpand").removeClass("image_rotate_oncollapse");
    }
   $("."+table_tr).toggle();
    footer_buttons_visibility()
}

  function togglelcperm(div_cls, lc_id, lc_cls) {
    if($("."+lc_cls+"#"+lc_id).is(":checked")){
      if($("."+div_cls).is(':visible'))
      {
        $("."+div_cls+"_img").addClass("image_rotate_oncollapse").removeClass("image_rotate_onexpand");
        $(".comm_lc_sub_div_"+lc_id).hide();
        $("."+div_cls).hide();
        $("#lc_div_"+lc_id).css("margin-bottom","0px")   
      }
      else{
        $("."+div_cls+"_img").addClass("image_rotate_onexpand").removeClass("image_rotate_oncollapse");
        $(".comm_lc_sub_div_"+lc_id).show();
        $("."+div_cls).show();
        $("#lc_div_"+lc_id).css("margin-bottom","10px") 
      }

    }
    else{
      $("."+div_cls+"_img").addClass("image_rotate_oncollapse").removeClass("image_rotate_onexpand");
      $(".comm_lc_sub_div_"+lc_id).hide();
      $("#lc_div_"+lc_id).css("margin-bottom","0px")   
    }
  }

function footer_buttons_visibility(){
  // hide footer bottom buttons when all sections are closed
  $('.inner_tr').each(function(){
      if($(this).css("display") == "none" ){ 
        $(".bottom_footer_buttons").css("display","none")
      }
      else{
        $(".bottom_footer_buttons").css("display","block");
        return false
      }
    });
}

// changing min-height of click form header dynamically
function dialog_header_style(){
  header_min_height = $('.header_hgt  .modal-footer').outerHeight();
  if(header_min_height > 45){
    $(".header_hgt").attr("style","min-height:"+header_min_height+"px !important")
  }
  else{
    $(".header_hgt").attr("style","min-height:45px !important")
  }
// applying margin for map
  $("#left.map_render").css("margin-top",$("#main_nav").outerHeight()+"px");
  
}
$(window).resize(function(){
  // dynm_viewbx_values();
  dialog_header_style();
  
})

function close_filter(){
  $('.jPushMenuBtn,body,.cbp-spmenu').removeClass('disabled active cbp-spmenu-open cbp-spmenu-push-toleft cbp-spmenu-push-toright');
  $("#filter_toggle").show();
  $("#filter_legend").hide();
}

function on_click_out_legend(){
   $(".close_icon").click(function(){
        close_filter();
    })
  // if(document.getElementById("alphasvg") != null){
  //   setTimeout(function(){ 
  //       document.getElementById("alphasvg").contentDocument.onclick = function (e) {
  //         close_filter();
  //       }
  //   },100);

  //   setTimeout(function(){ 
  //      $(document.getElementById("alphasvg").contentDocument.getElementsByTagName('svg')).on('tap',function(){
  //         close_filter();
  //       })
  //   },100);
  // }


}


function zoom_control_functionality(){
        var eventsHandler;

        eventsHandler = {
          haltEventListeners: ['touchstart', 'touchend', 'touchmove', 'touchleave', 'touchcancel']
        , init: function(options) {
            var instance = options.instance
              , initialScale = 1
              , pannedX = 0
              , pannedY = 0

            // Init Hammer
            // Listen only for pointer and touch events
            this.hammer = Hammer(options.svgElement, {
              inputClass: Hammer.SUPPORT_POINTER_EVENTS ? Hammer.PointerEventInput : Hammer.TouchInput
            })

            // Enable pinch
            this.hammer.get('pinch').set({enable: true})

            // Handle double tap
            this.hammer.on('doubletap', function(ev){
              instance.zoomIn()
            })

            // Handle pan
            this.hammer.on('panstart panmove', function(ev){
              // On pan start reset panned variables
              if (ev.type === 'panstart') {
                pannedX = 0
                pannedY = 0
              }

              // Pan only the difference
              instance.panBy({x: ev.deltaX - pannedX, y: ev.deltaY - pannedY})
              pannedX = ev.deltaX
              pannedY = ev.deltaY
            })

            // Handle pinch
            this.hammer.on('pinchstart pinchmove', function(ev){
              // On pinch start remember initial zoom
              if (ev.type === 'pinchstart') {
                initialScale = instance.getZoom()
                instance.zoom(initialScale * ev.scale)
              }

              instance.zoom(initialScale * ev.scale)

            })

            // Prevent moving the page on some devices when panning over SVG
            options.svgElement.addEventListener('touchmove', function(e){ e.preventDefault(); });
          }

        , destroy: function(){
            this.hammer.destroy()
          }
        }  
  nav_filter_click();
  on_click_out_legend()
  dynm_viewbx_values();
    var panZoom = window.panZoom =svgPanZoom(document.getElementById("alphasvg").contentDocument.getElementsByTagName('svg')[0], {
        zoomEnabled: true,
        controlIconsEnabled: true,
        customEventsHandler: eventsHandler
    });
   
}

function dynm_viewbx_values(){
  if(document.getElementById("alphasvg") != null){
    document.getElementById("alphasvg").contentDocument.getElementsByTagName('svg')[0].setAttribute("viewBox", "0 0 "+document.documentElement.clientWidth+" "+parseInt(document.documentElement.clientHeight-($(".nav_bar_container_app").outerHeight()+$("#main_nav").outerHeight()+$("#footer").height()+12)));
    document.getElementById("alphasvg").contentDocument.getElementsByTagName('svg')[0].setAttribute("width", document.documentElement.clientWidth);
    document.getElementById("alphasvg").contentDocument.getElementsByTagName('svg')[0].setAttribute("height", parseInt(document.documentElement.clientHeight-($(".nav_bar_container_app").outerHeight()+$("#main_nav").outerHeight()+$("#footer").outerHeight())));
  }
}

  function open_click_pop_up_after_save(svg_id){
    if(svg_id.length != 0){
      initialize_tooltip()
      $(document.getElementById("alphasvg").contentDocument.getElementById(svg_id)).trigger("click");
      expand_all_sections();
    }
  }
    function initialize_tooltip(){
    var dWidth = $(window).width() * 0.5;
            if(dWidth > 270 )
            {
                dWidth = 330
            } 
            $( "#dialog_toolltip" ).dialog({
              create:function (event, ui) {
                  $("#dialog_toolltip").prev().hide();
                  $(".ui-widget-content").css("border", 0);
                  $(".ui-dialog").css("padding",0);
                  $(".ui-dialog .ui-dialog-content").css("padding",0);
              },
              resizable:false,
              modal:false,
              width: dWidth,
              closeOnEscape:false,
              draggable:false,
              minHeight:0
            });
  }

  function on_page_load_blink_lot(){
    if(document.getElementById("alphasvg") != null){
      if($search_original_style != "expired"){
        $search_original_style=$(document.getElementById("alphasvg").contentDocument.getElementById($.urlParam('svg_id'))).attr("style");
        lot_stroke_interval = setInterval(function() {
          colors = ['#00ffff', '#32cd32', '#ffa500','#ffffff','red'];
          rand   = Math.floor(Math.random()*colors.length);
          if(document.getElementById("alphasvg") != null){
            $(document.getElementById("alphasvg").contentDocument.getElementById($.urlParam('svg_id'))).attr('style', $search_original_style+';stroke-width: 5px !important;stroke:'+colors[rand]+';display:inline');
          }
        },100);
      }
    }
  }

  function rm_blink_after_click_on_lot()
  {
    if(document.getElementById("alphasvg") != null){
      if($search_original_style != "expired"){
        style= $search_original_style
        $(document.getElementById("alphasvg").contentDocument.getElementById($.urlParam('svg_id'))).attr('style',$search_original_style);
        clearInterval(lot_stroke_interval);
          $search_original_style = "expired"
      }
    }
  }