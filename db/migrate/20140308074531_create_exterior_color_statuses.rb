class CreateExteriorColorStatuses < ActiveRecord::Migration
  def change
    create_table :exterior_color_statuses ,:id=>false do |t|
      t.primary_key :ID
      t.string :Exterior_Color
      t.string :Color

      t.timestamps
    end
  end
end
