class LotsController < ApplicationController  
  before_filter :authenticate
  before_filter :is_admin
  before_filter :find_lot , :except => [:tie_retie, :show_parcels_based_on_community, :show_map_based_on_parcel]
  before_filter :get_product
  before_filter :get_product_view
  before_filter :get_communities  
  before_filter :get_communities_view    
  before_filter :get_parcels    
  before_filter :get_parcels_view
  def tie    
    @community = params[:community]
    @lot.update_attribute :SVG_ID, params[:lot][:Mapping_ID]
    Tbldatum.invalidate_cache
    @lots = Tbldatum.where(:Phase_Name=> params[:parcel_name])    
    render :partial => "show_map_based_on_parcel", :locals => {:@parcel_map => params[:parcel_map] , :@parcel_name => params[:parcel_name]}    

  end

  def retie    
    @community = params[:community]
    @tied_lot = Tbldatum.where(:SVG_ID => params[:lot][:Mapping_ID]).first
    if @tied_lot && @tied_lot != @lot
      @tied_lot.update_attribute(:SVG_ID, nil)
      @lot.update_attribute :SVG_ID, params[:lot][:Mapping_ID]
    end
    Tbldatum.invalidate_cache
    @lots = Tbldatum.where(:Phase_Name=> params[:parcel_name])    
    render :partial => "show_map_based_on_parcel", :locals => {:@parcel_map => params[:parcel_map] , :@parcel_name => params[:parcel_name]}    
  end
  
  def tie_retie 
  end
  
  def show_parcels_based_on_community
    @community = params[:Community]
    @parcels = Tbldatum.get_community_parcels(params[:Community]).map{|g| [g.Phase_Name]}    
    render :partial => "show_parcels_based_on_community"
  end

  def show_map_based_on_parcel
    @community = params[:Community]
    @lots = Tbldatum.where(:Phase_Name=> params[:Parcel])
    @parcel_name = params[:Parcel]
    @parcel_map = params[:Parcel].delete(" ") unless params[:Parcel].nil?    
    render :partial => "show_map_based_on_parcel"
  end

  private
  def find_lot   
    @lot = Tbldatum.find params[:lot][:ID]
  end
end
