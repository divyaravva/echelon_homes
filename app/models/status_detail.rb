class StatusDetail < ActiveRecord::Base
	self.table_name = "GIS_Status_Details"
	has_many :tbldatums, :class_name => "Tbldatum", :foreign_key => "Lot_Builder"

  has_many :tbldatums_lot_status, :class_name => "Tbldatum", :foreign_key => "Lot_Status", :primary_key => "id"
  has_many :tbldatums_service_orders, :class_name => "Tbldatum", :foreign_key => "Outstanding_Service_Orders", :primary_key => "id"
  has_many :tbldatums_con_sch_sts, :class_name => "Tbldatum", :foreign_key => "Construction_Schedule_Status", :primary_key => "id"
  has_many :tbldatums_con_vpo_sts, :class_name => "Tbldatum", :foreign_key => "Construction_VPO_Status", :primary_key => "id"
  has_many :tbldatums_model_elev_sts, :class_name => "Tbldatum", :foreign_key => "Model_Elevation_Id", :primary_key => "id"
  has_many :tbldatums_lot_premium, :class_name => "Tbldatum", :foreign_key => "Lot_Premium", :primary_key => "id"
  has_many :tbldatums_completed_specs, :class_name => "Tbldatum", :foreign_key => "Completed_Specs", :primary_key => "id"
  has_many :tbldatums_sales_sts, :class_name => "Tbldatum", :foreign_key => "Lot_Buzz_Status", :primary_key => "id"
  
  

	attr_protected

	def self.get_legend_values(legend_type, community, view)
	  if( legend_type == "Model")
		  StatusDetail.order("StatusKey").find(:all, :conditions => ["LegendType IN(?) AND Community IN (?) and Is_Active = ?", legend_type, community, "1"])
    elsif legend_type == "Floor Plan" && view != "Floor Plans View"
      StatusDetail.order("StatusKey").find(:all, :conditions => ["LegendType IN(?) AND Community IN (?) and Is_Active = ? and lot_plan_sts = ?", legend_type, community, "1",1])      
    elsif legend_type == "Floor Plan" && view == "Floor Plans View"
      StatusDetail.order("StatusKey").find(:all, :conditions => ["LegendType IN(?) AND Community IN (?) and Is_Active = ? and lot_plan_sts in (?)", legend_type, community, "1",[1,0]])
	  else
	  	self.order("LegendOrder ASC").find(:all, :conditions => ["LegendType IN(?) AND Is_Active = ?", legend_type, "1"])
	  end
	end

	def self.get_legend_values_of_model_type
		 self.find(:all, :conditions => 'LegendType = "Model Type"').map(&:id)
	end

  def self.update_floor_plans
  	 recs_with_floor_plans = Tbldatum.all.select{|x|x.Floor_Plan_Name!='' && x.Community != nil}
  	 recs_with_floor_plans.each do |each_rec|
		unless each_rec.SVG_ID.blank?
			floor_plan_rec = self.find_or_initialize_by_StatusKey_and_LegendType_and_Community(each_rec.Floor_Plan_Name, "Floor Plan", each_rec.Community)
			floor_plan_rec.update_attributes(:StatusKey => each_rec.Floor_Plan_Name, :LegendType => "Floor Plan", :StatusLabel => each_rec.Floor_Plan_Name, :Community => each_rec.Community, :Status_Display_Name => each_rec.Floor_Plan_Name, :Is_Active => 1)
			each_rec.update_attributes(:Floor_Plan_Id => floor_plan_rec.id) unless floor_plan_rec.blank?
	    end
  	end
  end

  def self.insert_rec_fr_all_comm(community)
    self.where('Community = ? and LegendType NOT IN (?)',"Grey Fox",['Model','Floor Plan']).each do |rec|
      sts_detail_rec = StatusDetail.find_or_initialize_by_StatusKey_and_LegendType_and_Community(rec.StatusKey, rec.LegendType, community)
      sts_detail_rec.update_attributes(:StatusLabel => rec.StatusLabel, :LegendOrder => rec.LegendOrder, :ColorCode => rec.ColorCode, :Status_Display_Name => rec.Status_Display_Name)
    end
  end

  def self.other_to_lot_sts
    comm = ['Grey Fox', 'Blackthorne', 'New Bethel','Epler Trace','Traders Point','Devonshire','Grant Park']
    comm.each do |com|
      sts_rec = StatusDetail.find_or_initialize_by_StatusKey_and_LegendType_and_Community("Other","Sales Status",com)
      sts_rec.update_attributes(:StatusLabel => "Other", :LegendOrder => 8, :ColorCode => "#e1f765", :Status_Display_Name => "Other")
    end
  end


  def self.add_new_status_from_ihms
    all_comm = ['Blackthorne','Devonshire', 'Epler Trace', 'Grant Park','Grey Fox','Meadows at Bainbridge','New Bethel','Traders Point']
    all_sts = ["Option Lot","Option Lot Reservation","Option Lot Signed Agreement","Settled","Lot Hold","Available","Signed Agreement (Not Ratified)","Ratified","Reservation","Not Available"]
    all_comm.each do |comm|
      all_sts.each do |sts|
        sts_rec = StatusDetail.find_or_initialize_by_StatusKey_and_LegendType_and_Community(sts,"Lot Status",comm)
        sts_rec.update_attributes(:StatusLabel => sts, :Status_Display_Name => sts)
      end
    end
  end

  def self.update_status
    Tbldatum.where("SVG_ID is not null").each do |lot|
      completed_specs = StatusDetail.find_by_LegendType_and_StatusKey("Completed Specs", lot.Completed_Specs_Val)
      lot_premium = StatusDetail.find_by_LegendType_and_StatusKey("Lot Premium", lot.Lot_Premium_Status)
      schedule_status = StatusDetail.find_by_LegendType_and_StatusKey("Schedule Status", lot.Construction_Schedule_Status_Val)
      vpo_status = StatusDetail.find_by_LegendType_and_StatusKey("VPO Status", lot.Construction_VPO_Status_Val)
      lot_status = StatusDetail.find_by_LegendType_and_StatusKey("Lot Status", lot.Lot_Status_Val)
      outstanding_service_orders = StatusDetail.find_by_LegendType_and_StatusKey("Outstanding Service Orders", lot.Outstanding_Service_Orders_Val)
      lot_buzz_status = StatusDetail.find_by_LegendType_and_StatusKey("Sales Status", lot.Lot_Buzz_Status_Val) 

      temp_status={
        :Completed_Specs => (completed_specs.id unless completed_specs.nil?),
        :Lot_Premium => (lot_premium.id unless lot_premium.nil?),
        :Construction_Schedule_Status => (schedule_status.id unless schedule_status.nil?),
        :Construction_VPO_Status => (vpo_status.id unless vpo_status.nil?),
        :Lot_Status =>  (lot_status.id unless lot_status.nil?),
        :Outstanding_Service_Orders => (outstanding_service_orders.id unless outstanding_service_orders.nil?),
        :Lot_Buzz_Status => (lot_buzz_status.id unless lot_buzz_status.nil?)

      }
      lot.update_attributes(temp_status)
    end
  end

end