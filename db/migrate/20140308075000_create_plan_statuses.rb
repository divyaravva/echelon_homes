class CreatePlanStatuses < ActiveRecord::Migration
  def change
    create_table :plan_statuses ,:id=>false do |t|
      t.primary_key :ID
      t.string :Plan_Number
      t.string :Color

      t.timestamps
    end
  end
end
