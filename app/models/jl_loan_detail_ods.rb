class JlLoanDetailOds < ActiveRecord::Base
	attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_JLLOANDETAIL"
  self.primary_key = "LOANNUMBER"
  belongs_to :option_lot_master,:class_name =>"OptionLotMasterOds"
  has_one :jl_loan_master, :class_name =>"JlloanMasterOds", :foreign_key => "LOANNUMBER"

  def self.load_ods_jl_loan_detail(windows_db_nm)

  	truncate_ods_jl_loan_detail

    puts "********** 1A) Started Loading JlLoanDetailOds Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from "+windows_db_nm+".dbo.JLLOANDETAIL  where HOUSENUMBER != '00000000'");
      	puts "............................................."
      	puts "............................................."

        batch=[]
        batch_size = 1000

        result.each do |row|
          batch << JlLoanDetailOds.new(:COMPANYCODE => row['COMPANYCODE'],
                                              :DEVELOPMENTCODE => row['DEVELOPMENTCODE'],
                                              :HOUSENUMBER => row['HOUSENUMBER'],
                                              :LOANNUMBER => row['LOANNUMBER'],
                                              :TRANSACTION_DATE => row['TRANSACTION_DATE'],
                                              :SEQUENCENUMBER => row['SEQUENCENUMBER'],
                                              :DRAWTYPE => row['DRAWTYPE'],
                                              :BATCHNUM => row['BATCHNUM'],
                                              :AMOUNT => row['AMOUNT']
								                    )
            
            if batch.size >= batch_size
              JlLoanDetailOds.import batch
              batch = [] 
            end
        end
        JlLoanDetailOds.import batch
    end

    puts "Total no. of Loaded rows.........#{JlLoanDetailOds.count}"
    puts "********** 1B) Finished Loading JlLoanDetailOds Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_jl_loan_detail
		ActiveRecord::Base.connection.execute("TRUNCATE ODS_JLLOANDETAIL")
	end


end