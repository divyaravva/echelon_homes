class ContractMasterOds < ActiveRecord::Base
	attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_CONTRACTMASTER"

  def self.load_ods_contract_master(dev_codes, windows_db_nm)
      truncate_ods_contract_master
      puts "********** 1A) Started Loading ContractMasterOds Table contents at..#{Time.now} ***********"
      client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
        if (client.nil? == true)
        elsif (client.nil? == false && client.active? == true)
          result = client.execute("select * from "+windows_db_nm+".dbo.CONTRACTMASTER where DEVELOPMENTCODE in ("+dev_codes+")");
          	
            puts "............................................."
          	puts "............................................."

            batch=[]
            batch_size = 1000

            result.each do |row|
             batch << ContractMasterOds.new(:COMPANYCODE => row['COMPANYCODE'],
                        										:CONTRACTNUMBER => row['CONTRACTNUMBER'],
                        										:DEVELOPMENTCODE => row['DEVELOPMENTCODE'],
                        										:MODELCODE => row['MODELCODE'],
                        										:ELEVATIONCODE => row['ELEVATIONCODE'],
                        										:CATEGORYCODE => row['CATEGORYCODE'],
                        										:COSTCODE => row['COSTCODE'],
                        										:VENDORNUMBER => row['VENDORNUMBER'],
                        										:EXPIRATIONDATE => row['EXPIRATIONDATE'],
                        										:AMOUNT => row['AMOUNT']
    								                        )
            
              if batch.size >= batch_size 
                ContractMasterOds.import batch
                batch = [] 
              end
            end
            ContractMasterOds.import batch
           end
        puts "Total no. of Loaded rows.........#{ContractMasterOds.count}"
        puts "********** 1B) Finished Loading ContractMasterOds Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_contract_master
		ActiveRecord::Base.connection.execute("TRUNCATE ODS_CONTRACTMASTER")
	end
  
end