desc "Automate the Data from SQL server"
task :automate_data_refresh => :environment do
	puts "********** Started Automating ********* #{Time.now}"
	puts " "
	ActiveRecord::Base.transaction do
		DataLoadStatus.load_statuses
	end
	puts " "
	puts "********** Updating finished********** #{Time.now}"	
	puts " "
end

task :run_jar_file, [:refresh_type] => [:environment] do |t,args|
	puts "STARTED JOB"
	refresh_type = args[:refresh_type]	

	client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
	if(client.nil? == false && client.active? == true)
		result = client.execute("select * from [echelon_homes].[dbo].[DataLoadStatus] order by id desc");
		latest_rec = result.first unless result.blank?
		
		if latest_rec.nil? == true || latest_rec["Total_Job_Status"] !="Running"
			puts "Jar File execution started at.....#{Time.now}"
			result.cancel		
			DataRefreshStatus.execute_jar_file(refresh_type, client)
			puts "Jar File execution ended at.....#{Time.now}"

			after_jar_exec = client.execute("select * from [echelon_homes].[dbo].[DataLoadStatus] order by id desc")
			res=after_jar_exec.first

			if res["Total_Job_Status"] == "Running" && res["Jar_Status"]=="Succeeded"
				after_jar_exec.cancel
				puts "LOAD FROM WINDOWS TO LINUX Started at #{Time.now}"
				DataRefreshStatus.load_all_ods_tables
				puts "LOAD FROM WINDOWS TO LINUX Ended at #{Time.now}"			
				
				puts "ETL process Started at #{Time.now}"
				Tbldatum.etl_process(client)
				puts "ETL process Ended at #{Time.now}"
				
				puts "Cache Clear Started"	
				Rails.cache.clear
				puts "Cache Clear Ended"
				client.execute("update [echelon_homes].[dbo].[DataLoadStatus] set EndDate='#{Time.now.strftime("%Y-%m-%d %H:%M:%S")}', Total_Job_Status='Succeeded' where id=(select top 1 id from [echelon_homes].[dbo].[DataLoadStatus] order by id desc)");		
			else
				client.execute("update [echelon_homes].[dbo].[DataLoadStatus] set EndDate='#{Time.now.strftime("%Y-%m-%d %H:%M:%S")}', Total_Job_Status='Failed' where id=(select top 1 id from [echelon_homes].[dbo].[DataLoadStatus] order by id desc)");			
			end

			#fetch data load status from Windows
			res2=client.execute("select * from echelon_homes.dbo.DataLoadStatus");
			res2.each do |row|
				data_load_Status_rec = DataLoadStatus.find_or_initialize_by_id(row['id'])
            	data_load_Status_rec.update_attributes(:id => row['id'],
            				:StartDate => row['StartDate'],
                            :EndDate => row['EndDate'],
                            :Jar_File_Status => row['Jar_Status'],
                            :Status => row['Total_Job_Status'],
                            :Refreshed_Mode => row['Refreshed_Mode'],
                            :Environment => row['Environment']
                        ) 
        	end
		end
	end	
end
