class ZeroCountStatusesController  < ApplicationController

	before_filter :authenticate
    before_filter :has_roles  
    before_filter :get_product
    before_filter :get_product_view
    before_filter :get_comm_city_view
    before_filter :get_community_city
    before_filter :get_communities  
    before_filter :get_communities_view    
    before_filter :get_parcels    
  
  def index  
  	@zero_status = ZeroCountStatus.first
  end

  def edit       
    @zero_sts_details = ZeroCountStatus.find(params[:id]) unless params[:id].nil?
  end

  def update
   	zero_sts_details = ZeroCountStatus.find(params[:id]) unless params[:id].nil?    
    zero_sts_details.update_attributes({:zero_status_check => params[:zero_status_check]}) unless zero_sts_details.nil?
    redirect_to "/zero_count_statuses"
  end
end