class AddPicCountDocCountToProductSalesContracts < ActiveRecord::Migration

  def self.up

    add_column :GIS_Product_Sales_Contact, :pic_count, :integer, :null => false, :default => 0

    add_column :GIS_Product_Sales_Contact, :doc_count, :integer, :null => false, :default => 0

  end

  def self.down

    remove_column :GIS_Product_Sales_Contact, :pic_count

    remove_column :GIS_Product_Sales_Contact, :doc_count

  end

end
