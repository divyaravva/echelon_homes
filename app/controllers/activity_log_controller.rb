class ActivityLogController < ApplicationController
   
   before_filter :authenticate  
   before_filter :get_product
   before_filter :get_product_view      
   before_filter :get_communities   
   before_filter :get_communities_view
   before_filter :get_parcels
   before_filter :get_parcels_view
  
   def index     
    @lot_id=params[:lot_id]
   end

   def get_logs
      render :json => Tbldatum.all
   end

   def new          
   end
   
   def create 
      
   end   

   def edit    
   
   end

   def update    
   
   end

   def show
       
   end

   def destroy            
      
   end

end
