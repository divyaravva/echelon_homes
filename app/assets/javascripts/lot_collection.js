$('document').ready(function() {
  if ($('body.lot_collection_new').length || $('body.lot_collection_edit').length) {
    value = $("#builders_dropdown").val();
   

     showfloorplans(value);  
    var jsonObj;
    function CreateJson()
    {
        jsonObj = "[";
        $.each( $("#destinationFields>div"), function(index, div) {
            var $div = $(div);
            if(index>0) jsonObj += ",";        
            jsonObj += '{"lot_number":"' + $div.text().split(" - ")[0] + '"}';
        });
        jsonObj = jsonObj + "]";    
        return jsonObj;
    }
    $("#create_lot_collection_form, #update_lot_collection_form").submit(function(){      
        if($("#destinationFields>div").length==0){
            CreateJson();
            alert("A lot group should have atleast one available lot");
            return false
        }else{
            $("#selected_lot_list").val(CreateJson());
        }    
    });  


  }
});
function showfloorplans(value){        
     $.ajax({
            type: "GET",
            url: '/lot_collection/total_communities',
            data: {Builder: value},
            success: function(data){
              $("#community_dropdown").html(data);
            }
    });
};
