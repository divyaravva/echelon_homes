class ModelMasterOds < ActiveRecord::Base
	attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_MODELMASTER"

  def self.load_ods_model_master(windows_db_nm)

  	truncate_ods_model_master

    puts "********** 1A) Started Loading ModelMasterOds Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from "+windows_db_nm+".dbo.MODELMASTER");
      	puts "............................................."
      	puts "............................................."

        batch=[]
        batch_size = 1000

        result.each do |row|
          #ModelMasterOds.find_or_initialize_by_COMPANYCODE_and_DEVELOPMENTCODE_and_LOTNUMBER(row['COMPANYCODE'], row['DEVELOPMENTCODE'], row['LOTNUMBER'])
          batch << ModelMasterOds.new(:COMPANYCODE => row['COMPANYCODE'],
                                      :DEPOSITOVERRIDE => row['DEPOSITOVERRIDE'],
                                      :DEPOVERRIDEFLAG => row['DEPOVERRIDEFLAG'],
                                      :DESCRIPTION => row['DESCRIPTION'],
                                      :INACTIVE => row['INACTIVE'],
                                      :MODELCODE => row['MODELCODE'],
                                      :SELLINGPRICE => row['SELLINGPRICE'],
                                      :TERMINATOR => row['TERMINATOR']
								)
            if batch.size >= batch_size 
              ModelMasterOds.import batch
              batch = [] 
            end
        end
        ModelMasterOds.import batch
    end

    puts "Total no. of Loaded rows.........#{ModelMasterOds.count}"
    puts "********** 1B) Finished Loading ModelMasterOds Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_model_master
		ActiveRecord::Base.connection.execute("TRUNCATE ODS_MODELMASTER")
	end

 end