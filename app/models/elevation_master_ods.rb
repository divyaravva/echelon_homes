class ElevationMasterOds < ActiveRecord::Base
	attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_ELEVATIONMASTER"
  has_many :model_masters, :class_name => "ModelMasterOds", :foreign_key => [:MODELCODE], :primary_key => [:MODELCODE]

  def self.load_ods_elevation_master(dev_codes,windows_db_nm)

  	truncate_ods_elevation_master

    puts "********** 1A) Started Loading ElevationMasterOds Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from "+windows_db_nm+".dbo.ELEVATIONMASTER where DEVELOPMENTCODE in ("+dev_codes+")");
      	puts "............................................."
      	puts "............................................."

        batch=[]
        batch_size = 1000

        result.each do |row|
          #ElevationMasterOds.find_or_initialize_by_COMPANYCODE_and_DEVELOPMENTCODE_and_LOTNUMBER(row['COMPANYCODE'], row['DEVELOPMENTCODE'], row['LOTNUMBER'])
          batch << ElevationMasterOds.new(:COMPANYCODE => row['COMPANYCODE'],
                                          :DEVELOPMENTCODE => row['DEVELOPMENTCODE'],
                                          :MODELCODE => row['MODELCODE'],
                                          :ELEVATIONCODE => row['ELEVATIONCODE'],
                                          :DESCRIPTION => row['DESCRIPTION'],                                    
                                          :SALESPRICE => row['SALESPRICE']
								                         )
            
            if batch.size >= batch_size
              ElevationMasterOds.import batch
              batch = [] 
            end
        end
        ElevationMasterOds.import batch
    end

    puts "Total no. of Loaded rows.........#{ElevationMasterOds.count}"
    puts "********** 1B) Finished Loading ElevationMasterOds Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_elevation_master
		ActiveRecord::Base.connection.execute("TRUNCATE ODS_ELEVATIONMASTER")
	end


end