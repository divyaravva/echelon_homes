class JlBankMasterFileOds < ActiveRecord::Base
  attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_JLBANKMASTERFILE"
  
  def self.load_ods_jlbankmasterfile(windows_db_nm)

    truncate_ods_jlbankmasterfile

    puts "********** 1A) Started Loading JLBANKMASTERFILE Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from "+windows_db_nm+".dbo.JLBANKMASTERFILE");
        puts "............................................."
        puts "............................................."

        batch=[]
        batch_size = 1000

        result.each do |row|
          #HouseCostDetail.find_or_initialize_by_COMPANYCODE_and_DEVELOPMENTCODE_and_LOTNUMBER(row['COMPANYCODE'], row['DEVELOPMENTCODE'], row['LOTNUMBER'])
          batch << JlBankMasterFileOds.new(:COMPANYCODE => row['COMPANYCODE'],
                                          :BANKCODE => row['BANKCODE'],
                                          :DESCRIPTION => row['DESCRIPTION']
                                    )
            if batch.size >= batch_size
              JlBankMasterFileOds.import batch
              batch = [] 
            end  
        end
        JlBankMasterFileOds.import batch
        # HouseCostDetailOds.mass_insert(batch, :per_batch => 1000)
    end

    puts "Total no. of Loaded rows.........#{JlBankMasterFileOds.count}"
    puts "********** 1B) Finished Loading ODS_JLBANKMASTERFILE Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_jlbankmasterfile
    ActiveRecord::Base.connection.execute("TRUNCATE ODS_JLBANKMASTERFILE")
  end


end