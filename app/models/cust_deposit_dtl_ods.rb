class CustDepositDtlOds < ActiveRecord::Base
  attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_CUSTDEPOSITDTL"
  # self.primary_keys = :DEVELOPMENTCODE,:HOUSENUMBER

  def self.load_ods_cust_deposit(dev_codes,windows_db_nm)

    truncate_ods_cust_deposit

    puts "********** 1A) Started Loading CustDepositDtlOds Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from "+windows_db_nm+".dbo.CUSTDEPOSITDTL where DEVELOPMENTCODE in ("+dev_codes+")  and HOUSENUMBER != '00000000'");
        puts "............................................."
        puts "............................................."

        batch=[]
        batch_size = 1000

        result.each do |row|
          #HouseCostDetail.find_or_initialize_by_COMPANYCODE_and_DEVELOPMENTCODE_and_LOTNUMBER(row['COMPANYCODE'], row['DEVELOPMENTCODE'], row['LOTNUMBER'])
          batch << CustDepositDtlOds.new(:COMPANYCODE => row['COMPANYCODE'],
                                          :DEVELOPMENTCODE => row['DEVELOPMENTCODE'],
                                          :HOUSENUMBER => row['HOUSENUMBER'],
                                          :DEPOSIT_DATE => row['DEPOSIT_DATE'],
                                          :SEQUENCENUMBER => row['SEQUENCENUMBER'],
                                          :REMARKS => row['REMARKS'],
                                          :DEPOSITTYPE => row['DEPOSITTYPE'],
                                          :BATCHNUM => row['BATCHNUM'],
                                          :AMOUNT => row['AMOUNT']
                                    )
            if batch.size >= batch_size
              CustDepositDtlOds.import batch
              batch = [] 
            end  
        end
        CustDepositDtlOds.import batch
        # HouseCostDetailOds.mass_insert(batch, :per_batch => 1000)
    end

    puts "Total no. of Loaded rows.........#{CustDepositDtlOds.count}"
    puts "********** 1B) Finished Loading ODS_CUSTDEPOSITDTL Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_cust_deposit
    ActiveRecord::Base.connection.execute("TRUNCATE ODS_CUSTDEPOSITDTL")
  end


end