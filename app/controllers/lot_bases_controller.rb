class LotBasesController  < ApplicationController

	before_filter :authenticate
    before_filter :has_roles  
    before_filter :get_product
    before_filter :get_product_view
    before_filter :get_comm_city_view
    before_filter :get_community_city
    before_filter :get_communities  
    before_filter :get_communities_view    
    before_filter :get_parcels    
  
  def index  
  	@lot_basis = LotBasis.all
  end

  def edit       
    @lot_basis = LotBasis.find(params[:id]) unless params[:id].nil?
    @cost_codes = HouseCostSummaryOds.find_by_sql("SELECT DISTINCT CATEGORYCODE,COSTCODE  FROM west_port.ODS_HOUSECOSTSUMMARY")
  end

  def update
    lot_basis_details = LotBasis.find(params[:id]) unless params[:id].nil?    
    cost_code = params[:cost_code]
    # range_cost_codes = []
    # without_range_cost_codes = []
    cost_code_query = ""
    # cost_code.each do |c|
    #   if c.include?("-")
    #     range_cost_codes << c
    #   else
    #     without_range_cost_codes << c
    #   end
    # end
    # range_cost_codes.each do |rcc|
    #   range = rcc.strip.gsub("-"," and ")
    #   if rcc != range_cost_codes.last
    #     cost_code_query += "COSTCODE between #{range} or "
    #   else
    #     cost_code_query += "COSTCODE between #{range}"
    #   end
    # end
    # if !cost_code_query.blank?
      cost_code_query += "COSTCODE in ("+cost_code.join(",") +")"
    # else
    #   cost_code_query += "COSTCODE in ("+without_range_cost_codes.join(",") +")"
    # end
    # cost_code_query is formed to improve performance instead of forming query at the time of cal. value during etl_process
    lot_basis_details.update_attributes({:category_code => params[:category_code], :cost_code => params[:cost_code].join(","), :cost_code_query => cost_code_query}) unless lot_basis_details.nil?
    redirect_to "/lot_bases"
  end

  def cost_codes_for_category
    category_code = params[:category_code]
    if params[:selected_category_code] == category_code
      @selected_cost_codes = LotBasis.find(params[:id]).cost_code.split(",")
    else
      @selected_cost_codes = []
    end
    @cost_codes = HouseCostSummaryOds.find_by_sql("SELECT DISTINCT CATEGORYCODE,COSTCODE  FROM west_port.ODS_HOUSECOSTSUMMARY where CATEGORYCODE=#{category_code}")
     respond_to do |format|
        format.js 
      end
  end

 
end