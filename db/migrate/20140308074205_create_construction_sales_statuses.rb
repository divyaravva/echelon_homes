class CreateConstructionSalesStatuses < ActiveRecord::Migration
  def change
    create_table :construction_sales_statuses,:id=>false do |t|
      t.primary_key :ID
      t.string :Construction_Status
      t.string :Color

      t.timestamps
    end
  end
end
