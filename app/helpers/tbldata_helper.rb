module TbldataHelper
  def all_user_group_options
    Tblusergroup.fetch_user_groups
  end  
  def all_access_options
    Access.fetch_access
  end  
  def all_tabs_options
    Tab.fetch_tabs
  end
  def all_user_group_names
  	Tblusergroup.fetch_user_group_names  	
  end
  def all_role_definitions
    RoleDefinition.get_roles
  end 
end
