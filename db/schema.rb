# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140707100325) do

  create_table "GIS_Menu", :id => false, :force => true do |t|
    t.integer "Seq",                    :null => false
    t.string  "MenuName", :limit => 50, :null => false
    t.integer "Active",   :limit => 1,  :null => false
  end

  create_table "GIS_MenuOption", :id => false, :force => true do |t|
    t.string  "MenuName",       :limit => 50,   :null => false
    t.string  "MenuOptionName", :limit => 50,   :null => false
    t.integer "Seq",                            :null => false
    t.integer "Active",         :limit => 1,    :null => false
    t.string  "MapSQL",         :limit => 1000
    t.string  "DetailSQL",      :limit => 1000
  end

  create_table "GIS_Product_Sales_Contact", :id => false, :force => true do |t|
    t.integer  "Community_ID",                                                                            :null => false
    t.string   "Community",                  :limit => 40,                                                :null => false
    t.string   "Product_Segment_Name",       :limit => 80
    t.string   "Phase_Name",                 :limit => 40
    t.string   "Lot_Status",                 :limit => 40
    t.string   "Lot_Number",                 :limit => 20
    t.string   "Block",                      :limit => 20
    t.string   "BuilderName",                :limit => 150
    t.string   "Address_1",                  :limit => 50
    t.string   "Address_2",                  :limit => 50
    t.string   "City",                       :limit => 30
    t.string   "State",                      :limit => 10
    t.string   "Zip",                        :limit => 12
    t.integer  "Lot_Square_Feet"
    t.string   "Lot_Type",                   :limit => 40
    t.string   "ProductType",                :limit => 20
    t.string   "SquareFootRange",            :limit => 50
    t.string   "HUD_Number",                 :limit => 20
    t.string   "PlanName",                   :limit => 80
    t.decimal  "LotPrice",                                  :precision => 12, :scale => 2
    t.decimal  "Total_Escalated_Price",                     :precision => 12, :scale => 2
    t.decimal  "Settlement_Price",                          :precision => 12, :scale => 2
    t.decimal  "Settlement_Costs",                          :precision => 12, :scale => 2
    t.decimal  "PlanBasePrice",                             :precision => 12, :scale => 2
    t.decimal  "UnitBasePrice",                             :precision => 12, :scale => 2
    t.integer  "Estimated_Square_Feet"
    t.integer  "Unit_Square_Feet"
    t.string   "PlanDesc",                   :limit => 80
    t.string   "Bedrooms",                   :limit => 10
    t.string   "Bathrooms",                  :limit => 10
    t.integer  "Garage_Spaces",              :limit => 1
    t.string   "Home_Type",                  :limit => 30
    t.date     "LotLandContractDate"
    t.integer  "LotSoldYear"
    t.integer  "LotSoldMonth"
    t.string   "LotSoldYearMonth",           :limit => 62
    t.string   "SalesPriceRange",            :limit => 50
    t.string   "CostPerSquareFootRange",     :limit => 50
    t.string   "Home_Construction_Status",   :limit => 40
    t.string   "First_Name",                 :limit => 30
    t.string   "Last_Name",                  :limit => 80
    t.string   "Email",                      :limit => 100
    t.string   "address1Contact",            :limit => 127
    t.string   "zipContact",                 :limit => 16
    t.string   "StateContact",               :limit => 50
    t.string   "CityContact",                :limit => 50
    t.string   "MindbaseSegment",            :limit => 40
    t.string   "MindbaseSubsegment",         :limit => 40
    t.integer  "Military",                   :limit => 1
    t.string   "Type",                       :limit => 30
    t.string   "OriginalContactOrgin",       :limit => 20
    t.string   "CommunityContactOrgin",      :limit => 20
    t.string   "AgeRange",                   :limit => 30
    t.string   "FamilyFormation",            :limit => 100
    t.string   "NextHomeIWillBuy",           :limit => 100
    t.string   "Lifestyle",                  :limit => 75
    t.string   "TimeframeToMove",            :limit => 50
    t.string   "ReasonForMoving",            :limit => 50
    t.string   "Rank",                       :limit => 10
    t.string   "DesiredPriceRange",          :limit => 30
    t.string   "ChildrenEducation",          :limit => 50
    t.string   "LocationPreference",         :limit => 100
    t.string   "NeighborhoodPreference",     :limit => 100
    t.string   "RealtorCompanyName",         :limit => 150
    t.string   "realtorName",                :limit => 80
    t.string   "BuilderSalesAgent",          :limit => 80
    t.date     "NbrhdCreateDate"
    t.integer  "DaysToBuy"
    t.string   "DaysToBuyRange",             :limit => 50
    t.date     "DateSaleEntered"
    t.string   "Rn_Descriptor",              :limit => 80
    t.string   "ContractStatus",             :limit => 40
    t.date     "ContractDate"
    t.integer  "ContractYear"
    t.integer  "ContractMonth"
    t.string   "contractYearMonth",          :limit => 62
    t.date     "ContractCloseDate"
    t.decimal  "Contract_Sales_Price",                      :precision => 12, :scale => 2
    t.decimal  "Closed_Price",                              :precision => 12, :scale => 2
    t.decimal  "Base_Price",                                :precision => 12, :scale => 2
    t.integer  "total",                                                                                   :null => false
    t.date     "EstimatedContractCloseDate"
    t.string   "REAgentEmail",               :limit => 100
    t.string   "REAddress1",                 :limit => 50
    t.string   "REAddress2",                 :limit => 50
    t.string   "REZip",                      :limit => 12
    t.string   "RECity",                     :limit => 40
    t.string   "REState",                    :limit => 10
    t.string   "REPhone",                    :limit => 40
    t.integer  "LotWidth"
    t.datetime "ARCApprovalDate"
    t.string   "LotDepth",                   :limit => 10
    t.string   "BodyColor",                  :limit => 30
    t.integer  "GIS_Lot_ID",                                                                              :null => false
    t.date     "DateAdded",                                                                               :null => false
    t.string   "SVG_ID",                     :limit => 60
    t.integer  "pic_count",                                                                :default => 0, :null => false
    t.integer  "doc_count",                                                                :default => 0, :null => false
  end

  create_table "GIS_StatusKey", :id => false, :force => true do |t|
    t.string  "MenuOptionName", :limit => 50, :null => false
    t.string  "StatusKey",      :limit => 50, :null => false
    t.string  "StatusLabel",    :limit => 50, :null => false
    t.string  "ColorCode",      :limit => 7
    t.integer "LegendOrder",                  :null => false
  end

  create_table "accesses", :primary_key => "ID", :force => true do |t|
    t.string "name"
    t.string "alias_name"
  end

  create_table "construction_statuses", :primary_key => "ID", :force => true do |t|
    t.string   "Construction_Status"
    t.string   "Color"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "exterior_color_statuses", :primary_key => "ID", :force => true do |t|
    t.string   "Exterior_Color"
    t.string   "Color"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "gallery_images_to_lots", :force => true do |t|
    t.integer  "lot_info_id"
    t.integer  "image_gallery_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "home_statuses", :primary_key => "ID", :force => true do |t|
    t.string   "Description"
    t.string   "Color"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "homes", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "image_galleries", :force => true do |t|
    t.string   "navigation_link"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.string   "pics_file_name"
    t.string   "pics_content_type"
    t.integer  "pics_file_size"
    t.datetime "pics_updated_at"
  end

  create_table "land_dev_statuses", :primary_key => "ID", :force => true do |t|
    t.string   "Land_Dev_Status"
    t.string   "Color"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "lot_infos", :force => true do |t|
    t.string   "community"
    t.string   "lot_number"
    t.string   "lot_status"
    t.string   "lot_area"
    t.integer  "lot_status_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "SVG_ID"
  end

  create_table "lot_sales_status_builder", :primary_key => "ID", :force => true do |t|
    t.string   "Lot_Sales_Status_Builder"
    t.string   "Color"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "lot_sales_statuses", :primary_key => "ID", :force => true do |t|
    t.string   "Lot_Sales_Status"
    t.string   "Color"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "lot_statuses", :force => true do |t|
    t.string   "status"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "color"
  end

  create_table "lot_width_statuses", :primary_key => "ID", :force => true do |t|
    t.string   "Lot_Width_Data"
    t.string   "Color"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "lots", :force => true do |t|
    t.string   "name"
    t.string   "status"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "mainmenu", :primary_key => "ID", :force => true do |t|
    t.string "Name"
  end

  create_table "plan_statuses", :primary_key => "ID", :force => true do |t|
    t.string   "Plan_Number"
    t.string   "Color"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "reports", :force => true do |t|
    t.string   "document_file_name"
    t.string   "document_content_type"
    t.integer  "document_file_size"
    t.datetime "document_updated_at"
    t.integer  "tab_id"
    t.integer  "user_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "sales_statuses", :primary_key => "ID", :force => true do |t|
    t.string   "Sales_Status"
    t.string   "Color"
    t.string   "Sold_Status"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "subtabs", :primary_key => "ID", :force => true do |t|
    t.string  "name"
    t.integer "tab_id"
  end

  create_table "tabs", :primary_key => "ID", :force => true do |t|
    t.string  "name"
    t.integer "parent_tab_id"
    t.integer "has_child"
  end

  create_table "tblBuilders", :primary_key => "ID", :force => true do |t|
    t.string "LotBuilderName"
  end

  create_table "tblGeotechLotCondition", :primary_key => "ID", :force => true do |t|
    t.integer "Number",      :limit => 2
    t.string  "Description"
    t.string  "Color"
  end

  create_table "tblInternetAccessibility", :primary_key => "ID", :force => true do |t|
    t.integer "Number",      :limit => 2
    t.string  "Description"
    t.string  "Color"
  end

  create_table "tblaccessgroup", :primary_key => "ID", :force => true do |t|
    t.string  "Group_Name",       :limit => 50
    t.string  "Description"
    t.integer "fAccessGroup",     :limit => 2
    t.binary  "fData",            :limit => 1
    t.binary  "fUsers",           :limit => 1
    t.binary  "rActivityRecord",  :limit => 1
    t.binary  "rAllOtherReports", :limit => 1
  end

  create_table "tblactivityrecord", :primary_key => "ID", :force => true do |t|
    t.string   "FormName"
    t.integer  "RecordID"
    t.string   "FieldName"
    t.string   "ChangeFrom"
    t.string   "ChangeTo"
    t.string   "UserName"
    t.datetime "ActivityDate"
  end

  create_table "tblallocations", :primary_key => "ID", :force => true do |t|
    t.integer "AllocationNumber", :limit => 2
    t.string  "AllocationName"
    t.string  "LotColor"
  end

  create_table "tbldata", :primary_key => "ID", :force => true do |t|
    t.string   "Filing",                                                                :null => false
    t.integer  "Lot_N",                                                                 :null => false
    t.string   "Address"
    t.float    "Street_Width"
    t.integer  "Lot_Area"
    t.binary   "Corner",                                            :limit => 1
    t.binary   "Green_Street",                                      :limit => 1
    t.float    "Base_Lot_Price"
    t.float    "Adjusted_Lot_List_Price"
    t.float    "Lot_Sales_Price_per_Contract"
    t.string   "Lot_Builder"
    t.text     "Lot_Notes",                                         :limit => 16777215
    t.string   "Home_Owner"
    t.datetime "Lot_Sale_Closing_Date"
    t.datetime "Home_Sale_Closing_Date"
    t.binary   "Lot_Suborindation",                                 :limit => 1
    t.string   "Lot_Subordination_Fee_Computed_As"
    t.float    "Lot_Subordination_Fee"
    t.string   "Construction_Financing_By"
    t.float    "Amount_of_Construction_Financing"
    t.string   "Construction_Financing_Interest_Rate"
    t.datetime "Use_Easement_Recorded"
    t.string   "Home_Model_Name"
    t.integer  "Bedroom_Count",                                     :limit => 2
    t.integer  "Bathroom_Count",                                    :limit => 2
    t.string   "Master"
    t.string   "Stories"
    t.integer  "Non_Basement_Home_Area_SqFt"
    t.integer  "Basement_Home_Area_SqFt"
    t.integer  "Finished_Home_Area_SqFt"
    t.integer  "Total_Home_Area_SqFt"
    t.float    "Latest___Last_Home_List_Price"
    t.float    "Home_Sales_Price"
    t.string   "Realtor_Name_if_any"
    t.string   "Spec_Under_Contract_or_Model"
    t.string   "Contracted_Buyer"
    t.datetime "Anticipated_Closing_Date"
    t.datetime "Construction_Start_Date"
    t.datetime "Construction_Completion"
    t.string   "Color_Scheme_N"
    t.text     "Color_Scheme_Description",                          :limit => 16777215
    t.string   "Elevation_Name"
    t.datetime "ARC_Submittal_Date"
    t.datetime "ARC_Approval_Date"
    t.datetime "Plot_Plan_Submitted_to_ARC_Date"
    t.datetime "Plot_Plan_Approved_Date_by_ARC"
    t.datetime "URA_Submittal_for_Home_Model_Date"
    t.datetime "URA_Approval_for_Home_Model_Date"
    t.string   "Geotech_Lot_Condition_1_4"
    t.string   "Geotech_Lot_Requirements"
    t.string   "Deed_Restrictions"
    t.binary   "Street_Frontage_Complete",                          :limit => 1
    t.binary   "Alley_Complete",                                    :limit => 1
    t.binary   "Dry_Utilities_Complete",                            :limit => 1
    t.binary   "Wet_Utilities_Complete",                            :limit => 1
    t.binary   "Mailbox_Keys_Delivered",                            :limit => 1
    t.datetime "Lot_Completed"
    t.datetime "Estm_Lot_Ready_Date"
    t.integer  "Held_by",                                           :limit => 2,        :null => false
    t.string   "SVG_ID"
    t.string   "Open_Side"
    t.binary   "Model_Yes_No",                                      :limit => 1
    t.datetime "Legal_Sale_Date"
    t.datetime "Use_Easement_Benefitted_Recording_Date"
    t.string   "Use_Easement_Benefitted_Recording_N"
    t.string   "Use_Easement_Benefitted_Associated"
    t.datetime "Use_Easement_Burdened_Recording_Date"
    t.string   "Use_Easement_Burdened_Recording_N"
    t.string   "Use_Easement_Burdened_Associated"
    t.binary   "Released_To_Builders_Yes_No",                       :limit => 1
    t.float    "Lot_Note_Amount"
    t.datetime "Date_of_Construction_Financing_Note"
    t.string   "Construction_DOT_Recording_Number"
    t.string   "Construction_Lot_Release"
    t.datetime "Release_Date_for_Construction_DOT"
    t.string   "Release_Recording_Number_for_Construction_DOT"
    t.binary   "Cancelled_Construction_Note_Received",              :limit => 1
    t.string   "Reassignment_of_Collateral_Assignment_Recording_N"
    t.datetime "Date_of_Reassignment"
    t.string   "State_UCC_Termination_N"
    t.string   "County_UCC_Termination_N"
    t.datetime "Hold_Until"
    t.string   "Hold_Notes",                                        :limit => 50
    t.string   "Lot_Note_Holder",                                   :limit => 45
    t.datetime "Lot_Note_Date"
    t.datetime "Lot_Note_Maturity_Date"
    t.string   "Lot_DOT_Recording_Number",                          :limit => 45
    t.datetime "Lot_DOT_Recording_Date"
    t.string   "Lot_Release",                                       :limit => 45
    t.datetime "Release_Date_for_Lot_DOT"
    t.string   "Release_Recording_Number_for_Lot_DOT",              :limit => 100
    t.binary   "Cancelled_Lot_Note_Received",                       :limit => 1
    t.datetime "Maturity_Date_of_Construction_Financing_Note"
    t.datetime "Construction_DOT_Recording_Date"
    t.integer  "Internet_Accessibility"
    t.float    "Park_Fees_Amt_Paid"
    t.float    "Telecom_Fees_Amt_Paid"
    t.float    "Metro_District_Amt_Paid"
    t.float    "Taxes_and_Closing_Fees"
    t.float    "Misc_Adj"
    t.float    "Lot_Price_Less"
    t.float    "Master_Marketing_Fee"
    t.float    "GHN_Pay_off_Taken_at_Closing"
    t.float    "Net_Wired_into_GHN"
  end

  create_table "tbldata1", :primary_key => "ID", :force => true do |t|
    t.string   "Filing"
    t.integer  "Lot_Number"
    t.integer  "Lot_Area"
    t.boolean  "Corner"
    t.boolean  "Green_Street"
    t.float    "Base_Lot_Price"
    t.float    "Adjusted_Lot_List_Price"
    t.float    "Lot_Sales_Price_per_Contract"
    t.integer  "Lot_Builder_ID"
    t.string   "Lot_Notes"
    t.string   "Home_Owner"
    t.datetime "Lot_Sale_Closing_Date"
    t.datetime "Home_Sale_Closing_Date"
    t.boolean  "Lot_Suborindation"
    t.string   "Lot_Subordination_Fee_Computed_As"
    t.float    "Lot_Subordination_Fee"
    t.string   "Construction_Financing_By"
    t.float    "Amount_of_Construction_Financing"
    t.string   "Construction_Financing_Interest_Rate"
    t.datetime "Use_Easement_Recorded"
    t.string   "Home_Model_Name"
    t.integer  "Bedroom_Count"
    t.integer  "Bathroom_Count"
    t.string   "Master"
    t.string   "Stories"
    t.integer  "Non_Basement_Home_Area_SqFt"
    t.integer  "Basement_Home_Area_SqFt"
    t.integer  "Finished_Home_Area_SqFt"
    t.integer  "Total_Home_Area_SqFt"
    t.float    "Latest___Last_Home_List_Price"
    t.float    "Home_Sales_Price"
    t.string   "Realtor_Name_if_any"
    t.string   "Spec_Under_Contract_or_Model"
    t.string   "Contracted_Buyer"
    t.datetime "Anticipated_Closing_Date"
    t.datetime "Construction_Start_Date"
    t.datetime "Construction_Completion"
    t.string   "Color_Scheme_N"
    t.string   "Color_Scheme_Description"
    t.string   "Elevation_Name"
    t.datetime "ARC_Submittal_Date"
    t.datetime "ARC_Approval_Date"
    t.datetime "Plot_Plan_Submitted_to_ARC_Date"
    t.datetime "Plot_Plan_Approved_Date_by_ARC"
    t.datetime "URA_Submittal_for_Home_Model_Date"
    t.datetime "URA_Approval_for_Home_Model_Date"
    t.string   "Geotech_Lot_Condition_1_4"
    t.string   "Geotech_Lot_Requirements"
    t.string   "Deed_Restrictions"
    t.boolean  "Street_Frontage_Complete"
    t.boolean  "Alley_Complete"
    t.boolean  "Dry_Utilities_Complete"
    t.boolean  "Wet_Utilities_Complete"
    t.boolean  "Mailbox_Keys_Delivered"
    t.boolean  "Lot_Completed"
    t.datetime "Estm_Lot_Ready_Date"
    t.integer  "Lot_Sales_Status_ID"
    t.string   "Open_Side"
    t.boolean  "Model_Yes_No"
    t.datetime "Legal_Sale_Date"
    t.datetime "Use_Easement_Benefitted_Recording_Date"
    t.string   "Use_Easement_Benefitted_Recording_N"
    t.string   "Use_Easement_Benefitted_Associated"
    t.datetime "Use_Easement_Burdened_Recording_Date"
    t.string   "Use_Easement_Burdened_Recording_N"
    t.string   "Use_Easement_Burdened_Associated"
    t.boolean  "Released_To_Builders_Yes_No"
    t.float    "Lot_Note_Amount"
    t.datetime "Date_of_Construction_Financing_Note"
    t.string   "Construction_DOT_Recording_Number"
    t.string   "Construction_Lot_Release"
    t.datetime "Release_Date_for_Construction_DOT"
    t.string   "Release_Recording_Number_for_Construction_DOT"
    t.boolean  "Cancelled_Construction_Note_Received"
    t.string   "Reassignment_of_Collateral_Assignment_Recording_N"
    t.datetime "Date_of_Reassignment"
    t.string   "State_UCC_Termination_N"
    t.string   "County_UCC_Termination_N"
    t.datetime "Hold_Until"
    t.string   "Hold_Notes"
    t.string   "Lot_Note_Holder"
    t.datetime "Lot_Note_Date"
    t.datetime "Lot_Note_Maturity_Date"
    t.string   "Lot_DOT_Recording_Number"
    t.datetime "Lot_DOT_Recording_Date"
    t.string   "Lot_Release"
    t.datetime "Release_Date_for_Lot_DOT"
    t.string   "Release_Recording_Number_for_Lot_DOT"
    t.boolean  "Cancelled_Lot_Note_Received"
    t.datetime "Maturity_Date_of_Construction_Financing_Note"
    t.datetime "Construction_DOT_Recording_Date"
    t.integer  "Internet_Accessibility"
    t.float    "Park_Fees_Amt_Paid"
    t.float    "Telecom_Fees_Amt_Paid"
    t.float    "Metro_District_Amt_Paid"
    t.float    "Taxes_and_Closing_Fees"
    t.float    "Misc_Adj"
    t.float    "Lot_Price_Less"
    t.float    "Master_Marketing_Fee"
    t.float    "GHN_Pay_off_Taken_at_Closing"
    t.float    "Net_Wired_into_GHN"
    t.string   "Lot_Address"
    t.string   "Lot_City"
    t.string   "Lot_State"
    t.integer  "Lot_Zip_Code"
    t.string   "Lot_Country"
    t.integer  "Block"
    t.integer  "Building"
    t.integer  "Floor"
    t.string   "Conditions"
    t.datetime "Permit_Applied_Date"
    t.integer  "Permit_Number"
    t.datetime "Permit_Received_Date"
    t.string   "Lot_Accounting_Code"
    t.float    "Lot_Premium"
    t.string   "Plan_No"
    t.string   "Plan_Name"
    t.string   "Plan_Description"
    t.integer  "Plan_SQFT"
    t.integer  "Required_Color_ID"
    t.string   "Required_Elevation"
    t.integer  "Plan_Status_ID"
    t.float    "Retail_Price"
    t.integer  "Construction_Status_ID"
    t.datetime "Construction_Schedule_Status"
    t.datetime "Current_Finish_Date"
    t.datetime "Original_Finish_Date"
    t.string   "Last_Completed_Phase"
    t.string   "Last_Started_Phase"
    t.string   "Last_Completed_Task"
    t.string   "Last_Started_Task"
    t.integer  "Total_Days_Calendar"
    t.integer  "Total_Days_Working"
    t.integer  "Total_Days_on_Hold"
    t.integer  "Total_Days_on_Hold_Calender"
    t.integer  "Total_Schedule_Difference"
    t.integer  "Total_Schedule_Variance"
    t.integer  "Total_Variance_Calendar"
    t.integer  "Sales_Status_ID"
    t.string   "Status_Sales_Agreement"
    t.datetime "Create_Date"
    t.datetime "Submitted_Date"
    t.datetime "Finalize_Date"
    t.datetime "Projected_Finish_Date"
    t.datetime "Settlement_Date_Home"
    t.string   "Settlement_Type_Home"
    t.string   "Contingency"
    t.datetime "Contingency_Expire_Date"
    t.datetime "Contract_Accepted_Date"
    t.float    "Base_Price"
    t.float    "Total_Price"
    t.float    "Differential"
    t.float    "Incentive_Total"
    t.float    "Change_Order_Total"
    t.float    "Total_Price_Less_Differential"
    t.string   "Parcel_Number"
    t.string   "Ownership"
    t.integer  "Lot_Width_ID"
    t.integer  "Lot_Depth"
    t.string   "Constraints_Variances"
    t.float    "Premiums"
    t.integer  "Land_Development_Status_ID"
    t.datetime "Schedule"
    t.datetime "Takedown"
    t.float    "Sale_Price"
    t.datetime "Settlement_Date_Land"
    t.string   "Notes"
    t.integer  "Lot_Sales_Status_Builder_ID"
    t.integer  "Construction_Sales_Status_ID"
    t.datetime "created_at",                                        :null => false
    t.datetime "updated_at",                                        :null => false
    t.integer  "Home_Status_ID"
    t.datetime "Construction_Sales_Date"
    t.string   "Held_by"
    t.string   "Plan_Same"
    t.string   "Required_Color_Same"
    t.string   "Land_Development_Status"
    t.string   "Lot_Width_Same"
    t.string   "Sales_Status_Same"
    t.string   "Construction_Status_Same"
    t.string   "Lot_Builder_Same"
    t.string   "Lot_Sales_Status_Same"
    t.integer  "Phase"
    t.string   "SVG_ID"
  end

  create_table "tbldataattachments", :force => true do |t|
    t.integer  "imageable_id"
    t.string   "imageable_type"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "dimensions"
  end

  create_table "tbldatafrmfields", :primary_key => "ID", :force => true do |t|
    t.integer "Group_id"
    t.string  "Menu_Option_Name", :limit => 50
    t.integer "Access_id"
    t.string  "Level"
  end

  create_table "tblfilings", :primary_key => "ID", :force => true do |t|
    t.string "Filing", :limit => 50
  end

  create_table "tblusers", :primary_key => "ID", :force => true do |t|
    t.string  "Company"
    t.string  "Last_Name"
    t.string  "First_Name"
    t.string  "Email_Address"
    t.string  "Job_Title"
    t.string  "Business_Phone"
    t.string  "Home_Phone"
    t.string  "Mobile_Phone"
    t.string  "Fax_Number"
    t.text    "Address",         :limit => 16777215
    t.string  "City"
    t.string  "State"
    t.string  "ZIP_Code"
    t.string  "Country"
    t.text    "Web_Page",        :limit => 16777215
    t.text    "Notes",           :limit => 16777215
    t.text    "Attachments",     :limit => 16777215
    t.integer "Access_Group"
    t.string  "Password",        :limit => 20
    t.string  "ConfirmPassword"
    t.string  "Is_Active",                           :default => "0"
    t.string  "Is_Confirmed",                        :default => "0"
    t.string  "token"
  end

  create_table "uploadsvgs", :force => true do |t|
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

end
