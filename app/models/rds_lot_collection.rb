class RdsLotCollection< ActiveRecord::Base
  attr_protected
  self.table_name = "Rds_Lot_Collections"
  belongs_to :role_definition, :foreign_key => "Role_Definition_Id"
  belongs_to :lot_collection, :foreign_key => "Lot_Collection_Id"
  has_many :rds_lot_collections_menu_options, :dependent => :destroy, :foreign_key => "Rds_Lot_Collection_Id"  
  has_many :menu_options, :through => :rds_lot_collections_menu_options, :foreign_key => "Menu_Option_Id"
  # has_many :accesses, :through => :role_definition_rules, :foreign_key => "Role_Access_Id"
  has_many :rds_lot_collections_report_role_accesses_for_geneated_reps, :class_name =>"RdsLotCollectionsReportRoleAccess", :primary_key=>[:Role_Definition_Id,:Lot_Collection_Id],:foreign_key => [:Role_Id, :Lot_Collection_Id], :conditions => ["Role_Access_Id IN (1,2) AND Report_Type='Generated' "]  
  has_many :generated_reps_edit_access, :class_name =>"RdsLotCollectionsReportRoleAccess", :primary_key=>[:Role_Definition_Id,:Lot_Collection_Id],:foreign_key => [:Role_Id, :Lot_Collection_Id], :conditions => ["Role_Access_Id IN (1) AND Report_Type='Generated' "]  

def self.get_access_for_upload_report(role_id, lot_collection_id)
	upload_access_rec= self.find_by_Role_Definition_Id_and_Lot_Collection_Id(role_id, lot_collection_id)
	upload_access_rec.Can_Upload_Report unless upload_access_rec.blank?
end


end
	