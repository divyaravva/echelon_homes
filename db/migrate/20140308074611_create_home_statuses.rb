class CreateHomeStatuses < ActiveRecord::Migration
  def change
    create_table :home_statuses ,:id=>false do |t|
      t.primary_key :ID
      t.string :Description
      t.string :Color

      t.timestamps
    end
  end
end
