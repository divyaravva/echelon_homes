module ReportsHelper
	def generate_excel_report(available_lots, worksheet_name)				
		if worksheet_name =="Sales Status"
			report_columns =[
				"Community",
				"Status",
				"House Number",
				"Block #",
				"Lot #",				
				"Name",
				"Model",
				"Elevation",
				"Sales Amount",
				"A/S Date",
				"Ratified Date",
				"Settlement Date",
				"Mortgage Company",
				];
			report_data_columns=[
				"Community",
				"Lot_Status_Val",
				"Unique_ID",
				"Filing",
				"Lot_Number",
				"Buyer_Name",				
				"Model_Code",				
				"Elevation",
				"total_sales_price",
				"Contract_Date",
				"Ratified_Date",
				"Settlement_Date",
				"Mortgage_Lender"
				]
		end

		p=Axlsx::Package.new
	    wb=p.workbook      
	    unless available_lots.empty?  
	      available_lots.each_slice(5000).with_index do |sheet_arr, ind|
	        wb.add_worksheet(:name => worksheet_name+"_"+(ind+1).to_s) do |sheet|
	        bold_style = sheet.styles.add_style(:b => true)
	        header_style = sheet.styles.add_style(:b => true, :alignment => {:horizontal => :left, :vertical => :center, :sz =>45})
	        summary_style = sheet.styles.add_style(:b => true, :alignment => {:horizontal => :left, :vertical => :center, :sz =>25})
	        summing_style = summary_style = sheet.styles.add_style(:b => true)
          currency_format = sheet.styles.add_style :format_code => "[$$-409]#,##0;-[$$-409]#,##0"	        
          date_format = sheet.styles.add_style :format_code => 'MM/DD/YYYY'
	        header_row = sheet.styles.add_style(:bg_color => "D8BFD8", :fg_color => "000000",:border =>  Axlsx::STYLE_THIN_BORDER, :b => true, :alignment => {:horizontal => :left, :vertical => :center})
	        odd_row = sheet.styles.add_style(:bg_color => "F0E68C", :fg_color => "000000",:border =>  Axlsx::STYLE_THIN_BORDER, :alignment => {:horizontal => :left})
	        even_row = sheet.styles.add_style(:bg_color => "DCDCDC", :fg_color => "000000",:border =>  Axlsx::STYLE_THIN_BORDER, :alignment => {:horizontal => :left})
	        left_justified = sheet.styles.add_style(:alignment => {:horizontal => :left})
	        sheet.add_row [worksheet_name], :style => header_style
            sheet.add_row ["Total Records: "+available_lots.count.to_s]	              
            sheet.add_row [DateTime.now.strftime("%A, %B %d, %Y")]
            sheet.merge_cells "A1:F1"
            sheet.merge_cells "A2:F2"
            sheet.add_row	                      
            sheet.add_row report_columns, :style => bold_style
            sheet_arr.each_with_index do |sheet_row, sheet_arr_index|
                sheet.add_row report_data_columns.map{|lot| sheet_row[lot]}, :style => [nil,nil,nil,nil,nil,nil,nil,nil,currency_format,date_format,date_format,date_format,date_format,nil]
            end
              sheet.add_row	 	              
              sheet.add_row ["Total Records: "+available_lots.count.to_s]	              
              sheet.add_row [DateTime.now.strftime("%A, %B %d, %Y")]
              sheet.merge_cells "A1:F1"
              sheet.merge_cells "A2:F2"
          end          
	      end        
	    else
	       wb.add_worksheet(:name => "worksheet_name") do |sheet|    
	        sheet.add_row ["No records found"], :style=> sheet.styles.add_style(:border =>  Axlsx::STYLE_THIN_BORDER), :widths => :auto        
	       end
	    end
	    p.use_shared_strings = true
	    send_data p.to_stream().string, :type=>"application/excel", :disposition=>'attachment', :filename => worksheet_name+".xlsx"
	end
end
