class CreateSalesStatuses < ActiveRecord::Migration
  def change
    create_table :sales_statuses ,:id=>false do |t|
      t.primary_key :ID
      t.string :Sales_Status
      t.string :Color
      t.string :Sold_Status

      t.timestamps
    end
  end
end
