class ProductType < ActiveRecord::Base	
	self.table_name="Project_Type"
	has_many :product_type_menu_options, :foreign_key => "ProjectId"
	has_many :menu_options , :through => :product_type_menu_options
	
	def self.get_product
		Rails.cache.fetch("echelon_Project_Type") do
		 self.find_by_sql("select * from Project_Type");		
		end
	end	

 	def self.get_ids_of_product	
		self.all.map{|a|a.Id if a.Project_Name==@selected_product}.compact.map(&:inspect).join(',')
 	end

 	def self.product_status(array_of_hashes)
 		array_of_hashes.each do |hashes|
 			product = ProductType.find(hashes["id"])
 			 if hashes["active"].present?
 			 	product.update_attribute(:Active, 1)
 			 else
 			 	product.update_attribute(:Active, 0)
 			 end
 		end
 	end
 	def self.fetch_diabled_product
       @disabled_product = self.find_by_sql("select * from Project_Type where Active !=1").map{|g| g.Project_Name};
 	end
 	def self.get_all_products
 		self.find_by_sql("select * from Project_Type").map{|g| g.Project_Name}
 	end
end