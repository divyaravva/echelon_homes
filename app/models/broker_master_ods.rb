class BrokerMasterOds < ActiveRecord::Base
	attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_BROKERMASTER"

  def self.load_ods_broker_master(windows_db_nm)

  	truncate_ods_broker_master

    puts "********** 1A) Started Loading BrokerMasterOds Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from "+windows_db_nm+".dbo.BROKERMASTER");
      	puts "............................................."
      	puts "............................................."

        batch=[]
        batch_size = 1000

        result.each do |row|
          batch << BrokerMasterOds.new(:BROKERCODE => row['BROKERCODE'],
                                      :BROKERNAME => row['BROKERNAME'],
                                      :ADDRESS1 => row['ADDRESS1'],
                                      :CITY => row['CITY'],
                                      :STATE => row['STATE'],
                                      :DISTHOUSE => row['DISTHOUSE'],
                                      :PHONENUMBER => row['PHONENUMBER'],
                                      :EXTENSION => row['EXTENSION'],
                                      :FAXNUMBER => row['FAXNUMBER'],
                                      :WEBSITE => row['WEBSITE'],
                                      :EMAILADDRESS => row['EMAILADDRESS'],
                                      :FIELD1 => row['FIELD1'],
                                      :FIELD2 => row['FIELD2'],
                                      :FIELD3 => row['FIELD3'],
                                      :FIELD4 => row['FIELD4'],
                                      :FIELD5 => row['FIELD5'],
                                      :FIELD6 => row['FIELD6']
								)
            if batch.size >= batch_size 
              BrokerMasterOds.import batch
              batch = [] 
            end
        end
        BrokerMasterOds.import batch
    end

    puts "Total no. of Loaded rows.........#{BrokerMasterOds.count}"
    puts "********** 1B) Finished Loading BrokerMasterOds Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_broker_master
		ActiveRecord::Base.connection.execute("TRUNCATE ODS_BROKERMASTER")
	end

 end