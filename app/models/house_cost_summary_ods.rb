class HouseCostSummaryOds < ActiveRecord::Base
	attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_HOUSECOSTSUMMARY"
  self.primary_keys = :CATEGORYCODE, :COSTCODE
  has_one :category_cost_code, :foreign_key => [:CATEGORYCODE, :COSTCODE]

  def self.load_ods_house_cost_summary(dev_codes,windows_db_nm)

  	truncate_ods_house_cost_summary

    puts "********** 1A) Started Loading HouseCostSummaryOds Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
      #message = "Inside the Lots info Load Method"
      #Mailer.notify_user_on_client_failure(message).deliver
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from "+windows_db_nm+".dbo.HOUSECOSTSUMMARY where DEVELOPMENTCODE in ("+dev_codes+") and HOUSENUMBER != '00000000'");
      	puts "............................................."
      	puts "............................................."

        batch=[]
        batch_size = 1000

        result.each do |row|
          batch << HouseCostSummaryOds.new(:COMPANYCODE => row['COMPANYCODE'],
                                              :DEVELOPMENTCODE => row['DEVELOPMENTCODE'],
                                              :HOUSENUMBER => row['HOUSENUMBER'],
                                              :CATEGORYCODE => row['CATEGORYCODE'],
                                              :COSTCODE => row['COSTCODE'],
                                              :BUDGETAMOUNT => row['BUDGETAMOUNT'],
                                              :ACTUAL => row['ACTUAL'],
                                              :ORIGINALBUDGET => row['ORIGINALBUDGET']
								                    )
            
            if batch.size >= batch_size
              HouseCostSummaryOds.import batch
              batch = [] 
            end
        end
        HouseCostSummaryOds.import batch
    end

    puts "Total no. of Loaded rows.........#{HouseCostSummaryOds.count}"
    puts "********** 1B) Finished Loading HouseCostSummaryOds Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_house_cost_summary
		ActiveRecord::Base.connection.execute("TRUNCATE ODS_HOUSECOSTSUMMARY")
	end


end