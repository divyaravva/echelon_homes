class SchedhouseDetailOds < ActiveRecord::Base
	attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_SCHEDHOUSEDETAIL"
  belongs_to :option_lot_master, :class_name => "OptionLotMasterOds"
  belongs_to :schedule_activity, :class_name => "SchedActivitiesOds",:foreign_key => "ACTIVITYCODE"
  self.primary_keys = :DEVELOPMENTCODE,:HOUSENUMBER

  def self.load_ods_schedhouse_detail(dev_codes,windows_db_nm)

  	truncate_ods_schedhouse_detail

    puts "********** 1A) Started Loading SchedhouseDetailOds Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from "+windows_db_nm+".dbo.SCHEDHOUSEDETAIL where DEVELOPMENTCODE in ("+dev_codes+")  and HOUSENUMBER != '00000000'");
      	puts "............................................."
      	puts "............................................."

        batch=[]
        batch_size = 1000

        result.each do |row|
          batch << SchedhouseDetailOds.new(:COMPANYCODE => row['COMPANYCODE'],
                                          :DEVELOPMENTCODE => row['DEVELOPMENTCODE'],
                                          :HOUSENUMBER => row['HOUSENUMBER'],
                                          :STEPNUMBER => row['STEPNUMBER'],
                                          :ACTIVITYCODE => row['ACTIVITYCODE'],
                                          :LATESTARTDATE => row['LATESTARTDATE'],
                                          :LATEFINISHDATE => row['LATEFINISHDATE'],
                                          :ACTUALSTARTDATE => row['ACTUALSTARTDATE'],
                                          :ACTUALFINISHDATE => row['ACTUALFINISHDATE']
								                            )
            
            if batch.size >= batch_size
              SchedhouseDetailOds.import batch
              batch = [] 
            end
        end
        SchedhouseDetailOds.import batch
    end

    puts "Total no. of Loaded rows.........#{SchedhouseDetailOds.count}"
    puts "********** 1B) Finished Loading SchedhouseDetailOds Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_schedhouse_detail
		ActiveRecord::Base.connection.execute("TRUNCATE ODS_SCHEDHOUSEDETAIL")
	end


end