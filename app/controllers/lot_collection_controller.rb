class LotCollectionController < ApplicationController
  
  before_filter :authenticate
  before_filter :is_admin
  before_filter :get_product
  before_filter :get_product_view 
  before_filter :get_comm_city_view
  before_filter :get_community_city      
  before_filter :get_communities   
  before_filter :get_communities_view
  before_filter :get_parcels
  before_filter :get_parcels_view

  def index
    @lot_collections=LotCollection.order("collection_name").all
    @role = params[:role]
    @admin_id = RoleDefinition.find_by_Role_Definition("Admin")
    if params[:role].present?
        @role_name = RoleDefinition.find(params[:role]).Role_Definition
        @collection_for_role = RoleDefinition.get_rules.find(params[:role]).lot_collections.sort_by{|g| g.collection_name}
    end
     @total_lots = LotCollection.total_lots_in_all_collec
  end

   def new
    @builder_count = Builder.count
    @lot_collections = LotCollection.all
   end

   def parcels_of_community
   	@community = params[:Community]
    @parcels = Tbldatum.get_community_parcels(params[:Community]).map{|g| [g.Phase_Name]}
    render :partial => "parcels_of_community"
   end

   def total_communities
    @total_communities = LotCollectionLot.find_by_sql("select distinct(Community) from lot_collection_lots").map{|c| c.Community}
    render :partial => "communities_list"
   end

  def lot_numbers_of_house_type         
    @house_type = params[:House_Type]    
    @lot_numbers = Tbldatum.get_community_lot_numbers(params[:House_Type]).map{|g| [g.Unique_ID]}    
    render :partial => "lot_numbers_of_community"
  end

  def lots_of_parcel
   	@community = params[:Community]
   	@parcel = params[:Parcel]   	
    @lots = LotCollection.find_lots_for_create_collection(params[:Community],params[:Parcel]).map{|g| g.Lot_Number}
    render :partial => "lots_of_parcel"
  end

  def lots_of_community 
    @lots = LotCollection.find_lots_for_collection_creation(params[:community]).map{|g| [g.Unique_ID, g.Status]}
    @lot_collections = LotCollection.all     
    render :partial => "lots_of_community"
  end  
   
  def create  
    lot_collection=LotCollection.create(:collection_name => params[:name], 
                                        :description => params[:description])    
    selected_lots = JSON.parse params["selected_lots"]
    selected_lots.each do |lot|
      lot_info = Tbldatum.find(lot["lot_number"])
      lot_collection.lot_collection_lots.create(:Lot_Number => lot["lot_number"],
        :Community => lot_info.Community, :Phase => lot_info.Phase_Name)  
    end         
    Tbldatum.invalidate_cache
   redirect_to "/lot_collection" 
   end   

   def edit    
    @lot_collection = LotCollection.get_associated_object.find(params[:id]) unless params[:id].nil?
    @lot_collection_name = @lot_collection.collection_name unless @lot_collection.nil?
    @lot_collection_desc = @lot_collection.description unless @lot_collection.nil?
    first_lot_collection_lot = @lot_collection.lot_collection_lots.first  unless @lot_collection.nil? || @lot_collection.lot_collection_lots.empty?
    @community = @lot_collection.lot_collection_lots.first.Community unless @lot_collection.nil? || @lot_collection.lot_collection_lots.empty?
    @edit_flag = 1
    @selected_lots=@lot_collection.lots.map{|g| [g.Unique_ID, g.Status]} unless @lot_collection.nil? || @lot_collection.lots.empty?
    @assigned_lots_count = @selected_lots.map{|lot| lot[1] unless lot[1]=="Available"}.compact.count
    @lots = LotCollection.find_lots_for_collection_creation(@community).map{|g| [g.Unique_ID, g.Status]} 
    @lot_collections = LotCollection.all.select{|x| x.collection_name!=@lot_collection_name}         
   end

   def update
    lot_collection = LotCollection.find(params[:id]) unless params[:id].nil?
    lot_collection.update_attributes({:collection_name => params[:name],:description => params[:description]}) unless lot_collection.nil?
    community = lot_collection.lot_collection_lots.first.Community unless lot_collection.nil? || lot_collection.lot_collection_lots.empty?
    lot_collection.lot_collection_lots.each do |lcl|      
      lot_info = Tbldatum.find(lcl.Lot_Number)
        lot_collection.lot_collection_lots.create(:Lot_Number => lcl.Lot_Number, :Community => lcl.Community)
        lcl.destroy
    end    
    lot_collection.floor_plan_ids = params[:floor_plan_ids][0]['plan_ids'] unless params[:floor_plan_ids].nil?
    selected_lots = JSON.parse params["selected_lots"]
    selected_lots.each do |lot|
      lot_info = Tbldatum.find(lot["lot_number"])      
      lot_info.update_attributes(:Status => "Available")      
      lot_collection.lot_collection_lots.create(:Lot_Number => lot["lot_number"], 
            :Community => lot_info.Community, :Phase =>  lot_info.Phase_Name, :floor_type_id =>params[:builder_floor_types])  
      default_lot_collection.lot_collection_lots.find_by_Lot_Number(lot["lot_number"]).delete
    end
    Tbldatum.invalidate_cache
    if params[:view].blank? 
    redirect_to "/lot_collection"
    else
      redirect_to "/lot_collection/#{lot_collection.id}"
    end    
   end

   def show
    @lot_collection = LotCollection.find(params[:id]) unless params[:id].nil?
    first_lot_collection_lot = @lot_collection.lot_collection_lots.first  unless @lot_collection.nil? || @lot_collection.lot_collection_lots.empty?
    @community = first_lot_collection_lot.Community unless first_lot_collection_lot.nil?
    @phase = first_lot_collection_lot.Phase unless first_lot_collection_lot.nil?
    @selected_lots=@lot_collection.lot_collection_lots.map{|g| g.Lot_Number}
   end

   def destroy   
    lot_collection = LotCollection.find(params[:id]) unless params[:id].nil?
    lot_collection.destroy unless lot_collection.nil?
    Tbldatum.invalidate_cache
    @lot_collections=LotCollection.order("collection_name").all
    @role = params[:role]
    @admin_id = RoleDefinition.find_by_Role_Definition("Admin")
    if params[:role].present?
        @role_name = RoleDefinition.find(params[:role]).Role_Definition
        @collection_for_role = RoleDefinition.get_rules.find(params[:role]).lot_collections.sort_by{|g| g.collection_name}
    end
    @builder = params[:builder]
      if @builder.present?
        @builder_id = Builder.find_by_Builder_Name(@builder)
        @collection_for_builder = @builder_id.lot_collections
     end
    render :partial => "lot_collections_list"
   end

  def lot_status
       lot_collection_details = LotCollection.find(params[:id]) unless params[:id].nil?  
        if params[:status] == "archive"  
          lot_collection_details.update_attributes({:Is_Active => 0}) unless lot_collection_details.nil?
        else
          lot_collection_details.update_attributes({:Is_Active => 1}) unless lot_collection_details.nil?
        end
        Tbldatum.invalidate_cache
    @lot_collections=LotCollection.order("collection_name").all
      @role = params[:role]
      @admin_id = RoleDefinition.find_by_Role_Definition("Admin")
      if params[:role].present?
          @role_name = RoleDefinition.find(params[:role]).Role_Definition
          @collection_for_role = RoleDefinition.get_rules.find(params[:role]).lot_collections.sort_by{|g| g.collection_name}
      end
      @builder = params[:builder]
        if @builder.present?
          @builder_id = Builder.find_by_Builder_Name(@builder)
          @collection_for_builder = @builder_id.lot_collections
       end
        render :partial => "lot_collections_list"
  end

end
