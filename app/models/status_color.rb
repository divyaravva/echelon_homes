class StatusColor < ActiveRecord::Base
attr_protected
self.table_name = "GIS_Status_Details"  
self.primary_key= "ID"

	def self.lot_status_details
	find_by_sql("SELECT StatusKey,ColorCode,ID FROM GIS_Status_Details WHERE ColorCode !='' and id IN (SELECT MIN(id)
	               FROM GIS_Status_Details GROUP by StatusKey) GROUP BY StatusKey")
	end
end
