class Tbldatum < ActiveRecord::Base
  include ActionView::Helpers::NumberHelper
  require 'csv'
  attr_protected
  self.table_name="GIS_LotsInfo"
  self.primary_key = "Unique_ID"
  attr_accessor :permission, :held_by_value, :lot_builder_value ,:geo_tech_value
  
  has_many :gallery_images_to_lots, :dependent => :destroy
  has_many :tbldataattachments, :as => :imageable, :dependent => :destroy
  has_many :gallery_images_to_lots, :dependent => :destroy
  has_many :image_galleries, :through => :gallery_images_to_lots
  has_one :lot_collection_lot, :dependent => :destroy, :foreign_key => "Lot_Number"
  has_one :lot_collection, :through => :lot_collection_lot  
  has_one :status_detail, :foreign_key => "id" ,:primary_key=>"Lot_Buzz_Status", :conditions => ["LegendType IN (?)", ["Sales Status"]]  
  has_one :status_details_acc_view, :class_name => "StatusDetail",  :foreign_key => "id" ,:primary_key=>"Lot_Tracking", :conditions => ["LegendType IN (?)", ["Lot Ownership"]]  
  has_one :status_details_lot_sts_view, :class_name => "StatusDetail",  :foreign_key => "id" ,:primary_key=>"Lot_Status", :conditions => ["LegendType IN (?)", ["Lot Status"]]  
  has_one :status_details_warranty_view, :class_name => "StatusDetail",  :foreign_key => "id" ,:primary_key=>"Outstanding_Service_Orders", :conditions => ["LegendType IN (?)", ["Outstanding Service Orders"]]  
  has_one :status_details_cons_view, :class_name => "StatusDetail",  :foreign_key => "id" ,:primary_key=>"Construction_Schedule_Status", :conditions => ["LegendType IN (?)", ["Schedule Status"]]  
  has_one :status_details_cons_star_view, :class_name => "StatusDetail",  :foreign_key => "id" ,:primary_key=>"Construction_VPO_Status", :conditions => ["LegendType IN (?)", ["VPO Status"]]  
  has_one :status_details_sales_view, :class_name => "StatusDetail",  :foreign_key => "id" ,:primary_key=>"Lot_Status", :conditions => ["LegendType IN (?)", ["Lot Status"]]  
  has_one :status_details_sales_cir_view, :class_name => "StatusDetail",  :foreign_key => "id" ,:primary_key=>"Model_Elevation_Id", :conditions => ["LegendType IN (?)", ["Model"]]  
  has_one :status_details_sales_lot_num_view, :class_name => "StatusDetail",  :foreign_key => "id" ,:primary_key=>"Lot_Premium", :conditions => ["LegendType IN (?)", ["Lot Premium"]]  
  has_one :status_details_plan_view, :class_name => "StatusDetail",  :foreign_key => "id" ,:primary_key=>"Floor_Plan_Id", :conditions => ["LegendType IN (?)", ["Floor Plan"]]  
  belongs_to :builder
  scope :untied, ->{where("SVG_ID IS NULL")}
  scope :tied, ->{where("SVG_ID IS NOT NULL")}
  # scope :ownerhship, lambda { |opt| where("Lot_Tracking_Val =? and Filing !=? and SVG_ID !=?", opt, '', '') }
  
  scope :ownerhship, lambda { |opt|       
    where("Lot_Tracking_Val =? and Development_Code IN(?) and Filing !=? and SVG_ID !=?", opt, self.get_authorized_communities_for_logged_in_user,'', '') }
  
  def self.get_owned_lots_per_community
    Tbldatum.ownerhship("Lot Owned").group_by(&:Community_Description)
  end

  def self.get_optioned_lots_per_community
    Tbldatum.ownerhship("Lot Optioned").group_by(&:Community_Description)
  end

  def  self.get_lots_per_community_and_lender
    authorized_comminities = self.get_authorized_communities_for_logged_in_user
    Tbldatum.where("Development_Code IN(?) and Lot_Lender!=?", authorized_comminities, '', ).group_by(&:Community_Description)
  end

  def tied?
      self.SVG_ID 
  end
  scope :lots_approaching_completion_date,:conditions => ["Status=? AND Completion_Date<>? OR Completion_Date<>?", "Firm","",nil]
  belongs_to :referral, :foreign_key => "Referral_By"
  
  belongs_to :sales_representative,:class_name => "SalesRep", :foreign_key => "sales_agent"
  belongs_to :floor_plan, :foreign_key => "Plan_Id"
  # before_save {|lot| lot.updated_at_timestamp = DateTime.now.to_i }

  def set_attrs(params)    
    attrs = {}
    all_params=get_attrs()
    all_params.each do |k, v|
      if params.has_key?(v)
        attrs.merge!({k => params[v]})
      end
    end
    self.attributes=attrs    
  end

  def get_attrs
    attrs = {
        :SVG_ID => :test,
        :Lot_Buzz_Status => :lbuzz_status,
        :Estimated_Completion_Date => :est_cmp_date,
        :Lot_Size => :lot_size,
        :Description => :description,
        :Lot_Type_Id => :lot_type,
        :Sale_Price => :sale_price,
        :Address => :address
        }
  end

  def self.get_distinct_dev_codes
    self.get_authorized_communities_for_logged_in_user
  end

  def self.get_distinct_community_desc
    authorized_comminities = self.get_authorized_communities_for_logged_in_user
    self.where("Development_Code IN(?)", authorized_comminities).
        map{|community_desc| community_desc.Community_Description}.uniq.sort unless authorized_comminities.blank?
  end

  def self.get_distinct_filing
    all_lots_with_filing=[]
    self.all.each do |each_rec|
        unless each_rec.SVG_ID.blank?
        all_recs_hash = {}
           all_recs_hash["dev_code"] = each_rec.Development_Code
           all_recs_hash["filing"] = each_rec.Filing
           all_lots_with_filing << all_recs_hash
        end
      end
    all_lots_with_filing
  end

  def self.get_distinct_housenumbers
    all_lots_with_housenumbers=[]
    self.all.each do |each_rec|
      all_recs_hash = {}
         all_recs_hash["dev_code"] = each_rec.Development_Code
         all_recs_hash["filing"] = each_rec.Filing
         all_recs_hash["housenumber"] = each_rec.HouseNumber
         all_lots_with_housenumbers << all_recs_hash
      end
    all_lots_with_housenumbers
  end

  def self.get_community_parcels(community_name)
    find(:all, :select => 'DISTINCT Phase_Name',:conditions => { :Community => community_name})
  end
  def self.get_parcel_lots(community_name, phase_name)
    find(:all, :select => 'Lot_Number',:conditions => { :Community => community_name, :Phase_Name => phase_name})
  end

  def self.sales_status_for_lotbuzz(community)
    sales_status=StatusDetail.find_by_sql("select * from GIS_Status_Details where LegendType='Sales Status' and Is_Active=1 and Community='#{community}' order by LegendOrder")
    sales_status.map{|x| [x.Status_Display_Name,x.id] }
  end

    def self.get_lot_count(community, menu_option_name,status_key, phase_name, all_lots, legendtype)
      if(legendtype == "Floor Plan" || legendtype == "Model")
        status_key = StatusDetail.find_by_StatusKey_and_LegendType_and_Community(status_key,legendtype, community).id
      else
        status_key = StatusDetail.find_by_StatusKey_and_LegendType(status_key,legendtype).id
      end
      if phase_name.blank?
        if menu_option_name=="Accounting View" and legendtype == "Lot Status"        
          all_lots.select{|x| x.Lot_Status.to_i == status_key and x.Community==community and x.SVG_ID != nil unless x.Lot_Status.blank?}.count
        elsif menu_option_name=="Closing View" and legendtype == "Lot Status"        
          all_lots.select{|x| x.Lot_Status.to_i == status_key and x.Community==community and x.SVG_ID != nil unless x.Lot_Status.blank?}.count
        elsif menu_option_name=="Architecture View"  and legendtype == "Lot Status"
          all_lots.select{|x| x.Lot_Status.to_i == status_key and x.Community==community and x.SVG_ID != nil unless x.Lot_Status.blank?}.count 
        elsif menu_option_name=="Warranty View"  and legendtype == "Outstanding Service Orders"
          all_lots.select{|x| x.Outstanding_Service_Orders.to_i == status_key and x.Community==community and x.SVG_ID != nil unless x.Outstanding_Service_Orders.blank?}.count
        elsif menu_option_name=="Construction View"  and legendtype == "Schedule Status"
          all_lots.select{|x| x.Construction_Schedule_Status.to_i == status_key and x.Community==community and x.SVG_ID != nil unless x.Construction_Schedule_Status.blank?}.count    
        elsif menu_option_name=="Construction View"  and legendtype == "VPO Status"
          all_lots.select{|x| x.Construction_VPO_Status.to_i == status_key and x.Community==community and x.SVG_ID != nil unless x.Construction_VPO_Status.blank?}.count
        elsif menu_option_name=="Construction View" and legendtype == "Lot Status"        
          all_lots.select{|x| x.Lot_Status.to_i == status_key and x.Community==community and x.SVG_ID != nil unless x.Lot_Status.blank?}.count
        elsif menu_option_name=="Sales View"  and legendtype == "Model Type"
          all_lots.select{|x| x.Model_Status.to_i == status_key and x.Community==community and x.SVG_ID != nil unless x.Model_Status.blank?}.count    
        elsif menu_option_name=="Sales View"  and legendtype == "Model"
          all_lots.select{|x| x.Model_Elevation_Id.to_i == status_key and x.Community==community and x.SVG_ID != nil unless x.Model_Elevation_Id.blank?}.count    
        elsif menu_option_name=="Sales View"  and legendtype == "Completed Specs"
          all_lots.select{|x| x.Completed_Specs.to_i == status_key and x.Community==community and x.SVG_ID != nil unless x.Completed_Specs.blank?}.count
        elsif menu_option_name=="Sales View" and legendtype == "Lot Premium"
          all_lots.select{|x| x.Lot_Premium.to_i == status_key and x.Community==community and x.SVG_ID != nil unless x.Lot_Premium.blank?}.count      
        elsif menu_option_name=="Sales View"  and legendtype == "Lot Status"
          all_lots.select{|x| x.Lot_Status.to_i == status_key and x.Community==community and x.SVG_ID != nil unless x.Lot_Status.blank?}.count    
        elsif menu_option_name=="Sales View"  and legendtype == "Lot Ownership"
          all_lots.select{|x| x.Lot_Tracking.to_i == status_key and x.Community==community and x.SVG_ID != nil unless x.Lot_Tracking.blank?}.count    
        elsif (menu_option_name=="Sales Status"  || menu_option_name=="Floor Plans View" ) and legendtype == "Sales Status"
          all_lots.select{|x| x.Lot_Buzz_Status.to_i == status_key and x.Community==community and x.SVG_ID != nil unless x.Lot_Buzz_Status.blank?}.count
         elsif (menu_option_name=="Sales Status"  || menu_option_name=="Floor Plans View" ) and legendtype == "Floor Plan" 
          all_lots.select{|x| x if (( x.Floor_Plan_Id.split(",").include?(status_key.to_s)) and x.Community==community and x.SVG_ID != nil unless x.Floor_Plan_Id.blank?)}.count   
        end        
      else
        if menu_option_name=="Accounting View" and legendtype == "Lot Status"        
          all_lots.select{|x| x.Lot_Status.to_i == status_key and x.Phase_Name == phase_name and x.Community==community and x.SVG_ID != nil unless x.Lot_Status.blank?}.count
        elsif menu_option_name=="Closing View" and legendtype == "Lot Status"        
          all_lots.select{|x| x.Lot_Status.to_i == status_key and x.Phase_Name == phase_name and x.Community==community and x.SVG_ID != nil unless x.Lot_Status.blank?}.count
        elsif menu_option_name=="Architecture View"  and legendtype == "Lot Status"
          all_lots.select{|x| x.Lot_Status.to_i == status_key and x.Phase_Name == phase_name and x.Community==community and x.SVG_ID != nil unless x.Lot_Status.blank?}.count 
        elsif menu_option_name=="Warranty View"  and legendtype == "Outstanding Service Orders"
          all_lots.select{|x| x.Outstanding_Service_Orders.to_i == status_key and x.Phase_Name == phase_name and x.Community==community and x.SVG_ID != nil unless x.Outstanding_Service_Orders.blank?}.count
        elsif menu_option_name=="Construction View"  and legendtype == "Schedule Status"
          all_lots.select{|x| x.Construction_Schedule_Status.to_i == status_key and x.Phase_Name == phase_name and x.Community==community and x.SVG_ID != nil unless x.Construction_Schedule_Status.blank?}.count    
        elsif menu_option_name=="Construction View" and legendtype == "Lot Status"        
          all_lots.select{|x| x.Lot_Status.to_i == status_key and x.Community==community and x.SVG_ID != nil unless x.Lot_Status.blank?}.count
        elsif menu_option_name=="Construction View"  and legendtype == "VPO Status"
          all_lots.select{|x| x.Construction_VPO_Status.to_i == status_key and x.Phase_Name == phase_name and x.Community==community and x.SVG_ID != nil unless x.Construction_VPO_Status.blank?}.count
        elsif menu_option_name=="Sales View"  and legendtype == "Model Type"
          all_lots.select{|x| x.Model_Status.to_i == status_key and x.Phase_Name == phase_name and x.Community==community and x.SVG_ID != nil unless x.Model_Status.blank?}.count    
        elsif menu_option_name=="Sales View"  and legendtype == "Model"
          all_lots.select{|x| x.Model_Elevation_Id.to_i == status_key and x.Phase_Name == phase_name and x.Community==community and x.SVG_ID != nil unless x.Model_Elevation_Id.blank?}.count    
        elsif menu_option_name=="Sales View"  and legendtype == "Completed Specs"
          all_lots.select{|x| x.Completed_Specs.to_i == status_key and x.Phase_Name == phase_name and x.Community==community and x.SVG_ID != nil unless x.Completed_Specs.blank?}.count
        elsif menu_option_name=="Sales View" and legendtype == "Lot Premium"
          all_lots.select{|x| x.Lot_Premium.to_i == status_key and x.Phase_Name == phase_name and x.Community==community and x.SVG_ID != nil unless x.Lot_Premium.blank?}.count      
        elsif menu_option_name=="Sales View"  and legendtype == "Lot Status"
          all_lots.select{|x| x.Lot_Status.to_i == status_key and x.Phase_Name == phase_name and x.Community==community and x.SVG_ID != nil unless x.Lot_Status.blank?}.count    
        elsif menu_option_name=="Sales View"  and legendtype == "Lot Ownership"
          all_lots.select{|x| x.Lot_Tracking.to_i == status_key and x.Phase_Name == phase_name and x.Community==community and x.SVG_ID != nil unless x.Lot_Tracking.blank?}.count    
        elsif (menu_option_name=="Sales Status"  || menu_option_name=="Floor Plans View" ) and legendtype == "Sales Status"
          all_lots.select{|x| x.Lot_Buzz_Status.to_i == status_key and x.Phase_Name == phase_name and x.Community==community and x.SVG_ID != nil unless x.Lot_Buzz_Status.blank?}.count
         elsif (menu_option_name=="Sales Status"  || menu_option_name=="Floor Plans View" ) and legendtype == "Floor Plan" 
          all_lots.select{|x| x if (( x.Floor_Plan_Id.split(",").include?(status_key.to_s)) and x.Phase_Name == phase_name and x.Community==community and x.SVG_ID != nil unless x.Floor_Plan_Id.blank?)}.count   
        end
    end
      
    end

  def self.get_all_communities
      find(:all, :select => 'DISTINCT Community').map{|g| [g.Community]}
  end

  def self.load_into_category_cost_tbl
   book = Spreadsheet.open  "/home/liplw015/Desktop/categoryandcostcodesfor_Dev_Costs_byLot.xls"   
    excel_array = []
    sheet1 = book.worksheet 0
    # excel_cols= sheet1.row(0).map{|col| col.gsub(" ","_").gsub("-","_").strip.to_sym unless col.nil?} unless sheet1.nil?

      sheet1.each 1 do |row|
        db_row=nil
        db_row=CategoryCostCode.find_or_initialize_by_id(row[0])
        attrs={}         
        attrs[:COMPANYCODE] = row[0]
        attrs[:CATEGORYCODE] = row[1]
        attrs[:CATEGORYDESCRIPTION] = row[2]
        attrs[:COSTCODE] = row[3]
        attrs[:DESCRIPTION] = row[4]
        db_row.update_attributes(attrs) unless db_row.nil?
            
    end    
  end

  def self.get_all_svg_ids
    Rails.cache.fetch("echelon_get_all_svg_ids") do
      select(:SVG_ID).map{|x| x.SVG_ID}
    end
  end

  def get_color_code  
    Rails.cache.fetch("echelon_lot_color_#{self.id}") do
      status_detail.ColorCode
    end
  end


  def get_color_code_for_acc_view
      status_details_acc_view.ColorCode
  end

  def get_color_code_for_lot_sts_view
      status_details_lot_sts_view.ColorCode
  end
  def get_color_code_for_warranty_view
      status_details_warranty_view.ColorCode
  end

  def get_color_code_for_cons_view
      status_details_cons_view.ColorCode
  end

  def get_color_code_for_star_cons_view
      status_details_cons_star_view.ColorCode
  end

  def get_color_code_for_sales_view
      status_details_sales_view.ColorCode
  end

  def get_color_code_for_circles_sales_view
      status_details_sales_cir_view.ColorCode
  end

  def get_color_code_for_lot_num_sales_view
      status_details_sales_lot_num_view.ColorCode
  end
  def get_plan_color_code
      status_details_plan_view.ColorCode
  end

  def update_key_image_url gallerty_lot_new, gallerty_lot_old 
    unless gallerty_lot_new.image_gallery_id==gallerty_lot_old
      new_key_image = gallerty_lot_new.image_gallery
      GalleryImagesToLot.find_by_image_gallery_id(gallerty_lot_old).update_attributes({:key_image => 0}) unless gallerty_lot_old.nil?
      gallerty_lot_new.update_attributes({:key_image => 1})
      self.update_attributes({:Key_Image_Url => new_key_image.pics(:thumb_add_images), :Key_Image_Id => new_key_image.id })    
    end
  end

  def delete_key_image_url gallerty_lot_new, gallerty_lot_old   
    if gallerty_lot_new == gallerty_lot_old
      gallerty_lot_new.destroy
    else
      gallerty_lot_old.update_attributes({:key_image => 0}) unless gallerty_lot_old.nil?
      gallerty_lot_new.destroy unless gallerty_lot_new.nil?
    end
    self.update_attributes({:Key_Image_Url => nil, :Key_Image_Id => nil })  
  end

  def self.insert_key_image_url
    Tbldatum.all.each do |each_rec|
      image = Tbldatum.find(each_rec.Unique_ID).image_galleries.where("gallery_images_to_lots.product_type"=>"Lot Insight").where("gallery_images_to_lots.key_image"=>1).first unless Tbldatum.find(each_rec.Unique_ID).image_galleries.blank?
      db_row = Tbldatum.find(each_rec.Unique_ID)
      image_url = image.pics(:thumb_add_images) unless image.blank?
      unless image_url.blank?
        db_row.update_attributes(:Key_Image_Url => image_url, :Key_Image_Id => image.id)  
      else
        db_row.update_attributes(:Key_Image_Url => nil, :Key_Image_Id => nil)
      end
    end
  end

  def find_key_image_of_lot
    self.gallery_images_to_lots.find_by_key_image 1
  end

  def find_gallery_image_of_lot gallerty_lot_id
    self.gallery_images_to_lots.find_by_image_gallery_id gallerty_lot_id
  end

  def find_all_gallery_image_of_lot gallerty_lot_id
    self.gallery_images_to_lots.find_all_by_image_gallery_id gallerty_lot_id
  end

  def self.lot_types
    lot_types=LotType.order("Lot_Type").where(:Is_Active => 1)
    lot_types.map{|x| [x.Lot_Type,x.Lot_Type] }.unshift(["select ",""])
  end

  def self.invalidate_entire_lot_cache lot_id
    Rails.cache.delete("echelon_lot_#{lot_id}")
    Rails.cache.delete("echelon_lot_color_#{lot_id}") 
    Rails.cache.delete("echelon_total_lots")
    Rails.cache.delete("echelon_get_all_active_builders")
  end

  def self.invalidate_lot_cache lot_id
    Rails.cache.delete("echelon_lot_#{lot_id}")  
    Rails.cache.delete("echelon_total_lots")
    Rails.cache.delete("echelon_get_all_active_builders")
  end

  def self.invalidate_cache 
    Rails.cache.clear
  end

  def get_floor_plan
    Rails.cache.fetch("echelon_floor_plan_of_lot_#{self.Unique_ID}_#{self.updated_at_timestamp}") do
      self.floor_plan
    end
  end

  def self.get_objects_as_json(report_type, status, dev_code)
    filtered_lots = []
    if report_type == "Sales Status"
      if (status.blank? == true and dev_code.blank? == true)
        available_lots = self.find_by_sql("select * from GIS_LotsInfo where SVG_ID != ''")
      elsif (status.blank? == false and dev_code.blank? == true)
        available_lots = self.find_by_sql("select * from GIS_LotsInfo where SVG_ID != '' and Lot_Status_Val='"+status+"'")
      elsif (status.blank? == true and dev_code.blank? == false)
         available_lots = self.find_by_sql("select * from GIS_LotsInfo where SVG_ID != '' and Development_Code='"+dev_code+"'")
      else
        available_lots = self.find_by_sql("select * from GIS_LotsInfo where SVG_ID != '' and Development_Code='"+dev_code+"' and Lot_Status_Val='"+status+"'")
      end
        available_lots.each do |lot|
          filtered_lots << lot.as_json
        end    
    end
  end

  def self.get_objects_as_json_for_community_view(report_type, dev_code)  
    filtered_lots = []  
    if dev_code.blank?
      filtered_lots = Tbldatum.fetch_Rp_SPECS_and_MODELS.sort_by{|c| c[:Development_Code]}.map{|lot| lot.generate_report_spec_aging_json}
      filtered_lots = self.get_spec_report_community_view_data(filtered_lots)
    else
      filtered_lots = Tbldatum.fetch_Rp_SPECS_and_MODELS.sort_by{|c| c[:Development_Code]}.select{|x| x.Development_Code == dev_code }.map{|lot| lot.generate_report_spec_aging_json}
      filtered_lots = self.get_spec_report_community_view_data(filtered_lots)
    end
    filtered_lots
  end

  def self.get_objects_as_json_plan_view(report_type, model_name)  
    filtered_lots = []
    if model_name.blank?
      filtered_lots = Tbldatum.fetch_Rp_SPECS_and_MODELS.map{|lot| lot.generate_report_spec_aging_json}
      filtered_lots = self.get_spec_report_plan_view_data(filtered_lots)
    else
      filtered_lots = Tbldatum.fetch_Rp_SPECS_and_MODELS.select{|x| x.Model_Name == model_name}.map{|lot| lot.generate_report_spec_aging_json}
      filtered_lots = self.get_spec_report_plan_view_data(filtered_lots)
    end
    filtered_lots
  end

  def self.validate_ytubeurl(y_url)
    if (y_url =~ /http[s]?:\/\//)
        y_url
    else
      y_url = "http://#{y_url}"
    end
     y_url
  end

# updating release date based on option lots
  def self.update_info_columns
    begin
        ActiveRecord::Base.transaction do        
           option_lot = OptionLotMasterOds.find_by_sql("select * from  ODS_OPTIONLOTMASTER where  COMPANYCODE  = '001' 
      and  ((LOTCONVERDATE = '' or LOTCONVERDATE is null) and 
      ((OPTIONEXPDATE = '' or OPTIONEXPDATE is null) or OPTIONEXPDATE >= CURDATE()) and 
      (RELEASESALESDATE <= CURDATE() and RELEASESALESDATE <> ''))");   
            etl_initial_data=HouseMasterOds.includes()+option_lot
            load_array={}
            cost_code_query = LotBasis.first.cost_code_query
            category_code = LotBasis.first.category_code
            etl_initial_data.each do |etl_rec|
              src_tbl = etl_rec.attributes.keys.include?("LOTCONVERDATE")
              if src_tbl
                unique_id = "#{etl_rec.DEVELOPMENTCODE}-#{etl_rec.LOTNUMBER}"
                house_number = "#{etl_rec.LOTNUMBER}" 
              else
                unique_id = "#{etl_rec.DEVELOPMENTCODE}-#{etl_rec.HOUSENUMBER}"
                house_number =   "#{etl_rec.HOUSENUMBER}" 
              end
              if src_tbl
                load_array={
                  :lot_comments => (etl_rec.LOTCOMMENTS),
                }
              else
                load_array={       
                 # :Lot_Basis => (etl_rec.generate_lot_basis_amount(cost_code_query,category_code) unless etl_rec.blank?),
                 # :Projected_Cost_at_Completion => (etl_rec.generate_projected_cost),
                 # :Construction_VPO_Status_Val => (etl_rec.generate_field_CONSTRUCTION_VPO_STATUS unless etl_rec.nil?),
                 :lot_comments => (etl_rec.COMMENTS),
                } 
              end           
              db_row=Tbldatum.find_or_initialize_by_Unique_ID(unique_id)
              db_row.attributes=load_array 
              db_row.attributes.each do|k,v| 
                if k != 'Unique_ID'
                  if Tbldatum.columns_hash[k.to_s].type==:string                
                    db_row.send("#{k}=",v.blank?? v : v.strip)
                  end
                end
              end
              db_row.save unless db_row.nil?
             end


            # Tbldatum.all.each do |lot|
            #     lot_status = StatusDetail.find_by_LegendType_and_StatusKey("Lot Status", lot.Lot_Status_Val)
            #     lot_buzz_status = StatusDetail.find_by_LegendType_and_StatusKey("Sales Status", lot.Lot_Buzz_Status_Val) 
            #     schedule_status = StatusDetail.find_by_LegendType_and_StatusKey("Schedule Status", lot.Construction_Schedule_Status_Val)
            #     temp_status={
            #       # :Lot_Status =>  (lot_status.id unless lot_status.nil?),
            #       # :Lot_Buzz_Status => (lot_buzz_status.id unless lot_buzz_status.nil?),
            #       # :Construction_Schedule_Status => (schedule_status.id unless schedule_status.nil?),
            #     }
            #     lot.update_attributes(temp_status)
            # end 
        end
    rescue Exception => e    
      ActiveRecord::Base.logger.error e.message         
    end
  end

  def self.etl_process(client)
    begin
      ActiveRecord::Base.transaction do     
      option_lot = OptionLotMasterOds.find_by_sql("select * from  ODS_OPTIONLOTMASTER where  COMPANYCODE  = '001' 
      and  ((LOTCONVERDATE = '' or LOTCONVERDATE is null) and 
      ((OPTIONEXPDATE = '' or OPTIONEXPDATE is null) or OPTIONEXPDATE >= CURDATE()) and 
      (RELEASESALESDATE <= CURDATE() and RELEASESALESDATE <> ''))");   
     cost_code_query = LotBasis.first.cost_code_query
     category_code = LotBasis.first.category_code
          etl_initial_data=HouseMasterOds.
          includes(
              :house_cost_detail,
              :cust_deposit_dtl,
              :jl_house,
              :jl_loan_detail,
              :podist,
              :house_cost_summary,
              :prospect_conting,
              :prospect_finance,
              :jlloan_master,
              :schedule_house_detail_spec_aging,
              :schedule_house_detail_last_activity,
              :house_cost_detail_variance_to_date,
              :schedule_house_detail_next_activity,
              :sales_person,
              :warranty_master_service_rep_name,
              :warranty_master_warranty_issues_aged,
              :warranty_master_based_on_completion_date,
              :warranty_master
            )+option_lot
            load_array={}
            prices_for_tot_sales_price = {}

          etl_initial_data.reverse_each do |etl_rec|
            src_tbl = etl_rec.attributes.keys.include?("LOTCONVERDATE")
            if src_tbl
              unique_id = "#{etl_rec.DEVELOPMENTCODE}-#{etl_rec.LOTNUMBER}"
              house_number = "#{etl_rec.LOTNUMBER}" 
              block_number = etl_rec.TAXBLOCK
              lot_number = etl_rec.TAXLOT
            else
              unique_id = "#{etl_rec.DEVELOPMENTCODE}-#{etl_rec.HOUSENUMBER}"
              house_number =   "#{etl_rec.HOUSENUMBER}" 
              block_number =   etl_rec.BLOCKNUMBER
              lot_number = etl_rec.LOTNUMBER
            end
            community_name=""
            if etl_rec.DEVELOPMENTCODE=="CW"
              community="Crystal Wood"
            end
              if src_tbl
                load_array={
                 #default fields
                     :Community => community,
                     :Phase_Name => block_number,
                     :Filing => block_number,
                     # :community_city => (etl_rec.generate_community_city unless etl_rec.blank?),
                      # common section
                     :Address => (etl_rec.ADDRESS1 unless etl_rec.blank?),
                     :Contract_Date => (etl_rec.prospect_master.CONTRACTDATE unless etl_rec.prospect_master.blank?),
                     :Development_Code => (etl_rec.DEVELOPMENTCODE unless etl_rec.blank?),
                     :HouseNumber=> house_number,
                     :Lot_Number => (etl_rec.TAXLOT unless etl_rec.blank?),
                     :Lot_Status_Val => (etl_rec.generate_field_LOT_STATUS unless etl_rec.nil?),
                     :Ratified_Date => (etl_rec.prospect_master.RATIFIEDDATE unless etl_rec.prospect_master.blank?),
                     :Lot_Premium_Status => (etl_rec.generate_field_LOT_PREMUIM_STATUS unless etl_rec.nil?) ,
                     :Completed_Specs_Val => (etl_rec.generate_field_COMPLETED_SPEC_STATUS unless etl_rec.nil?),
                     :Construction_Schedule_Status_Val => (etl_rec.generate_field_CONSTRUCTION_SCHEDULE_STATUS unless etl_rec.nil?),       
                     :Construction_VPO_Status_Val => (etl_rec.generate_field_CONSTRUCTION_VPO_STATUS unless etl_rec.nil?),
                     :Outstanding_Service_Orders_Val => (etl_rec.generate_field_OUTSTANDING_SERVICE_ORDER_STATUS unless etl_rec.nil?),       
                     :Lot_Buzz_Status_Val => (etl_rec.generate_field_LOTBUZZ_STATUS unless etl_rec.nil?),
                     :Release_Sale_Date => (etl_rec.RELEASESALESDATE unless etl_rec.blank?),
                     :Contract_Date => (etl_rec.prospect_master.CONTRACTDATE unless etl_rec.prospect_master.blank?),
                     :Ratified_Date => (etl_rec.prospect_master.RATIFIEDDATE unless etl_rec.prospect_master.blank?),
                     :Incentive_Description_1 => (etl_rec.prospect_master.OTHERAMT1DESC unless etl_rec.prospect_master.blank?),
                     :Incentive_Description_2 => (etl_rec.prospect_master.OTHERAMT2DESC unless etl_rec.prospect_master.blank?),
                     :Incentive_Description_3 => (etl_rec.prospect_master.OTHERAMT3DESC unless etl_rec.prospect_master.blank?),
                     :Incentive_1 => (etl_rec.prospect_master.OTHERAMT1 unless etl_rec.prospect_master.blank?),
                     :Incentive_2 => (etl_rec.prospect_master.OTHERAMT2 unless etl_rec.prospect_master.blank?),
                     :Incentive_3 => (etl_rec.prospect_master.OTHERAMT3 unless etl_rec.prospect_master.blank?),
                     :Coop_Agent => (etl_rec.prospect_master.agent_record.AGENTNAME unless etl_rec.prospect_master.blank? || etl_rec.prospect_master.agent_record.blank?),
                     :Coop_Broker => (etl_rec.prospect_master.broker_record.BROKERNAME unless etl_rec.prospect_master.blank? || etl_rec.prospect_master.broker_record.blank?),
                     :Coop_Agent_Fee => (etl_rec.prospect_master.AGENTAMOUNT unless etl_rec.prospect_master.blank?),
                     :Coop_Broker_Fee => (etl_rec.prospect_master.BROKERAMOUNT unless etl_rec.prospect_master.blank?),
                     :Contingency_Fee_Paid => (etl_rec.prospect_conting.AMOUNT unless etl_rec.prospect_conting.blank?),
                     :Mortgage_Lender => (etl_rec.prospect_finance.lender_master.NAME.strip unless etl_rec.prospect_finance.blank? || etl_rec.prospect_finance.lender_master.blank?),
                     :Loan_Officer => (etl_rec.prospect_finance.LOANOFFICER unless etl_rec.prospect_finance.blank?),
                     :Points => (etl_rec.prospect_finance.POINTS unless etl_rec.prospect_finance.blank?),
                     :Closing_Costs => (etl_rec.prospect_finance.CLOSINGCOSTS unless etl_rec.prospect_finance.blank?),
                     :Closing_Costs_Credit => (etl_rec.prospect_finance.BUILDERBUYDOWN unless etl_rec.prospect_finance.blank?),
                     :lot_comments => (etl_rec.LOTCOMMENTS),
                     # :Lot_Premium_Val => (etl_rec.LOTPREMIUM unless etl_rec.blank?),
                   
                }
              else
                load_array={       
                   #default fields
                  :Community => community,
                  :Phase_Name => block_number,
                  :Filing => block_number,
                  # :community_city => (etl_rec.generate_community_city unless etl_rec.blank?),
                  # common section
                  :Address => (etl_rec.ADDRESS1 unless etl_rec.blank?),
                  :Contract_Date => (etl_rec.prospect_master.CONTRACTDATE unless etl_rec.prospect_master.blank?),
                  :Development_Code => (etl_rec.DEVELOPMENTCODE unless etl_rec.blank?),
                  :Elevation => (etl_rec.ELEVATIONCODE unless etl_rec.blank?),
                  :Elevation_Description => (etl_rec.elevation_master_desc.DESCRIPTION.strip unless etl_rec.elevation_master_desc.blank?),
                  :HouseNumber=> house_number,
                  :Lot_Number => lot_number,
                  :Lot_Status_Val => (etl_rec.generate_field_LOT_STATUS unless etl_rec.nil?),
                  :Model_Code => (etl_rec.MODELCODE unless etl_rec.blank?),
                  :Model_Description => (etl_rec.model_masters.DESCRIPTION unless etl_rec.model_masters.blank?),
                  :Ratified_Date => (etl_rec.prospect_master.RATIFIEDDATE unless etl_rec.prospect_master.blank?),
                  :Release_Sale_Date => (etl_rec.SALESRELEASEDATE unless etl_rec.blank?),
                  :Settlement_Date => (etl_rec.SETTLEMENT_DATE unless etl_rec.blank?),

                  #------accounting view------
                  :actual_cost_to_date => (etl_rec.house_cost_detail.sum(:AMOUNT) unless etl_rec.house_cost_detail.blank?),
                  :customer_deposits_received => (etl_rec.cust_deposit_dtl.pluck(:AMOUNT).select { |c| c > 0 }.inject(&:+) unless etl_rec.cust_deposit_dtl.blank?),
                  :total_loan_commitment => (etl_rec.jl_house.LOANAMOUNT unless etl_rec.blank? || etl_rec.jl_house.blank?),
                  :total_loan_drawn_to_date => (etl_rec.jl_loan_detail.sum(:AMOUNT) unless etl_rec.jl_loan_detail.blank?),
                  :Developer => (etl_rec.option_lot_master.SELLERNAME unless etl_rec.option_lot_master.blank?),

                  # changed values based on status and pushed to prices_for_tot_sales_price hash object
                  # :Lot_Basis => (etl_rec.contract_master.AMOUNT unless etl_rec.contract_master.blank?),

                  :Lot_Basis => (etl_rec.generate_lot_basis_amount(cost_code_query,category_code) unless etl_rec.blank?),
                  # :Lot_Premium_Val => (etl_rec.LOTPREMIUM unless etl_rec.blank?),

                  # changed values based on status and pushed to prices_for_tot_sales_price hash object
                  # :base_sales => (etl_rec.BASEPRICE unless etl_rec.blank?),

                  :committed_costs_to_date => (etl_rec.podist.sum(:AMOUNT) unless etl_rec.podist.blank?), 
                  :current_budget => (etl_rec.house_cost_summary.sum(:BUDGETAMOUNT) unless etl_rec.house_cost_summary.blank?),
                  :Incentives =>  (etl_rec.PROMISSORYAMT1.blank? ? 0 : etl_rec.PROMISSORYAMT1) + (etl_rec.PROMISSORYAMT2.blank? ? 0: etl_rec.PROMISSORYAMT2) + (etl_rec.PROMISSORYAMT3.blank? ? 0: etl_rec.PROMISSORYAMT3),

                  # changed values based on status and pushed to prices_for_tot_sales_price hash object
                  # :option_sales => (etl_rec.OPTIONSPRICE unless etl_rec.blank?),

                  :Projected_Cost_at_Completion => (etl_rec.generate_projected_cost),
                  # :Projected_Gross_Profit => (etl_rec.generate_Projected_Gross_Profit),
                  # :total_sales_price => (etl_rec.generate_total_sales_price),

                  # ------closing view -------
                  :Estimated_Settlement_Date => (etl_rec.ESTSETTL_DATE unless etl_rec.blank?),
                  # Base Price field is present in accounting view -- base_sales
                  # Lot_Premium is present in accounting view
                  # :Total_Options => same as option_sales in accounting view
                  :Incentive_Description_1 => (etl_rec.prospect_master.OTHERAMT1DESC unless etl_rec.prospect_master.blank?),
                  :Incentive_Description_2 => (etl_rec.prospect_master.OTHERAMT2DESC unless etl_rec.prospect_master.blank?),
                  :Incentive_Description_3 => (etl_rec.prospect_master.OTHERAMT3DESC unless etl_rec.prospect_master.blank?),
                  # changed values based on status and pushed to prices_for_tot_sales_price hash object
                  :Incentive_1 => (etl_rec.prospect_master.OTHERAMT1 unless etl_rec.prospect_master.blank?),
                  :Incentive_2 => (etl_rec.prospect_master.OTHERAMT2 unless etl_rec.prospect_master.blank?),
                  :Incentive_3 => (etl_rec.prospect_master.OTHERAMT3 unless etl_rec.prospect_master.blank?),
                  # :Total_Customer_Deposits => same as customer_deposits_received
                  # total_loan_drawn_to_date in accounting view
                  :Salesperson_1 => (etl_rec.generate_field_sales_person1_LASTNAME_FIRSTNAME),
                  :Salesperson_2 => (etl_rec.generate_field_sales_person2_LASTNAME_FIRSTNAME),
                  :Salesperson_3 => (etl_rec.generate_field_sales_person3_LASTNAME_FIRSTNAME),
                  :Coop_Agent => (etl_rec.prospect_master.agent_record.AGENTNAME unless etl_rec.prospect_master.blank? || etl_rec.prospect_master.agent_record.blank?),
                  :Coop_Broker => (etl_rec.prospect_master.broker_record.BROKERNAME unless etl_rec.prospect_master.blank? || etl_rec.prospect_master.broker_record.blank?),
                  :Coop_Agent_Fee => (etl_rec.prospect_master.AGENTAMOUNT unless etl_rec.prospect_master.blank?),
                  :Coop_Broker_Fee => (etl_rec.prospect_master.BROKERAMOUNT unless etl_rec.prospect_master.blank?),
                  :Contingency_Fee_Paid => (etl_rec.prospect_conting.AMOUNT unless etl_rec.prospect_conting.blank?),
                  :Mortgage_Lender => (etl_rec.prospect_finance.lender_master.NAME.strip unless etl_rec.prospect_finance.blank? || etl_rec.prospect_finance.lender_master.blank?),
                  :Loan_Officer => (etl_rec.prospect_finance.LOANOFFICER unless etl_rec.prospect_finance.blank?),
                  :Title_Company => (etl_rec.TITLE_CO unless etl_rec.blank?),
                  :Points => (etl_rec.prospect_finance.POINTS unless etl_rec.prospect_finance.blank?),
                  :Closing_Costs => (etl_rec.prospect_finance.CLOSINGCOSTS unless etl_rec.prospect_finance.blank?),
                  :Closing_Costs_Credit => (etl_rec.prospect_finance.BUILDERBUYDOWN unless etl_rec.prospect_finance.blank?),
                  :Loan_Number => (etl_rec.jl_house.LOANNUMBER unless etl_rec.jl_house.blank?),
                  :Loan_Bank_Name => (etl_rec.generate_loan_bank_name unless etl_rec.blank?),

                  # ----------construction view-------------
                  :Construction_Loan_Commitment => (etl_rec.jlloan_master.TODATEDRAWNAMT unless etl_rec.jlloan_master.blank?),
                  :SPEC => (etl_rec.generate_field_spec unless etl_rec.nil?),
                  :SPEC_Aging => (etl_rec.generate_field_SPEC_AGING unless etl_rec.nil?),             
                  # :Total_House_Bud_Est_Costs => same as current_budget
                  :Variances_To_Date => etl_rec.generate_variance_to_date,
                  :YTD_Costs => (etl_rec.house_cost_summary.sum(:ACTUAL) unless etl_rec.nil?),  
                  :YTD_Loan_Draws => (etl_rec.jl_loan_detail.sum(:AMOUNT) unless etl_rec.nil?),
                  :Last_Completed_Activity => (etl_rec.schedule_house_detail_last_activity.ACTIVITYCODE unless etl_rec.schedule_house_detail_last_activity.nil?), 
                  :Last_Completed_Activity_Date => (etl_rec.schedule_house_detail_last_activity.ACTUALFINISHDATE unless etl_rec.schedule_house_detail_last_activity.nil?), 
                  :Last_Completed_Activity_Description => (etl_rec.schedule_house_detail_last_activity.schedule_activity.DESCRIPTION unless etl_rec.schedule_house_detail_last_activity.nil? || etl_rec.schedule_house_detail_last_activity.schedule_activity.nil?), 
                  :Next_Activity => (etl_rec.generate_field_NEXT_ACTIVITY unless etl_rec.nil? || etl_rec.schedule_house_detail_last_activity.nil?),
                  :Purchase_Order_Release_Stage => (etl_rec.POSTAGE unless etl_rec.blank?),
                  :Total_Slippage =>(etl_rec.sched_house_header.FIELD3 unless etl_rec.sched_house_header.blank?),
                  :Work_Order_Release_Stage => (etl_rec.WOSTAGE unless etl_rec.blank?),
                  # for schedule status legend
                  :Construction_Schedule_Status_Val => (etl_rec.generate_field_CONSTRUCTION_SCHEDULE_STATUS unless etl_rec.nil?),       
                  :Construction_VPO_Status_Val => (etl_rec.generate_field_CONSTRUCTION_VPO_STATUS unless etl_rec.nil?),       
                 

                  # ------------sales view-------------------
                  # :Estimated_Settlement_Date => present in closing view
                  # :Elevation == repeated present in common section
                  # :Lot_Premium = present in accounting view
                  # :Model_Name, Model_Description, Elevation, Elevation_Description are present in common section
                  :Orientation => (etl_rec.ORIENTATION unless etl_rec.blank?),
                  :salesperson_firstname => (etl_rec.sales_person.FIRSTNAME unless etl_rec.sales_person.blank?),
                  :salesperson_lastname => (etl_rec.sales_person.LASTNAME unless etl_rec.sales_person.blank?),

                  # for legends
                  :Lot_Premium_Status => (etl_rec.generate_field_LOT_PREMUIM_STATUS unless etl_rec.nil?) ,
                  :Model_Status_Val => (etl_rec.generate_field_MODEL_STATUS unless etl_rec.nil? || etl_rec.nil?),
                  :Completed_Specs_Val => (etl_rec.generate_field_COMPLETED_SPEC_STATUS unless etl_rec.nil?),
                  :lot_comments => (etl_rec.COMMENTS),
                  # ----------warranty view------------
                  :Buyer_Name => (etl_rec.generate_field_LASTNAME_FIRSTNAME unless etl_rec.blank?),
                  # Settlement_Date -- present in common section
                  # modelname and elevation descr are present in common section
                  :Service_Rep_Name => (etl_rec.generate_field_SERVICEREPCODE unless etl_rec.nil?),
                  :Days_Warranty_Issues_Aged => (etl_rec.generate_field_WARRANTY_ISSUES_AGED unless etl_rec.nil?),
                  :Outstanding_Warranty_Issues =>  (etl_rec.warranty_master_based_on_completion_date.count.to_s unless etl_rec.warranty_master_based_on_completion_date.blank?),
                  :Total_Warranty_Issues => (etl_rec.warranty_master.count.to_s unless etl_rec.warranty_master.blank?),
                  :Warranty_Costs => (etl_rec.generate_field_Warranty_Costs unless etl_rec.nil?),   
                  :Warranty_Date => (etl_rec.WARRANTY_DATE unless etl_rec.blank?),
                  :BUYERSNAME2 => (etl_rec.BUYERSNAME2 unless etl_rec.blank?),  
                  :BUYERSNAME3 => (etl_rec.BUYERSNAME3 unless etl_rec.blank?),  
                  :Address1 => (etl_rec.ADDRESS1 unless etl_rec.blank?),  
                  :Address2 => (etl_rec.ADDRESS2 unless etl_rec.blank?),  
                  :Address3 => (etl_rec.ADDRESS3 unless etl_rec.blank?),  
                  :HOMEPHONE => (etl_rec.HOMEPHONE unless etl_rec.blank?),  
                  :CELLPHONE => (etl_rec.CELLPHONE unless etl_rec.blank?),  
                  :EMAIL => (etl_rec.EMAIL unless etl_rec.blank?),  

                  # for legends 
                  :Outstanding_Service_Orders_Val => (etl_rec.generate_field_OUTSTANDING_SERVICE_ORDER_STATUS unless etl_rec.nil?),       

                  #---------------site-conditions----------------------
                  :Dev_Costs_By_Lot => (etl_rec.generate_field_DEV_COSTS_BY_LOT unless etl_rec.nil?),
                  # --------------sales status-------------------------
                  :Lot_Buzz_Status_Val => (etl_rec.generate_field_LOTBUZZ_STATUS unless etl_rec.nil?),
                  # :Floor_Plan_Name => (etl_rec.generate_field_floor_plan_names unless etl_rec.nil?),
                  :Rp_Sales_Price => (etl_rec.generate_field_Rp_Sales_Price unless etl_rec.nil?),


                  # spec_aging_reports
                  :Rp_SPEC_or_MODEL => (etl_rec.generate_field_Rp_SPEC_or_MODEL unless etl_rec.nil?),
                  :Model_Name => (etl_rec.generate_field_MODELCODE_NAME unless etl_rec.nil?),
                  :Rp_QC_Stake_Date => (etl_rec.generate_field_Rp_QC_Stake_Date unless etl_rec.nil?),
                  :Rp_Aging => (etl_rec.generate_field_Rp_Aging unless etl_rec.nil?),
                  :Rp_Retail_Price => (etl_rec.generate_field_Rp_Retail_Price unless etl_rec.nil?),
                  :Rp_Discount => (etl_rec.generate_field_Rp_Discount unless etl_rec.nil?),
                  :Rp_GM_Percent => (etl_rec.generate_field_Rp_GM_Percent unless etl_rec.nil?),
                  :Rp_Construction_Lender => (etl_rec.generate_field_Rp_Construction_Lender unless etl_rec.nil?),
                  :Rp_Loan_Amount => (etl_rec.jl_loan_detail.sum(:AMOUNT) unless etl_rec.nil?),
                } 
              end  
              prices_for_tot_sales_price = etl_rec.generate_all_prices
              load_array = load_array.merge(prices_for_tot_sales_price)
              db_row=Tbldatum.find_or_initialize_by_Unique_ID(unique_id)
              db_row.attributes=load_array 
              db_row.attributes.each do|k,v| 
                if k != 'Unique_ID'
                  if Tbldatum.columns_hash[k.to_s].type==:string                
                    db_row.send("#{k}=",v.blank?? v : v.strip)
                  end
                end
              end
              db_row.save unless db_row.nil?
            end
          Tbldatum.all.each do |lot|
                completed_specs = StatusDetail.find_by_LegendType_and_StatusKey("Completed Specs", lot.Completed_Specs_Val)
                lot_premium = StatusDetail.find_by_LegendType_and_StatusKey("Lot Premium", lot.Lot_Premium_Status)
                schedule_status = StatusDetail.find_by_LegendType_and_StatusKey("Schedule Status", lot.Construction_Schedule_Status_Val)
                vpo_status = StatusDetail.find_by_LegendType_and_StatusKey("VPO Status", lot.Construction_VPO_Status_Val)
                lot_status = StatusDetail.find_by_LegendType_and_StatusKey("Lot Status", lot.Lot_Status_Val)
                outstanding_service_orders = StatusDetail.find_by_LegendType_and_StatusKey("Outstanding Service Orders", lot.Outstanding_Service_Orders_Val)
                lot_buzz_status = StatusDetail.find_by_LegendType_and_StatusKey("Sales Status", lot.Lot_Buzz_Status_Val) 
                total_sales_price = (lot.Lot_Premium_Val.to_i + lot.base_sales.to_i + lot.option_sales.to_i + lot.upgrades_price.to_i + lot.Incentives.to_i)
                projected_gross_profit = total_sales_price.to_i - lot.current_budget.to_i
                gross_margin = (projected_gross_profit.to_i.fdiv(total_sales_price.to_i) if total_sales_price > 0)

                temp_status={
                  
                  :Completed_Specs => (completed_specs.id unless completed_specs.nil?),
                  :Lot_Premium => (lot_premium.id unless lot_premium.nil?),
                  :Construction_Schedule_Status => (schedule_status.id unless schedule_status.nil?),
                  :Construction_VPO_Status => (vpo_status.id unless vpo_status.nil?),
                  :Lot_Status =>  (lot_status.id unless lot_status.nil?),
                  :Outstanding_Service_Orders => (outstanding_service_orders.id unless outstanding_service_orders.nil?),
                  :Lot_Buzz_Status => (lot_buzz_status.id unless lot_buzz_status.nil?),
                  :total_sales_price => (total_sales_price unless total_sales_price.blank?),
                  :Projected_Gross_Profit => (projected_gross_profit unless projected_gross_profit.blank?),
                  :gross_margin => (gross_margin unless gross_margin.blank?)

                }
                lot.update_attributes(temp_status)
            end
      end
      self.update_aging_field
      Tbldatum.update_SVG_ID
      Tbldatum.add_model_or_floor_plan_to_status_details_to_and_fro
      client.execute("update [echelon_homes].[dbo].[DataLoadStatus] set EndDate='#{Time.now.strftime("%Y-%m-%d %H:%M:%S")}', Total_Job_Status='Succeeded' where id=(select top 1 id from [echelon_homes].[dbo].[DataLoadStatus] order by id desc)");    
      Mailer.notify_user_on_etl_process("Succeeded","Successful","").deliver
    rescue Exception => e    
        self.update_aging_field
        ActiveRecord::Base.logger.error e.message         
        # self.update_aging_field
        client.execute("update [echelon_homes].[dbo].[DataLoadStatus] set EndDate='#{Time.now.strftime("%Y-%m-%d %H:%M:%S")}', Total_Job_Status='Failed' where id=(select top 1 id from [echelon_homes].[dbo].[DataLoadStatus] order by id desc)");     
        Mailer.notify_user_on_etl_process("Failed","Failure",e.message).deliver
    end    
  end

  def self.fetch_Rp_SPECS_and_MODELS
   authorized_dev_codes = self.get_authorized_communities_for_logged_in_user
      
    unless authorized_dev_codes.blank?
      Tbldatum.where("Development_Code IN(?) and SVG_ID!=?", authorized_dev_codes,'').find_all_by_Rp_SPEC_or_MODEL("SPEC") + 
      Tbldatum.where("Development_Code IN(?) and SVG_ID!=?", authorized_dev_codes,'').find_all_by_Rp_SPEC_or_MODEL("MODEL")+
      SpecAgingLot.where("Development_Code IN(?)",  authorized_dev_codes).all
    end
  end

  def generate_report_spec_aging_json
  {
        #_wd = with dollar symbol
        "Id"  => self.id,
        "HouseNumber" => self.HouseNumber,
        "Rp_SPEC_or_MODEL" => self.Rp_SPEC_or_MODEL,
        "Development_Code"=> self.Development_Code,
        "Development_Code_Val"=> self.Development_Code,
        "Address"=> self.Address,
        "Model_Name"=> (self.Model_Name.blank?? "":self.Model_Name),
        "Rp_QC_Stake_Date"=> (self.Rp_QC_Stake_Date.strftime("%m-%d-%Y") unless self.Rp_QC_Stake_Date.nil?),
        "Rp_Aging"=> self.Rp_Aging,
        "Rp_Retail_Price"=> self.Rp_Retail_Price,
        "Rp_Retail_Price_wd"=> number_to_currency(self.Rp_Retail_Price, :precision => 0),
        "Rp_Discount"=> self.Rp_Discount,
        "Rp_Discount_wd"=> number_to_currency(self.Rp_Discount,:precision => 0),
        "Rp_Sales_Price"=> self.Rp_Sales_Price,
        "Rp_Sales_Price_wd"=> number_to_currency(self.Rp_Sales_Price,:precision => 0) ,
        "Landscaped"=> self.Landscaped,
        "Rp_GM_Percent"=> (self.Rp_GM_Percent.round(1) unless self.Rp_GM_Percent.nil?),
        "Rp_Loan_Amount"=> self.Rp_Loan_Amount,
        "Rp_Loan_Amount_wd"=> number_to_currency(self.Rp_Loan_Amount,:precision => 0),
        "Rp_Construction_Lender"=> self.Rp_Construction_Lender,
        "Class_Name" => self.class.name,
        "Filing" => self.Filing,
        "Filing_Val" => self.Filing,
        "Count"=> ""
  }
  end 
 
  def self.get_model_names_of_model_status(id)
    @modal_names = Tbldatum.find_all_by_Model_Status(id).uniq(&:Model_Name)
  end

  def self.get_lot_count_for_sub_filters(community, status_key, model_name, phase_name, all_lots)
    if phase_name.blank?
      all_lots.select{|x| x.Model_Status.to_i == status_key and x.Model_Name == model_name and x.Community==community and x.SVG_ID != nil unless x.Model_Status.blank?}.count    
    else
      all_lots.select{|x| x.Model_Status.to_i == status_key and x.Model_Name == model_name and x.Phase_Name == phase_name and x.Community==community and x.SVG_ID != nil unless x.Model_Status.blank?}.count    
    end    
  end

  def generate_owned_optioned_json
    {
      "Id"  => self.id,
      "Development_Code" => self.Development_Code,
      "JobNumber" => ("#{self.Development_Code}-#{self.HouseNumber}" unless self.Development_Code.nil? || self.HouseNumber.nil?),
      "CommunityDescription" => self.Community_Description,      
      "Address" => self.Address,
      "LotNumber" => self.Lot_Number,
      "Filing" => self.Filing,
      "PhaseName" => self.Phase,
      "Block" => self.Block,
      "LotSF" => self.Lot_SF,
      "LotStatusVal" => self.Lot_Status_Val,
      "NewStartStakeDate" => self.New_Start_Stake_Date,
      "LotCost" => self.Lot_Cost,
      "LotCostVal" => ActionController::Base.helpers.number_to_currency(self.Lot_Cost,:precision => 0 ),      
      "LotBasis" => self.Lot_Basis,
      "LotBasisVal" => ActionController::Base.helpers.number_to_currency(self.Lot_Basis,:precision => 0 ),
      "LotPremium" => self.Premium,
      "LotPremiumVal" => ActionController::Base.helpers.number_to_currency(self.Premium,:precision => 0 ),
      "RetailSalesPrice" => (self.Lot_Basis + self.Premium unless self.Lot_Basis.nil? || self.Premium.nil?),
      "RetailSalesPriceVal" => ActionController::Base.helpers.number_to_currency((self.Lot_Basis + self.Premium unless self.Lot_Basis.nil? || self.Premium.nil?),:precision => 0 )
    }
  end

  def update_lot_new_stake_date new_start_stake_date    
    self.update_attributes({:New_Start_Stake_Date => new_start_stake_date})
  end

  def self.get_lot_aging_dashboard_owned_summary
        owned_lots_arr=[]
        Tbldatum.get_owned_lots_per_community.each do |community,lots_array|
         owned_lots_arr << {
            :Community => community, 
            :LotsOwned => lots_array.count, 
            :NewStarts => lots_array.inject(0){|sum, lot| (lot.New_Start_Stake_Date.nil? ? sum+0: sum+1)},
            :JobCosts =>  ActionController::Base.helpers.number_to_currency(lots_array.inject(0){|sum, lot| sum+lot.YTD_Costs},:precision => 0) ,
            :JobCostsVal =>  lots_array.inject(0){|sum, lot| sum+lot.YTD_Costs}.round(0) ,
            :LotEquity =>  ActionController::Base.helpers.number_to_currency(lots_array.inject(0){|sum, lot| sum+lot.YTD_Costs} - lots_array.inject(0){|sum, lot| sum+lot.Rp_Loan_Amount} ,:precision => 0 ),
            :LotEquityVal =>  (lots_array.inject(0){|sum, lot| sum+lot.YTD_Costs} - lots_array.inject(0){|sum, lot| sum+lot.Rp_Loan_Amount}).round(0),
            :RetailSalesPrice => ActionController::Base.helpers.number_to_currency(lots_array.inject(0){|sum, lot| sum+lot.Rp_Retail_Price},:precision => 0 ),
            :RetailSalesPriceVal => lots_array.inject(0){|sum, lot| sum+lot.Rp_Retail_Price}.round(0)
          }          
        end
        owned_lots_arr
  end

  def self.get_lot_aging_dashboard_optioned_summary
        owned_lots_arr=[]
        Tbldatum.get_optioned_lots_per_community.each do |community,lots_array|
         owned_lots_arr << {
            :Community => community, 
            :LotsOptioned => lots_array.count, 
            :NewStarts => lots_array.inject(0){|sum, lot| (lot.New_Start_Stake_Date.nil? ? sum+0: sum+1)},
            :LotCosts =>  ActionController::Base.helpers.number_to_currency(lots_array.inject(0){|sum, lot| sum+lot.Lot_Cost},:precision => 0) ,
            :LotCostsVal =>  lots_array.inject(0){|sum, lot| sum+lot.Lot_Cost}.round(2),
            :RetailSalesPrice => ActionController::Base.helpers.number_to_currency(lots_array.inject(0){|sum, lot| sum+lot.Rp_Retail_Price},:precision => 0 ),
            :RetailSalesPriceVal => lots_array.inject(0){|sum, lot| sum+lot.Rp_Retail_Price}.round(2),
            :RpDespositBalance => ActionController::Base.helpers.number_to_currency(lots_array.first.Rp_Deposit_Balance,:precision => 0 ),
            :RpDespositBalanceVal => lots_array.first.Rp_Deposit_Balance.round(2)
          }          
        end
        owned_lots_arr
  end

  def self.get_lot_aging_lots_owned_loan_amount_by_lender_summary
    loan_amount_lender=[]

    Tbldatum.get_lots_per_community_and_lender.each do|community,community_group|        
       temp={:Community => community}
       community_group.group_by(&:Lot_Lender).each do |lender, lender_group|
        if lender=="VHCO"
          temp[lender.to_sym]=lender_group.inject(0){|sum, lot| sum+lot.Lot_Cost}.round(2)
        else
          temp[lender.to_sym]=lender_group.inject(0){|sum, lot| sum+lot.Rp_Loan_Amount}.round(2)
        end  
       end
      loan_amount_lender<<temp     
    end
    loan_amount_lender
  end

  def self.get_columns_for_lot_aging_dashboard_loan_by_lender
    authorized_comminities = self.get_authorized_communities_for_logged_in_user
     Tbldatum.where("Development_Code IN(?)", authorized_comminities).select(:Lot_Lender).map{|lender| lender.Lot_Lender unless lender.Lot_Lender.blank?}.uniq.compact.sort
  end

  def self.get_spec_report_community_view
      self.get_spec_report_community_view_data(Tbldatum.fetch_Rp_SPECS_and_MODELS.map{|lot| lot.generate_report_spec_aging_json})
  end  

  def self.get_spec_report_plan_view
      self.get_spec_report_plan_view_data(Tbldatum.fetch_Rp_SPECS_and_MODELS.map{|lot| lot.generate_report_spec_aging_json})
  end

# used to get no. for records for each dev. code
  def self.get_spec_report_community_view_data lot_arr 
    lot_arr=lot_arr.sort_by { |hsh| hsh["Development_Code"] }    
    dev_codes_hash = lot_arr.each_with_object(Hash.new(0)) { |obj,counts| counts[obj["Development_Code"]] += 1 }
    lot_arr=lot_arr.reverse
    dev_codes_hash.each do |k,v |
      lot_arr.each_with_index do |lot, i|
        if(lot["Development_Code"]==k)
          lot_arr[i]["Count"]=v
          break
        end
      end
    end
    lot_arr=lot_arr.reverse
    lot_arr    
  end

  def self.get_spec_report_plan_view_data lot_arr    
    lot_arr = lot_arr.sort_by { |hsh| hsh["Model_Name"] }    
    dev_codes_hash = lot_arr.each_with_object(Hash.new(0)) { |obj,counts| counts[obj["Model_Name"]] += 1 }
    lot_arr=lot_arr.reverse
    dev_codes_hash.each do |k,v |
      lot_arr.each_with_index do |lot, i|
        if(lot["Model_Name"]==k )
          lot_arr[i]["Count"]=v
          break
        end
      end
    end
    lot_arr=lot_arr.reverse
    lot_arr
  end

  def self.update_aging_field
    self.all.each do |each_rec|
      stake_date = each_rec.Rp_QC_Stake_Date
      each_rec.update_attributes(:Rp_Aging =>  (Date.today-stake_date).to_i ) unless stake_date.blank?
    end
     SpecAgingLot.all.each do |each_spec_rec|
      stake_date = each_spec_rec.Rp_QC_Stake_Date
      each_spec_rec.update_attributes(:Rp_Aging =>  (Date.today-stake_date).to_i ) unless stake_date.blank?
     end

  end

  def self.get_authorized_communities_for_logged_in_user
    unless Tblusers.current.blank?
      user = Tblusers.find_user(Tblusers.current)
      lotinsight_id = ProductType.find_by_Project_Name("Lot Insight").Id

      authorized_comminities = user.roles.map{|x|x.rds_lot_collections}.flatten.
        select{|x|x.Product_Type_Id.split(",").include?(lotinsight_id.to_s)}.
        map{|z|z.lot_collection}.flatten.map{|lc|lc.lot_collection_lots}.flatten.
        map{|x|x.Community}.uniq

        Tbldatum.where("Community IN(?)", authorized_comminities).
        map{|dev_code| dev_code.Development_Code}.uniq.sort unless authorized_comminities.blank?
    end
  end

  def self.get_community_from_community_description(comm_description)
    self.find_by_Community_Description(comm_description).Development_Code unless comm_description.blank?
  end


  def self.get_contract_terms_summary
    authorized_comminities = self.get_authorized_communities_for_logged_in_user
    LotOptionedContractTermsSummary.where("Development_Code IN (?)", authorized_comminities)
  end

  def self.add_model_or_floor_plan_to_status_details_to_and_fro
    distinct_models=Tbldatum.group(:Model_Description, :Community).where("SVG_ID is not null").select{|x| x unless x.Model_Description.blank?}.sort_by { |i| [i.Model_Description ? 0 : 1, i.Model_Description]  unless i.Model_Description.blank?}
    colors=["#BC123A", "#CA12B3", "#AC8181", "#BC9F93", "#67CAC7", "#FF8CF7", "#679E95", "#8FCA2A", "#4DC187", "#511CCB", "#F2174B"]
    cnt = 0
    distinct_models.each do |lot|
      cnt += 1
      # for model
      n_model_elevation=StatusDetail.find_by_StatusKey_and_LegendType_and_Community("#{lot.Model_Description}","Model",lot.Community)
      if n_model_elevation.nil?
          n_model_elevation=StatusDetail.find_or_initialize_by_StatusKey_and_LegendType_and_Community("#{lot.Model_Description}","Model",lot.Community)
          n_model_elevation.update_attributes(LegendType: "Model",Status_Display_Name:"#{lot.Model_Description}", StatusLabel: "#{lot.Model_Description}" ,StatusKey:"#{lot.Model_Description}",Community:lot.Community,ColorCode:colors[cnt%colors.count],Is_Active: 1)
      end
    end
    distinct_models_frm_elevation_master=ElevationMasterOds.group(:MODELCODE, :DEVELOPMENTCODE)
    distinct_models_frm_elevation_master.each_with_index do |e,i|
      model_description = ModelMasterOds.find_by_MODELCODE(e.MODELCODE).DESCRIPTION.strip
      if e.DEVELOPMENTCODE=="CW"
          community="Crystal Wood"
      end
      n_floor_plan = StatusDetail.find_by_StatusKey_and_LegendType_and_Community(model_description,"Floor Plan",community)
      require 'color-generator'
      generator = ColorGenerator.new saturation: 0.8, lightness: 0.75
      n_floor_plan.update_attributes(:ColorCode => "#"+generator.create_hex) unless n_floor_plan.blank?
      if n_floor_plan.nil?
          n_floor_plan=StatusDetail.find_or_initialize_by_StatusKey_and_LegendType_and_Community(model_description,"Floor Plan",community)
          n_floor_plan.update_attributes(Status_Display_Name:model_description, StatusLabel: model_description,Is_Active: 1, ColorCode:"#"+generator.create_hex)
      end
    end
    Tbldatum.where("SVG_ID is not null").each do |lt|
      model_elevation_id=StatusDetail.find_by_StatusKey_and_Community_and_LegendType("#{lt.Model_Description}",lt.Community,"Model")
      unless model_elevation_id.nil? 
        lt.update_attributes(:Model_Elevation_Id => model_elevation_id.id)
      end

       floor_plan_rec=StatusDetail.find_by_StatusKey_and_Community_and_LegendType("#{lt.Model_Description}",lt.Community,"Floor Plan")
      unless floor_plan_rec.nil? 
        floor_plan_rec.update_attributes(:lot_plan_sts => 1)      
        lt.update_attributes(:Floor_Plan_Id => floor_plan_rec.id, :Floor_Plan_Name => floor_plan_rec.StatusKey )    
      end
    end
  end


  def self.update_SVG_ID_for_existing_lots
     self.where(:Development_Code => "EV").each do |rec|
      unless rec.Lot_Number.blank? || rec.Phase_Name.blank?
        if rec.Lot_Number.to_s.length == 1
          svg_id = "WH_IN_EV_SEC0#{rec.Phase_Name}_L000#{rec.Lot_Number.upcase}"
        elsif rec.Lot_Number.to_s.length == 2
          svg_id = "WH_IN_EV_SEC0#{rec.Phase_Name}_L00#{rec.Lot_Number.upcase}"
        elsif rec.Lot_Number.to_s.length == 3
          svg_id = "WH_IN_EV_SEC0#{rec.Phase_Name}_L0#{rec.Lot_Number.upcase}"
        elsif rec.Lot_Number.to_s.length == 4
          svg_id = "WH_IN_EV_SEC0#{rec.Phase_Name}_L#{rec.Lot_Number.upcase}"
        end
        rec.update_attributes(:SVG_ID => svg_id)
      end
    end
    self.where(:Development_Code => "EP").each do |rec|
      unless rec.Lot_Number.blank?|| rec.Phase_Name.blank?
        if rec.Lot_Number.to_s.length == 1
          svg_id = "WH_IN_EP_SEC0#{rec.Phase_Name}_L000#{rec.Lot_Number.upcase}"
        elsif rec.Lot_Number.to_s.length == 2
          svg_id = "WH_IN_EP_SEC0#{rec.Phase_Name}_L00#{rec.Lot_Number.upcase}"
        elsif rec.Lot_Number.to_s.length == 3
          svg_id = "WH_IN_EP_SEC0#{rec.Phase_Name}_L0#{rec.Lot_Number.upcase}"
        elsif rec.Lot_Number.to_s.length == 4
          svg_id = "WH_IN_EP_SEC0#{rec.Phase_Name}_L#{rec.Lot_Number.upcase}"
        end
        rec.update_attributes(:SVG_ID => svg_id)
      end
    end
  end

  def self.truncate_n_create_lot_collection_lots
    ActiveRecord::Base.connection.execute("TRUNCATE lot_collection_lots")   
    lot_groups = self.group(:Community).where("Community IS NOT NULL")
    lot_groups.each do |group|     
      lc=LotCollection.find_or_create_by_collection_name("#{group.Community}-ALL-LOTS") 
      lots=self.where("Community=? and SVG_ID !=?",group.Community, '')
      lots.each do |lot|
        # rec_presence = LotCollectionLot.find_by_Lot_Number(lot.Unique_ID)
        # if rec_presence.blank?
          lc.lot_collection_lots.create(        
            :Community => lot.Community, 
            # :community_city => lot.community_city, 
            :Phase =>lot.Phase_Name, 
            :Lot_Number => lot.Unique_ID,
            :Lot_Collection_Id => lc.id
          )
        # end
     end     
    end

 end


  def self.update_lot_collection_lots
    ActiveRecord::Base.connection.execute("TRUNCATE lot_collection_lots")   
    lot_groups = self.group(:Community).where("Community IS NOT NULL")
    lot_groups.each do |group|     
      lc=LotCollection.find_or_create_by_collection_name_and_community_city("#{group.Community}-ALL-LOTS", "Community") 
      lots=self.where("Community=? and SVG_ID !=?",group.Community, '')
      lots.each do |lot|
        rec_presence = LotCollectionLot.find_by_Lot_Number(lot.Unique_ID)
        if rec_presence.blank?
          lc.lot_collection_lots.create(        
            :Community => lot.Community, 
            # :community_city => lot.community_city, 
            :Phase =>lot.Phase_Name, 
            :Lot_Number => lot.Unique_ID,
            :Lot_Collection_Id => lc.id
          )
        end
      end     
    end
  end

  def self.run_when_inserting_new_communities
    StatusKey.insert_legendtypes_fr_all_comm("Emerald")
    StatusKey.insert_legendtypes_fr_all_comm("Westwood Landing")
    # add code in below method for new community
    Tbldatum.update_SVG_ID
    Tbldatum.update_lot_collection_lots
    Tbldatum.add_model_or_floor_plan_to_status_details_to_and_fro
  end

  def self.download_images_frm_ftp
    require 'net/ftp'
    ftp=Net::FTP.new
    ftp.connect("ftp.westport-home.com",21)
    ftp.passive = true
    ftp.login("sales","wpsales001")
    
    ftp.chdir("Development Info/Lot Photos")
    dir_names = ftp.nlst("")
    dir_names.each_with_index do |dir,i|
      puts ".......#{dir}"
      if i == 0
        ftp.chdir("#{dir}")
      else
        ftp.chdir("../#{dir}")
      end
      if !Dir.exists?("/home/spandana/Documents/projects/west_port/upload_images/#{dir}")
        Dir.mkdir("/home/spandana/Documents/projects/west_port/upload_images/#{dir}")
      end
      files = ftp.nlst
      files = files.select{ |i| !i[/\.zip$/] }
      files.each do |filename|
        puts ".....#{filename}"
        if !File.exist?("/home/spandana/Documents/projects/west_port/upload_images/#{dir}/#{filename}")
         ftp.getbinaryfile(filename,"/home/spandana/Documents/projects/west_port/upload_images/#{dir}/#{filename}")
        end
      end
    end
    ftp.close
  end
  def self.upload_images_to_lot_and_glry
    # upload image to imageGallery and refresh paperclip
    dir_names = Dir.entries('upload_images').select {|entry| File.directory? File.join('upload_images',entry) and !(entry =='.' || entry == '..') }
    dir_names.each do |x|
      Dir.entries("upload_images/#{x}").select {|f| !File.directory? f}.each do |image|
        if x=="GC"
              community="Grey Fox Commons"
            elsif x == "ET"
              community="Epler Trace"
            elsif x == "WT"
              community="Woods at Traders Point"
            elsif x == "NB & NP"
              community="New Bethel"
            elsif x == "BV"
              community="Blackthorne Villas"
            elsif x == "DS"
              community="Devonshire"
            elsif x == "GP"
              community="Grant Park"
            elsif x == "MB"
              community="Meadows at Bainbridge"
            elsif x == "EV" ||  x == "EP"
              community="Emerald Village"
            elsif x == "WL"
              community="Westwood Landing"
            elsif x == "BP"
              community="Bayberry Place"
            elsif x == "P7"
              community="Preston Trails"
            elsif x == "SW"
              community="Sawmill"
            elsif x == "EG"
              community="Eden Gate"
            elsif x == "SH"
              community="Sheffield Park"
            elsif x.DEVELOPMENTCODE == "PL"
              community="Prairie Lake"
            end
        if image.split(".")[1].downcase == "jpg" || image.split(".")[1].downcase == "png"
          lot_nums = []
          if image.start_with?('LI')
            product_type = "Lot Insight"
          elsif image.start_with?('LB')
            product_type = "Lot Buzz"
          else
            product_type = "Lot Insight, Lot Buzz"
          end
          image_modified_name = image.gsub("LI_","").gsub("LB_","")
          if image_modified_name.include?("-")
            image_modified_name = image_modified_name.sub("Lot","")
            lot_number = image_modified_name.split("-")[0].strip
          elsif image_modified_name.include?("_")
            image_modified_name = image_modified_name.sub("Lot","")
            lot_number = image_modified_name.split("_")[0].strip
          else
            lot_number = image_modified_name.split(".")[0]
          end
          if lot_number.include?("&")
            ln = lot_number.split("&")
            ln_1 = ln[0]
            ln_2 = ln[0].gsub(/[^\d]/, '')+ln[1]
            lot_nums.push(ln_1, ln_2)
          else
            lot_nums.push(lot_number)
          end
          lot_nums.each do |lot_number|
            lot_rec = Tbldatum.find_by_Community_and_Lot_Number(community,lot_number)
            unless lot_rec.blank?
              pics_file_type = MIME::Types.type_for(image).first.content_type
              created_at = updated_at = pics_updated_at = Time.now.to_s(:db)
              img = ImageGallery.find_or_initialize_by_pics_file_name_and_product_type_and_community(image, product_type,community)
              img_gall_rec = img.update_attributes(:created_at => created_at, :updated_at => updated_at, :pics_updated_at => pics_updated_at, :pics_content_type => pics_file_type, :pics_file_size => "111")
              # create directory in shared folder and create all sizes using paperclip 
              if !Dir.exists?("/var/www/west_port/shared/pictures/image_galleries/#{img.id}")
                Dir.mkdir("/var/www/west_port/shared/pictures/image_galleries/#{img.id}")
                Dir.mkdir("/var/www/west_port/shared/pictures/image_galleries/#{img.id}/original")
                FileUtils.cp("upload_images/#{x}/#{image}","/var/www/west_port/shared/pictures/image_galleries/#{img.id}/original")
                ImageGallery.find(img.id).pics.reprocess!
              end
              # upload image to gallery_images_to_lot
             
                gallery_rec = GalleryImagesToLot.find_or_initialize_by_tbldatum_id_and_product_type_and_image_gallery_id_and_community(lot_rec.Unique_ID,product_type,img.id, lot_rec.Community)
                gallery_rec.update_attributes(:community => lot_rec.Community)
                GalleryImagesToLot.update_keyimage_if_lot_having_one_image(lot_rec.Unique_ID ,community, product_type)
            end
          end
        end
      end
    end
  end
  def self.upload_images_to_lot_and_glry_server
    # upload image to imageGallery and refresh paperclip
    dir_names = Dir.entries('upload_images').select {|entry| File.directory? File.join('upload_images',entry) and !(entry =='.' || entry == '..') }
    dir_names.each do |x|
      dev_codes = x.split("-")
      Dir.entries("upload_images/#{x}").select {|f| !File.directory? f}.each do |image|
        if dev_codes[0] == "GC"
          community = "Grey Fox"
        elsif dev_codes[0] == "ET"
          community = "Epler Trace"
        elsif dev_codes[0] == "WT"
          community = "Traders Point"
        elsif dev_codes[0] == "NB" ||  dev_codes[1] == "NP"
          community = "New Bethel"
        elsif dev_codes[0] == "BV"
          community = "Blackthorne"
        elsif dev_codes[0] == "DS"
          community = "Devonshire"
        elsif dev_codes[0] == "GP"
          community = "Grant Park"
        elsif dev_codes[0] == "MB"
          community = "Meadows at Bainbridge"
        elsif dev_codes[0] == "EV" ||  dev_codes == "EP"
          community = "Emerald"
        elsif dev_codes[0] == "WL"
          community = "Westwood Landing"
        end
        if image.split(".")[1].downcase == "jpg" || image.split(".")[1].downcase == "png"
          if image.include?("-")
            image_lot_num = image.sub("Lot","")
            lot_number = image_lot_num.split("-")[0].strip
          else
            lot_number = image.split(".")[0]
          end
          dev_codes.each do |dev_code|
            lot_rec = Tbldatum.find_by_Community_and_Lot_Number(community,lot_number)
            unless lot_rec.blank?
              pics_file_type = MIME::Types.type_for(image).first.content_type
              created_at = updated_at = pics_updated_at = Time.now.to_s(:db)
              img = ImageGallery.find_or_initialize_by_pics_file_name_and_product_type_and_community(image.gsub(' ','%20'), "Lot Insight",community)
              img_gall_rec = img.update_attributes(:created_at => created_at, :updated_at => updated_at, :pics_updated_at => pics_updated_at, :pics_content_type => pics_file_type, :pics_file_size => "111")
              # create directory in shared folder and create all sizes using paperclip 
              if !Dir.exists?("/var/www/west_port/shared/pictures/image_galleries/#{img.id}")
                Dir.mkdir("/var/www/west_port/shared/pictures/image_galleries/#{img.id}")
                Dir.mkdir("/var/www/west_port/shared/pictures/image_galleries/#{img.id}/original")
                FileUtils.cp("upload_images/#{dev_code}/#{image.gsub(' ','%20')}","/var/www/west_port/shared/pictures/image_galleries/#{img.id}/original")
                ImageGallery.find(img.id).pics.reprocess!
              end
              # upload image to gallery_images_to_lot
                gallery_rec = GalleryImagesToLot.find_or_initialize_by_tbldatum_id_and_product_type_and_image_gallery_id_and_community(lot_rec.Unique_ID,"Lot Insight",img.id, lot_rec.Community)
                GalleryImagesToLot.update_keyimage_if_lot_having_one_image(lot_rec.Unique_ID ,community, "Lot Insight")
            end
          end
        end
      end
    end
  end

   def self.update_SVG_ID
    Tbldatum.all.each do |lot|
      # if lot.SVG_ID.blank?
        # if lot.community_city == 'Indianapolis'
        #   comm_city='IN'
        # elsif lot.community_city == 'Fort Wayne'
        #   comm_city='FW'
        # elsif lot.community_city == 'Columbus'
        #   comm_city='CO'
        # end
        # if lot.Community != 'Prairie Lake'
          # unless lot.Lot_Number.blank? || lot.Phase_Name.blank?
          #   if lot.Lot_Number.to_s.length == 1
          #     svg_id = "WH_#{comm_city}_#{lot.Development_Code}_SEC0#{lot.Phase_Name.gsub('-','_')}_L000#{lot.Lot_Number.upcase}"
          #   elsif lot.Lot_Number.to_s.length == 2
          #     svg_id = "WH_#{comm_city}_#{lot.Development_Code}_SEC0#{lot.Phase_Name.gsub('-','_')}_L00#{lot.Lot_Number.upcase}"
          #   elsif lot.Lot_Number.to_s.length == 3
          #     svg_id = "WH_#{comm_city}_#{lot.Development_Code}_SEC0#{lot.Phase_Name.gsub('-','_')}_L0#{lot.Lot_Number.upcase}"
          #   elsif lot.Lot_Number.to_s.length == 4
          #     svg_id = "WH_#{comm_city}_#{lot.Development_Code}_SEC0#{lot.Phase_Name.gsub('-','_')}_L#{lot.Lot_Number.upcase}"
          #   end
          # end
                  if  lot.Community == 'Crystal Wood'
                    unless lot.Lot_Number.blank? || lot.Phase_Name.blank?
                          
                      if lot.Lot_Number[0] == "0"
                          lot_number = lot.Lot_Number[1..-1]
                      else
                          lot_number = lot.Lot_Number
                      end

                          # if lot.Phase_Name.length == 1
                          #   phase_name = "0"+lot.Phase_Name.gsub('-','_')
                          # else
                          #   phase_name = lot.Phase_Name.gsub('-','_')
                          # end

                          # if lot_number.to_s.length == 1
                          #   svg_id = "WH_#{lot.Development_Code}_BL#{phase_name}_L000#{lot_number.upcase}"
                          # elsif lot_number.to_s.length == 2
                          #   svg_id = "WH_#{lot.Development_Code}_BL#{phase_name}_L00#{lot_number.upcase}"
                          # elsif lot_number.to_s.length == 3
                          #   svg_id = "WH_#{lot.Development_Code}_BL#{phase_name}_L0#{lot_number.upcase}"
                          # elsif lot_number.to_s.length == 4
                          #   svg_id = "WH_#{lot.Development_Code}_BL#{phase_name}_L#{lot_number.upcase}"
                          # end

                          svg_id = "EC_#{lot.Development_Code}_L#{lot_number}"
                    end
                  end

                  if !lot.Community.blank? && !lot.Filing.blank?
                    # if File.exist?("public/#{lot.Community.delete(" ")}.svg")
                      # file_read_orig = File.open("public/#{lot.Community.delete(" ")}.svg").read
                      # copy_file_read = file_read_orig
                      # if copy_file_read.match(/\b#{svg_id}\b/) != nil
                        lot.update_attributes(:SVG_ID => svg_id)
                        if !svg_id.blank?
                          Tbldatum.update_new_lts_in_lot_collection_lots(lot.Community,lot.Unique_ID,lot.Phase_Name, lot.community_city)
                        end
                      # end
                    # end
                  end
      # end
    end
  end

  # def self.update_new_lts_in_lot_collection_lots_old(community,unique_id,phase_name,community_city)
  #   lc=LotCollection.find_or_create_by_collection_name("#{community}-ALL-LOTS")
  #   lc.update_attributes(:community_city => community_city)
  #   lcl_rec= lc.lot_collection_lots.find_by_Lot_Number_and_Community_and_Phase(unique_id,community,phase_name)
  #   if(lcl_rec == nil)
  #     lc.lot_collection_lots.create(        
  #       :Community => community, 
  #       :community_city => community_city,
  #       :Phase =>phase_name, 
  #       :Lot_Number => unique_id,
  #       :Lot_Collection_Id => lc.id
  #       )
  #   end
  # end

  def self.update_new_lts_in_lot_collection_lots(community,unique_id,phase_name,community_city)
    # lc_check = LotCollection.find_by_collection_name_and_community_city("#{community}-ALL-LOTS", community_city)
    lc_check = LotCollection.find_by_collection_name("#{community}-ALL-LOTS")
    lc=LotCollection.find_or_create_by_collection_name("#{community}-ALL-LOTS")
    # lc.update_attributes(:community_city => community_city)
    if lc_check.blank? 
      RoleDefinition.add_new_comm_to_roles(lc)
    end
    lcl_rec= lc.lot_collection_lots.find_by_Lot_Number_and_Community_and_Phase(unique_id,community,phase_name)
    if(lcl_rec == nil)
      lc.lot_collection_lots.create(        
        :Community => community, 
        # :community_city => community_city,
        :Phase =>phase_name, 
        :Lot_Number => unique_id,
        :Lot_Collection_Id => lc.id
        )
    end
  end

#delete this metod #author divya
# def self.update_lot_collection_lots_temporary_for_echelon_homes
#   self.all.each do |lot|
#     Tbldatum.update_new_lts_in_lot_collection_lots(lot.Community,lot.Unique_ID,lot.Phase_Name, lot.community_city)
#   end
# end


  end

