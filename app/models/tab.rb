class Tab < ActiveRecord::Base
  attr_accessible :name, :parent_tab_id,:has_child
  has_many :reports, :class_name => "Report", :foreign_key => "tab_id"

 def self.fetch_tabs
  self.select("name,ID").map{|g| [g.name, g.ID]}
 end

 def self.main_menu_tabs
 	self.where("parent_tab_id is NULL")
 end
 
 def self.sub_menu_items(id)
 	self.where("parent_tab_id = #{id}")
 end 

end