class ProductTypeMenuOption < ActiveRecord::Base	
	self.table_name="MenuOption_ProjectType"
	belongs_to :product_type, :foreign_key => "ProjectId"	
	belongs_to :menu_option	,:foreign_key => "MenuOptionId"	
end