class RdsLotCollectionsMenuOption < ActiveRecord::Base
  attr_protected
  self.table_name = "Rds_Lot_Collections_Menu_Options"
  belongs_to :menu_option, :foreign_key => "Menu_Option_Id"
  belongs_to :rds_lot_collections, :foreign_key => "Rds_Lot_Collection_Id"
  has_one :rds_lot_collections_menu_options_role_access, :dependent => :destroy, :class_name => "RdsLotCollectionsMenuOptionRoleAccess",:foreign_key => "Rds_Lot_Collections_Menu_Options_Id"
  has_one :role_access ,:through => :rds_lot_collections_menu_options_role_access, :foreign_key => "Role_Access_Id",:class_name => "Access"
end
