class ImageGallery < ActiveRecord::Base  
  attr_protected
  # has_many :gallery_images_to_lots, :dependent => :destroy
  before_destroy :nullify_key_image_urls_for_lots   
  has_many :gallery_images_to_lots, :dependent => :destroy
  # has_many :lot_infos, :through => :gallery_images_to_lots
  # validates_attachment_presence :pics  
  has_attached_file :pics, :styles => lambda { |a|
    if a.instance.is_image?
        {
      # :large => {
      #     :geometry => "800x600!",
      #     :quality => "95",
      #     :sharpen => "0x1.2"
      # },
      # :small => {
      #     :geometry => "130x130!",
      #     :quality => "95",
      #     :sharpen => "0x1.2"
      # },
      :thumb => Proc.new { |instance| instance.resize },
      :thumb_add_images => Proc.new { |instance| instance.resize_add_images },  
      :thumb_community_images => Proc.new { |instance| instance.resize_community_image },
      :thumb_view_details_images => Proc.new { |instance| instance.resize_view_details_community_image }  
      
  }
  elsif a.instance.is_pdf?
    {
      :thumb_add_images => [Proc.new { |instance| instance.resize_add_images }, :jpg],
      :thumb => [Proc.new { |instance| instance.resize }, :jpg] 
    }
  else
        Hash.new
      end
},
      :processors => [:thumbnail],
      :convert_options => { :all => '-colorspace RGB -flatten -density 300 -quality 100' }, 

          :url => "/pictures/:class/:id/:style/:basename.:extension",
          :path => ATTACHMENT_PATH+"shared/pictures/:class/:id/:style/:basename.:extension"

      
def is_image? 
  return false unless pics.content_type
    ['image/jpeg', 'image/pjpeg', 'image/gif', 'image/png', 'image/x-png', 'image/jpg'].include?(pics.content_type)
end

def is_pdf? 
  return false unless pics.content_type
    ['application/pdf'].include?(pics.content_type)
end

def resize  
  geo = Paperclip::Geometry.from_file(pics.to_file(:original))
  if geo.width > 800 && geo.height < 600
    new_width = 800
    new_height = geo.height*(800/geo.width)
  elsif geo.height > 600 && geo.width < 800
    new_height = 600
    new_width = geo.width*(600/geo.height)
  elsif geo.height > 600 && geo.width > 800
   
    x = geo.width/800
    y = geo.height/600

    if x > y
      new_height = geo.height*(800/geo.width)
      new_width = 800
    elsif x < y
      new_height = 600
      new_width = geo.width*(600/geo.height)
    else      
      new_width = 800
      new_height = geo.height*(800/geo.width)
    end     

  else    
    new_height = geo.height
    new_width = geo.width  
  end
  "#{new_width.round}x#{new_height.round}!"
end

def resize_add_images  
  geo = Paperclip::Geometry.from_file(pics.to_file(:original))
  if geo.width > 130 && geo.height < 130
    new_width = 130
    new_height = geo.height*(130/geo.width)
  elsif geo.height > 130 && geo.width < 130
    new_height = 130
    new_width = geo.width*(130/geo.height)
  elsif geo.height > 130 && geo.width > 130
   
    x = geo.width/130
    y = geo.height/130

    if x > y
      new_height = geo.height*(130/geo.width)
      new_width = 130
    elsif x < y
      new_height = 130
      new_width = geo.width*(130/geo.height)
    else      
      new_width = 130
      new_height = geo.height*(130/geo.width)
    end     

  else    
    new_height = geo.height
    new_width = geo.width  
  end
  "#{new_width.round}x#{new_height.round}!"
end

def resize_community_image
  geo = Paperclip::Geometry.from_file(pics.to_file(:original))
  if geo.width > 750 && geo.height < 450
    new_width = 750
    new_height = geo.height*(750/geo.width)
  elsif geo.height > 450 && geo.width < 750
    new_height = 450
    new_width = geo.width*(450/geo.height)
  elsif geo.height > 450 && geo.width > 750
   
    x = geo.width/750
    y = geo.height/450

    if x > y
      new_height = geo.height*(750/geo.width)
      new_width = 750
    elsif x < y
      new_height = 450
      new_width = geo.width*(450/geo.height)
    else      
      new_width = 750
      new_height = geo.height*(750/geo.width)
    end     

  else    
    new_height = geo.height
    new_width = geo.width  
  end
  "#{new_width.round}x#{new_height.round}!"
end


def resize_view_details_community_image
  geo = Paperclip::Geometry.from_file(pics.to_file(:original))
  if geo.width > 480 && geo.height < 280
    new_width = 480
    new_height = geo.height*(480/geo.width)
  elsif geo.height > 280 && geo.width < 480
    new_height = 280
    new_width = geo.width*(280/geo.height)
  elsif geo.height > 280 && geo.width > 480
   
    x = geo.width/480
    y = geo.height/280

    if x > y
      new_height = geo.height*(480/geo.width)
      new_width = 480
    elsif x < y
      new_height = 280
      new_width = geo.width*(280/geo.height)
    else      
      new_width = 480
      new_height = geo.height*(480/geo.width)
    end     

  else    
    new_height = geo.height
    new_width = geo.width  
  end
  "#{new_width.round}x#{new_height.round}!"
end

def self.update_plan_keyimage_if_plan_having_one_image(plan_id)

images = ImageGallery.find_all_by_plan_id(plan_id)
unless images.blank?
    plan_key_img_exists = images.select{|x| x.plan_key_image == "1"}.blank?
    if plan_key_img_exists == false
    else
          statuskey = StatusDetail.find(plan_id).StatusKey  
          statuskey_recs = StatusDetail.find_all_by_StatusKey_and_LegendType(statuskey,"Floor Plan")
          image=''

            statuskey_recs.each do |each_rec|
              if images.count == 1  
                image = ImageGallery.find_by_plan_id(each_rec.id)    
                image.update_attributes(:plan_key_image => 1)
                @updated_key_img_flag == true
            else    
             image = ImageGallery.find_all_by_plan_id(each_rec.id).sort_by(&:created_at).last
             image.update_attributes(:plan_key_image => 1) unless image.blank?
             @updated_key_img_flag == true
            end
          end

          Tbldatum.all.each do |each_tbl_rec|
             if each_tbl_rec.Floor_Plan_Id == "#{plan_id}" && each_tbl_rec.Key_Image_Url.blank?
                each_tbl_rec.update_attributes(:Key_Image_Url => image.pics(:thumb_add_images), :Key_Image_Id => image.id) unless image.blank?
            end 
            Tbldatum.invalidate_cache
          end
    end
end
end

def self.update_key_image_after_deletion(lot_id, community, product_type)
    key_image_rec = GalleryImagesToLot.find(:all, :conditions => ["tbldatum_id =? and community=? and product_type  like ? and key_image=?", lot_id, community, "%#{product_type}%", 1])       
    tbldatum_rec = Tbldatum.find(lot_id)
    # unless tbldatum_rec.image_galleries.blank?
        if key_image_rec.blank?
          unless tbldatum_rec.Floor_Plan_Id.blank?
            floor_plans = tbldatum_rec.Floor_Plan_Id.split(",")
          else
            floor_plans = []
          end
            if floor_plans.count == 1
                all_plan_images = ImageGallery.find_all_by_plan_id(floor_plans[0])
                
                if all_plan_images.blank?
                  # key_image_to_update = GalleryImagesToLot.find_all_by_tbldatum_id_and_community_and_product_type(lot_id, community, product_type).sort_by(&:created_at).last
                  key_image_to_update = GalleryImagesToLot.find(:all, :conditions => ["tbldatum_id =? and community=? and product_type  like ?", lot_id, community, "%#{product_type}%"]).sort_by(&:created_at).last 
                  key_image_to_update.update_attributes(:key_image => 1) unless key_image_to_update.blank?
                  key_image_id = key_image_to_update.image_gallery_id unless key_image_to_update.blank?
                  tbldatum_rec.update_attributes(:Key_Image_Url => key_image_to_update.image_gallery.pics(:thumb_add_images), :Key_Image_Id => key_image_id) unless key_image_to_update.blank?
                else
                  key_image_to_update = all_plan_images.select{|x| x.plan_key_image == "1"} unless all_plan_images.blank?
                  key_image_id = key_image_to_update[0].id unless key_image_to_update[0].blank?
                  tbldatum_rec.update_attributes(:Key_Image_Url => key_image_to_update[0].pics(:thumb_add_images), :Key_Image_Id => key_image_id) unless key_image_to_update.blank?
                end
            else      
                 # key_image_to_update = GalleryImagesToLot.find_all_by_tbldatum_id_and_community_and_product_type(lot_id, community, product_type).sort_by(&:created_at).last
                 key_image_to_update = GalleryImagesToLot.find(:all, :conditions => ["tbldatum_id =? and community=? and product_type  like ?", lot_id, community, "%#{product_type}%"]).sort_by(&:created_at).last 
                 key_image_to_update.update_attributes(:key_image => 1) unless key_image_to_update.blank?
                 key_image_id = key_image_to_update.image_gallery_id unless key_image_to_update.blank?
                 tbldatum_rec.update_attributes(:Key_Image_Url => key_image_to_update.image_gallery.pics(:thumb_add_images), :Key_Image_Id => key_image_id) unless key_image_to_update.blank?
            end
        end
      # else
      #   tbldatum_rec.update_attributes(:Key_Image_Url => nil, :Key_Image_Id => nil)
      # end
end

def self.get_lots_based_on_product_type(product_type, community, opted_parcel)  
  all_image_ids = self.find_by_sql("SELECT DISTINCT(tbldatum_id) FROM gallery_images_to_lots where product_type like '%#{product_type}%' and community='#{community}' ORDER BY tbldatum_id ").map{|x| x.tbldatum_id}
  all_plan_ids = ImageGallery.get_plan_images(product_type).map{|x| x.plan_id}            
  houses = []

  unless all_image_ids.blank?
    all_image_ids.each do |each_img_id|
      if opted_parcel.blank?
        tbldatum_rec = Tbldatum.find_by_Unique_ID(each_img_id)
      else
        tbldatum_rec = Tbldatum.find_by_Unique_ID_and_Phase_Name(each_img_id, opted_parcel)
      end
    unless tbldatum_rec.blank?
      houses_hash={}      
      houses_hash["lot_nbr"] = tbldatum_rec.Lot_Number
      houses_hash["phase_name"] = tbldatum_rec.Phase_Name
      houses << houses_hash
    end
  end
  end
  
  unless all_plan_ids.blank?
    all_plan_ids.each do |each_plan_id|      
      if opted_parcel.blank?
       tbldatum_plan_recs = Tbldatum.find_all_by_Floor_Plan_Id_and_Community(each_plan_id,community)
      else
        tbldatum_plan_recs = Tbldatum.find_all_by_Floor_Plan_Id_and_Phase_Name_and_Community(each_plan_id, opted_parcel,community)
      end
      
      unless tbldatum_plan_recs.blank?
        tbldatum_plan_recs.each do |each_tbl_rec|
          houses_hash={}      
          houses_hash["lot_nbr"] = each_tbl_rec.Lot_Number
          houses_hash["phase_name"] = each_tbl_rec.Phase_Name
          houses << houses_hash
        end
      end
    end
  end
  houses.uniq
end

def self.get_plan_images(product_type)  
  self.find_by_sql("SELECT DISTINCT(plan_id) FROM image_galleries where plan_id != '' and product_type='#{product_type}' ORDER BY plan_id ")
end

def self.get_ids_for_community_based_on_product_type(product_type, community)  
  find_by_sql("SELECT DISTINCT(product_sales_contract_parcel_id) FROM gallery_images_to_communities where product_type='#{product_type}' and community='#{community}' ORDER BY product_sales_contract_parcel_id ")
end

def self.validate_url(link_name)
      if (link_name =~ /http[s]?:\/\//)
      link_name
    else
      link_name = "http://#{link_name}"
    end
    link_name
end


private 

def nullify_key_image_urls_for_lots  
  self.gallery_images_to_lots.each do |gil|
    unless gil.is_key_image_lot
      gil.lot.update_attributes(:Key_Image_Url => nil, :Key_Image_Id => nil)
    end
    Tbldatum.invalidate_lot_cache gil.lot.Unique_ID
  end

end

  def self.update_LI_to_LB_count
    Tbldatum.all.each do |x|
      x.update_attributes(:pic_count_lotbuzz => x.pic_count_lotinsight)
    end
  end
end
