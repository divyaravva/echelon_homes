class SpecAgingController < ApplicationController

  before_filter :authenticate
  before_filter :is_admin
  before_filter :get_product
  before_filter :get_product_view 
  before_filter :get_comm_city_view
  before_filter :get_community_city      
  before_filter :get_communities   
  before_filter :get_communities_view
  before_filter :get_parcels
  before_filter :get_parcels_view

  def index
    @spec_aging_rec=SpecAging.all
  end

  def show
    @spec_aging_details = SpecAging.find(params[:id]) unless params[:id].nil?
  end

  def destroy    
    spec_aging = SpecAging.find(params[:id]) unless params[:id].nil?
    spec_aging.destroy unless lot_types.nil?
    Tbldatum.invalidate_cache
    redirect_to "/spec_aging"
  end

  def new     
  end

  def create   	
  end  

  def edit       
    @spec_aging_details = SpecAging.find(params[:id]) unless params[:id].nil?
    @activity_list = SchedhouseDetailOds.all.map{|x| x.ACTIVITYCODE}.uniq
    @step_recs = SchedhouseDetailOds.find_by_sql("SELECT DISTINCT ACTIVITYCODE,STEPNUMBER, (select trim(DESCRIPTION) from ODS_SCHEDACTIVITIES where ACTIVITYCODE = a.ACTIVITYCODE) as description FROM `ODS_SCHEDHOUSEDETAIL` as a")
  end

  def update
    spec_aging_details = SpecAging.find(params[:id]) unless params[:id].nil?    
    # spec_aging_details.update_attributes(:Activity_Code => params[:activity_code_dropdown], :Step_Number => params[:step_num_dropdown], :Description => params[:description]) unless spec_aging_details.nil?
    spec_aging_details.update_attributes(:Activity_Code => params[:activity_code_dropdown]) unless spec_aging_details.nil?
    Tbldatum.update_spec_aging_n_compl_specs
    Rails.cache.clear
    redirect_to "/spec_aging"
  end

  def destroy    
    lot_type = SpecAging.find(params[:id]) unless params[:id].nil?
    lot_type.destroy unless lot_type.nil?
    @lot_types=SpecAging.all
    render :partial => "spec_aging"
  end

end