class SpecAgingLot < ActiveRecord::Base
  include ActionView::Helpers::NumberHelper
  attr_protected
  self.table_name = "Spec_Aging_lots"


   def generate_report_spec_aging_json
	  {
	  	    "Id"  => self.id,
	        "HouseNumber" => self.HouseNumber,
	        "Rp_SPEC_or_MODEL" => self.Rp_SPEC_or_MODEL,
	        "Development_Code"=> self.Development_Code,
	        "Development_Code_Val"=> self.Development_Code,
	        "Address"=> self.Address,
	        "Model_Name"=> (self.Model_Name.blank?? "":self.Model_Name),
	        "Rp_QC_Stake_Date"=> (self.Rp_QC_Stake_Date.strftime("%m-%d-%Y") unless self.Rp_QC_Stake_Date.nil?),
	        "Rp_Aging"=> self.Rp_Aging,
	        "Rp_Retail_Price"=> self.Rp_Retail_Price,
	        "Rp_Retail_Price_wd"=> number_to_currency(self.Rp_Retail_Price, :precision => 0),
	        "Rp_Discount"=> self.Rp_Discount,
	        "Rp_Discount_wd"=> number_to_currency(self.Rp_Discount,:precision => 0),
	        "Rp_Sales_Price"=> self.Rp_Sales_Price,
	        "Rp_Sales_Price_wd"=> number_to_currency(self.Rp_Sales_Price,:precision => 0) ,
	        "Landscaped"=> self.Landscaped,
	        "Rp_GM_Percent"=> (self.Rp_GM_Percent.round(1) unless self.Rp_GM_Percent.nil?),
	        "Rp_Loan_Amount"=> self.Rp_Loan_Amount,
	        "Rp_Loan_Amount_wd"=> number_to_currency(self.Rp_Loan_Amount,:precision => 0),
	        "Rp_Construction_Lender"=> self.Rp_Construction_Lender,
	        "Class_Name" => self.class.name,
	        "Filing" => self.Filing,
	        "Filing_Val" => self.Filing,
	        "Count"=> ""
	  }
	 end

end

