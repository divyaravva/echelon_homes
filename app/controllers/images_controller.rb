class ImagesController < ApplicationController
  # before_filter :authenticate
  before_filter :has_roles 
  before_filter :get_product
  before_filter :get_product_view
  before_filter :get_communities_view
  before_filter :get_parcels_view
  before_filter :set_res_header, :only => [:upload_images, :upload_files]

  def display
    images= []    
    images1= []    
    images2= []    
      lot_record = Tbldatum.find(params[:lot_id])
      images1 = lot_record.image_galleries.where("gallery_images_to_lots.product_type LIKE ?","%#{@opted_product}%")
      floor_plans_count = lot_record.Floor_Plan_Id.split(",").count unless lot_record.Floor_Plan_Id.blank?
      if floor_plans_count == 1
        images2 =ImageGallery.find_all_by_plan_id_and_product_type(lot_record.Floor_Plan_Id, @opted_product)
        if images2.count == 0
          key_image = Tbldatum.find(params[:lot_id]).image_galleries.where("gallery_images_to_lots.product_type LIKE ? and gallery_images_to_lots.key_image=?", "%#{@opted_product}%", 1)
        else
          key_image = ImageGallery.find_all_by_plan_id_and_plan_key_image_and_product_type(lot_record.Floor_Plan_Id,1,@opted_product).first 1
        end
      else
        key_image = Tbldatum.find(params[:lot_id]).image_galleries.where("gallery_images_to_lots.product_type LIKE ? and gallery_images_to_lots.key_image=?", "%#{@opted_product}%", 1)
      end
      
      images = images1 + images2
      unless key_image.blank?
        view_images_community_website = images-key_image
        view_images_community_website.unshift(key_image[0])
      else
        view_images_community_website = images
      end

    render :partial => '/images/display.html.erb', :locals => {:images => images, :view_images_community_website => view_images_community_website, :key_image => key_image, :lot_record => lot_record, :images_count => images.count, :view => params[:view], :lot_number => params[:lot_id], :community => params[:community], :community_parcel => params[:community_parcel_name],
      :development_status => params[:development_status], :lot_nbr_for_cta => lot_record.Lot_Number, :filing => lot_record.Phase_Name}
      
  end

  def view_docs    
    docs=Tbldatum.find(params[:lot_id]).tbldataattachments.where("image_content_type not like ? and product_type =? ","%image%", @opted_product)
    render :partial => '/images/view_docs.html.erb', :locals => {:images => docs, :images_count => docs.count}
  end

  def edit
    
    images= []    
    images1= []    
    images2= [] 
    tbldatum_rec = Tbldatum.find(params[:lot_id])
    images1 = tbldatum_rec.image_galleries.where("gallery_images_to_lots.product_type LIKE ?","%#{@opted_product}%")
    floor_plans_count = tbldatum_rec.Floor_Plan_Id.split(",").count unless tbldatum_rec.Floor_Plan_Id.blank?
      if floor_plans_count == 1
        images2 = ImageGallery.find_all_by_plan_id_and_product_type(tbldatum_rec.Floor_Plan_Id, @opted_product)      
      end
      
    images = images1 + images2
    @selected_key_image_id = tbldatum_rec.Key_Image_Id

    render :partial => '/images/edit.html.erb', :locals => {:images => images, :lot_id => params[:lot_id], 
      user_role: params[:user_role], lot_number: tbldatum_rec.Lot_Number, access_permission: params[:access_permission], :parent_tab_id => params[:parent_tab_id], :phase_parcel => params[:phase_parcel], :project => @opted_product, :community => @opted_community}
  end

  def display_docs   
    tbldatum_rec = Tbldatum.find(params[:lot_id]) 
    docs=tbldatum_rec.tbldataattachments.where("image_content_type not like ? and product_type =? ","%image%", @opted_product)
    render :partial => '/images/display_docs.html.erb', :locals => {:images => docs, :files_count => docs.count, 
      :lot_id => params[:lot_id], lot_number: tbldatum_rec.Lot_Number, :user_role => params[:user_role], access_permission: params[:access_permission], :parent_tab_id => params[:parent_tab_id],:phase_parcel => params[:phase_parcel]}
  end

  def show
    @doc = Tbldataattachment.find(params[:id])
    send_file @doc.image.path,
              :filename => @doc.image_file_name,
              :type => @doc.image_content_type,
              :disposition => 'attachment'

  end

  def delete_images    
    @tbldata = Tbldatum.find(params[:lot_id])
    
    unless params[:tbldataattachments].blank?
      params[:tbldataattachments].each do |attachment|  
        unless attachment["id"].blank?
          img = ImageGallery.find(attachment["id"])  
          img.update_attributes( :title => attachment["title"], :description => attachment["description"], :navigation_link => attachment["navigation_link"])              
        end
      end
    end
    unless @tbldata.nil?
      existing_key_image = @tbldata.gallery_images_to_lots.find_by_key_image 1
       new_key_image= params["key_image"].select{|images| images unless images["_key_image"].nil? || !images["_destroy"].nil?}
       unless new_key_image.empty?
         if  new_key_image[0]["plan_id"].blank?
          future_key_image = @tbldata.find_gallery_image_of_lot new_key_image.first["id"]                      
          # key_image_rec = GalleryImagesToLot.find_by_tbldatum_id_and_key_image_and_product_type(params[:lot_id],1, @opted_product )
          key_image_rec = GalleryImagesToLot.where("tbldatum_id =? and key_image =? and product_type like ? ", params[:lot_id], 1, "%#{@opted_product}%" )[0]
          key_image_rec.update_attributes(:key_image => 0) unless key_image_rec.blank?
          # lot_rec = GalleryImagesToLot.find_by_tbldatum_id_and_image_gallery_id_and_product_type(params[:lot_id], future_key_image.image_gallery_id, @opted_product)
          lot_rec =  GalleryImagesToLot.where("tbldatum_id =? and image_gallery_id =? and product_type like ? ", params[:lot_id], future_key_image.image_gallery_id, "%#{@opted_product}%" )[0]
          lot_rec.update_attributes(:key_image => 1) unless lot_rec.blank?
          if lot_rec.image_gallery.pics_file_name.blank?
              hover_image = "http://img.youtube.com/vi/"+lot_rec.image_gallery.ytube_video_id+"/default.jpg"
          else
              hover_image = lot_rec.image_gallery.pics(:thumb_add_images)
          end
            @tbldata.update_attributes(:Key_Image_Url => hover_image, :Key_Image_Id => lot_rec.image_gallery_id)
        end
      end
      key_image_delete= params["key_image"].select{|images| images unless images["_key_image"].nil? || images["_destroy"].nil?}

      unless key_image_delete.empty?
          future_key_image_delete = @tbldata.find_gallery_image_of_lot key_image_delete.first["id"]          
          @tbldata.delete_key_image_url(future_key_image_delete, existing_key_image) 
          ImageGallery.update_key_image_after_deletion(@tbldata.id, params[:community], params[:project])         
      end 

      unless params["key_image"].blank?
        params["key_image"].each do |key_image|    
          unless key_image["plan_id"].blank? 
            unless key_image["_key_image"].nil? 
              @update_flag = "plan image"                       
              if key_image["pics_file_name"].blank?
                lot_rec = ImageGallery.find_by_plan_id_and_ytube_video_id(key_image["plan_id"], key_image["ytube_video_id"])
                 @tbldata.update_attributes(:Key_Image_Url => lot_rec.ytube_video_id, :Key_Image_Id => lot_rec.id)
              else
                lot_rec = ImageGallery.find_by_plan_id_and_pics_file_name(key_image["plan_id"], key_image["pics_file_name"])
                @tbldata.update_attributes(:Key_Image_Url => lot_rec.pics(:thumb_add_images), :Key_Image_Id=> lot_rec.id)
              end
             end
          end
        end
      end

      images_to_delete = params["key_image"].map{|images| images["id"].to_i unless !images["_key_image"].nil? || images["_destroy"].nil?}     
      unless images_to_delete.compact.empty?        
        images = @tbldata.find_all_gallery_image_of_lot images_to_delete
        images.each do |img|
          img.destroy        
        end        
      end
      floor_plan = @tbldata.Floor_Plan_Id.split(",").count unless @tbldata.Floor_Plan_Id.blank?
      if  floor_plan == 1
          if ImageGallery.find_all_by_plan_id(@tbldata.Floor_Plan_Id).blank? && @tbldata.gallery_images_to_lots.count == 0
              @tbldata.update_attributes(:Key_Image_Url => "", :Key_Image_Id => 0)
          end
      end
      Tbldatum.invalidate_lot_cache @tbldata.Unique_ID
      render :json=> {:status=>"success", :msg => "Key Image deleted", :update_flag => @update_flag}
    end

  end
  # def delete_images_old    
  #   @tbldata = Tbldatum.find(params[:lot_id])
    
  #   unless params[:tbldataattachments].blank?
  #     params[:tbldataattachments].each do |attachment|  
  #       img = ImageGallery.find(attachment["id"])  
  #       img.update_attributes( :title => attachment["title"], :description => attachment["description"], :navigation_link => attachment["navigation_link"])              
  #     end
  #   end


  #   unless @tbldata.nil?
  #     existing_key_image = @tbldata.find_key_image_of_lot
  #      new_key_image= params["key_image"].select{|images| images unless images["_key_image"].nil? || !images["_destroy"].nil?}
  #      unless new_key_image.empty?
  #        if  new_key_image[0]["plan_id"].blank?
  #         future_key_image = @tbldata.find_gallery_image_of_lot new_key_image.first["id"]                      
  #       key_image_rec = GalleryImagesToLot.find_by_tbldatum_id_and_key_image_and_product_type(params[:lot_id],1, @opted_product )
  #       key_image_rec.update_attributes(:key_image => 0) unless key_image_rec.blank?

  #       lot_rec = GalleryImagesToLot.find_by_tbldatum_id_and_image_gallery_id_and_product_type(params[:lot_id], future_key_image.image_gallery_id, @opted_product)
  #       lot_rec.update_attributes(:key_image => 1) unless lot_rec.blank?
  #       @tbldata.update_attributes(:Key_Image_Url => lot_rec.image_gallery.pics(:thumb_add_images), :Key_Image_Id => lot_rec.image_gallery_id)
  #      end
  #      end
  #     key_image_delete= params["key_image"].select{|images| images unless images["_key_image"].nil? || images["_destroy"].nil?}

  #     unless key_image_delete.empty?
  #         future_key_image_delete = @tbldata.find_gallery_image_of_lot key_image_delete.first["id"]          
  #         @tbldata.delete_key_image_url(future_key_image_delete, existing_key_image) 
  #         ImageGallery.update_key_image_after_deletion(@tbldata.id, params[:community], params[:project])         
  #     end 

  #   unless params["key_image"].blank?
  #   params["key_image"].each do |key_image|    
  #    unless key_image["plan_id"].blank? 
  #       unless key_image["_key_image"].nil? 
  #        @update_flag = "plan image"                       

  #             if key_image["pics_file_name"].blank?
  #               lot_rec = ImageGallery.find_by_plan_id_and_ytube_video_id(key_image["plan_id"], key_image["ytube_video_id"])
  #                @tbldata.update_attributes(:Key_Image_Url => lot_rec.ytube_video_id, :Key_Image_Id => lot_rec.id)
  #             else
  #             lot_rec = ImageGallery.find_by_plan_id_and_pics_file_name(key_image["plan_id"], key_image["pics_file_name"])
  #               @tbldata.update_attributes(:Key_Image_Url => lot_rec.pics(:thumb_add_images), :Key_Image_Id=> lot_rec.id)
  #             end
  #         end
  #     end
  #   end
  #     end

  #     images_to_delete = params["key_image"].map{|images| images["id"].to_i unless !images["_key_image"].nil? || images["_destroy"].nil?}     
  #     unless images_to_delete.compact.empty?        
  #       images = @tbldata.find_all_gallery_image_of_lot images_to_delete
  #       images.each do |img|
  #         img.destroy        
  #       end        
  #     end
  #     if @tbldata.Floor_Plan_Id.split(",").count == 1
  #         if ImageGallery.find_all_by_plan_id(@tbldata.Floor_Plan_Id).blank? && @tbldata.gallery_images_to_lots.count == 0
  #            @tbldata.update_attributes(:Key_Image_Url => "", :Key_Image_Id => 0)
  #         end
  #     end
      
      

  #     Tbldatum.invalidate_lot_cache @tbldata.Unique_ID
  #     render :json=> {:status=>"success", :msg => "Key Image deleted", :update_flag => @update_flag}
  #   end
  # end




def upload_files 
    all_files_array = Array.new
    file_exists_array = Array.new
    @tbldata = Tbldatum.find(params[:lot_id])     
    unless @tbldata.nil?
      if params[:tbldataattachments_attributes].present? && params[:tbldataattachments_attributes][0][:image].present?
          params[:tbldataattachments_attributes].each do |img| 
          all_files = (img[:image].original_filename.include?(" ")) ? img[:image].original_filename.tr!(" ", "_") : img[:image].original_filename
           all_files_array << all_files
          end
          
          all_files_array.each do |each_file|  
          file_exists = @tbldata.tbldataattachments.where("image_file_name =? and product_type = ? and community = ?", each_file, params[:product_type], params[:community]).exists?
            if file_exists
              file_exists_array << each_file+" already exists"
            end
          end          
          unless file_exists_array.empty?
          render :json=> {:status=>"error", :upload_file_errors=>file_exists_array}
          else 
          image_obj = params["tbldataattachments_attributes"]            
          (0...image_obj.count).each do |i|       
              @tbldata.tbldataattachments.create( image_obj[i].merge(:product_type => params[:product_type], :community => params[:community]) )
          end            
          Tbldatum.invalidate_lot_cache @tbldata.Unique_ID  
          render :json=> {:status=>"success"}    
          end
      else
        render :json=> {:status=>"error",:msg=> "Please Upload a File"}
    end
    else
        render :json=> {:status=>"error", :msg=> "Please Upload Again"}
    end
end

  def delete_files
    res_array = Array.new
    @tbldata = Tbldatum.find(params[:lot_id])
    unless @tbldata.nil?                  
      params[:tbldataattachments].each do |attachment|        
        res_array << Tbldataattachment.find(attachment["id"]) unless attachment["_destroy"].nil?
      end
      if res_array.empty? 
        render :json=> {:status=>"Error", :msg => "Select an item to delete"}  
      elsif @tbldata.tbldataattachments.destroy(res_array)
        Tbldatum.invalidate_lot_cache @tbldata.Unique_ID
        render :json=> {:status=>"success", :msg => "Deleted"}    
      else              
        render :json=> {:status=>"error",:msg=> "Please try again"}
      end    
    end 
  end


def upload_images
  i= -1 
  
    all_images_array = Array.new
    gallery_image_exists = Array.new
    images_exists_array = Array.new
    @tbldata = Tbldatum.find(params[:lot_id]) 
    prd_type=params[:product_type]
     floor_plans_count = @tbldata.Floor_Plan_Id.split(",").count unless @tbldata.Floor_Plan_Id.blank?
      if floor_plans_count == 1
        floor_plan_id = @tbldata.Floor_Plan_Id
        plan_images = ImageGallery.find_all_by_plan_id_and_product_type(floor_plan_id, @opted_product) unless floor_plan_id.blank?
      end

     youtuue_url_wout_http = params[:youtube_url].sub(/^https?\:\/\//, '')   
     yo_url_video_id = youtuue_url_wout_http.split("/").last.split("=").last unless youtuue_url_wout_http.blank?
    url_in_gallery = ImageGallery.where("ytube_video_id = ? and product_type = ? and community = ?", yo_url_video_id, params[:product_type], params[:community]).exists?
    gallery_id = ImageGallery.find_by_ytube_video_id_and_product_type_and_community(yo_url_video_id, params[:product_type], params[:community]) unless params[:youtube_url].blank?
    # url_to_lot_exists = GalleryImagesToLot.find_by_tbldatum_id_and_product_type_and_image_gallery_id(params[:lot_id],params[:product_type],gallery_id.id) unless gallery_id.blank?    
    url_to_lot_exists = GalleryImagesToLot.where("tbldatum_id =? and image_gallery_id =? and product_type like ? ", params[:lot_id], gallery_id.id, "%#{prd_type}%" )[0] unless gallery_id.blank?
unless @tbldata.nil?
  if params[:tbldataattachments_attributes].present? && params[:tbldataattachments_attributes][0][:pics].present? || params[:youtube_url].present?
    if params[:tbldataattachments_attributes].present? && params[:tbldataattachments_attributes][0][:pics].present?
        params[:tbldataattachments_attributes].each do |img| 
        all_files = (img[:pics].original_filename.include?(" ")) ? img[:pics].original_filename.tr!(" ", "_") : img[:pics].original_filename
        all_images_array << all_files
      end

      all_images_array.each do |each_image|          
      gallery_image_exists = ImageGallery.where("pics_file_name = ? and product_type LIKE ? and community = ? and plan_id !=?", each_image, "%#{params[:product_type]}%", params[:community], nil).exists?

      # image_id = ImageGallery.find_by_pics_file_name_and_product_type_and_community(each_image, params[:product_type], params[:community])
      image_id = ImageGallery.where("pics_file_name = ? and product_type LIKE ? and community = ?", each_image, "%#{params[:product_type]}%", params[:community])[0]
      # gallery_exists = GalleryImagesToLot.find_by_tbldatum_id_and_product_type_and_image_gallery_id(params[:lot_id],params[:product_type],image_id.id) unless image_id.blank?
      gallery_exists = GalleryImagesToLot.where("tbldatum_id =? and image_gallery_id =? and product_type like ? ", params[:lot_id], image_id.id, "%#{prd_type}%" )[0] unless image_id.blank?
      if gallery_exists
        images_exists_array << each_image+" already exists"
      end

      unless images_exists_array.empty?
        @result = "error"
      else 
        if gallery_image_exists
          i=i+1         

          @tbldata.gallery_images_to_lots.create(:product_type => params[:product_type], :image_gallery_id => image_id.id, :community => params[:community])   
          @result = "success"          
        else 
          i=i+1
        if url_to_lot_exists
            @msg = "error_for_youtube"
        else 
          if url_in_gallery                        
            @tbldata.gallery_images_to_lots.create(:product_type => params[:product_type], :image_gallery_id => gallery_id.id, :community => params[:community])   
            @result = "success"
          else  
             image_obj = params["tbldataattachments_attributes"][i].merge(:product_type => params[:product_type], :community => params[:community])
            unless params[:youtube_url].blank?                 
                y_url=Tbldatum.validate_ytubeurl(params[:youtube_url])
               if ( (y_url =~ /^http[s]?:\/\/www\.youtube\.com\/watch\?v=([a-zA-Z0-9_-]*)/) || (y_url =~ /^http[s]?:\/\/youtu\.be([a-zA-Z0-9_-]*)/) )
                  youtube_rec_id1=ImageGallery.create(:youtube_url => youtuue_url_wout_http, :ytube_video_id => yo_url_video_id, :product_type => params[:product_type], :community => params[:community]) unless params[:youtube_url].blank?     
                  @tbldata.gallery_images_to_lots.create(:product_type => params[:product_type], :image_gallery_id => youtube_rec_id1.id, :tbldatum_id => @tbldata.id, :community => params[:community]) unless youtube_rec_id1.blank? 
                  @image_id=ImageGallery.create(image_obj)
                  @tbldata.gallery_images_to_lots.create(:product_type => params[:product_type], :image_gallery_id => @image_id.id, :tbldatum_id => @tbldata.id, :community => params[:community])   
                  @result = "success"
                else
                  @msg="error_for_valid_url"
                end
              else
                @image_id=ImageGallery.create(image_obj)
                @tbldata.gallery_images_to_lots.create(:product_type => params[:product_type], :image_gallery_id => @image_id.id, :tbldatum_id => @tbldata.id, :community => params[:community])   
                @result = "success"
              end
          end  
        end   
        end
      end
    end
  else
        if url_to_lot_exists
          @msg = "error_for_youtube"
        else 
          if url_in_gallery                        
            @tbldata.gallery_images_to_lots.create(:product_type => params[:product_type], :image_gallery_id => gallery_id.id, :community => params[:community])   
            @result = "success"
          else                   
                y_url=Tbldatum.validate_ytubeurl(params[:youtube_url])
                if ( (y_url =~ /^http[s]?:\/\/www\.youtube\.com\/watch\?v=([a-zA-Z0-9_-]*)/) || (y_url =~ /^http[s]?:\/\/youtu\.be([a-zA-Z0-9_-]*)/) )
                  youtube_rec_id1=ImageGallery.create(:youtube_url => youtuue_url_wout_http, :ytube_video_id => yo_url_video_id, :product_type => params[:product_type], :community => params[:community]) unless params[:youtube_url].blank?     
                  @tbldata.gallery_images_to_lots.create(:product_type => params[:product_type], :image_gallery_id => youtube_rec_id1.id, :tbldatum_id => @tbldata.id, :community => params[:community]) unless youtube_rec_id1.blank? 
                  @result = "success"
                else
                  @msg="error_for_valid_url"
                end
          end  
        end    
  end

    if @result == "success"
         render :json=> {:status=>"success"}        
          GalleryImagesToLot.update_keyimage_if_lot_having_one_image(params[:lot_id] ,params[:community], params[:product_type]) if plan_images.blank?         
          Tbldatum.invalidate_lot_cache @tbldata.Unique_ID
    elsif @msg == "error_for_youtube"
          render :json => {:status => "error", :msg => "Youtube url already exists"}
      elsif @msg == "error_for_valid_url"
        render :json=> {:status=>"error", :msg=>"Please ener valid youtube url"}        
    else
        render :json=> {:status=>"error", :upload_image_errors=>images_exists_array}
    end
    
else
      render :json=> {:status=>"error",:msg=> "Please upload an image or Youtube Url"}
  end
else
      render :json=> {:status=>"error", :msg=> "Please Upload Again"}
end

end

# for uploading and deleting from gallery

  def add_images
        @lot = Tbldatum.find(params[:lot_id])        
        unless @lot.nil?
          existing_key_image = @lot.gallery_images_to_lots.find_by_key_image 1
          params[:image_galleries].each do|img|
              if !img[:image].nil?
                @lot.gallery_images_to_lots.create(:product_type => img[:product_type], :image_gallery_id => img[:id], :community => params[:community])
                GalleryImagesToLot.update_keyimage_if_lot_having_one_image(params[:lot_id], params[:community], params[:product_type])
                @msg = "Added successfully"
              elsif !img[:_delete].nil?
                if existing_key_image == @lot.find_gallery_image_of_lot(img[:id])
                  @lot.update_attributes({:Key_Image_Url => nil, :Key_Image_Id => nil})
                end                
                @lot.image_galleries.destroy(ImageGallery.find(img[:id]))
                @msg = "Deleted successfully"    
              end
          end          
          Tbldatum.invalidate_lot_cache @lot.Unique_ID
          render :json=> {:status=>"success", :msg=> @msg}
        end
  end
  def display_plan_images
     view_images_community_website = images =ImageGallery.find_all_by_plan_id_and_product_type(params[:plan_id], @opted_product)
      key_image = ImageGallery.find_all_by_plan_id_and_plan_key_image_and_product_type(params[:plan_id],1,@opted_product).first 1
      view_images_community_website = images-key_image
      view_images_community_website.unshift(key_image[0])
     render :partial => '/images/display_plan_images', :locals => {:images => images, :view_images_community_website => view_images_community_website, :key_image => key_image, :plan_id => params[:plan_id], :images_count => images.count, :plan_view => params[:plan_view],  :community => params[:community],:key_image => key_image, :view =>""}
  end


end
