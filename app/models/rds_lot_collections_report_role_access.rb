class RdsLotCollectionsReportRoleAccess < ActiveRecord::Base
  attr_protected
  self.table_name = "Rds_Lot_Collections_Reports_Role_Access"  
  belongs_to :role_access,:foreign_key => "Role_Access_Id",:class_name => "Access"
  belongs_to :rds_lot_collections_menu_options, :foreign_key => "Rds_Lot_Collection_Id"
  has_one :report, :foreign_key => "id", :primary_key => "Report_Id"
end
