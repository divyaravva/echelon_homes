var perPage=2;
var end_date=new Date();
var start_date= new Date(end_date.getFullYear(),end_date.getMonth(),01)
end_date=(end_date.getMonth()+1)+"/"+end_date.getDate()+"/"+end_date.getFullYear();
start_date=(start_date.getMonth()+1)+"/"+start_date.getDate()+"/"+start_date.getFullYear();
default_start_date = start_date
default_end_date = end_date
var index_temp=Handlebars.compile($("#index_template").html());
var data_temp=Handlebars.compile($("#tbldata_template").html());
var page_temp=Handlebars.compile($("#page_template").html());
var dropdown_temp=Handlebars.compile($("#dropdown_template").html());
//tbldata backbone model
var TbldataModel= Backbone.Model.extend({
  url : "/reports/generate",
});

var BuildersModel= Backbone.Model.extend({
  url : "/builder/get_builder_list",
});

var BuildersCollection= Backbone.Collection.extend({
  url : "/builder/get_builder_list",
  model: BuildersModel,  
});

//tbldata backbone collection
var TbldataCollection= Backbone.Collection.extend({ 
  url : "/reports/generate",
  model: TbldataModel 
});

var TbldataIndexView= Backbone.View.extend({
  el: $("#report_header"),
  render: function(dataCollection){ 
    var that=this
      buildersCollection.fetch({      
      success: function(){        
        that.$el.html(index_temp({builders:buildersCollection.toJSON() })); 
      $("#anticipated_start_date").datepicker({        
                  showOn: "button" ,
                  buttonText: "Choose",
                  buttonImageOnly: true,
                  buttonImage: "/assets/calendar.gif",
                  closeText: "Close",                  
                  maxDate: end_date                  
                });  
      $("#anticipated_end_date").datepicker({        
                  showOn: "button" ,
                  buttonText: "Choose",
                  buttonImageOnly: true,
                  buttonImage: "/assets/calendar.gif",
                  closeText: "Close",
                  minDate: start_date,
                  maxDate: end_date
                });  

      $("#anticipated_start_date").datepicker( "setDate" , start_date);   
      $("#anticipated_end_date").datepicker( "setDate" , end_date);
      $('#example-getting-started').multiselect({
        maxHeight: 200,
        includeSelectAllOption: true,
        numberDisplayed: 1        
      });
      }  });   
    

  },
  events:{      
    "click #anticipated_close_date_ok": "filterByEventDate",
    "change #anticipated_start_date": "set_end_date",
    "change #anticipated_end_date": "set_start_date"    
  },
  filterByEventDate: function(){
    if($("#anticipated_start_date").val()){
      start_date=$("#anticipated_start_date").val();
    }
    if($("#anticipated_end_date").val()){
      end_date=$("#anticipated_end_date").val();
    }

    var selected = $("#example-getting-started option:selected");
        var selectedBuilders = [];
        selected.each(function () {
           selectedBuilders.push($(this).val())
        });              
    var tbldataCollectionFiltered = tbldataCollection.filter(function(lot){      
      return Date.parse(lot.get("Event_Date")) >= Date.parse(start_date) && Date.parse(lot.get("Event_Date")) <= Date.parse(end_date)
    });
    
    tbldataCollectionFiltered = tbldataCollectionFiltered.filter(function(lot){
      return (_.indexOf(selectedBuilders, lot.get("Builder")) > -1);
    })          
    
    tbldataCollectionFiltered = new TbldataCollection(tbldataCollectionFiltered)    
    tbldataView.render(convert_model_to_json(tbldataCollectionFiltered), $("#allocation_dd :selected").val()+" ("+start_date+" - "+end_date+")", get_report_summary(tbldataCollectionFiltered),$("#allocation_dd :selected").val()); 
  },
  set_end_date: function(){
    $("#anticipated_end_date").datepicker('option', {minDate: $("#anticipated_start_date").val() })
  },
  set_start_date: function(){
    $("#anticipated_start_date").datepicker('option', {maxDate: $("#anticipated_end_date").val() })
  }  
});

//tbldata(tbldata display table) backbone view
var TbldataView= Backbone.View.extend({
  el: $("#report_tbldata"),
  render: function(options, displayName, summary, report_type){    
    this.$el.html(data_temp({tbldata: options,displayName: displayName, summary: summary, report_type: report_type})); 
    $("#tbldata_table").tablesorter(); 
  },
  events:{
   "click #print_excel": "print_excel"
  },
  print_excel: function(ev){
   ev.preventDefault()
   var builders = $("#example-getting-started").val();   
   var report_type=$("#allocation_dd").val()   
   if(report_type==null)
    report_type="";
   window.location="/reports/print_report?report_type="+report_type+"&start_date="+start_date+"&end_date="+end_date+"&builders="+builders;

 } 

});



//tbldropdown(allocation dropdown) backbone view
var TblDropDownView= Backbone.View.extend({  
  el: $("#allocation_list"),
  initialize: function(){
    this.render();
  },
  render: function(){  
                this.$el.html(dropdown_temp({report_type: ["Home Sales","Home Sale Cancellations","Home Sales and Cancellations", "Home Sales by Outside Realtor","Home Sales by Referrer"]}));
  },
    events: {
    "change #allocation_dd":"filterCollection"
  },
  filterCollection: function(ev){ 
    $("#ajax_loader").css("display","block");        
    var that=this;  
    tbldataIndexView.render();
    tbldataCollection.fetch({
      data: { report_type: $("#allocation_dd :selected").val() }, 
      processData: true,
      success: function(){                
                start_date = default_start_date
                end_date = default_end_date
                 var tbldataCollectionFiltered = tbldataCollection.filter(function(lot){      
                    return Date.parse(lot.get("Event_Date")) >= Date.parse(start_date) && Date.parse(lot.get("Event_Date")) <= Date.parse(end_date)
                 });
                tbldataCollectionFiltered = new TbldataCollection(tbldataCollectionFiltered)                
                tbldataView.render(convert_model_to_json(tbldataCollectionFiltered),$("#allocation_dd :selected").val()+" ("+start_date+" - "+end_date+")", get_report_summary(tbldataCollectionFiltered),$("#allocation_dd :selected").val());                
                $("#anticipated_start_date").datepicker("setDate", start_date);
                $("#anticipated_end_date").datepicker("setDate", end_date);
      }
    });      
  }
});

function get_report_summary(lot_collection){
      report_types_obj = lot_collection.groupBy("Event")
      summary={}
      report_types=[]
      for(report_type in report_types_obj){
          report_types.push(report_type)
          if(report_type=="Home Sales"){
            summary["Sales"]=report_types_obj[report_type].length
            report_types_obj[report_type]= new TbldataCollection(report_types_obj[report_type])
            builder_summary = report_types_obj[report_type].groupBy("Builder")                    
            evts_based_on_builder=[]
            for(builder in builder_summary){                      
              evts_based_on_builder.push(builder+"-"+builder_summary[builder].length)
            }                    
            summary["Sales"] = summary["Sales"]+" ("+evts_based_on_builder.join(", ")+")"
          }
          else if(report_type=="Home Sale Cancellation"){
            summary["Cancellations"]=report_types_obj[report_type].length
            report_types_obj[report_type]= new TbldataCollection(report_types_obj[report_type])
            builder_summary = report_types_obj[report_type].groupBy("Builder")                    
            evts_based_on_builder=[]
            for(builder in builder_summary){                      
              evts_based_on_builder.push(builder+"-"+builder_summary[builder].length)
            }                    
            summary["Cancellations"] = summary["Cancellations"]+" ("+evts_based_on_builder.join(", ")+")"
          }
      }
      if(report_types.length>1 && report_types.indexOf("Home Sales")>-1 && report_types.indexOf("Home Sale Cancellation")>-1){
        summary["Net"] = report_types_obj["Home Sales"].length-report_types_obj["Home Sale Cancellation"].length
      }

      return summary
}

function convert_model_to_json(json_data){
  return json_data.map( function(model) { return model.toJSON()} )
}

var tbldataCollection
var tbldataIndexView
var tblDropDownView
var tbldataView

var MyRouter = Backbone.Router.extend({
  routes:{
    "" : "loadPage"
  },
  loadPage: function(){
    tbldataCollection= new TbldataCollection();
    buildersCollection = new BuildersCollection()
    tbldataIndexView= new TbldataIndexView();
    tblDropDownView= new TblDropDownView();
    tbldataView =new TbldataView();      
  }
})
var myRouter= new MyRouter();
Backbone.history.start();
