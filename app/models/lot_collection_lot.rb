class LotCollectionLot < ActiveRecord::Base
  attr_protected
  self.table_name = "lot_collection_lots"
  belongs_to :lot_collection, :foreign_key => "Lot_Collection_Id"
  belongs_to :lot, :class_name => "Tbldatum", :primary_key => "Unique_ID", :foreign_key => "Lot_Number"

  def self.lot_count_for_collection(id)
   	find_by_sql("select Lot_Collection_Id from lot_collection_lots where Lot_Collection_Id=\"#{id}\"").count
  end

 def self.get_builder_id_of_lot(id)
   builder_id = find_by_Lot_Number(id).lot_collection.builder_id
   SalesReps.get_sales_agents(builder_id)
 end

end
   