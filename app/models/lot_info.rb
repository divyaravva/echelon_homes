class LotInfo < ActiveRecord::Base
   attr_protected
  belongs_to :status_of_lot, :foreign_key => 'lot_status_id', :class_name => "LotStatus"
  has_many :gallery_images_to_lots, :dependent => :destroy
  has_many :image_galleries, :through => :gallery_images_to_lots
  

  GalleryImagesToLot

  
  scope :untied, ->{where("SVG_CIRCLE_ID IS NULL")}
  scope :tied, ->{where("SVG_CIRCLE_ID IS NOT NULL")}
  def tied?
      self.SVG_CIRCLE_ID 
  end

end
