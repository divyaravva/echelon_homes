class WarrantyMasterOds < ActiveRecord::Base
  attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_WARRANTYMASTER"

  def self.load_ods_warranty_master(dev_codes,windows_db_nm)

    truncate_ods_warranty_master

    puts "********** 1A) Started Loading WarrantyMasterOds Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
      #message = "Inside the Lots info Load Method"
      #Mailer.notify_user_on_client_failure(message).deliver
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from "+windows_db_nm+".dbo.WARRANTYMASTER where DEVELOPMENTCODE in ("+dev_codes+")  and HOUSENUMBER != '00000000'");
        puts "............................................."
        puts "............................................."

        batch=[]
        batch_size = 400

        result.each do |row|    
          batch << WarrantyMasterOds.new(:COMPANYCODE => row['COMPANYCODE'],
                                            :DEVELOPMENTCODE => row['DEVELOPMENTCODE'],
                                            :HOUSENUMBER => row['HOUSENUMBER'],                                            
                                            :COMPLETION_DATE => row['COMPLETION_DATE'],
                                            :COMMENTS => row['COMMENTS'],
                                            :SERVICE_DATE => row['SERVICE_DATE'],
                                            :SERVICEREPCODE => row['SERVICEREPCODE'],
                                            :NOTE => row['NOTE']
                                      )
            
            if batch.size >= batch_size
              WarrantyMasterOds.import batch
              batch = [] 
            end
        end
        # WarrantyMasterOds.mass_insert(batch, :per_batch => 1000)
        WarrantyMasterOds.import batch
    end

    puts "Total no. of Loaded rows.........#{WarrantyMasterOds.count}"
    puts "********** 1B) Finished Loading WarrantyMasterOds Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_warranty_master
    ActiveRecord::Base.connection.execute("TRUNCATE ODS_WARRANTYMASTER")
  end


end