class MenuOption < ActiveRecord::Base	
	self.table_name="GIS_MenuOption"
	self.primary_key="Id"
	attr_protected

	has_many :product_type_menu_options, :foreign_key => "MenuOptionId"
	has_many :product_types , :through => :product_type_menu_options
	has_one :rds_lot_collections_menu_option , :dependent => :destroy, :foreign_key => "Menu_Option_Id"
	has_one :role_access , :through => :rds_lot_collections_menu_option		

	def self.filter_by_menu_name(menu_name)
		 # self.find_all_by_MenuName(menu_name)
		 @menuname = '"'+menu_name+'"'
		 self.find_by_sql("select Distinct(mo.MenuOptionName) from GIS_MenuOption as mo, 
		 	MenuOption_ProjectType as mp, Project_Type as pt  where mp.ProjectId In 
		 	(select Id from Project_Type where Active=1) and  
		 	mp.MenuOptionId = mo.Id  and mp.ProjectId =  pt.Id
                        and mo.MenuName="+@menuname)

	end
   
	def self.get_menu
		@selected=ProductType.get_ids_of_product
		# self.find_by_sql("select Distinct(MenuName) from GIS_MenuOption as mo , MenuOption_ProjectType as mp where mp.MenuOptionId = mo.Id and mp.ProjectId= "+@selected)
		self.find_by_sql("select Distinct(MenuName) from GIS_MenuOption as mo,MenuOption_ProjectType as mp, tbldatafrmfields as td where mp.MenuOptionId = mo.Id and td.Access_id In (select ID from accesses where name!='No Access') and mp.ProjectId="+@selected+" and td.ID=mo.Id")
	end
	def self.get_all_menu
		# self.find_by_sql("select distinct(MenuName) from GIS_MenuOption as mo, MenuOption_ProjectType as mp 
            # where mp.ProjectId In (select Id from Project_Type where Active=1) and mp.MenuOptionId = mo.Id;")
		self.find_by_sql("select mo.MenuOptionName,mo.MenuName,mo.Id, pt.Project_Name from GIS_MenuOption as mo, 
MenuOption_ProjectType as mp, Project_Type as pt
            where mp.ProjectId In (select Id from Project_Type where Active=1) 
and mp.MenuOptionId = mo.Id and mp.ProjectId = pt.id;")

	end

	def self.project_name(menu_option)
		self.find_by_sql("select distinct(pt.Project_Name) from Project_Type as pt, GIS_MenuOption as mo, MenuOption_ProjectType
as mp where mp.MenuOptionId = mo.Id and mp.ProjectId = pt.id and mo.MenuOptionName='"+menu_option+"'and pt.Active=1" )
		
	end

	def self.get_menu_options_by_project_type product_type	
		product_type = ProductType.find_by_Project_Name(product_type)
		product_type.menu_options
	end


	def self.get_menu_options_by_project_type_id product_type	
		product_type = ProductType.find(product_type)
		product_type.menu_options.where("MenuName !=?", "Reports")
	end

	def self.get_menus1(product_type)
    product_Id = ProductType.find_by_sql("select Id from Project_Type where Project_Name='"+product_type+"'")
	self.find_by_sql("select Distinct(mo.MenuName) from GIS_MenuOption as mo, MenuOption_ProjectType as mp where mp.Id=mo.Id
and mp.ProjectId=#{product_Id[0].Id} and mo.MenuName not in ('Reports','Gallery');").map{|g| g.MenuName}

	end

	def self.get_menus(product_type,user_info,community)
		user_info.roles.map{|role| role.rds_lot_collections}.flatten.map{|rds_lc| rds_lc unless rds_lc.lot_collection.lot_collection_lots.where(:Community=> community).empty?}.compact.map{|rds_lc| rds_lc.rds_lot_collections_menu_options}.flatten.map{|menu| menu.menu_option}.uniq.reject{ |menu| menu.MenuName == "Gallery" }.map{|d| d.MenuName}

	end

	def self.get_sub_menus(menu_name)
		self.find_by_sql("select Distinct MenuOptionName from GIS_MenuOption where MenuName='#{menu_name}'").map{|g| g.MenuOptionName}
	end
	def self.get_lot_status(filter)
		StatusDetail.find_by_sql("select * from GIS_Status_Details where LegendType='#{filter}' group by StatusKey").uniq
	end

	def self.get_communities_list
		Tbldatum.find(:all, :select => 'DISTINCT Community', :order => 'Community ASC').map{|c| [c.Community,c.Community]}.unshift(["select ",""])
	end
	def self.filter_values(product_type)
		# StatusColor.find_by_sql("select distinct(LegendType) from GIS_LotStatusKey where Product_Type='#{product_type}' and LegendType NOT IN ('Floor Plan', 'Lot Clearing', 'Dirt Import', 'Dirt Export')").map{|m| m.LegendType}
		StatusColor.find_by_sql("select distinct(LegendType) from GIS_LotStatusKey where Product_Type='#{product_type}'").map{|m| m.LegendType}
	end

	def self.get_menus_based_on_filter(prdct_ids)
		@menu_options = []
		unless prdct_ids.blank?
		   prdct_ids.each do |each_prct_id|
             @menu_options+= self.get_menu_options_by_project_type_id(each_prct_id)
           end
        end
         @menu_options
	end

	def self.get_uploaded_report_menu_option_id
	   self.find_by_MenuOptionName("Uploaded Reports").Id.to_s
	end

	def self.get_generated_report_menu_option_id
	   self.find_by_MenuOptionName("Generated Reports").Id.to_s
	end

end