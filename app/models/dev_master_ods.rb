class DevMasterOds < ActiveRecord::Base
	attr_protected  
  require 'tiny_tds'

  self.table_name = "ODS_DEVMASTER"

  def self.load_ods_devmaster(windows_db_nm)

  	truncate_ods_devmaster

    puts "********** 1A) Started Loading DevMaster Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from "+windows_db_nm+".dbo.DevMaster");
      	puts "............................................."
      	puts "............................................."

        batch=[]
        batch_size = 1000

        result.each do |row|
          #DevMaster.find_or_initialize_by_COMPANYCODE_and_DEVELOPMENTCODE_and_LOTNUMBER(row['COMPANYCODE'], row['DEVELOPMENTCODE'], row['LOTNUMBER'])
          batch << DevMasterOds.new(row)
            if batch.size >= batch_size
              DevMasterOds.import batch
              batch = [] 
            end  
        end
        # DevMasterOds.mass_insert(batch, :per_batch => 1000)
        DevMasterOds.import batch
    end

    puts "Total no. of Loaded rows.........#{DevMasterOds.count}"
    puts "********** 1B) Finished Loading DevMaster Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_devmaster
		ActiveRecord::Base.connection.execute("TRUNCATE ODS_DEVMASTER")
	end


end