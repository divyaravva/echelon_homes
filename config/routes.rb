Ghmmap::Application.routes.draw do
  match 'contact_forms/contact_form_list' => 'contact_forms#contact_form_list', :via => :post
  match 'contact_form/contact_form_list' => 'contact_form#contact_form_list', :via => :post
  match '/lot_bases/cost_codes_for_category' => 'lot_bases#cost_codes_for_category', :via => :get
  match '/images/display_plan_images' => 'images#display_plan_images', :via=> :get
  match '/products/render_street_numbers' => 'products#render_street_numbers', :via => [:get, :post]
  match '/set_time_zone/save' => 'set_time_zone#save', :via => :post  
  match '/set_time_zone/get_zones' => 'set_time_zone#get_zones', :via => :get  
  resources :set_time_zone, :zero_count_statuses, :lot_bases
  match '/tbldata/model_type_filtering' => 'tbldata#model_type_filtering' , :via => :post
  match '/roles/destroy' => 'roles#destroy', :via => :get
  match '/reports/get_logs' => 'reports#get_logs'
  match '/reports/get_revenue_records' => 'reports#get_revenue_records'
  match '/reports/get_generated_reports' => 'reports#get_generated_reports'
  match '/reports/get_generated_reports_for_edit_access' => 'reports#get_generated_reports_for_edit_access'
  match '/activity_log/get_logs' => 'activity_log#get_logs'
  match '/activity_log/:lot_id' => 'activity_log#index'    
  resources :activity_log
 match '/lot_types/destroy' => 'lot_types#destroy', :via => :get
 match '/lot_types/change_status' => 'lot_types#change_status', :via => :get
  resources :lot_types
  match '/lotbuzz/lead_request' => 'lotbuzz#lead_request', :via => [:get, :post]
  match '/lot_status/status_update' => 'lot_status#status_update', :via => :get
  match '/lot_status/display_names_for_status' => 'lot_status#display_names_for_status', :via => :get
  match '/lot_status/display_names' => 'lot_status#display_names', :via => :post

  match '/status_color/get_filter_values' => 'status_color#get_filter_values', :via => :get
  match '/status_color/get_community' => 'status_color#get_community', :via => :get
  match '/status_color/get_menu' => 'status_color#get_menu', :via => :get
  match '/status_color/get_sub_menu' => 'status_color#get_sub_menus', :via => :get
  match '/status_color/get_lot_status' => 'status_color#get_lot_status', :via => :get
  match '/lotbuzz/render_communitylegend_view' => 'lotbuzz#render_communitylegend_view', :via => :get  

  match '/lot_collection/destroy' => 'lot_collection#destroy', :via => :get
  match '/roles/roles_status' => 'roles#roles_status', :via => :get
  match '/lot_collection/lot_status' => 'lot_collection#lot_status', :via => :get
  match '/builder_floor_types/product_type_status' => 'builder_floor_types#product_type_status', :via => :get
  match '/builder/builder_status' => 'builder#builder_status', :via => :get
  match '/sales_reps/sales_rep_status' =>'sales_reps#sales_rep_status', :via => :get
  match '/referrals/referral_type_status' => 'referrals#referral_type_status', :via => :get
  match '/plan_field_display_name/display_names' => 'plan_field_display_name#display_names', :via => :post

  match 'lotbuzz/lotbuzz_view' => 'lotbuzz#lotbuzz_view', :via => :get
  match '/lotbuzz/:community/:parcel' => 'lotbuzz#index', :via => :get
  match '/update_product_selection' => 'products#update_product_selection', :via => :post
  match '/admin' => 'products#select_product', :via => :get
  match '/:community/:parcel/reports/delete' => 'reports#delete', :via => :get
  match '/reports/print_report' => 'reports#print_report', :via => :get  
  match '/reports/delete' => 'reports#delete', :via => :get  
  match '/reports/generate' => 'reports#generate', :via => :get  
  match '/reports/activity_log' => 'reports#activity_log', :via => :get  
  match '/reports/update_uploaded_reports_info' => 'reports#update_uploaded_reports_info', :via => :get
  match '/reports/fetch_spec_aging_lots' => 'reports#fetch_spec_aging_lots', :via => :get  
  match '/reports/save_spec_aging_rec' => 'reports#save_spec_aging_rec', :via => :post
  match '/reports/update_spec_aging_lots' => 'reports#update_spec_aging_lots', :via => :post
  match '/reports/delete_spec_aging_rec' => 'reports#delete_spec_aging_rec', :via => :post
      
  match '/reports/update_lot_new_stake_date_owned' => 'reports#update_lot_new_stake_date_owned', :via => :post
  match '/reports/update_lot_new_stake_date_optioned' => 'reports#update_lot_new_stake_date_optioned', :via => :post
  match '/reports/save_lot_aging_contract_terms_summary' => 'reports#save_lot_aging_contract_terms_summary', :via => :post
  match '/reports/fetch_lot_aging_data' => 'reports#fetch_lot_aging_data', :via => :get
  match '/reports/update_lot_aging_contract_terms_summary' => 'reports#update_lot_aging_contract_terms_summary', :via => :post
  match '/reports/delete_contract_term_summary' => 'reports#delete_contract_term_summary', :via => :post
  match '/reports/update_budget_amount' => 'reports#update_budget_amount', :via => :post
  
  resources :reports
  match '/communities/render_community_view' => 'communities#render_community_view', :via => :get  
  match "/communities/render_community_legend_view" => 'communities#render_community_legend_view', :via => :get  
  match '/products/:project/communities/:community/:parcel' => 'tbldata#index', :via => [:get, :post]
  match '/products/:project/communities/:community' => 'tbldata#index', :via => [:get, :post]

  match '/status_key/destroy' => 'status_key#destroy', :via => :get

  match '/status_key/lot_plan_status' => 'status_key#lot_plan_status', :via => :get

  match '/status_key/render_statuskey_list' => 'status_key#render_statuskey_list', :via => :get

  match '/status_key/display_names' => 'status_key#display_names', :via => :get

  match '/status_key/edit_plan_images/:plan_id' => 'status_key#edit_plan_images' 

  match '/status_key/view_plan_images/:plan_id' => 'status_key#view_plan_images' 

  resources :status_key do
    collection do
      post "delete_images"
      post "upload_images"  
    end 
  end
  resources :contact_forms
  resources :contact_form

  resources :lot_status
  resources :spec_aging
  resources :option_lots_status
  match '/products/:project/communities/:community/:parcel/:menu/:menuoption/:access_id' => 'tbldata#index', :via => [:get, :post]
  match '/products/:project/communities/:community/:parcel/:menu/:menuoption' => 'tbldata#index', :via => [:get, :post]
  match '/products/:project/communities/:community/:menu/:menuoption/:access_id' => 'tbldata#index', :via => [:get, :post]
  match '/products/:project/communities/:community/:menu/:menuoption' => 'tbldata#index', :via => [:get, :post]

  
  match '/products/:project' => 'products#get_selected_product'  
  resources :communities
  resources :products  
  resources :status_color
  match '/lot_collection/parcels_of_community' => 'lot_collection#parcels_of_community', :via => :get  
  match '/lot_collection/lots_of_parcel' => 'lot_collection#lots_of_parcel', :via => :get  
  match 'lot_collection/total_communities' => 'lot_collection#total_communities', :via => :get
  match '/lot_collection/lot_numbers_of_house_type' => 'lot_collection#lot_numbers_of_house_type', :via => :get  
  match '/lot_collection/lots_of_community' => 'lot_collection#lots_of_community', :via => :get
  resources :lot_collection
  match '/floor_plan_collection/plan_types_of_builder' => 'floor_plan_collection#plan_types_of_builder', :via => :get  
  match '/floor_plan_collection/plans_of_plan_type' => 'floor_plan_collection#plans_of_plan_type', :via => :get  
  resources :floor_plan_collection

  match '/sales_reps/index_sales_reps_of_builder' => 'sales_reps#index_sales_reps_of_builder', :via => :get


  match '/roles/get_lot_collections' => 'roles#get_lot_collections', :via => :get  
  match '/roles/get_role_view_per_collection' => 'roles#get_role_view_per_collection', :via => :get 
  match '/roles/get_views_based_on_product_type' => 'roles#get_views_based_on_product_type', :via => :get  
  match '/roles/get_reprots_based_on_product_type' => 'roles#get_reprots_based_on_product_type', :via => :get   
  match '/roles/get_reports_based_on_report_type' => 'roles#get_reports_based_on_report_type', :via => :get   
  resources :roles
  match '/builder/get_builder_list' => 'builder#get_builder_list', :via => :get  
  
  resources :builder
  resources :sales_reps
  resources :referrals
  
  match '/manage_click_fields/display_names_for_lot' => 'manage_click_fields#display_names_for_lot', :via => :get
  match '/manage_click_fields/update_display_names' => 'manage_click_fields#update_display_names', :via => :post
    resources :manage_click_fields
  
  match '/users/change_status_and_date' => 'users#change_status_and_date', :via => :get
  resources :builder_floor_types
  resources :users do
    post :update_info, on: :collection
    post :delete_info, on: :collection
    post :create_info, on: :collection
    post :reinvite_info, on: :collection
  end  

  resources :user_groups do 
    post :update_info, on: :collection
    # post :delete_info, on: :collection
    post :create_info, on: :collection
  end 
  match '/user_access_levels/change_group_view' => 'user_access_levels#change_group_view', :via => :get
  match '/user_access_levels/update_info' => 'user_access_levels#update_info', :via => :post
  match '/user_access_levels/create_user_group' => 'user_access_levels#create_user_group', :via => :post
  match '/user_access_levels/create_user_group_view' => 'user_access_levels#create_user_group_view', :via => :get
  match '/user_access_levels/edit_user_group_view' => 'user_access_levels#edit_user_group_view', :via => :get
  match '/user_access_levels/delete_user_group' => 'user_access_levels#delete_user_group', :via => :get
  

  resources :user_access_levels do     
  end

  resources :uploadsvgs
 

    match '/tbldata/render_role_view' => 'tbldata#render_role_view', :via => [:get, :post]
    match '/tbldata/render_legend_view' => 'tbldata#render_legend_view', :via => [:get, :post]
    match '/tbldata/rollover_data/' => 'tbldata#rollover_data', :via => :get
    match '/tbldata/loading_data/' => 'tbldata#loading_data', :via => :get
    match '/tbldata/render_view_if_role_nor_exist' => 'tbldata#render_view_if_role_nor_exist', :via => :get
  resources :tbldata do
	  get "lots_list", :on => :member
	  collection do 
		post "upload"
		get "admin"
		get "map_lot_with_svg"    
	  end
  end

  match '/lots/show_parcels_based_on_community' => 'lots#show_parcels_based_on_community', :via => :get
  match '/lots/show_map_based_on_parcel' => 'lots#show_map_based_on_parcel', :via => :get
  match 'parcels/show_map_based_on_community' => 'parcels#show_map_based_on_community', :via => :get
  match '/products/:project/show_map_based_on_community' => 'parcels#show_map_based_on_community', :via => :get
  match '/parcels/tie' => 'parcels#tie', :via => :get
  match '/parcels/retie' => 'parcels#retie', :via => :get
  match '/lots/tie' => 'lots#tie', :via => :get
  match '/lots/retie' => 'lots#retie', :via => :get
  match '/products/:project/tie_retie_parcels' => 'parcels#tie_retie'
  match '/products/:project/tie_retie_lots' => 'lots#tie_retie'

  resources :lots do
    put :tie, on: :collection
    put :retie, on: :collection
    get :tie_retie, on: :collection
  end
  resources :parcels do
    put :tie, on: :collection
    put :retie, on: :collection
    get :tie_retie, on: :collection
  end

 post "home/show"
 match '/home/sign_out' => 'home#sign_out', :as => :signout
  resources :home do
	collection do
		get "mainpage"
		get "sign_out"
		get "sign_in"
    post "sign_in_process"
    get "reset_pwd"
      get "registration"
      post "update_registration"
      get "updateactive"
      get "forgotpwd"
      post "update_password"
      post "update_forgotpwd"
      get "confirm_forgotpwd"
      post "confirm_forgotpwd_signin"
      get "confirm"
	end 
  end
  resources :images do
    collection do
      post "delete_images"
      post "upload_images"
      post "upload_files"
      post "delete_files"
    end 
  end

  match '/images/display/:lot_id' => 'images#display'
  match '/images/view_docs/:lot_id' => 'images#view_docs'
  match '/images/edit/:lot_id' => 'images#edit'  
  match '/images/edit/:lot_id/:user_role' => 'images#edit'  
  match '/images/display_docs/:lot_id' => 'images#display_docs'

  match '/images/add_images' => 'images#add_images'

  match '/data_load_statuses/fetch_date_and_status' => 'data_load_statuses#fetch_date_and_status', :via => :get
  match '/data_load_statuses/data_refresh' => 'data_load_statuses#data_refresh', :via => :get
  match '/data_load_statuses/check_refresh_completed' => 'data_load_statuses#check_refresh_completed', :via => :get

  match '/image_gallery/upload_view' => 'image_gallery#upload_view', :via => :get
  match '/image_gallery/save_images' => 'image_gallery#save_images', :via => :post
  match '/image_gallery/delete_images' => 'image_gallery#delete_images', :via => :post
  match '/image_gallery/gallery_items/:lot_id' => 'image_gallery#gallery_items', :via => :get
  match '/image_gallery/index' => 'image_gallery#index', :via => :get
  match '/products/:project/communities/:community/:parcel/image_gallery/index' => 'image_gallery#index', :via => :get
  match '/image_gallery/show_images_based_on_product_type' => 'image_gallery#show_images_based_on_product_type', :via => :get
  resources :image_gallery

  match '/lotbuzz/demo' => 'lotbuzz#demo', :via => :get
  match '/lotbuzz/index' => 'lotbuzz#index', :via => :get
  match '/lotbuzz/:community' => 'lotbuzz#community_view_index', :via => :get
  resources :lotbuzz
   root :to => 'home#sign_in'
 
end
