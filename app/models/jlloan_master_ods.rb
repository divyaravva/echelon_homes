class JlloanMasterOds < ActiveRecord::Base
  attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_JLLOANMASTER"

  self.primary_key = :LOANNUMBER

   has_one :jl_bank_master_file, :class_name => "JlBankMasterFileOds", :foreign_key => [:BANKCODE],:primary_key => [:BANKCODE], :conditions => {:COMPANYCODE => '001'}    
  def self.load_ods_jlloan_master(windows_db_nm)

    truncate_ods_jlloan_master

    puts "********** 1A) Started Loading JlloanMasterOds Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from "+windows_db_nm+".dbo.JLLOANMASTER");
        puts "............................................."
        puts "............................................."

        batch=[]
        batch_size = 400

        result.each do |row|    
          batch << JlloanMasterOds.new(:COMPANYCODE => row['COMPANYCODE'],
                                      :LOANNUMBER => row['LOANNUMBER'],
                                      :BANKCODE => row['BANKCODE'],
                                      :CATEGORYCODE => row['CATEGORYCODE'],
                                      :COSTCODE => row['COSTCODE'],
                                      :TODATEDRAWNAMT => row['TODATEDRAWNAMT']                                          
                                    )
            
            if batch.size >= batch_size
              JlloanMasterOds.import batch
              batch = [] 
            end
        end
        JlloanMasterOds.import batch
    end

    puts "Total no. of Loaded rows.........#{JlloanMasterOds.count}"
    puts "********** 1B) Finished Loading JlloanMasterOds Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_jlloan_master
    ActiveRecord::Base.connection.execute("TRUNCATE ODS_JLLOANMASTER")
  end


end