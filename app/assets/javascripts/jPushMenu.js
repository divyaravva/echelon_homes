/*!
 * jPushMenu.js
 * 1.1.1
 * @author: takien
 * http://takien.com
 * Original version (pure JS) is created by Mary Lou http://tympanus.net/
 */

(function($) {
		
	$.fn.jPushMenu = function(customOptions) {
		var o = $.extend({}, $.fn.jPushMenu.defaultOptions, customOptions);
		
		/* add class to the body.*/
		
		$('body').addClass(o.bodyClass);
		$(this).addClass('jPushMenuBtn');
		$(this).click(function() {
			// $("#filter_toggle").hide();
			var target         = '',
			push_direction     = '';
			
		
			if($(this).is('.'+o.showLeftClass)) {
				target         = '.cbp-spmenu-left';
				push_direction = 'toright';
			}
			else if($(this).is('.'+o.showRightClass)) {
				target         = '.cbp-spmenu-right';
				push_direction = 'toleft';
			}
			else if($(this).is('.'+o.showTopClass)) {
				target         = '.cbp-spmenu-top';
			}
			else if($(this).is('.'+o.showBottomClass)) {
				target         = '.cbp-spmenu-bottom';
			}
			

			$(this).toggleClass(o.activeClass);
			$(target).addClass(o.menuOpenClass);
			$("#filter_legend").show(100);
			$(".cta_form").hide(100);
			// $("#filter_toggle").hide();
			if($(this).is('.'+o.pushBodyClass)) {
				$('body').toggleClass( 'cbp-spmenu-push-'+push_direction );
			}
			
			/* disable all other button*/
			$('.jPushMenuBtn').not($(this)).toggleClass('disabled');
			
			return false;
		});
		var jPushMenu = {
			close: function (o) {
				$('.jPushMenuBtn,body,.cbp-spmenu').removeClass('disabled active cbp-spmenu-open cbp-spmenu-push-toleft cbp-spmenu-push-toright');
				$("#filter_legend").hide()
			}
		}
		
		if(o.closeOnClickOutside) {
			 // $("body").click(function() { 
				// jPushMenu.close();
			 // }); 
			$("body").click(function(e) {
		        if (e.target.id == "filter_toggle"| $(e.target).parents("#filter_toggle").size()) {
		        }
		         else {
		         //    jPushMenu.close();
		         //    // $("#filter_toggle").show();
		         //    if(document.getElementById("alphasvg") != null){
			        //     document.getElementById("alphasvg").contentDocument.onclick = function (e) {
			        //     	if (window.matchMedia('(max-width: 767px)').matches){
			        //     		$(".offcanvas-toggle").removeClass("is-open").show();
			        //     	}
			        //     	$("#js-bootstrap-offcanvas").removeClass("in");
			        //     	$('body').removeClass('offcanvas-stop-scrolling');
			        //     }
			        // }
					 
					
		        }
		    });


			// document.getElementById("alphasvg").contentDocument.onclick = function (e) {jPushMenu.close();}
			 // $('.cbp-spmenu').mouseleave(function(){
			 // 	jPushMenu.close();
			 // });
		
			 $('.cbp-spmenu,.toggle-menu').click(function(e){ 
				 e.stopPropagation(); 
			 });
		 }
	};
 
   /* in case you want to customize class name,
   *  do not directly edit here, use function parameter when call jPushMenu.
   */
	$.fn.jPushMenu.defaultOptions = {
		bodyClass       : 'cbp-spmenu-push',
		activeClass     : 'menu-active',
		showLeftClass   : 'menu-left',
		showRightClass  : 'menu-right',
		showTopClass    : 'menu-top',
		showBottomClass : 'menu-bottom',
		menuOpenClass   : 'cbp-spmenu-open',
		pushBodyClass   : 'push-body',
		closeOnClickOutside: true
	};
})(jQuery);