class ApplicationController < ActionController::Base
  protect_from_forgery
  # before_filter :profiler_testing
  before_filter :chosen_menus
  # def profiler_testing
  #   Rack::MiniProfiler.authorize_request
  # end
  def is_super_admin        
      authenticate_or_request_with_http_basic do |username, password|      
        username == "sadmin" && password == "lotvue"
    end
  end

  def authenticate
  	if session[:current_user].nil?
  		redirect_to "/home/sign_in"
  	else
  	 @user = session[:current_user]
  	   @udata = Tblusers.find_user @user
  	end
  end

 # To access current user in models
  def set_current_user
    Tblusers.current = session[:current_user]    
  end


  def has_roles    
    unless @udata.nil?
      if @udata.get_roles_count == 0
        render  "/home/no_roles"
      end
    end
  end

  def is_admin    
  	unless @udata.is_admin?
  		render "/home/unauthorised" 	
  	end 
  end

  def convert_to_byte(boolean)
    return boolean.eql?("1") ? "\x01" : "\x00"
  end

  def convert_to_float(string)
    return string.to_f()
  end

 def get_product_view      
  pjct_id = ProductType.find_by_Project_Name("Lot Insight").Id
  unless @udata.blank?
    if(@udata.roles.map{|role| role.rds_lot_collections}.flatten.map{|x|x.Product_Type_Id.split(",")}.flatten.uniq.include?(pjct_id.to_s) == true)
     @opted_product = params[:project].nil?? "Lot Insight" : params[:project]
     @chosen_project = params[:project].nil?? "Lot Insight" : params[:project] 
    else
     @opted_product = params[:project].nil?? "Lot Buzz" : params[:project]
     @chosen_project = params[:project].nil?? "Lot Buzz" : params[:project] 
    end
  else
    @opted_product = params[:project].nil?? "Lot Insight" : params[:project]
    @chosen_project = params[:project].nil?? "Lot Insight" : params[:project] 
  end
   @previous_selected_pro = params[:project].nil?? session[:project] : params[:project]
  end  

  def get_community_city
    # @comm_cities = @udata.roles.map{|role| role.rds_lot_collections}.flatten.map{|x|x.community_city}.flatten.uniq
    @comm_cities = @udata.get_role_lot_collections.map{|lcl| lcl.community_city}.uniq.compact
  end

  def get_comm_city_view
    @opted_comm_city = params[:comm_city].nil?? nil : params[:comm_city]
    @chosen_comm_city = params[:comm_city].nil?? "Select City" : params[:comm_city]  
    @previous_selected_com_city = params[:comm_city].nil?? session[:comm_city] : params[:comm_city]
  end
  # private
  def get_product    
    @project = ProductType.get_product    
  end

  def get_communities
    @communities = @udata.get_villages(@opted_product)
    # @communities = @udata.get_villages(@opted_product, @opted_comm_city)
  end

  def get_communities_view  
    @opted_community = params[:community].nil?? nil : params[:community]
    @opted_community_map = params[:community].nil?? nil : params[:community].delete(" ")
    @chosen_community = params[:community].nil?? "Select Community" : params[:community]  
    @previous_selected_com = params[:community].nil?? session[:community] : params[:community]
  end

  def get_parcels    
    @parcels = @udata.get_plats(params[:community]).sort
    @opted_parcel = params[:parcel].nil?? nil : params[:parcel]
    @chosen_parcel = params[:parcel].nil?? "Select Phase" : params[:parcel]
  end

  def chosen_menus
    @menu= params[:menu]
    @menuoption= params[:menuoption]
    @access_id = params[:access_id]
  end
  
  def get_parcels_view    
    @previous_selected_phase = params[:parcel].nil?? session[:parcel] : params[:parcel]
  end

  def set_res_header
    response.headers["Content-Type"] = "text/html; charset=utf-8"
  end

end
