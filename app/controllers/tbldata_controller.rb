class TbldataController < ApplicationController
  before_filter :authenticate
  before_filter :has_roles
  before_filter :get_product
  before_filter :get_product_view
  before_filter :get_comm_city_view
  before_filter :get_community_city
  before_filter :get_communities  
  before_filter :get_communities_view    
  before_filter :get_parcels    
  # before_filter :get_parcels_view


  # GET /tbldata
  # GET /tbldata.json
  def index   
    if params[:parcel].blank?
      @previous_selected_phase = ''
    else
      @previous_selected_phase = params[:parcel].nil?? session[:parcel] : params[:parcel]
    end
    @role = params[:role]
    @parent_tab_id=params[:parent_tab_id]
    # @access_permission=params[:access_permission]     
    @udata=Tblusers.find(session[:current_user]) if session[:current_user].present?
  end

  def admin
    raise "adminpage".inspect
  end

  def render_view_if_role_nor_exist
    render :partial => "tbldata/render_view_if_role_nor_exist"
  end

  # GET /tbldata/1
  # GET /tbldata/1.json
  def show
    @tbldatum = Tbldatum.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @tbldatum }
    end
  end

  # GET /tbldata/new
  # GET /tbldata/new.json
  def new
    @tbldatum = Tbldatum.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @tbldatum }
    end
  end

  # GET /tbldata/1/edit
  def edit
    @tbldatum = Tbldatum.find(params[:id])
  end

  # POST /tbldata
  # POST /tbldata.json
  def create
    @tbldatum = Tbldatum.new(params[:tbldatum])

    respond_to do |format|
      if @tbldatum.save
        format.html { redirect_to @tbldatum, notice: 'Tbldatum was successfully created.' }
        format.json { render json: @tbldatum, status: :created, location: @tbldatum }
      else
        format.html { render action: "new" }
        format.json { render json: @tbldatum.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /tbldata/1
  # PUT /tbldata/1.json
  def update
    @tbldatum = Tbldatum.find(params[:id])

    respond_to do |format|
      if @tbldatum.update_attributes(params[:tbldatum])
        format.html { redirect_to @tbldatum, notice: 'Tbldatum was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @tbldatum.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tbldata/1
  # DELETE /tbldata/1.json
  def destroy
    @tbldatum = Tbldatum.find(params[:id])
    @tbldatum.destroy

    respond_to do |format|
      format.html { redirect_to tbldata_url }
      format.json { head :no_content }
    end
  end

  def upload  
    @tbldata = Tbldatum.find(params[:idlot])
    # tbldata = Tbldatum.find(params[:idlot])    
    set_date_attrs 
 
    save_floor_plans
    
    if params[:lot_type].present?
      x = LotType.find_by_Lot_Type(params[:lot_type])
      params[:lot_type] = x.id;
  end
  
    set_radio_attrs
    # previous_home_status = tbldata.Status
    # before_save_lot_object = tbldata
    @tbldata.set_attrs(params)
    attr_changes = @tbldata.changes                      
    if @tbldata.save
      Tbldatum.invalidate_entire_lot_cache @tbldata.Unique_ID      

      render :json => {"result"=> {"status" => "success"}}
    else
      if @tbldata.errors.any?
        render :json => {"result"=> {"status" => "notvalid", "errors_msgs" => @tbldata.errors.messages.values.flatten }}
      else
        render :json =>{"result"=> {"status" => "failed"}}
      end 
    end

  end

  def save_floor_plans  
    if params[:floor_plans].blank?
      floor_plans = params[:floor_plans]
      floor_plan = ''
    else  
    floor_plans = params[:floor_plans].split(",")
    floor_plans.join(", ")  
    floor_plan=""
    floor_plans.each_with_index do |d,i|
      status_id= StatusDetail.find_by_StatusKey_and_Community(d, params[:community])
      if i != floor_plans.length-1
        floor_plan = floor_plan+"#{status_id.id}"+","
      else
        floor_plan = floor_plan+"#{status_id.id}"
      end
    end
  end
    @tbldata.update_attributes( :Floor_Plan_Id => floor_plan)
  end

  def set_date_attrs
      data_params=[:lscd, :hscd, :elrd, :cdrdate, :lotcomplete, :csdt , :ccd, :hold_until, :ars, :contract_date,
       :lndate, :lnmdate, :ldrdate, :rdfld, :don,:mdcfn, :dor, :doreass, :ueberd, :ueburd, :arcsd, :arcad,
        :ppstgd, :ppad, :usfhmd,:uafhmd,:acd]
      data_params.each do |val|
        if params.has_key?(val)        
          params[val]=(params[val].blank?) ? "" : DateTime.strptime(params[val], "%m/%d/%Y")
          params[val]=(params[val].blank?) ? "" : params[val]
        end
      end
  end

  def set_radio_attrs
    data_params=[:wuc, :mkd, :gs, :ls, :sfc, :ace, :duce, :model, :cnr, :rtbys, :corner, :clnr,:green_street,:subordination_fee, :lso, :ccnr]
    data_params.each do |val|
      if params.has_key?(val)
        params[val]=convert_to_byte(params[val])
      end
    end
  end

  def lots_list
    @lots = Tbldatum.find(:all, :conditions => ["Filing=? AND SVG_ID IS NULL", params[:id]], :select => "ID, Lot_N")
    render :json => @lots
  end

  def map_lot_with_svg
    @lot = Tbldatum.find(:first, :conditions => ["ID=?", params[:id]])
    @lot.update_attribute("SVG_ID", params[:svg_id])
    render :text => :success
  end

  def render_legend_view 
    @role = params[:role]
    @parent_tab_id=params[:parent_tab_id]
    session[:mainmenu] = @parent_tab_id
    session[:menuoption] = @role
    # @access_permission_id=params[:access_permission]
    # @access_permission = Access.get_name_by_id(params[:access_permission])
    @is_touch = params[:is_touch]
    @opted_community = params[:community]
    @opted_comm_city = params[:comm_city]    
    @opted_parcel = params[:phase_parcel]
    @opted_product = params[:project]
    @type = params[:legendtype]
    @legend_keys = params[:legend_keys]

    @filter_type = params[:filter_type]
    @legend_data = StatusDetail.get_legend_values(@type, @opted_community, @role)
    @legendtype_array = @legend_data.map(&:LegendType).uniq
    @type_check = @legendtype_array.include?(@type)
    @model_type_filtering = params[:model_filter_vals]
    @model_type_checked = params[:model_filter_checked]
    @second_filter_ids = params[:secondary_filter_ids]
    @menu_option_id = MenuOption.find_by_MenuOptionName("#{@role}").Id
    if @legend_keys.blank?
      @all_lots = @udata.get_lots(@opted_community, @opted_parcel, @menu_option_id).uniq 
    else
      @all_lots = @udata.get_lots_for_hover(@opted_community, @opted_parcel, @legend_keys, @role, @udata, @menu_option_id,@filter_type, @model_type_filtering, @model_filter, @second_filter_ids).uniq
    end
    render :partial => "tbldata/commonlegend"

  end


  def render_role_view
    @svg_id = params[:svg_id]   
    @is_admininstrator = @udata.is_admin?
    menu_option_name = params[:role].downcase.gsub(' ','_')
    @menuoptionname = params[:role]
    @menuoption = 'Home Sales Status'
    @opted_community = params[:community]
    @opted_parcel = params[:phase_parcel]    
    @opted_product = params[:project]    
    @opted_comm_city = params[:comm_city]    
    @parcel_map = params[:phase_parcel].delete(" ") unless params[:phase_parcel].nil?
    @role = params[:role]
    @parent_tab_id=params[:parent_tab_id]
    @filter = (params[:filter].nil? || params[:filter].blank?)? nil : params[:filter]
    @legend_keys = params[:legend_keys]
    @tab_name = params[:role]
    @filter_type = params[:filter_type]
    @is_touch = params[:is_touch]  
    session[:mainmenu] = @parent_tab_id
    session[:menuoption] = @role
    @model_type_filtering = params[:model_filter_vals]
    @model_type_checked = params[:model_filter_checked]
    @second_filter_ids = params[:secondary_filter_ids]
    @menu_option_id = MenuOption.find_by_MenuOptionName("#{@menuoptionname}").Id
    if @filter.nil?  
      @click_data = @udata.get_lots(@opted_community, @opted_parcel, @menu_option_id)
      @hover_data = @click_data 
    else      
       @click_data = @udata.get_lots(@opted_community, @opted_parcel, @menu_option_id) 
       @all_lots = @hover_data = @udata.get_lots_for_hover(@opted_community, @opted_parcel, @legend_keys, @menuoptionname, @udata, @menu_option_id, @filter_type, @model_type_filtering, @model_filter, @second_filter_ids).uniq 
       # @all_lots = Tbldatum.get_filtered_lot_colors(@legend_keys, params[:role], @opted_community, @udata, @opted_parcel)
    end
    if @is_touch == "1"
         if @tab_name == "Uploaded Reports"
            @tab_name ="Uploaded Reports"
            @reports = Report.get_the_report_based_on_current_tab(@udata)
            render :template => "reports/view", :layout => false
          elsif @tab_name == "Generated Reports"
            render :partial => "tbldata/"+menu_option_name
          else
            render :partial => "tbldata/"+menu_option_name+"_touch"     
          end
    # elsif @parent_tab_id != "Reports"
        #render :partial => "tbldata/"+menu_option_name     
        # render :partial => "tbldata/"+params[:community].downcase+"_"+menu_option_name
    else
      if @tab_name == "Uploaded Reports"
        @tab_name ="Uploaded Reports"
        @reports = Report.get_the_report_based_on_current_tab(@udata)
        render :template => "reports/view", :layout => false  
      else
        render :partial => "tbldata/"+menu_option_name
      end
    end   
    
  end

  def model_type_filtering
    @model_type_filtering = params[:model_type_filtering]
    @opted_community = params[:community]
    @opted_parcel = params[:parcel_name]
    @all_filtering_lots = params[:all_lots]
    @model_filter_id = params[:id]
    @clicked = "true"
    render :partial => "tbldata/model_type_filtering"
  end

end
