class OptionLotsStatusController < ApplicationController

  before_filter :authenticate
  before_filter :is_admin
  before_filter :get_product
  before_filter :get_product_view
  before_filter :get_comm_city_view
  before_filter :get_community_city      
  before_filter :get_communities   
  before_filter :get_communities_view
  before_filter :get_parcels
  before_filter :get_parcels_view

  def index
    @option_lots=OptionLotsStatus.first
  end

  def show
    
  end

  def destroy    
    
  end

  def new     
  end

  def create   	
  end  

  def edit 
    @option_details = OptionLotsStatus.first unless params[:id].nil?
    if @option_details.option_lots == "Yes"
       @yes_checked = "true"
       @no_checked = "false"
    else
      @no_checked = "true"
      @yes_checked = "false"
    end

  end

  def update
    option_details = OptionLotsStatus.first unless params[:id].nil?
    option_details.update_attributes(:option_lots => params[:option_lots]) unless option_details.nil?
    Tbldatum.update_release_date_using_option_lots
    Rails.cache.clear
    redirect_to "/option_lots_status"
  end

  def destroy    
   
  end

end