class LotSalesStatusBuilder < ActiveRecord::Migration
  def change
    create_table :lot_sales_status_builder ,:id=>false do |t|
      t.primary_key :ID
      t.string :Lot_Sales_Status_Builder
      t.string :Color

      t.timestamps
    end
  end
end
