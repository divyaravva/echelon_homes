class HouseOptionsOds < ActiveRecord::Base
	attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_HOUSEOPTIONS"

  def self.load_ods_house_options(dev_codes,windows_db_nm)

  	truncate_ods_house_options

    puts "********** 1A) Started Loading HouseOptionsOds Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from "+windows_db_nm+".dbo.HOUSEOPTIONS where DEVELOPMENTCODE in ("+dev_codes+")");
      	puts "............................................."
      	puts "............................................."

        batch=[]
        batch_size = 1000

        result.each do |row|
          batch << HouseOptionsOds.new(:COMPANYCODE => row['COMPANYCODE'],
                                        :DEVELOPMENTCODE => row['DEVELOPMENTCODE'],
                                        :HOUSENUMBER => row['HOUSENUMBER'],
                                        :OPTIONCODE => row['OPTIONCODE'],
                                        :SALESPRICE => row['SALESPRICE']
								                    )
            
            if batch.size >= batch_size
              HouseOptionsOds.import batch
              batch = [] 
            end
        end
        HouseOptionsOds.import batch
    end

    puts "Total no. of Loaded rows.........#{HouseOptionsOds.count}"
    puts "********** 1B) Finished Loading HouseOptionsOds Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_house_options
		ActiveRecord::Base.connection.execute("TRUNCATE ODS_HOUSEOPTIONS")
	end


end