class Tblusers < ActiveRecord::Base
  attr_protected  
  #has_one :group, :class_name => 'Tblusergroup', :primary_key => 'Access_Group', :foreign_key => 'ID'
  belongs_to :group, :class_name => 'Tblusergroup', :primary_key => 'ID', :foreign_key => 'Access_Group'
  validates_uniqueness_of :Email_Address, :message => "Email Address has already taken." 
  has_many :reports,:class_name => "Report", :foreign_key => "user_id", :primary_key => "ID"
  has_many :user_role_definitions,:foreign_key => "User_Id", :dependent => :destroy
  has_many :roles, :through=> :user_role_definitions, :class_name => "RoleDefinition"

  has_many :rds_lot_collections, :through=> :roles, :class_name => "RdsLotCollection"
  
  has_many :lot_collections, :through=> :rds_lot_collections, :class_name => "LotCollection"
  has_many :lots, :through=> :lot_collections, :class_name => "Tbldatum"

  has_many :rds_lot_collections_menu_options, :through=> :rds_lot_collections, :class_name => "RdsLotCollectionsMenuOption"
  has_many :role_access, :through=> :rds_lot_collections_menu_options, :class_name => "RdsLotCollectionsMenuOptionRoleAccess"


 def self.current
    Thread.current[:user]
  end
  def self.current=(user)
    Thread.current[:user] = user
  end
  
def is_admin?
  Rails.cache.fetch("echelon_is_adminstrator_#{self.id}") do
   self.group.Group_Name == "Admin"
  end
end

def self.create_token
  SecureRandom.urlsafe_base64(nil, false)
end

def self.get_first_name(id)  
	   self.find(id).First_Name unless id.nil?  
end

def get_roles
  Rails.cache.fetch("echelon_role_ids_of_user_#{self.id}") do
    self.role_ids.join(",")
  end
end

def get_roles_count
  Rails.cache.fetch("echelon_roles_count_of_user_#{self.id}") do
    self.roles.count unless self.nil?
  end
end

def self.get_user_definitions
  Rails.cache.fetch("echelon_user_and_associated_models") do
    includes(
          :roles => [
                      {
                        :rds_lot_collections=>
                          [
                            {:rds_lot_collections_menu_options=>
                              [
                                :rds_lot_collections_menu_options_role_access,
                                :role_access
                              ]
                            },
                            :menu_options
                          ]
                      },
                      :lot_collections => 
                      {
                         :lots => [
                                    :status_detail,
                                  ]
                      }                     
                    ]
              )
  end
end

  def get_villages opted_product
    pr_id = ProductType.find_by_Project_Name(opted_product).Id.to_s
    Rails.cache.fetch("echelon_villages_of_user_#{self.id}_#{pr_id}") do
      self.get_role_lot_collections_for_village(pr_id).map{|lcl| lcl.Community}.compact.uniq.sort
    end
  end

  def get_role_lot_collections_for_village(opted_product)
     user = self.get_user_info
     Rails.cache.fetch("echelon_lot_collection_lots_of_role_of_user_#{self.id}_#{opted_product}") do      
       user.roles.map{|role| role.rds_lot_collections.select{|z|z.Product_Type_Id.split(",").include?(opted_product)}.
       flatten}.flatten.
       map{|z|z.lot_collection}.flatten
      .map{|lc|lc.lot_collection_lots}.flatten
    end
  end

  def get_plats village
    Rails.cache.fetch("echelon_plats_for_#{village}_of_user_#{self.id}") do
      self.get_role_lot_collections.map{|lcl| lcl.Phase if lcl.Community==village}.compact.uniq    
    end  
  end

  def get_menus(village, plat, product_type)
    product_type_id = ProductType.find_by_Project_Name(product_type).Id
    user = self.get_user_info    
    Rails.cache.fetch("echelon_menus_for_#{village}_#{plat}_#{product_type_id}_of_user_#{self.id}") do     
     array_pro_type = ProductTypeMenuOption.find_all_by_ProjectId(product_type_id).map{|x| x.MenuOptionId}
     user.roles.map{|role| role.rds_lot_collections}.flatten
     .map{|rds_lc| rds_lc unless rds_lc.lot_collection.lot_collection_lots.where(:Community=>village, :Phase=>plat).empty?}.compact
     .map{|rds_lc| rds_lc.rds_lot_collections_menu_options}.flatten.sort_by{|k| k.Menu_Option_Id}
     .map{|menu| menu.menu_option}.select{|xs| array_pro_type.include?(xs.id.to_s)}.uniq.group_by(&:MenuName)
   end

  end

  def get_menus_for_community(village, product_type)
    product_type_id = ProductType.find_by_Project_Name(product_type).Id
    user = self.get_user_info    
    Rails.cache.fetch("echelon_menus_for_#{village}_#{product_type_id}_of_user_#{self.id}") do     
     array_pro_type = ProductTypeMenuOption.find_all_by_ProjectId(product_type_id).map{|x| x.MenuOptionId}
     user.roles.map{|role| role.rds_lot_collections}.flatten
     .map{|rds_lc| rds_lc unless rds_lc.lot_collection.lot_collection_lots.where(:Community=>village).empty?}.compact
     .map{|rds_lc| rds_lc.rds_lot_collections_menu_options}.flatten.sort_by{|k| k.Menu_Option_Id}
     .map{|menu| menu.menu_option}.select{|xs| array_pro_type.include?(xs.id.to_s)}.uniq.group_by(&:MenuName)
   end

  end

  def get_role_lot_collections
     user = self.get_user_info
     Rails.cache.fetch("echelon_lot_collection_lots_of_role_of_user_#{self.id}") do      
      user.roles.map{|role| role.lot_collections}.flatten
      .map{|lc|lc.lot_collection_lots}.flatten
    end
  end

  def get_lots(village, plat, menu_option)
    get_lots_based_on_village_plat(village,plat, menu_option)
  end

  def get_lots_for_hover(village, plat, legend_keys, role, user_info, menu_option,type, model_filter_vals, model_filter, secondary_filter_ids)
    @all_lots = user_info.get_lots(village, plat, menu_option)
      unless legend_keys.blank?
          unless legend_keys['filter1'].blank?            
              @ww=[]
              legend_keys['filter1'].each do |key1|
                key1 = StatusDetail.find_by_StatusKey_and_LegendType(key1,type["first_filter"][0]).id
                if(role == "Sales Status" || role == "Floor Plans View")
                  @ww+= @all_lots.select{|x| x if ( x.Lot_Buzz_Status.to_i == key1 ) unless x.Lot_Buzz_Status.blank? || key1.blank?} 
                elsif role == "Architecture View" || role == "Construction View"
                  @ww+= @all_lots.select{|x| x if ( x.Lot_Status.to_i == key1 ) unless x.Lot_Status.blank? || key1.blank?} 
                elsif role == "Warranty View"
                  @ww+= @all_lots.select{|x| x if ( x.Outstanding_Service_Orders.to_i == key1 ) unless x.Outstanding_Service_Orders.blank? || key1.blank?} 
               elsif role == "Sales View"
                  @ww+= @all_lots.select{|x| x if ( x.Lot_Status.to_i == key1 ) unless x.Lot_Status.blank? || key1.blank?} 
                
                end
              end
              @all_lots=@ww 
           end
         unless legend_keys['filter2'].blank?   
            @ww=[]
            legend_keys['filter2'].each do |key2|
              if role == "Sales Status" || role == "Floor Plans View"
                key2 = StatusDetail.find_by_StatusKey_and_LegendType_and_Community(key2,type["second_filter"][0], village).id
              else
                key2 = StatusDetail.find_by_StatusKey_and_LegendType(key2,type["second_filter"][0]).id
              end
                if (role == "Sales Status"  || role == "Floor Plans View")
                  @ww+= @all_lots.select{|x| x if (( x.Floor_Plan_Id.split(",").include?("#{key2}" )) unless x.Floor_Plan_Id.blank?) }    
                elsif role == "Sales View"
                  @ww+= @all_lots.select{|x| x if ( x.Lot_Tracking.to_i == key2 ) unless x.Lot_Tracking.blank?} 
                elsif role == "Construction View"
                  @ww+= @all_lots.select{|x| x if ( x.Construction_Schedule_Status.to_i == key2 ) unless x.Construction_Schedule_Status.blank? || key2.blank?} 
                elsif role == "Architecture View"
                  @ww+= @all_lots.select{|x| x if ( x.Lot_Tracking.to_i == key2 ) unless x.Lot_Tracking.blank?} 
                elsif role == "Accounting View" || role == "Closing View" || role == "Site Conditions View" || role == "Construction View"
                  @ww+= @all_lots.select{|x| x if ( x.Lot_Status.to_i == key2 ) unless x.Lot_Status.blank?} 
                end  
            end              
           @all_lots=@ww              
        end

        unless legend_keys['filter3'].blank?   
            @ww=[]
            legend_keys['filter3'].each do |key3|
              key3 = StatusDetail.find_by_StatusKey_and_LegendType(key3,type["third_filter"][0]).id
                if role == "Sales View"
                   @ww+= @all_lots.select{|x| x if ( x.Lot_Premium.to_i == key3 ) unless x.Lot_Premium.blank?} 
                elsif role == "Construction View"
                  @ww+= @all_lots.select{|x| x if ( x.Construction_VPO_Status.to_i == key3 ) unless x.Construction_VPO_Status.blank?} 
                end  
            end              
           @all_lots=@ww              
        end

        unless legend_keys['filter4'].blank?   
            @ww=[]
            legend_keys['filter4'].each do |key4|
              key4 = StatusDetail.find_by_StatusKey_and_LegendType_and_Community(key4,type["fourth_filter"][0], village).id
                if role == "Sales View"
                  @ww+= @all_lots.select{|x| x if ( x.Model_Elevation_Id.to_i == key4 ) unless x.Model_Description.blank? || key4.blank?}
                end  
            end              
           @all_lots=@ww              
        end

      unless legend_keys['filter5'].blank?   
            @ww=[]
            legend_keys['filter5'].each do |key5|
              key5 = StatusDetail.find_by_StatusKey_and_LegendType(key5,type["fifth_filter"][0]).id
                if role=="Sales View"
                  @ww+= @all_lots.select{|x| x if ( x.Completed_Specs.to_i == key5 ) unless x.Completed_Specs.blank?} 
                end  
            end              
           @all_lots=@ww              
        end
    end               
     @all_lots       
  end


  def get_lots_based_on_village_plat(village, plat, menu_option)
      user = self.get_user_info
      if plat.blank?
         lots = Rails.cache.fetch("echelon_lots_of_#{village}_of_user_#{self.id}") do              
           user.lots.where(:Community=>village).where("SVG_ID IS NOT NULL").flatten.uniq
         end
      else
         lots = Rails.cache.fetch("echelon_lots_of_#{plat}_for_#{village}_of_user_#{self.id}") do              
           user.lots.where(:Community=>village, :Phase_Name=>plat).where("SVG_ID IS NOT NULL").flatten.uniq
         end
      end      
      
      temp_colection = {}
      total_lots = Rails.cache.fetch("echelon_total_lots") do    
        Tbldatum.all
      end  
      lots.map do |lot|      
        #if Rails.cache.read("vantage_lot_#{lot.Unique_ID}").nil?                                     
            lot = total_lots.select{|l| l if l.Unique_ID == lot.Unique_ID}[0]
            lot_collection_id = lot.lot_collection.id 
            temp_colection[lot_collection_id] = [] unless temp_colection.keys.include?(lot_collection_id)                          
            if temp_colection[lot_collection_id].empty?
              lot.permission = get_access_permission_of_role_definition(lot, menu_option)
              temp_colection[lot_collection_id] = lot.permission
            else
               lot.permission = temp_colection[lot_collection_id]              
            end
        lot
      end
  end

  def get_access_permission_of_role_definition(lot, menu_option)
    Tblusers.includes()    
    #self.role_access.where("Rds_Lot_Collections.Lot_Collection_Id" => lot.lot_collection.id, "Rds_Lot_Collections_Menu_Options.Menu_Option_Id" => 1).map(&:Role_Access_Id)
    self.rds_lot_collections_menu_options.where("Rds_Lot_Collections.Lot_Collection_Id" => lot.lot_collection.id, "Menu_Option_Id" => menu_option).
    flatten.map{|x| x.rds_lot_collections_menu_options_role_access.role_access.ID}
  end

  def get_user_info    
    Rails.cache.fetch("echelon_user_info_of_user_#{self.id}") do
     Tblusers.get_user_definitions.find(self.id)
   end
  end

def self.notify_users
  where(:Invitation_Status => "Registered", :Email_Notifications => "Yes")
end


def self.get_email_address_of_eventer user_id
  begin 
    self.find(user_id).Email_Address unless user_id.nil?
  rescue
    nil
  end
end

def self.find_user id
  Rails.cache.fetch("echelon_user_#{id}") do
    self.find id unless id.nil?
  end
end

def self.invalidate_user id
  Rails.cache.delete("echelon_user_#{id}")
end

def self.can_see_upload_button(user, opted_community)
 l_c_id=LotCollectionLot.find_by_Community(opted_community).Lot_Collection_Id
 can_upload_report = ''
 can_upload_report=user.roles.map{|role| role.rds_lot_collections.where(:Lot_Collection_Id=>l_c_id)}.flatten.
 map{|z|z.Can_Upload_Report}.uniq.include?(1)
end

def self.get_uploaded_reports_with_view_access(user)
  reports_with_view_access = []

  if user.roles.map{|x| x.Role_Definition}.include?("Admin") == true
    reports_with_view_access << Report.find(:all, :conditions => ["report_type =?", "Uploaded"])
  else
    user.roles.each do |each_role|
     each_role.rds_lot_collections_report_role_accesses.each do |each_role_access_rec|
      reports_with_view_access <<  each_role_access_rec.report
     end

    unless user.reports.blank?
     reports_with_view_access << user.reports.where("report_type =?", "Uploaded")
    end

    end
  end
  
  reports_with_view_access.flatten.uniq
end
  
def self.get_generated_reports_with_view_and_edit_access(user)
  reports_with_view_and_edit_access = []

  if user.is_admin? == true
    all_reps = Report.find(:all, :conditions => ["report_type =?", "Generated"])
    all_reps.each do |each_rep|
    all_reps_hash = {}
       all_reps_hash["report_name"] = each_rep.document_file_name
       reports_with_view_and_edit_access << all_reps_hash
    end
  else
    user.roles.map{|x|x.rds_lot_collections}.flatten.each do |each_role|
      unless each_role.rds_lot_collections_report_role_accesses_for_geneated_reps.blank?
        each_role.rds_lot_collections_report_role_accesses_for_geneated_reps.each do |each_role_access_rec|
          reports_with_view_access_hash={}
          reports_with_view_access_hash["report_name"] =  each_role_access_rec.report.document_file_name
          reports_with_view_and_edit_access << reports_with_view_access_hash
        end
      end
    end
  end
  reports_with_view_and_edit_access.uniq
end

def self.get_generated_reports_for_edit_access(user)
  reports_with_view_and_edit_access = []

  if user.is_admin? == true
    all_reps = Report.find(:all, :conditions => ["report_type =?", "Generated"])
    all_reps.each do |each_rep|
    all_reps_hash = {}
       all_reps_hash["report_name"] = each_rep.document_file_name
       all_reps_hash["acc_prmsn"] = "Edit Access"
       reports_with_view_and_edit_access << all_reps_hash
    end
  else
    user.roles.map{|x|x.rds_lot_collections}.flatten.each do |each_role|
      unless each_role.generated_reps_edit_access.blank?
        each_role.generated_reps_edit_access.each do |each_role_access_rec|
          reports_with_view_access_hash={}
          reports_with_view_access_hash["report_name"] =  each_role_access_rec.report.document_file_name
          # reports_with_view_access_hash["acc_prmsn"] =  each_role_access_rec.role_access.name unless each_role_access_rec.role_access.blank?
          reports_with_view_access_hash["acc_prmsn"] =  "Edit Access"
          reports_with_view_and_edit_access << reports_with_view_access_hash
        end
      end
    end
  end
  reports_with_view_and_edit_access.uniq
end


end