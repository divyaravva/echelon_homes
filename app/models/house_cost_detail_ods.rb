class HouseCostDetailOds < ActiveRecord::Base
	attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_HOUSECOSTDETAIL"

  def self.load_ods_house_cost_detail(dev_codes,windows_db_nm)

  	truncate_ods_house_cost_detail
    puts "********** 1A) Started Loading HouseCostDetail Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from "+windows_db_nm+".dbo.HOUSECOSTDETAIL where DEVELOPMENTCODE in ("+dev_codes+") and HOUSENUMBER != '00000000'");
      	puts "............................................."
      	puts "............................................."

        batch=[]
        batch_size = 1000

        result.each do |row|
          #HouseCostDetail.find_or_initialize_by_COMPANYCODE_and_DEVELOPMENTCODE_and_LOTNUMBER(row['COMPANYCODE'], row['DEVELOPMENTCODE'], row['LOTNUMBER'])
          batch << HouseCostDetailOds.new(:COMPANYCODE => row['COMPANYCODE'],
                                          :DEVELOPMENTCODE => row['DEVELOPMENTCODE'],
                                          :HOUSENUMBER => row['HOUSENUMBER'],
                                          :CATEGORYCODE => row['CATEGORYCODE'],
                                          :COSTCODE => row['COSTCODE'],
                                          :OPTIONCODE => row['OPTIONCODE'],
                                          :AMOUNT => row['AMOUNT'],
                                          :TRANSACTION_DATE => row[:TRANSACTION_DATE],
                                          :SEQUENCENUMBER => row[:SEQUENCENUMBER],
                                          :SOURCECODE => row[:SOURCECODE],
                                          :REMARKS => row[:REMARKS],
                                          :OPTIONCODE => row[:OPTIONCODE],
                                          :VARIANCECODE => row[:VARIANCECODE],
                                          :MEMO => row[:MEMO],
                                          :BATCHNUM => row[:BATCHNUM],
                                          :TERMINATOR => row[:TERMINATOR]
                                    )
            if batch.size >= batch_size
              HouseCostDetailOds.import batch
              batch = [] 
            end  
        end
        HouseCostDetailOds.import batch
        # HouseCostDetailOds.mass_insert(batch, :per_batch => 1000)
    end

    puts "Total no. of Loaded rows.........#{HouseCostDetailOds.count}"
    puts "********** 1B) Finished Loading HouseCostDetail Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_house_cost_detail
		ActiveRecord::Base.connection.execute("TRUNCATE ODS_HOUSECOSTDETAIL")
	end


end