class StatusKeyController < ApplicationController

  before_filter :authenticate
  before_filter :has_roles  
  before_filter :get_product
  before_filter :get_product_view
  before_filter :get_comm_city_view
  before_filter :get_community_city 
  before_filter :get_communities  
  before_filter :get_communities_view    
  before_filter :get_parcels    
  before_filter :get_parcels_view


  def index
    @statuskey=StatusDetail.order("StatusKey").where("LegendType = ?", "Floor Plan").uniq_by(&:StatusKey)
  end

  def show
    @legend_details = StatusDetail.find(params[:id]) unless params[:id].nil?
  end

  def destroy    
    plan_name = StatusDetail.find_all_by_StatusKey_and_LegendType(params[:statuskey],"Floor Plan") unless params[:statuskey].nil?
    plan_name.each do |each_plan_rec|
      each_plan_rec.destroy unless plan_name.nil?
    end
    @statuskey=StatusDetail.order("StatusKey").where("LegendType = ?", "Floor Plan").uniq_by(&:StatusKey)
    render :partial => "status_key_list"
  end

  def new     
     @plan_name = StatusDetail.all.select{|x| x.StatusKey!=@status_name}
  end

  def create 
    link_name = validate_url(params[:link_name]) unless params[:link_name].blank?
    params[:plan_description_status] = "off"  unless params[:plan_description_status].blank? == false
    params[:priced_from_status] = "off" unless params[:priced_from_status].blank? == false
    params[:sq_feet_status] = "off" unless params[:sq_feet_status].blank? == false
    params[:floors_status] = "off" unless params[:floors_status].blank? == false
    params[:bedrooms_status] = "off" unless params[:bedrooms_status].blank? == false
    params[:bathrooms_status] = "off" unless params[:bathrooms_status].blank? == false
    params[:full_bathrooms_status] = "off" unless params[:full_bathrooms_status].blank? == false
    params[:garages_status] = "off" unless params[:garages_status].blank? == false

    params[:community].each do |community|
    status_color_details=StatusDetail.create({:Link_Name => link_name, :StatusKey => params[:plan_name], 
      :LegendOrder => ":LegendOrder+1", :StatusLabel => params[:plan_name], :LegendType => "Floor Plan", 
      :Community => community, 
      :Plan_Description => params[:plan_description], :Plan_Description_Status => params[:plan_description_status], :Priced_From => params[:priced_from], :Priced_From_Status => params[:priced_from_status], :Sq_Feet => params[:sq_feet], :Sq_Feet_Status => params[:sq_feet_status],
      :Floors => params[:floors], :Floors_Status => params[:floors_status], 
      :Bedrooms => params[:bedrooms], :Bedrooms_Status => params[:bedrooms_status],
      :Full_Bathrooms => params[:full_bathrooms], :Full_Bathrooms_Status => params[:full_bathrooms_status],
      :Bathrooms => params[:bathrooms], :Bathrooms_Status => params[:bathrooms_status], :Garages =>  params[:garages], :Garages_Status => params[:garages_status], :Status_Display_Name => params[:plan_name]})
    end
    redirect_to "/status_key"
  end  

  def edit  
    @communities_edit = Tbldatum.find(:all, :select => 'DISTINCT Community')
    @legend_details = StatusDetail.find(params[:id]) unless params[:id].nil?
    @status_name = @legend_details.StatusKey unless @legend_details.nil? 
    @legend_type =  @legend_details.LegendType
    @plans_for_community = StatusDetail.find_all_by_StatusKey_and_LegendType(@status_name, "Floor Plan").map{|x|x.Community} 
    @link_name = @legend_details.Link_Name unless @legend_details.nil?   
    @plan_name = StatusDetail.all.select{|x| x.StatusKey!=@status_name && x.LegendType !=  @legend_type}
    @count_of_images =  ImageGallery.where(:plan_id => params[:id]).count
  end

  def edit_plan_images    
    images = ImageGallery.where(:plan_id => params[:plan_id])
    key_image_rec = ImageGallery.find_by_plan_id_and_plan_key_image(params[:plan_id],1)       
    @selected_key_image_id = key_image_rec.id unless key_image_rec.blank?  
    render :partial => '/status_key/edit_plan_images.html.erb', :locals => {:images => images, :plan_id => params[:plan_id], :plan_name => params[:plan_name]}
  end

  def view_plan_images    
    images = ImageGallery.where(:plan_id => params[:plan_id])
    render :partial => '/status_key/view_plan_images.html.erb', :locals => {:images => images, :plan_id => params[:plan_id], :plan_name => params[:plan_name]}
  end

  def plan_update_attrs
    attrs = {
      :Link_Name => :link_name,
      :StatusKey => :plan_name,
      :StatusLabel => :plan_name,
      :Status_Display_Name => :plan_name,
      :Plan_Description => :plan_description,
      :Plan_Description_Status => :plan_description_status,
      :Sq_Feet => :sq_feet, 
      :Sq_Feet_Status => :sq_feet_status,
      :Floors => :floors,
      :Floors_Status => :floors_status, 
      :Bedrooms => :bedrooms,
      :Bedrooms_Status => :bedrooms_status,
      :Bathrooms => :bathrooms, 
      :Full_Bathrooms => :full_bathrooms, 
      :Bathrooms_Status => :bathrooms_status,
      :Full_Bathrooms_Status => :full_bathrooms_status,
      :Garages =>  :garages, 
      :Garages_Status => :garages_status
     
    }
  end

  def update
    legend_details = StatusDetail.find(params[:id]) unless params[:id].nil?    
    params[:plan_description_status] = "off"  unless params[:plan_description_status].blank? == false
    params[:sq_feet_status] = "off" unless params[:sq_feet_status].blank? == false
    params[:floors_status] = "off" unless params[:floors_status].blank? == false
    params[:bedrooms_status] = "off" unless params[:bedrooms_status].blank? == false
    params[:bathrooms_status] = "off" unless params[:bathrooms_status].blank? == false
    params[:full_bathrooms_status] = "off" unless params[:full_bathrooms_status].blank? == false
    params[:garages_status] = "off" unless params[:garages_status].blank? == false
    link_name = validate_url(params[:link_name]) unless params[:link_name].blank?
    plan_name = params[:plan_name]

   attrs = {}
    all_params=plan_update_attrs()
    all_params.each do |k, v|
      if params.has_key?(v)
        attrs.merge!({k => params[v]})
      end
    end
    all_status_recs = StatusDetail.find_all_by_StatusKey_and_LegendType(legend_details.StatusKey,"Floor Plan")
    all_status_recs.each do |each_sts_rec|
      each_sts_rec.update_attributes(attrs)
    end
    redirect_to "/status_key"
  end


  def set_attrs(params)    
  end

  def validate_url(link_name)
    if (link_name =~ /http[s]?:\/\//)
      link_name
    else
      link_name = "http://#{link_name}"
    end
    link_name
  end

  def lot_plan_status
    lot_plan_details = StatusDetail.find(params[:id]) unless params[:id].nil?   
    all_status_recs = StatusDetail.find_all_by_StatusKey(params[:statuskey])
    all_status_recs.each do |each_rec|
      if params[:status] == "archive" 
        each_rec.update_attributes({:Is_Active => 0}) unless each_rec.nil?
      else
         each_rec.update_attributes({:Is_Active => 1}) unless each_rec.nil?
      end
    end
    @statuskey=StatusDetail.order("StatusKey").where("LegendType = ?", "Floor Plan").uniq_by(&:StatusKey)
    render :partial => "status_key_list"
  end

  def display_names
  end

  def render_statuskey_list
    @statuskey=StatusDetail.order("StatusKey").where("LegendType = ?", "Floor Plan").uniq_by(&:StatusKey)
    render :partial => "status_key_list"
  end

  def upload_images 
    all_images_array = Array.new
    images_exists_array = Array.new
    params[:product_type] = "Lot Buzz"
    all_plan_recs = StatusDetail.find_all_by_StatusKey_and_LegendType(params[:plan_name],"Floor Plan")      
    yo_url_wout_http = params[:youtube_url].sub(/^https?\:\/\//, '')
    yo_url_video_id = yo_url_wout_http.split("/").last.split("=").last unless yo_url_wout_http.blank?
    yo_image_exists = ImageGallery.where("ytube_video_id = ? and plan_id = ?", yo_url_video_id, params[:plan_id]).exists?
    if params[:tbldataattachments_attributes].present? && params[:tbldataattachments_attributes][0][:pics].present? || params[:youtube_url].present?
      if params[:tbldataattachments_attributes].present? && params[:tbldataattachments_attributes][0][:pics].present?
        params[:tbldataattachments_attributes].each do |img| 
          all_files = (img[:pics].original_filename.include?(" ")) ? img[:pics].original_filename.tr!(" ", "_") : img[:pics].original_filename
          all_images_array << all_files
        end
        all_images_array.each do |each_image|          
          image_exists = ImageGallery.where("pics_file_name = ? and plan_id = ? ", each_image, params[:plan_id]).exists?
          if image_exists
            images_exists_array << each_image+" already exists"
          end
        end

        unless images_exists_array.empty?
          render :json=> {:status=>"error", :upload_image_errors=>images_exists_array}
        else  
          image_obj = params["tbldataattachments_attributes"]
          if yo_image_exists
            render :json=> {:status=>"error", :msg=>"Youtube url already exists"}
          else  
            unless params[:youtube_url].blank?
              y_url=Tbldatum.validate_ytubeurl(params[:youtube_url])
              if ( (y_url =~ /^http[s]?:\/\/www\.youtube\.com\/watch\?v=([a-zA-Z0-9_-]*)/) || (y_url =~ /^http[s]?:\/\/youtu\.be([a-zA-Z0-9_-]*)/) )
                all_plan_recs.each do |each_plan_rec|
                  ImageGallery.create(:community=>each_plan_rec.Community,:youtube_url => yo_url_wout_http,  :ytube_video_id => yo_url_video_id, :product_type => params[:product_type], :plan_id => each_plan_rec.id) unless params[:youtube_url].blank?      
                end
                (0...image_obj.count).each do |i|                   
                  all_plan_recs.each do |each_plan_rec|
                     ImageGallery.create( image_obj[i].merge(:community=>each_plan_rec.Community,:product_type => params[:product_type], :plan_id => each_plan_rec.id) )
                  end
                end 
                render :json=> {:status=>"success"}  
                 ImageGallery.update_plan_keyimage_if_plan_having_one_image(params[:plan_id])
              else
                render :json=> {:status=>"error", :msg=>"Please ener valid youtube url"}  
              end
            else
              (0...image_obj.count).each do |i|   
                all_plan_recs.each do |each_plan_rec|    
                  ImageGallery.create( image_obj[i].merge(:community=>each_plan_rec.Community,:product_type => params[:product_type], :plan_id =>each_plan_rec.id) )
                end
              end
              render :json=> {:status=>"success"} 
               ImageGallery.update_plan_keyimage_if_plan_having_one_image(params[:plan_id])
            end                               
          end
        end  
      else  
        if yo_image_exists
          render :json=> {:status=>"error", :msg=>"Youtube url already exists"}
        else  
          y_url=Tbldatum.validate_ytubeurl(params[:youtube_url])
          if ( (y_url =~ /^http[s]?:\/\/www\.youtube\.com\/watch\?v=([a-zA-Z0-9_-]*)/) || (y_url =~ /^http[s]?:\/\/youtu\.be([a-zA-Z0-9_-]*)/) )
            #saving the url without http or https bcz if we are saving the entire url it contains
            #http or https. while validating the uniqueness it will be checking for entire url, if we are 
            #giving as http and gallery already contains with https, it is saving as other link.
            all_plan_recs.each do |each_plan_rec|
              ImageGallery.create(:community=>each_plan_rec.Community, :ytube_video_id => yo_url_video_id, :youtube_url => yo_url_wout_http, :product_type => params[:product_type], :plan_id => each_plan_rec.id) unless params[:youtube_url].blank?      
            end
            render :json=> {:status=>"success"}  
             ImageGallery.update_plan_keyimage_if_plan_having_one_image(params[:plan_id])
          else
            render :json=> {:status=>"error", :msg=>"Please ener valid youtube url"}  
          end
        end
      end
    else
      render :json=> {:status=>"error",:msg=> "Please Upload an image or Youtube Url"}
    end
  end

  def delete_images    
    all_plan_recs = StatusDetail.find_all_by_StatusKey_and_LegendType(params[:plan_name],"Floor Plan")            
    res_array = Array.new
    # @tbldata = Tbldatum.find(params[:lot_id])
    # unless @tbldata.nil?                  
      params[:tbldataattachments].each do |attachment|  
        img = ImageGallery.find(attachment["id"])  
        unless attachment["_destroy"].nil?
            all_plan_recs.each do |each_plan_rec|
              if attachment["youtube_url"].blank?
                gal_recs = ImageGallery.find_by_plan_id_and_pics_file_name(each_plan_rec.id, attachment["pics_file_name"])
              else
                gal_recs = ImageGallery.find_by_plan_id_and_youtube_url(each_plan_rec.id, attachment["youtube_url"])
              end

               if gal_recs.plan_key_image == "1"
                 Tbldatum.all.each do |each_tbl_rec|
                     if each_tbl_rec.Floor_Plan_Id == params[:plan_id] && each_tbl_rec.Key_Image_Url == gal_recs.pics(:thumb_add_images)
                        each_tbl_rec.update_attributes(:Key_Image_Url => "", :Key_Image_Id => "") 
                    end 
                Tbldatum.invalidate_cache
              end
            end
            gal_recs.destroy unless gal_recs.blank?              
             # all_gal_recs.each do |each_gal_rec|
              #   each_gal_rec.destroy
              # end
            end
        else
            all_plan_recs.each do |each_plan_rec|
              if attachment["youtube_url"].blank?
                each_gal_rec = ImageGallery.find_by_plan_id_and_pics_file_name(each_plan_rec.id, attachment["pics_file_name"])
              else
                each_gal_rec = ImageGallery.find_by_plan_id_and_youtube_url(each_plan_rec.id, attachment["youtube_url"])
              end    
              each_gal_rec.update_attributes( :title => attachment["title"], :description => attachment["description"], :navigation_link => attachment["navigation_link"]) unless each_gal_rec.blank?
            end
        end
      end
      unless params["key_image"].blank?
        params["key_image"].each do |key_image|
          unless key_image["_key_image"].nil? 
            all_plan_recs.each do |each_plan_rec|     
              key_image_rec = ImageGallery.find_by_plan_id_and_plan_key_image_and_product_type(each_plan_rec.id,1, "Lot Buzz" )
              key_image_rec.update_attributes(:plan_key_image => 0) unless key_image_rec.blank?
              lot_rec = ImageGallery.find_by_plan_id_and_pics_file_name(each_plan_rec.id, key_image["pics_file_name"])
              lot_rec.update_attributes(:plan_key_image => key_image["_key_image"]) unless lot_rec.blank?
              unless key_image_rec.blank?
                Tbldatum.all.each do |each_tbl_rec|
                  if each_tbl_rec.Floor_Plan_Id == params[:plan_id] && each_tbl_rec.Key_Image_Url == key_image_rec.pics(:thumb_add_images)
                    each_tbl_rec.update_attributes(:Key_Image_Url => lot_rec.pics(:thumb_add_images), :Key_Image_Id => lot_rec.id) unless lot_rec.blank?
                  end 
                  Tbldatum.invalidate_cache
                end
              end
            end
          end
        end 
      end
       ImageGallery.update_plan_keyimage_if_plan_having_one_image(params[:plan_id])
        render :json=> {:status=>"success", :msg => "Deleted"}    
  end
end