class SetTimeZone < ActiveRecord::Base
	self.table_name="set_time_zone"
	self.primary_key="Id"
	attr_protected

	def self.get_time_zone
		Rails.cache.fetch("echelon_timezone") do
			self.first.blank? || self.first.TimeZone.blank? ? "UTC" : self.first.TimeZone
		end
	end

	def self.convert_utc_to_app_set_time_zone utc_date
		utc_date.in_time_zone(self.get_time_zone) unless utc_date.nil?
	end

	def self.get_todays_date
		DateTime.now.in_time_zone self.get_time_zone
	end

	def self.get_todays_date_without_zone_info
		(DateTime.now.in_time_zone self.get_time_zone).to_date
	end

	def self.get_todays_date_in_mdy_format
		(DateTime.now.in_time_zone self.get_time_zone).strftime("%m/%d/%Y")
	end

	def self.get_months_first_date_in_mdy_format
		(DateTime.now.in_time_zone SetTimeZone.get_time_zone).beginning_of_month.strftime("%m/%d/%Y")
	end

	def self.get_saved_time_zone
		self.first.blank? || self.first.TimeZone.blank? ? "" : self.first.TimeZone
	end

	def self.save timezone, user
		prev_obj=self.first.nil? ? self.new(:TimeZone=>"UTC") : self.first
		new_obj = self.first.nil? ? self.new(:TimeZone=>"UTC") : self.first
		new_obj.TimeZone=timezone
		attr_changes=new_obj.changes
		new_obj.save
		if timezone == "Eastern Time (US & Canada)"
			abbr = "EST"
		elsif timezone == "Mountain Time (US & Canada)"
			abbr = "MST"
		elsif timezone  == "Hawaii"
			abbr = "HST"
		elsif timezone  == "Alaska"
			abbr = "AKST"
		elsif timezone  == "Pacific Time (US & Canada)"
			abbr = "PST"
		elsif timezone  == "Arizona"
			abbr = "AZ"
		elsif timezone  == "Central Time (US & Canada)"
			abbr = "CST"
		elsif timezone  == "Indiana (East)"
			abbr = "IST"
		end
		SetTimeZone.first.update_attributes(:Abbrevation => abbr)
		# if new_obj.save
		# 	event_obj = ActivityLogger.log_activity_on_lots("TimeZone Change", "System", new_obj, user, prev_obj, attr_changes)  
		# 	Mailer.notify_on_time_zone_change(prev_obj.TimeZone, new_obj.TimeZone, event_obj.event_date, event_obj.event_by_email).deliver			
		# end
	end
end