module LotsHelper
  def untied_lots_options
    ProductSalesContract.untied.collect {|lot| [lot.Lot_Number, lot.GIS_Lot_ID]}
  end
  
  def all_lots_options(phase_name)
    # ProductSalesContract.where(:Phase_Name => ["Phase 1 - Parcel H1","Phase 1 - Parcel H2"]).collect {|lot| [lot.tied? ? (lot.Phase_Name+"#Lot No-"+lot.Lot_Number + " (Tied)") : lot.Phase_Name+"#Lot No-"+lot.Lot_Number, lot.GIS_Lot_ID]}
    # ProductSalesContract.where(:Phase_Name => ["Phase 1A - Parcel K2"]).collect {|lot| [lot.tied? ? (lot.Phase_Name+"#Lot No-"+lot.Lot_Number + " (Tied)") : lot.Phase_Name+"#Lot No-"+lot.Lot_Number, lot.GIS_Lot_ID]}
    Tbldatum.where(:Phase_Name => phase_name).collect {|lot| [lot.tied? ? (lot.Development_Code+"-"+lot.Phase_Name+"-"+lot.Lot_Number.to_s + " (Tied)") : lot.Development_Code+"-"+lot.Phase_Name+"-"+lot.Lot_Number.to_s, lot.Unique_ID ]} 
  end
end
