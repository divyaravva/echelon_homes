class RolesController < ApplicationController
   
   before_filter :authenticate
   before_filter :is_admin
   before_filter :get_product
   before_filter :get_product_view 
   before_filter :get_comm_city_view
   before_filter :get_community_city     
   before_filter :get_communities   
   before_filter :get_communities_view
   before_filter :get_parcels
   before_filter :get_parcels_view
  
  def index
    @roles = RoleDefinition.order("Role_Definition").all
  end

   def new   
    @lot_collections_count = LotCollection.count
    @roles = RoleDefinition.all
    @product_types = ProductType.all
    @all_collections = LotCollection.all
    @uploaded_rpt_id = MenuOption.get_uploaded_report_menu_option_id
    @generated_rpt_id = MenuOption.get_generated_report_menu_option_id
    @generated_reports_array = @generated_reports = Report.get_generated_reports
    @uploaded_reports_array = @uploaded_reports = Report.get_uploaded_reports
    @menu_options = MenuOption.find(:all, :conditions => ["MenuName !=?", "Reports"])
    @menus_for_reports = MenuOption.find(:all, :conditions => ["MenuName =?", "Reports"])
    @uploaded_report_menu = true
    @generated_report_menu = true
    @comm_city_lc = LotCollection.uniq.pluck(:community_city)
    @lot_collections = LotCollection.group(:community_city,:collection_name).order(:community_city)
  end
   
  def create 
    role = RoleDefinition.create(:Role_Definition => params[:name])
      role.lot_collection_ids = params[:collection_keys_ids].keys 
      keys_arr = []
      params[:collection_keys_ids].keys.each do |key|
        if params[key.to_s].present?
          keys_arr << key
        end
      end
      role.lot_collection_ids = keys_arr
      product_type_ids=Array.new      
      role.rds_lot_collections.each do |role_lot_collection|
        product_type_ids.clear()
        can_upload_flag = ''
        unless params[:can_upload].blank?
          unless params[:can_upload][role_lot_collection.Lot_Collection_Id.to_s].blank?
            params[:can_upload][role_lot_collection.Lot_Collection_Id.to_s].each do |each_flag|          
              can_upload_flag = each_flag.blank? ? 0 : 1  
            end
          end
        end
        params[:prodcut_type_checkboxes][role_lot_collection.Lot_Collection_Id.to_s].each do |pr_id|          
          product_type_ids<<pr_id["product_type"]          
        end        
        role_lot_collection.update_attributes(:Product_Type_Id => product_type_ids.join(","), :Can_Upload_Report => can_upload_flag)      
      end
      menu_arr = Array.new      
      role.rds_lot_collections.each do |role_lot_collection|
        menu_arr.clear()     
        params[:collection_keys_ids][role_lot_collection.Lot_Collection_Id.to_s].each do |moi|
          menu_arr << moi["menu_options"]
        end
        role_lot_collection.menu_option_ids = menu_arr
        role_lot_collection.rds_lot_collections_menu_options.each_with_index do |role_lot_collection_menu_option, counter| 
          params[:collection_keys_ids][role_lot_collection.Lot_Collection_Id.to_s].each_with_index do |moi_access, i|
            ac = moi_access["menu_options"].to_i
            if role_lot_collection_menu_option.Menu_Option_Id == ac
              role_lot_collection_menu_option.role_access = Access.find(params[:collection_keys_ids][role_lot_collection.Lot_Collection_Id.to_s][i]["permission"]["#{ac}"][0].to_i)
            end
          end
        end
      end

      role.rds_lot_collections.each do |role_lot_collection|
        unless params[:collection_keys_ids_rep].blank?
          params[:collection_keys_ids_rep][role_lot_collection.Lot_Collection_Id.to_s].each_with_index do |moi_access_rep, i|
            ac_rep = moi_access_rep["reports"].to_i
            lot_collection_id = role_lot_collection.Lot_Collection_Id
            access_per = params[:collection_keys_ids_rep][role_lot_collection.Lot_Collection_Id.to_s][i]["reports_permission"]["#{ac_rep}"][0].to_i         
            report_type = params[:collection_keys_ids_rep][role_lot_collection.Lot_Collection_Id.to_s][i]["report_type"]
            RdsLotCollectionsReportRoleAccess.create(:Lot_Collection_Id => lot_collection_id, :Role_Id => role.id, :Role_Access_Id => access_per, :Report_Id => ac_rep, :Report_Type => report_type)
          end 
        end    
      end 
      RoleCommonPermissions.delete_all(role_id: role.id)
      params[:common_menus_permi].each do |prdt,menus_n_perm|
        if params[:comm_prdt_type].include?(prdt)
          params[:common_menus_permi][prdt].each do |menu, perm|
            comm_per_rec = RoleCommonPermissions.create(:role_id => role.id, :product_id => prdt, :menu_id => menu, :permission => perm[0])
          end

          params[:common_generated_report_permi].each do |genr_prdt, menus_n_perm|
            if params[:comm_prdt_type].include?(genr_prdt) && prdt == genr_prdt
              params[:common_generated_report_permi][genr_prdt].each do |menu, report_n_perm|
                params[:common_generated_report_permi][genr_prdt][menu].each do |report_nm, perm|
                  report_id = params[:report_nm_id]["#{report_nm}"]["#{menu}"]
                  comm_per_rec = RoleCommonPermissions.create(:role_id => role.id, :product_id => genr_prdt, :menu_id => menu, :permission => perm[0], :report_name => report_nm,:report_id => report_id)
                end
              end
            end
          end
          if params["can_upload_chk"].present?
            can_upload = 1
          end
          params[:common_uploaded_report_permi].each do |uplo_prdt, menus_n_perm|
            if params[:comm_prdt_type].include?(uplo_prdt) && prdt == uplo_prdt
              params[:common_uploaded_report_permi][uplo_prdt].each do |menu, report_n_perm|
                params[:common_uploaded_report_permi][uplo_prdt][menu].each do |report_nm, perm|
                  report_id = params[:report_nm_id]["#{report_nm}"]["#{menu}"]
                  comm_per_rec = RoleCommonPermissions.create(:role_id => role.id, :product_id => uplo_prdt, :menu_id => menu, :permission => perm[0], :report_name => report_nm, :can_upload => can_upload,:report_id => report_id)
                end
              end
            end
          end

        end
      end

      Tbldatum.invalidate_cache
      redirect_to "/roles"
  end   

  def edit 
      @edit = "edit"
      @role_comm_per = RoleCommonPermissions.where("role_id = ?", params[:id])
      @all_collections = LotCollection.all
      @comm_city_lc = LotCollection.uniq.pluck(:community_city)
      @role_definition = RoleDefinition.get_rules.find(params[:id])
      @lc_coll_nms = @role_definition.lot_collections.map{|x| x.collection_name.gsub(" ","")}
      @rds_colls = @role_definition.rds_lot_collections
      @product_types = ProductType.where(:Active => 1)
      @roles = RoleDefinition.all.select{|x| x.Role_Definition!=@role_definition.Role_Definition}
      @role_def_rec = RoleDefinition.find(params[:id])      
      @role_id = @role_def_rec.id

      @uploaded_rpt_id = MenuOption.get_uploaded_report_menu_option_id
      @generated_rpt_id = MenuOption.get_generated_report_menu_option_id
      @menu_options =MenuOption.find(:all, :conditions => ["MenuName !=?", "Reports"])

      @menus_for_reports = MenuOption.find(:all, :conditions => ["MenuName =?", "Reports"])

      @generated_reports_array = @generated_reports =  Report.get_generated_reports
      @generated_report_menu = true 
      @uploaded_report_menu = true
      @uploaded_reports_array = @uploaded_reports = Report.get_uploaded_reports
  end

  def update 
    role = RoleDefinition.find(params[:id])
    role.update_attributes(:Role_Definition => params[:name])
    role.lot_collection_ids = params[:collection_keys_ids].keys 
    keys_arr = []
    params[:collection_keys_ids].keys.each do |key|
      if params[key.to_s].present?
        keys_arr << key
      end
    end
    role.lot_collection_ids = keys_arr
    product_type_ids=Array.new
    menu_arr = Array.new
    role.rds_lot_collections.each do |role_lot_collection|
      product_type_ids.clear()
      can_upload_flag = ''

      unless params[:can_upload].blank?
        unless params[:can_upload][role_lot_collection.Lot_Collection_Id.to_s].blank?
          params[:can_upload][role_lot_collection.Lot_Collection_Id.to_s].each do |each_flag|          
            can_upload_flag = each_flag.blank? ? 0 : 1  
          end
        end
      end
      params[:prodcut_type_checkboxes][role_lot_collection.Lot_Collection_Id.to_s].each do |pr_id|          
        product_type_ids<<pr_id["product_type"]
      end   

      role_lot_collection.update_attributes(:Product_Type_Id => product_type_ids.join(","), 
        :Can_Upload_Report => can_upload_flag)

      menu_arr.clear()
      params[:collection_keys_ids][role_lot_collection.Lot_Collection_Id.to_s].each do |moi|
          if product_type_ids.include?(MenuOption.find(moi["menu_options"]).product_types.first.Id.to_s)
            menu_arr << moi["menu_options"]
          end
      end 
      role_lot_collection.menu_option_ids = menu_arr
      role_lot_collection.rds_lot_collections_menu_options.each_with_index do |role_lot_collection_menu_option, counter| 
          params[:collection_keys_ids][role_lot_collection.Lot_Collection_Id.to_s].each_with_index do |moi_access, i|
            ac = moi_access["menu_options"].to_i
            
            if role_lot_collection_menu_option.Menu_Option_Id == ac
              role_lot_collection_menu_option.role_access = Access.find(params[:collection_keys_ids][role_lot_collection.Lot_Collection_Id.to_s][i]["permission"]["#{ac}"][0].to_i)
            end
          end
      end  

      unless params[:collection_keys_ids_rep].blank?              
          params[:collection_keys_ids_rep][role_lot_collection.Lot_Collection_Id.to_s].each_with_index do |moi_access_rep, i|
            ac_rep = moi_access_rep["reports"].to_i
            lot_collection_id = params[:collection_keys_ids_rep][role_lot_collection.Lot_Collection_Id.to_s][i]["lot_collection_id"]
            access_per = params[:collection_keys_ids_rep][role_lot_collection.Lot_Collection_Id.to_s][i]["reports_permission"]["#{ac_rep}"][0].to_i         
            report_type = params[:collection_keys_ids_rep][role_lot_collection.Lot_Collection_Id.to_s][i]["report_type"]
            report_access_rec = RdsLotCollectionsReportRoleAccess.find_or_initialize_by_Role_Id_and_Report_Id_and_Report_Type_and_Lot_Collection_Id(params[:id], ac_rep, report_type, lot_collection_id)
            report_access_rec.update_attributes(:Lot_Collection_Id => lot_collection_id, :Role_Access_Id => access_per) unless report_access_rec.blank?
          end 
        end
    end
    RoleCommonPermissions.delete_all(role_id: params[:id])
      params[:common_menus_permi].each do |prdt,menus_n_perm|
        if params[:comm_prdt_type].include?(prdt)
          params[:common_menus_permi][prdt].each do |menu, perm|
            comm_per_rec = RoleCommonPermissions.create(:role_id => params[:id], :product_id => prdt, :menu_id => menu, :permission => perm[0])
          end

          params[:common_generated_report_permi].each do |genr_prdt, menus_n_perm|
            if params[:comm_prdt_type].include?(genr_prdt) && prdt == genr_prdt
              params[:common_generated_report_permi][genr_prdt].each do |menu, report_n_perm|
                params[:common_generated_report_permi][genr_prdt][menu].each do |report_nm, perm|
                  report_id = params[:report_nm_id]["#{report_nm}"]["#{menu}"]
                  comm_per_rec = RoleCommonPermissions.create(:role_id => params[:id], :product_id => genr_prdt, :menu_id => menu, :permission => perm[0], :report_name => report_nm, :report_id => report_id)
                end
              end
            end
          end
          if params["can_upload_chk"].present?
            can_upload = 1
          end
          params[:common_uploaded_report_permi].each do |uplo_prdt, menus_n_perm|
            if params[:comm_prdt_type].include?(uplo_prdt) && prdt == uplo_prdt
              params[:common_uploaded_report_permi][uplo_prdt].each do |menu, report_n_perm|
                params[:common_uploaded_report_permi][uplo_prdt][menu].each do |report_nm, perm|
                  report_id = params[:report_nm_id]["#{report_nm}"]["#{menu}"]
                  comm_per_rec = RoleCommonPermissions.create(:role_id => params[:id], :product_id => uplo_prdt, :menu_id => menu, :permission => perm[0], :report_name => report_nm, :can_upload => can_upload, :report_id => report_id)
                end
              end
            end
          end

        end
      end

    Tbldatum.invalidate_cache
    if params[:view].blank? 
      redirect_to "/roles"
    else
      redirect_to "/roles/#{role.id}"
    end    
  end
  
  def show
    @role_definition = RoleDefinition.get_rules.find(params[:id])
    role = RoleDefinition.find(params[:id])
    @role_id = role.id            
    @admin_role = role if role.is_admin? 
    @role_users =  RoleDefinition.get_rules.find(params[:id]).users  
    @all_reports = Report.all
  end

  def destroy    
    RoleDefinition.find(params[:id]).destroy
    Tbldatum.invalidate_cache
    @roles = RoleDefinition.order("Role_Definition").all
    render :partial => "roles_list"
    # redirect_to "/roles"
  end

  def get_lot_collections      
    @lot_collections = LotCollection.order(:collection_name).map{|l_c| [l_c.id,l_c.collection_name]}
    if params[:selected_lot_collection].present?
     @lot_collections=@lot_collections.reject{|lc| params[:selected_lot_collection].include?lc[0].to_s}
    end
    render :partial => "get_lot_collection"
  end

  def get_role_view_per_collection      
    @uploaded_reports_array = Report.get_uploaded_reports
    @generated_reports_array = Report.get_generated_reports
    @uploaded_report_menu = true
    @generated_report_menu = true
    @uploaded_rpt_id = MenuOption.get_uploaded_report_menu_option_id
    @generated_rpt_id = MenuOption.get_generated_report_menu_option_id
    @lot_collections = LotCollection.find_all_by_id(params[:selected_lot_collection])
    @menu_options = MenuOption.find(:all, :conditions => ["MenuName !=?", "Reports"])
    @menus_for_reports = MenuOption.find(:all, :conditions => ["MenuName =?", "Reports"])
    @product_types = ProductType.where(:Active => 1)
    render :partial => "get_role_view_per_collection"
  end

  def get_views_based_on_product_type 
    @role_definition = RoleDefinition.get_rules.find(params[:id]) unless params[:id].blank?
    product_type_keys = params[:product_type_keys]      
    unless product_type_keys.blank?                           
      @menu_options = MenuOption.get_menus_based_on_filter(product_type_keys['filter1']) unless product_type_keys['filter1'].blank?             
    end
      render :partial => "get_views_based_on_product_type", :locals => {:lot_collection_id => params[:lot_collection_id], :i => params[:i]}
  end

   def get_reprots_based_on_product_type  
    unless params[:id].blank?
      @role_definition = RoleDefinition.get_rules.find(params[:id])
      @role_def_rec = RoleDefinition.find(params[:id])
      @can_upload_flag = @role_def_rec.Can_Upload_Report
      @role_id = @role_def_rec.id
    end
    
    @uploaded_rpt_id = MenuOption.get_uploaded_report_menu_option_id
    @generated_rpt_id = MenuOption.get_generated_report_menu_option_id

    product_type_keys = params[:product_type_keys]
      unless product_type_keys.blank?        
        unless product_type_keys['filter1'].blank?            
              @menus_for_reports=[]
              product_type_keys['filter1'].each do |key1|
              if(key1 == "1")
                @menus_for_reports+= MenuOption.find(:all, :conditions => ["MenuName =?", "Reports"])
        
                unless params[:id].blank?
                 RoleDefinition.find(params[:id]).rds_lot_collections.first.rds_lot_collections_menu_options.each do |x|            
                    if(x.Menu_Option_Id.to_s.include?(@generated_rpt_id))
                      @generated_reports_array=  Report.get_generated_reports
                      @generated_report_menu = true              
                    elsif(x.Menu_Option_Id.to_s.include?(@uploaded_rpt_id))
                      @uploaded_reports_array=  Report.get_uploaded_reports
                      @uploaded_report_menu = true
                    end
                  end
                else
                  @uploaded_report_menu = true
                  @generated_report_menu = true
                  @uploaded_reports_array=  Report.get_uploaded_reports
                  @generated_reports_array=  Report.get_generated_reports
                end
              end
        end
      end
        @menus_for_reports
    end
      
    render :partial => "get_reports_based_on_product_type",:locals => {:lot_collection_id => params[:lot_collection_id], :i => params[:i]}
   end

   def get_reports_based_on_report_type
    unless params[:role_id].blank?
      @role_def_rec = RoleDefinition.find(params[:role_id])
      @can_upload_flag = @role_def_rec.Can_Upload_Report
      @role_id = @role_def_rec.id
    end
    
     report_keys = params[:report_keys]
     @uploaded_rpt_id = MenuOption.get_uploaded_report_menu_option_id
     @generated_rpt_id = MenuOption.get_generated_report_menu_option_id

      unless report_keys.blank?
        unless report_keys['filter1'].blank?            
              report_keys['filter1'].each do |key1|
                if(key1 == @uploaded_rpt_id)
                  @uploaded_report_menu = true
                  @uploaded_reports_array= Report.get_uploaded_reports
                else
                  @generated_report_menu = true
                  @generated_reports_array= Report.get_generated_reports
                end
              end
        end
      end
    render :partial => "get_reports_based_on_report_type",:locals => {:lot_collection_id => params[:lot_collection_id], :role_id => params[:role_id]}
   end

   def roles_status
      role_details = RoleDefinition.find(params[:id]) unless params[:id].nil?  
      if params[:status] == "archive"  
        role_details.update_attributes({:Is_Active => 0}) unless role_details.nil?
      else
        role_details.update_attributes({:Is_Active => 1}) unless role_details.nil?
      end
      Tbldatum.invalidate_cache
      @roles = RoleDefinition.order("Role_Definition").all
       render :partial => "roles_list"
   end

end
