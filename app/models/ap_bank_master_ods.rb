class ApBankMasterOds < ActiveRecord::Base
	attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_APBANKMASTER"
  self.primary_key = :BANKCODE
  
  def self.load_ods_ap_bank_master

  	truncate_ods_ap_bank_master

    puts "********** 1A) Started Loading ApBankMasterOds Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from Vantage_Homes_ODS.dbo.APBANKMASTER");
      	puts "Total no. of ODS rows.........#{result.count}"
      	puts "............................................."
      	puts "............................................."

        batch=[]
        batch_size = 1000

        result.each do |row|
          batch << ApBankMasterOds.new(:COMPANYCODE => row['COMPANYCODE'],
                                       :BANKCODE => row['BANKCODE'],
                                       :DESCRIPTION => row['DESCRIPTION']
                                      )
            if batch.size >= batch_size 
              ApBankMasterOds.import batch
              batch = [] 
            end
        end
        ApBankMasterOds.import batch
    end

    puts "Total no. of Loaded rows.........#{ApBankMasterOds.count}"
    puts "********** 1B) Finished Loading ApBankMasterOds Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_ap_bank_master
		ActiveRecord::Base.connection.execute("TRUNCATE ODS_APBANKMASTER")
	end


end