class SalesSubsidiaryOds < ActiveRecord::Base
	attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_SALESSUBSIDIARY"

  def self.load_ods_sales_subsidiary(dev_codes,windows_db_nm)

  	truncate_ods_sales_subsidiary

    puts "********** 1A) Started Loading SalesSubsidiaryOds Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from "+windows_db_nm+".dbo.SALESSUBSIDIARY where DEVELOPMENTCODE in ("+dev_codes+")");
      	puts "............................................."
      	puts "............................................."

        batch=[]
        batch_size = 1000

        result.each do |row|
          batch << SalesSubsidiaryOds.new(:COMPANYCODE => row['COMPANYCODE'],
                                              :DEVELOPMENTCODE => row['DEVELOPMENTCODE'],
                                              :HOUSENUMBER => row['HOUSENUMBER'],
                                              :AMOUNT => row['AMOUNT']
								                    )
            
            if batch.size >= batch_size
              SalesSubsidiaryOds.import batch
              batch = [] 
            end
        end
        SalesSubsidiaryOds.import batch
    end

    puts "Total no. of Loaded rows.........#{SalesSubsidiaryOds.count}"
    puts "********** 1B) Finished Loading SalesSubsidiaryOds Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_sales_subsidiary
		ActiveRecord::Base.connection.execute("TRUNCATE ODS_SALESSUBSIDIARY")
	end


end