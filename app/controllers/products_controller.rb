class ProductsController < ApplicationController
   # before_filter :authenticate,:except => [:select_product, :update_product_selection]
   before_filter :authenticate
   before_filter :has_roles,:except => [:select_product, :update_product_selection]
   before_filter :is_super_admin, :only => [:select_product, :update_product_selection]
   before_filter :get_product
   before_filter :get_product_view  
   before_filter :get_comm_city_view
   before_filter :get_community_city 
   before_filter :get_communities
   before_filter :get_communities_view
   before_filter :get_parcels
   before_filter :get_parcels_view
	
  def get_selected_product    
  end

  def select_product
  end

  def update_product_selection
    ProductType.product_status(params[:product_types])
    Tbldatum.invalidate_cache
    redirect_to "/admin"
  end

  def render_street_numbers
    @street_name_id = params[:Street_Name_Id]
    @menu = session[:mainmenu]
    @menuoption = session[:menuoption]
    render :partial => "products/street_numbers_dropdown"
  end
end