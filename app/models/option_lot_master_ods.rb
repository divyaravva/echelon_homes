class OptionLotMasterOds < ActiveRecord::Base
	attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_OPTIONLOTMASTER"
  self.primary_keys = :DEVELOPMENTCODE, :LOTNUMBER

  has_one :prospect_master_option,:class_name =>"ProspectMasterOds",:foreign_key => [:DEVELOPMENTCODE, :HOUSENUMBER]
  has_many :house_cost_detail,:class_name =>"HouseCostDetailOds",:foreign_key => [:DEVELOPMENTCODE,:HOUSENUMBER]
  has_many :cust_deposit_dtl,:class_name =>"CustDepositDtlOds",:foreign_key => [:DEVELOPMENTCODE,:HOUSENUMBER]
  has_one :jl_house,:class_name =>"JlHousesOds",:foreign_key => [:DEVELOPMENTCODE,:HOUSENUMBER]
  has_many :jl_loan_detail,:class_name =>"JlLoanDetailOds",:foreign_key => [:DEVELOPMENTCODE, :HOUSENUMBER]
  has_one :jl_loan_detail_single,:class_name =>"JlLoanDetailOds",:foreign_key => [:DEVELOPMENTCODE, :HOUSENUMBER], :conditions => 'LOANNUMBER<>""'
  has_many :podist,:class_name =>"PodistOds",:foreign_key => [:DISTDEV, :DISTHOUSE]
  has_many :house_cost_summary,:class_name =>"HouseCostSummaryOds",:foreign_key => [:DEVELOPMENTCODE, :HOUSENUMBER]
  has_one :contract_master,:class_name =>"ContractMasterOds",:primary_key => [:DEVELOPMENTCODE],:foreign_key => [:DEVELOPMENTCODE], :conditions => {:CATEGORYCODE => '015', :COSTCODE => '01', :VENDORNUMBER => '320010', :EXPIRATIONDATE => nil }   
  has_one :prospect_master,:class_name =>"ProspectMasterOds",:foreign_key => [:DEVELOPMENTCODE, :HOUSENUMBER]
  has_one :elevation_master, :class_name => "ElevationMasterOds", :foreign_key => [:ELEVATIONCODE], :primary_key => [:ELEVATIONCODE]
  has_one :elevation_master_desc, :class_name => "ElevationMasterOds", :foreign_key => [:ELEVATIONCODE], :primary_key => [:ELEVATIONCODE]
  has_one :model_masters, :class_name => "ModelMasterOds", :foreign_key => [:MODELCODE], :primary_key => [:MODELCODE], :conditions => {:COMPANYCODE => '001'}  
  has_one :prospect_conting, :class_name => "ProspectContingOds", :foreign_key => [:CASENUMBER],:primary_key => [:CASENUMBER], :conditions => ['SATISFIEDDATE != ?', nil]  
  has_one :prospect_finance, :class_name => "ProspectFinanceOds", :foreign_key => [:CASENUMBER],:primary_key => [:CASENUMBER]  
  has_one :jlloan_master, :class_name => "JlloanMasterOds", :foreign_key => [:LOANNUMBER],:primary_key => [:LOAN_NUM], :conditions => {:COMPANYCODE => '001'}    
  has_one :schedule_house_detail_spec_aging,:class_name =>"SchedhouseDetailOds",:foreign_key => [:DEVELOPMENTCODE, :HOUSENUMBER], :conditions => {:STEPNUMBER => '21100', :ACTIVITYCODE => 'Y15'}   
  has_one :schedule_house_detail_spec_aging_test,:class_name =>"SchedhouseDetailOds",:foreign_key => [:DEVELOPMENTCODE, :HOUSENUMBER]
  has_many :house_cost_detail_variance_to_date,:class_name =>"HouseCostDetailOds",:foreign_key => [:DEVELOPMENTCODE,:HOUSENUMBER], :conditions => ["COSTCODE BETWEEN 60 AND 95 or COSTCODE = 98"]
  has_one :schedule_house_detail_last_activity,:class_name =>"SchedhouseDetailOds",:foreign_key => [:DEVELOPMENTCODE, :HOUSENUMBER], :order => 'ACTUALFINISHDATE DESC, ACTIVITYCODE DESC'
  has_one :schedule_house_detail_next_activity, :class_name =>"SchedhouseDetailOds",:foreign_key => [:DEVELOPMENTCODE, :HOUSENUMBER] ,:conditions => ["ACTUALFINISHDATE IS NULL AND LATEFINISHDATE>='#{Date.today}'"], :order => 'LATEFINISHDATE ASC'
  has_one :sales_person, :class_name =>"SalesPersonMasterOds",:foreign_key => [:COMPANYCODE, :SALESPERSONCODE], :primary_key => [:COMPANYCODE, :SALESMANCODE]
  has_many :warranty_master_service_rep_name,:class_name =>"WarrantyMasterOds",:foreign_key => [:DEVELOPMENTCODE, :HOUSENUMBER], :conditions => "SERVICEREPCODE<>''", :select => ("DISTINCT DEVELOPMENTCODE,HOUSENUMBER,SERVICEREPCODE")
  has_many :warranty_master_warranty_issues_aged,:class_name =>"WarrantyMasterOds",:foreign_key => [:DEVELOPMENTCODE, :HOUSENUMBER], :conditions => "COMPLETION_DATE IS NULL AND SERVICE_DATE<'#{Date.today}'"
  has_many :warranty_master_based_on_completion_date,:class_name =>"WarrantyMasterOds",:foreign_key => [:DEVELOPMENTCODE, :HOUSENUMBER], :conditions => {:COMPLETION_DATE => nil}
  has_many :warranty_master,:class_name =>"WarrantyMasterOds",:foreign_key => [:DEVELOPMENTCODE, :HOUSENUMBER]
  has_one :schedule_house_detail_Y15,:class_name =>"SchedhouseDetailOds",:foreign_key => [:DEVELOPMENTCODE, :HOUSENUMBER]
  has_one :dev_master,:class_name =>"DevMasterOds",:primary_key => [:DEVELOPMENTCODE],:foreign_key => [:DEVELOPMENTCODE]
  has_one :option_lot_master, :class_name => "OptionLotMasterOds",:primary_key => [:LOTNUMBER, :DEVELOPMENTCODE],:foreign_key => [:LOTNUMBER, :DEVELOPMENTCODE]
  has_many :house_cost_detail_discount,:class_name =>"HouseCostDetailOds",:foreign_key => [:DEVELOPMENTCODE,:HOUSENUMBER], :conditions => ["OPTIONCODE BETWEEN 89900 AND 89920 AND AMOUNT<0"]
  has_many :sales_subsidiary, :class_name => "SalesSubsidiaryOds", :foreign_key => [:DEVELOPMENTCODE, :HOUSENUMBER, :COMPANYCODE], :primary_key => [:DEVELOPMENTCODE, :HOUSENUMBER, :COMPANYCODE]
  has_many :house_options, :class_name => "HouseOptionsOds", :foreign_key => [:DEVELOPMENTCODE, :HOUSENUMBER, :COMPANYCODE], :primary_key => [:DEVELOPMENTCODE, :HOUSENUMBER, :COMPANYCODE]


  has_one :schedule_house_detail_next_activity, :class_name =>"SchedhouseDetailOds",:foreign_key => [:DEVELOPMENTCODE, :HOUSENUMBER] ,:conditions => ["ACTUALFINISHDATE IS NULL AND LATEFINISHDATE>='#{Date.today}'"], :order => 'LATEFINISHDATE ASC, ACTIVITYCODE DESC'
  
  def self.load_ods_option_lot_master(dev_codes,windows_db_nm)
  	begin
		  	truncate_ods_option_lot_master

		    puts "********** 1A) Started Loading OptionLotMasterOds Table contents at..#{Time.now} ***********"
		    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
		    if (client.nil? == true)
		    elsif (client.nil? == false && client.active? == true)
		      result = client.execute("select * from "+windows_db_nm+".dbo.OPTIONLOTMASTER where DEVELOPMENTCODE in ("+dev_codes+")  and LOTNUMBER != '00000000'");
		      	puts "............................................."
		      	puts "............................................."
		      	
		      	batch = []
		        batch_size = 1000

		        result.each do |row|
		          batch << OptionLotMasterOds.new(:COMPANYCODE => row['COMPANYCODE'],
              																:DEVELOPMENTCODE => row['DEVELOPMENTCODE'],
              																:LOTNUMBER => row['LOTNUMBER'],
              																:TAXBLOCK => row['TAXBLOCK'],
              																:TAXLOT => row['TAXLOT'],
              													      :ADDRESS1 => row['ADDRESS1'],
              													      :RELEASESALESDATE => row['RELEASESALESDATE'],
              													      :SELLERNAME => row['SELLERNAME'],
              													      :LOTPREMIUM => row['LOTPREMIUM'],
                                              :LOTCONVERDATE => row['LOTCONVERDATE'],
                                              :LOTCOMMENTS => row['LOTCOMMENTS']
												                      )
									if batch.size >= batch_size 
		                OptionLotMasterOds.import batch
		                batch = [] 
		              end              
		        	end
              # OptionLotMasterOds.mass_insert(batch, :per_batch => 1000)
		        	OptionLotMasterOds.import batch
		    end

		    puts "Total no. of Loaded rows.........#{OptionLotMasterOds.count}"
		    puts "********** 1B) Finished Loading OptionLotMasterOds Table contents...#{Time.now} ***********"
			#Mailer.notify_user_on_table_load("Successful").deliver
		rescue
			message =  "Unable to load table OptionLotMasterOds"
			#Mailer.notify_user_on_table_load_failure(message).deliver
		end
  end

  def self.truncate_ods_option_lot_master
		ActiveRecord::Base.connection.execute("TRUNCATE ODS_OPTIONLOTMASTER")
	end

   def generate_field_LOT_STATUS
    # according to logic in ihms
     company_code = self.COMPANYCODE
     development_code = self.DEVELOPMENTCODE
     lot_number = self.LOTNUMBER
     option_lot = OptionLotMasterOds.find_by_sql("select * from  ODS_OPTIONLOTMASTER where  COMPANYCODE  = '"+company_code+"' 
      and DEVELOPMENTCODE = '"+development_code+"' and LOTNUMBER = '"+lot_number+"' and  ((LOTCONVERDATE = '' or LOTCONVERDATE is null) and 
      ((OPTIONEXPDATE = '' or OPTIONEXPDATE is null) or OPTIONEXPDATE >= CURDATE()) and 
      (RELEASESALESDATE <= CURDATE() and RELEASESALESDATE <> ''))");
    unless option_lot.blank?
      if option_lot[0].prospect_master_option.blank?
        lot_status = "Option Lot"
      elsif !option_lot[0].prospect_master_option.blank?
        if option_lot[0].prospect_master_option.CONTRACTDATE.blank?
          lot_status = "Option Lot Reservation"
        else
          lot_status = "Option Lot Signed Agreement"
        end
      end
    end
  end

  def generate_field_SPEC_AGING_original
    val="" 
    spec = "N" 
     unless self.prospect_master.nil? || self.prospect_master.LASTNAME.nil? || self.prospect_master.FIRSTNAME.nil?
      if self.prospect_master.LASTNAME.strip=="SPEC" || self.prospect_master.FIRSTNAME.strip=="SPEC"
        spec = "Y"
      end
    end 
    if spec == "Y"  
      unless self.schedule_house_detail_spec_aging.nil? || self.schedule_house_detail_spec_aging.ACTUALFINISHDATE.nil?
        val= (Date.today-self.schedule_house_detail_spec_aging.ACTUALFINISHDATE).to_i
      end
    end
    val
  end

  def generate_field_SPEC_AGING
    val="" 
    spec = "N" 
    if self.generate_field_LOT_STATUS == "SPEC"
      spec = "Y"
    end 
    if spec == "Y" 
    dev_code = self.DEVELOPMENTCODE
    house_num = self.LOTNUMBER 
    # step_num = SpecAging.first.Step_Number
    activity_code = SpecAging.first.Activity_Code
      # sched_rec = SchedhouseDetailOds.find_by_DEVELOPMENTCODE_and_HOUSENUMBER_and_STEPNUMBER_and_ACTIVITYCODE(dev_code,house_num,step_num,activity_code) 
      sched_rec = SchedhouseDetailOds.find_by_DEVELOPMENTCODE_and_HOUSENUMBER_and_ACTIVITYCODE(dev_code,house_num,activity_code) 
      unless sched_rec.nil? || sched_rec.ACTUALFINISHDATE.nil?
        # val= (Date.today-sched_rec.ACTUALFINISHDATE).to_i
        val= (Date.today-sched_rec.ACTUALFINISHDATE)
      end
    end
    val
  end


  def generate_field_spec    
    unless self.prospect_master.nil? || self.prospect_master.LASTNAME.nil? || self.prospect_master.FIRSTNAME.nil?
      if self.prospect_master.LASTNAME.strip=="SPEC" || self.prospect_master.FIRSTNAME.strip=="SPEC"
        "Y"
      else
        "N"
      end
    else
      "N"
    end
  end

  def generate_field_NEXT_ACTIVITY
    next_activity_des="" 
      next_activity = self.schedule_house_detail_next_activity
      unless next_activity.nil?
        unless next_activity.ACTIVITYCODE.blank?
          next_activity_des+=next_activity.ACTIVITYCODE
        end
        unless next_activity.schedule_activity.nil? || next_activity.schedule_activity.DESCRIPTION.blank?
          next_activity_des+=" - "
          next_activity_des+=next_activity.schedule_activity.DESCRIPTION
        end
      end
    next_activity_des
  end

  def generate_field_LASTNAME_FIRSTNAME    
    (self.prospect_master.nil? ? "" : self.prospect_master.FIRSTNAME)+" "+(self.prospect_master.nil? ? "" : self.prospect_master.LASTNAME)
  end 

  def generate_field_SERVICEREPCODE
    self.warranty_master_service_rep_name.map{|s_p| s_p.SERVICEREPCODE}.join(",")
  end

  def generate_field_Warranty_Costs
    amount=0
    amount+= ::GlTrans2013Ods.where("GLACCOUNT = '230#{self.dev_master.DIVISIONCODE}000' AND TRANSREMARK like '%#{self.LOTNUMBER[0..4]}%'").sum(:AMOUNT) unless self.dev_master.nil? || self.dev_master.DIVISIONCODE.nil? 
    amount+= ::GlTrans2014Ods.where("GLACCOUNT = '230#{self.dev_master.DIVISIONCODE}000' AND TRANSREMARK like '%#{self.LOTNUMBER[0..4]}%'").sum(:AMOUNT) unless self.dev_master.nil? || self.dev_master.DIVISIONCODE.nil? 
    amount+= ::GlTrans2015Ods.where("GLACCOUNT = '230#{self.dev_master.DIVISIONCODE}000' AND TRANSREMARK like '%#{self.LOTNUMBER[0..4]}%'").sum(:AMOUNT) unless self.dev_master.nil? || self.dev_master.DIVISIONCODE.nil? 
    amount+= ::GlTrans2016Ods.where("GLACCOUNT = '230#{self.dev_master.DIVISIONCODE}000' AND TRANSREMARK like '%#{self.LOTNUMBER[0..4]}%'").sum(:AMOUNT) unless self.dev_master.nil? || self.dev_master.DIVISIONCODE.nil? 
  amount    
  end

  def generate_field_sales_person1_LASTNAME_FIRSTNAME    
    unless self.prospect_master.blank?
      unless self.prospect_master.sales_person1.blank?
        (self.prospect_master.sales_person1.FIRSTNAME.nil? ? "" : self.prospect_master.sales_person1.FIRSTNAME.strip)+" - "+(self.prospect_master.sales_person1.LASTNAME.nil? ? " - " : self.prospect_master.sales_person1.LASTNAME.strip)
      end
    end
  end

  def generate_field_sales_person2_LASTNAME_FIRSTNAME    
    unless self.prospect_master.blank?
      unless self.prospect_master.sales_person2.blank?
        (self.prospect_master.sales_person2.FIRSTNAME.nil? ? "" : self.prospect_master.sales_person2.FIRSTNAME.strip)+" - "+(self.prospect_master.sales_person2.LASTNAME.nil? ? " - " : self.prospect_master.sales_person2.LASTNAME.strip)
      end
    end
  end
  
  def generate_field_sales_person3_LASTNAME_FIRSTNAME    
    unless self.prospect_master.blank?
      unless self.prospect_master.sales_person3.blank?
        (self.prospect_master.sales_person3.FIRSTNAME.nil? ? "" : self.prospect_master.sales_person3.FIRSTNAME.strip)+" - "+(self.prospect_master.sales_person3.LASTNAME.nil? ? " - " : self.prospect_master.sales_person3.LASTNAME.strip)
      end
    end
  end

  def generate_field_WARRANTY_ISSUES_AGED 
    self.warranty_master_warranty_issues_aged.map{|wia| (Date.today-wia.SERVICE_DATE).to_i}.max
  end

  def generate_field_DEV_COSTS_BY_LOT
    amount=0
    self.house_cost_summary.each do |h_c_s| 
      amount+= h_c_s.ACTUAL unless h_c_s.category_cost_code.nil? || h_c_s.ACTUAL.nil? 
    end  
    amount
  end

  def generate_field_CONSTRUCTION_SCHEDULE_STATUS
    construction_schedule_status=""
    unless self.schedule_house_detail_last_activity.nil? || self.schedule_house_detail_last_activity.ACTUALFINISHDATE.nil? || self.schedule_house_detail_last_activity.LATEFINISHDATE.nil?
      
      if self.schedule_house_detail_last_activity.ACTUALFINISHDATE == self.schedule_house_detail_last_activity.LATEFINISHDATE
        construction_schedule_status="On Track"
      elsif self.schedule_house_detail_last_activity.ACTUALFINISHDATE > self.schedule_house_detail_last_activity.LATEFINISHDATE
        construction_schedule_status="Lag"
      elsif self.schedule_house_detail_last_activity.ACTUALFINISHDATE < self.schedule_house_detail_last_activity.LATEFINISHDATE
        construction_schedule_status="Lead"
      end        
    end
    construction_schedule_status
  end

  def generate_variance_to_date
    variance_to_date = 0
    if !self.house_cost_summary.blank?
      variance_to_date = self.house_cost_summary.where("ACTUAL > 0").sum(:ACTUAL) - self.house_cost_summary.where("ACTUAL > 0").sum(:BUDGETAMOUNT)
    end
    variance_to_date
  end
  
  def generate_field_CONSTRUCTION_VPO_STATUS_old
      construction_vpo_status=""
      unless self.house_cost_detail.empty?
        vpo_sum= self.house_cost_detail.sum(:AMOUNT) 
        if vpo_sum==0
          construction_vpo_status ="On Track"
        elsif vpo_sum>0
          construction_vpo_status ="Over Budget"
        elsif vpo_sum<0
          construction_vpo_status ="Under Budget"
        end 
      end
      construction_vpo_status
  end

  def generate_field_CONSTRUCTION_VPO_STATUS
      construction_vpo_status=""
      unless self.generate_variance_to_date.blank?
        vpo_sum= self.generate_variance_to_date
        if vpo_sum==0
          construction_vpo_status ="On Track"
        elsif vpo_sum>0
          construction_vpo_status ="Over Budget"
        elsif vpo_sum<0
          construction_vpo_status ="Under Budget"
        end 
      end
      construction_vpo_status
  end 

  def generate_field_OUTSTANDING_SERVICE_ORDER_STATUS    
    so_status=""
    if self.generate_field_LOT_STATUS=="Closed" 
      if self.warranty_master.empty?
        so_status="No requests in queue"
      elsif self.warranty_master_warranty_issues_aged.empty?
        so_status="On Track"
      elsif self.warranty_master_warranty_issues_aged.count>0
        so_status="Aged"
      end
    else
      so_status="Not yet Settled"
    end
    so_status
  end

  def generate_field_LOT_PREMUIM_STATUS
      lot_premuim=""
     unless self.LOTPREMIUM.nil? 
        if self.LOTPREMIUM==0
          lot_premuim="Base Lot"
        elsif self.LOTPREMIUM<0
          lot_premuim="- Premium"
        elsif self.LOTPREMIUM>0
          lot_premuim="+ Premium"
        end
     end
     lot_premuim
  end

  def generate_field_MODEL_STATUS
    model_code_status=""
    model_code=self.MODELCODE
    unless model_code.nil?
      if !(/^1[0-9]{2}$/.match(model_code).nil?) || !(/^2[0-9]{2}$/.match(model_code).nil?) || !(/^3[0-9]{2}$/.match(model_code).nil?)
        model_code_status="Ranch Plans"
      elsif !(/^6[0-9]{2}$/.match(model_code).nil?) || !(/^7[0-9]{2}$/.match(model_code).nil?) || !(/^8[0-9]{2}$/.match(model_code).nil?)
        model_code_status="Two-Story Plans"
      elsif !(/^5[0-9]{2}$/.match(model_code).nil?)
        model_code_status="Patio Homes"
      end
      model_code_status
    end 
  end
  def generate_field_COMPLETED_SPEC_STATUS_original
    completed_spec_status=""
    unless self.schedule_house_detail_Y15.nil? || self.generate_field_LASTNAME=="N"

      if !self.schedule_house_detail_Y15.LATEFINISHDATE.blank? && self.schedule_house_detail_Y15.ACTUALFINISHDATE.blank?
        complete_days=(Date.today-self.schedule_house_detail_Y15.LATEFINISHDATE).to_i
        if complete_days<=30
          completed_spec_status="Complete in 1-30 days"
        elsif complete_days>30 and complete_days<=60
          completed_spec_status="Complete in 31-60 days"
        elsif complete_days>60 and complete_days<=90
          completed_spec_status="Complete in 61-90 days"
        elsif complete_days>90
          completed_spec_status="Complete in 90+ days"
        end
      elsif !self.schedule_house_detail_Y15.ACTUALFINISHDATE.blank?
        aged_days=(Date.today-self.schedule_house_detail_Y15.ACTUALFINISHDATE).to_i
        if  aged_days>=1 && aged_days<=30
          completed_spec_status="Aged 1-30 days"
        elsif aged_days>30 and aged_days<=60
          completed_spec_status="Aged 31-60 days"
        elsif aged_days>60 and aged_days<=90
          completed_spec_status="Aged 61-90 days"        
        elsif aged_days>90
          completed_spec_status="Aged 90+ days"
        end
      end  
    end
    completed_spec_status
  end

  def generate_field_COMPLETED_SPEC_STATUS
    completed_spec_status=""
    dev_code = self.DEVELOPMENTCODE
    house_num = self.LOTNUMBER 
    activity_code = SpecAging.first.Activity_Code
    sched_rec = SchedhouseDetailOds.find_by_DEVELOPMENTCODE_and_HOUSENUMBER_and_ACTIVITYCODE(dev_code,house_num,activity_code) 
    # unless sched_rec.nil? || self.generate_field_LOT_STATUS!="Spec"
    if !sched_rec.nil? && self.generate_field_LOT_STATUS=="SPEC"
      if !sched_rec.LATEFINISHDATE.blank? && sched_rec.ACTUALFINISHDATE.blank?
        complete_days=(Date.today-sched_rec.LATEFINISHDATE).to_i
        if complete_days<=30
          completed_spec_status="Complete in 1-30 days"
        elsif complete_days>30 and complete_days<=60
          completed_spec_status="Complete in 31-60 days"
        elsif complete_days>60 and complete_days<=90
          completed_spec_status="Complete in 61-90 days"
        elsif complete_days>90
          completed_spec_status="Complete in 90+ days"
        end
      elsif !sched_rec.ACTUALFINISHDATE.blank?
        aged_days=(Date.today-sched_rec.ACTUALFINISHDATE).to_i
        if  aged_days>=1 && aged_days<=30
          completed_spec_status="Aged 1-30 days"
        elsif aged_days>30 and aged_days<=60
          completed_spec_status="Aged 31-60 days"
        elsif aged_days>60 and aged_days<=90
          completed_spec_status="Aged 61-90 days"        
        elsif aged_days>90
          completed_spec_status="Aged 90+ days"
        end
      end  
    end
    completed_spec_status
  end


  def generate_projected_cost_old
     bud_amt = 0;
      act_amt = 0;
      sum = 0;
    unless self.house_cost_summary.blank?
      self.house_cost_summary.each do |rec|
        if (rec.BUDGETAMOUNT.blank? ? 0 : rec.BUDGETAMOUNT) >  (rec.ACTUAL.blank? ? 0 : rec.ACTUAL)
          sum += (rec.BUDGETAMOUNT.blank? ? 0 : rec.BUDGETAMOUNT)
        else
          sum += (rec.ACTUAL.blank? ? 0 : rec.ACTUAL)
        end
      end
    end
    sum
  end

  def generate_projected_cost
     bud_amt = 0;
      act_amt = 0;
      sum = 0;
    unless self.house_cost_summary.blank?
      self.house_cost_summary.each do |rec|
        if (rec.ACTUAL.blank? ? 0 : rec.ACTUAL) >   (rec.BUDGETAMOUNT.blank? ? 0 : rec.BUDGETAMOUNT)
          sum += rec.ACTUAL.to_i
        else
          sum += rec.BUDGETAMOUNT.to_i
        end
      end
    end
    sum
  end

  def generate_Projected_Gross_Profit
    upgrade_price = base_price = lot_premium = options_price = 0;
    incentives = (self.PROMISSORYAMT1.blank? ? 0 : self.PROMISSORYAMT1) + (self.PROMISSORYAMT2.blank? ? 0: self.PROMISSORYAMT2) + (self.PROMISSORYAMT3.blank? ? 0: self.PROMISSORYAMT3)
    if self.BASEPRICE.blank?
      base_price = 0
    else
      base_price = self.BASEPRICE
    end
    if self.LOTPREMIUM.blank?
      lot_premium = 0
    else
      lot_premium = self.LOTPREMIUM
    end
    if self.UPGRADEPRICE.blank?
      upgrade_price = 0
    else
      upgrade_price = self.UPGRADEPRICE
    end
    if self.OPTIONSPRICE.blank?
      options_price = 0
    else
      options_price = self.OPTIONSPRICE
    end
    sum = base_price + lot_premium + options_price + upgrade_price  -  incentives  - self.generate_projected_cost
  end

  def generate_field_LOTBUZZ_STATUS
    lot_status=""
    if self.generate_field_LOT_STATUS == "Option Lot" || self.generate_field_LOT_STATUS == "Available"
      lot_status="Available Lot"
    elsif self.generate_field_LOT_STATUS == "Option Lot Reservation" || self.generate_field_LOT_STATUS == "Option Lot Signed Agreement"  || self.generate_field_LOT_STATUS == "Lot Hold" || self.generate_field_LOT_STATUS == "Signed Agreement (Not Ratified)" || self.generate_field_LOT_STATUS == "Reservation" 
      lot_status = "Homesite Reservation"
    elsif self.generate_field_LOT_STATUS == "Settled" || self.generate_field_LOT_STATUS == "Ratified"
      lot_status = "Sold"
    elsif self.generate_field_LOT_STATUS == "Not Available"
      lot_status = "Unavailable / Not For Sale"
    elsif self.generate_field_LOT_STATUS == "SPEC"
      lot_status = "Spec Home / Quick Move-in"
    elsif self.generate_field_LOT_STATUS == "MODEL"
      lot_status = "Model Home"
    end
   
    lot_status
  end
    def generate_field_floor_plan_names
    desscription =""
      unless self.nil?
        unless self.elevation_master.nil?
          unless self.elevation_master.model_masters.nil? 
            unless self.elevation_master.model_masters[0].nil? 
             desscription = self.elevation_master.model_masters[0].DESCRIPTION
            end
          end
        end
      end
      desscription
  end

  def generate_field_Rp_Sales_Price
    coop_marketing_fee=0     
    unless self.prospect_master.blank?
      baseprice=self.prospect_master.BASEPRICE unless self.prospect_master.BASEPRICE.blank?
      lotpremium = self.prospect_master.LOTPREMIUM unless self.prospect_master.LOTPREMIUM.blank?
      options_price = self.prospect_master.OPTIONSPRICE unless self.prospect_master.OPTIONSPRICE.blank?
      upgrade_price = self.prospect_master.UPGRADESPRICE unless self.prospect_master.UPGRADESPRICE.blank?
      coop_marketing_fee = baseprice + lotpremium + options_price + upgrade_price
    end 
    coop_marketing_fee
  end

  def generate_field_Rp_SPEC_or_MODEL
    unless self.prospect_master.nil? || self.prospect_master.LASTNAME.nil?
      if self.prospect_master.LASTNAME.strip=="SPEC"
        "SPEC"
      elsif self.prospect_master.LASTNAME.strip=="MODEL"
        "MODEL"
      else
        nil
      end
    else
      nil
    end
  end

  def generate_field_MODELCODE_NAME   
    modelcode_name=""
    unless self.nil?
      modelcode_name+=self.MODELCODE
      unless self.elevation_master_desc.nil?
        modelcode_name+=" , "+self.elevation_master_desc.DESCRIPTION
      end
    end
    modelcode_name
  end

  def generate_loan_bank_name
    unless self.jl_house.blank? 
      unless self.jl_house.jlloan_master.blank? 
        unless self.jl_house.jlloan_master.jl_bank_master_file.blank?
          unless self.jl_house.jlloan_master.jl_bank_master_file.DESCRIPTION.blank?
            bank_name = self.jl_house.jlloan_master.jl_bank_master_file.DESCRIPTION.strip
          end
        end
      end
    end
    bank_name
  end

  def generate_field_BUDGETAMOUNT
    house_cost_summary_sum=0
    unless self.house_cost_summary.empty?
      house_cost_summary_sum = self.house_cost_summary.sum(:BUDGETAMOUNT)
    end
    house_cost_summary_sum
  end

  def generate_field_Rp_QC_Stake_Date
    qc_stake_date=nil
    unless self.schedule_house_detail_Y15.nil?
      unless self.schedule_house_detail_Y15.ACTUALFINISHDATE.blank?
       qc_stake_date = self.schedule_house_detail_Y15.ACTUALFINISHDATE
      else 
       qc_stake_date = self.schedule_house_detail_Y15.ACTUALSTARTDATE
      end
    end
    qc_stake_date
  end

    def generate_field_Rp_Aging 
     rp_aging = nil
     unless self.generate_field_Rp_QC_Stake_Date.nil?
      (Date.today-self.generate_field_Rp_QC_Stake_Date).to_i
     end
  end

  def generate_field_Rp_Discount
    rp_discount = 0
     unless self.house_cost_detail_discount.empty?
     rp_discount = self.house_cost_detail_discount.sum(:AMOUNT)
     end
     rp_discount
  end

  def generate_field_Rp_Retail_Price    
    self.generate_field_Rp_Sales_Price-self.generate_field_Rp_Discount
  end  

  def generate_field_Rp_GM_Percent
    unless self.generate_field_Rp_Sales_Price==0
      ((self.generate_field_Rp_Sales_Price - generate_field_BUDGETAMOUNT)/self.generate_field_Rp_Sales_Price)*100 
    else
      nil
    end
  end
 
  def generate_field_Rp_Construction_Lender
    bank_des=nil
    unless self.jl_loan_detail_single.nil? || self.jl_loan_detail_single.jl_loan_master.nil? || self.jl_loan_detail_single.jl_loan_master.jl_bank_master_file.nil?
      bank_des = self.jl_loan_detail_single.jl_loan_master.jl_bank_master_file.DESCRIPTION.strip
    end  
    bank_des
  end

  def generate_release_date
    release_date = nil
    option_lots_sts = OptionLotsStatus.first.option_lots
    if option_lots_sts == "Yes"
      unless self.option_lot_master.blank?
        if self.option_lot_master.LOTCONVERDATE.blank?
          release_date = self.option_lot_master.RELEASESALESDATE
        else
          release_date = self.RELEASE_DATE
        end
      end
    else
      release_date = self.RELEASE_DATE
    end
    release_date
  end

  def generate_community_city
    cmm_city=''
    if !self.dev_master.blank?
      division_code = self.dev_master.DIVISIONCODE
      # cmm_city = self.dev_master.SUPERCITY
      if  division_code == "004"
        cmm_city = "Indianapolis"
      # elsif division_code == "002"
      #   cmm_city = "Fort Wayne"
      # elsif division_code == "003"
      #   cmm_city = "Columbus"
      end
    end
    cmm_city
  end

  def generate_total_sales_price
    total_sales_price = 0
    if self.generate_field_LOT_STATUS == "Option Lot Reservation" || self.generate_field_LOT_STATUS == "Option Lot Signed Agreement"
      if !self.prospect_master.blank?
        total_sales_price = self.LOTPREMIUM.to_i + self.prospect_master.BASEPRICE.to_i + self.prospect_master.OPTIONSPRICE.to_i + self.prospect_master.UPGRADESPRICE.to_i + self.prospect_master.OTHERAMT1.to_i + self.prospect_master.OTHERAMT2.to_i + self.prospect_master.OTHERAMT3.to_i
      else
        total_sales_price = self.LOTPREMIUM.to_i
      end
    elsif (!self.prospect_master.blank? && (self.generate_field_LOT_STATUS == "Signed Agreement (Not Ratified)" || self.generate_field_LOT_STATUS == "Ratified" || self.generate_field_LOT_STATUS == "Reservation") )
      total_sales_price = self.prospect_master.BASEPRICE.to_i + self.prospect_master.LOTPREMIUM.to_i +  self.prospect_master.OPTIONSPRICE.to_i + self.prospect_master.UPGRADESPRICE.to_i + self.prospect_master.OTHERAMT1.to_i + self.prospect_master.OTHERAMT2.to_i + self.prospect_master.OTHERAMT3.to_i
    elsif self.generate_field_LOT_STATUS == "Settled"
      total_sales_price = self.sales_subsidiary.sum(:AMOUNT)
    else
      # total_sales_price = self.BASEPRICE.to_i + self.LOTPREMIUM.to_i + self.PROMISSORYAMT1.to_i + self.PROMISSORYAMT2.to_i + self.PROMISSORYAMT3.to_i + self.house_options.sum(:SALESPRICE)
    end
    total_sales_price
  end

  def generate_all_prices
    if self.generate_field_LOT_STATUS == "Option Lot Reservation" || self.generate_field_LOT_STATUS == "Option Lot Signed Agreement"
      {
        :Lot_Premium_Val => self.LOTPREMIUM,
        :base_sales => (self.prospect_master.BASEPRICE unless self.prospect_master.blank?) ,
        :option_sales => (self.prospect_master.OPTIONSPRICE unless self.prospect_master.blank?),
        :upgrades_price => (self.prospect_master.UPGRADESPRICE unless self.prospect_master.blank?),
        :Incentives => ((self.prospect_master.OTHERAMT1.to_i) + (self.prospect_master.OTHERAMT2.to_i) + (self.prospect_master.OTHERAMT3.to_i) unless self.prospect_master.blank?),
        :Incentive_1 => (self.prospect_master.OTHERAMT1 unless self.prospect_master.blank?),
        :Incentive_2 => (self.prospect_master.OTHERAMT2 unless self.prospect_master.blank?),
        :Incentive_3 => (self.prospect_master.OTHERAMT3 unless self.prospect_master.blank?),

      }
    elsif (!self.prospect_master.blank? && (self.generate_field_LOT_STATUS == "Signed Agreement (Not Ratified)" || self.generate_field_LOT_STATUS == "Ratified" || self.generate_field_LOT_STATUS == "Reservation") )
      {
        :Lot_Premium_Val => self.prospect_master.LOTPREMIUM,
        :base_sales => (self.prospect_master.BASEPRICE unless self.prospect_master.blank?),
        :option_sales => (self.prospect_master.OPTIONSPRICE unless self.prospect_master.blank?),
        :upgrades_price => (self.prospect_master.UPGRADESPRICE unless self.prospect_master.blank?),
        :Incentives => ((self.prospect_master.OTHERAMT1.to_i) + (self.prospect_master.OTHERAMT2.to_i) + (self.prospect_master.OTHERAMT3.to_i) unless self.prospect_master.blank?),
        :Incentive_1 => (self.prospect_master.OTHERAMT1 unless self.prospect_master.blank?),
        :Incentive_2 => (self.prospect_master.OTHERAMT2 unless self.prospect_master.blank?),
        :Incentive_3 => (self.prospect_master.OTHERAMT3 unless self.prospect_master.blank?),

      }
    elsif self.generate_field_LOT_STATUS == "Settled"
      {
        :Lot_Premium_Val => self.LOTPREMIUM,
        :base_sales => self.BASEPRICE,
        :option_sales => self.OPTIONSPRICE,
        :upgrades_price => self.UPGRADESPRICE,
        :Incentives => ((self.prospect_master.OTHERAMT1.to_i) + (self.prospect_master.OTHERAMT2.to_i) + (self.prospect_master.OTHERAMT3.to_i) unless self.prospect_master.blank?),
        :Incentive_1 => (self.prospect_master.OTHERAMT1 unless self.prospect_master.blank?),
        :Incentive_2 => (self.prospect_master.OTHERAMT2 unless self.prospect_master.blank?),
        :Incentive_3 => (self.prospect_master.OTHERAMT3 unless self.prospect_master.blank?),

      }
    else
      {
        :Lot_Premium_Val => self.LOTPREMIUM,
        :base_sales => "",
        :Incentives =>  "",
        :option_sales => (self.house_options.sum(:SALESPRICE) unless self.house_options.blank?),
        :upgrades_price => "",
        :Incentive_1 => "",
        :Incentive_2 => "",
        :Incentive_3 => "",
      }
    end
  end
end