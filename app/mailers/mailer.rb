class Mailer < ActionMailer::Base
  default from: "support@lotvue.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.mailer.invitation.subject
  #

def lead_request_in_community_web(details)
  @name = details.Name
  @email_address = details.Email_Address
  @contact_num = details.Contact_Num
  @lot_number = details.Lot_Number
  @phase_name = details.Phase_Name
  @community_name = details.Community_Name
  @city_name = Tbldatum.find_by_Community(@community_name).community_city
  mail_to = ContactForm.find_by_city_name(@city_name).Email_Address
  mail(to: mail_to, subject: "Lead Request")
end


def invitation(invitation)
  @user = invitation.Email_Address
  @first_name = invitation.First_Name
  @last_name = invitation.Last_Name
  @token = invitation.token
  @url= SITE_ROOT_DOMAIN_NAME+'/home/registration'+'?token=' + @token
  mail(to: invitation.Email_Address, subject: ' LotVue User Registration Invitation')
end

def confirmation(invitation)
  @user = invitation.Email_Address
  @first_name = invitation.First_Name
  @last_name = invitation.Last_Name
  @token = invitation.token
  @url= SITE_ROOT_DOMAIN_NAME
  # @url= SITE_ROOT_DOMAIN_NAME+'/home/updateactive'+'?token=' + @token
  mail(to: invitation.Email_Address, subject: 'LotVue User Registration Confirmation').deliver!
end

def forgotpwd(invitation)
  @user = invitation.Email_Address
  @token = invitation.token
  @url= SITE_ROOT_DOMAIN_NAME+'/home/confirm_forgotpwd'+'?token=' + @token
  mail(to: invitation.Email_Address, subject: 'Reset Password Instructions')
end

def notify_on_status_change(lots)  
  @lots = lots  
  users_to_notify = Tblusers.notify_users
      unless users_to_notify.empty?
        users_to_notify.each do |user|          
          @first_name = user.First_Name
          @last_name = user.Last_Name          
        end
        emails = users_to_notify.collect(&:Email_Address).join(",")        
        emails+=",support-group@lotvue.com"
        mail(to: emails, subject: "Lot Closed Via Automation in #{SITE_ROOT_DOMAIN_NAME}")
      end  
end

def notify_on_lot_event(lot, previous_lot_status,current_lot_status, event_date, event_name, event_by)  
  @lot = lot
  @previous_lot_status=previous_lot_status
  @current_lot_status=current_lot_status
  @event_name = event_name
  @event_date = event_date 

  unless event_by.blank? || Tblusers.find(event_by).blank?
    @event_by = Tblusers.find(event_by).Email_Address unless event_by.blank?   
  end

  users_to_notify = Tblusers.notify_users
      unless users_to_notify.empty?
        users_to_notify.each do |user|          
          @first_name = user.First_Name
          @last_name = user.Last_Name          
        end
        emails = users_to_notify.collect(&:Email_Address).join(",")        
        emails+=",support-group@lotvue.com"
        mail(to: emails, subject: "#{event_name} in #{SITE_ROOT_DOMAIN_NAME} at #{event_date}")
      end  
end


  def notify_user_on_failure_jar_execute(message)
    @message = message
    mail(to: "divyar@lotvue.com", subject: "Connection to IHMS for Echelon Homes failed in #{SITE_ROOT_DOMAIN_NAME}")
  end

  def notify_user_on_successful_jar_execute(message)
    @message = message
    mail(to: "support@lotvue.com", subject: "Connection to Echelon Homes Jar is successful and data loaded in #{SITE_ROOT_DOMAIN_NAME}")
  end

  def notify_user_on_etl_process(message1,message2,err_msg)
    @message1 = message1
    @message2 = message2
    @err_msg = err_msg
    mail(to: "support@lotvue.com", subject: "Echelon Homes>> Data Refresh in #{SITE_ROOT_DOMAIN_NAME} from Amazon VM >>"+message1)
  end

end
