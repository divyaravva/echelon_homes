class UserGroupsController < ApplicationController
	before_filter :authenticate
	before_filter :is_admin

	def index
		@user_groups = Tblusergroup.all
	end

	def update_info		
		@user_groups = Tblusergroup.find(params[:user_group][:ID])
		unless @user_groups.nil?
			@user_groups.update_attributes(params[:user_group].reject{|group_info| group_info == "ID"})
		end 		
		redirect_to user_groups_url
	end
	def create_info		
		if params[:user_group].present? 
			user_group=Tblusergroup.create(params[:user_group])
			gac=GroupAccessLevel.new
			gac.User_Group=user_group.ID
			gac.save
		end
		redirect_to user_groups_url
	end

	# def delete_info
	# 	@user = User.find(params[:user][:ID])
	# 	@user.delete
	# 	redirect_to users_url
	# end
end