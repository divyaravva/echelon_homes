class SchedHouseHeaderOds < ActiveRecord::Base
  attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_SCHEDHOUSEHEADER"


  def self.load_ods_sched_house_header(windows_db_nm)

    truncate_ods_sched_house_header

    puts "********** 1A) Started Loading SCHEDHOUSEHEADER Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from "+windows_db_nm+".dbo.SCHEDHOUSEHEADER");
        puts "............................................."
        puts "............................................."

        batch=[]
        batch_size = 400

        result.each do |row|    
          batch << SchedHouseHeaderOds.new(:COMPANYCODE => row['COMPANYCODE'],
                                      :DEVELOPMENTCODE => row['DEVELOPMENTCODE'],
                                      :HOUSENUMBER => row['HOUSENUMBER'],
                                      :DESCRIPTION => row['DESCRIPTION'],
                                      :UNPACKEDHOUSENUM => row['UNPACKEDHOUSENUM'],
                                      :HOLDFLAG => row['HOLDFLAG'],
                                      :ARCHIVEFLAG => row['ARCHIVEFLAG'],
                                      :LOADDATE => row['LOADDATE'],
                                      :FIELD3 => row['FIELD3'],                                     
                                    )
            
            if batch.size >= batch_size
              SchedHouseHeaderOds.import batch
              batch = [] 
            end
        end
        SchedHouseHeaderOds.import batch
    end

    puts "Total no. of Loaded rows.........#{SchedHouseHeaderOds.count}"
    puts "********** 1B) Finished Loading SchedHouseHeaderOds Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_sched_house_header
    ActiveRecord::Base.connection.execute("TRUNCATE ODS_SCHEDHOUSEHEADER")
  end


end