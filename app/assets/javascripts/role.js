//First Add Lot Collection Button Click
$("#add_lot_collection").click(function(){
    $.ajax({
            type: "GET",
            url: '/roles/get_lot_collections',
            data: {selected_lot_collection : selected_collections},
            success: function(data) {
                $("#lot_collection_list").html(data);
            }
    });
    $("#add_lot_collection_dialog").modal({
                            backdrop: false,
                            keyboard: false,
                            show: true
    }); 

});

//Second Add Button Click in Dialog

$('#add_lot_collection_dialog #add').on('click',function () {
    new_selected_collection_array = [];
     lotCollections = $("input[name=lot_collections_list]");        
        for (i = 0; i < lotCollections.length; i++)
        {
            if (lotCollections[i].checked){
                selected_collections.push(lotCollections[i].value);
                new_selected_collection_array.push(lotCollections[i].value);
            }
        }
        // for(i = 0; i < selected_collections.length; i++){
        //     $("#panel_"+selected_collections[i]).remove();
        // }
        $.ajax({
            type: "GET",
            url: '/roles/get_role_view_per_collection',           
            data: {selected_lot_collection : new_selected_collection_array},
            success: function(data) {                
                $("#roles_selection_list").append(data);             
                $(".help-block").remove(); 
            }
        });
    });
function delete_collection(lot_collection_id){    
    var i = selected_collections.indexOf(lot_collection_id);    
    if(i != -1) {
        selected_collections.splice(i, 1);
    }
    $("input[name='collection_keys_ids["+lot_collection_id+"][][menu_options]']").rules("remove")
    $("#panel_"+lot_collection_id).remove();
}
