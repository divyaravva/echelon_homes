class GlTrans2016Ods < ActiveRecord::Base
	attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_GLTRANS2016"

  def self.load_ods_gltrans2016(dev_codes,windows_db_nm)

  	truncate_ods_gltrans2016

    puts "********** 1A) Started Loading GlTrans2016 Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
      #message = "Inside the Lots info Load Method"
      #Mailer.notify_user_on_client_failure(message).deliver
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from "+windows_db_nm+".dbo.GLTRANS2016 where DEVELOPMENTCODE in ("+dev_codes+")  and HOUSENUMBER != '00000000'");
      	puts "............................................."
      	puts "............................................."

        batch=[]
        batch_size = 1000

        result.each do |row|
          #GlTrans2016.find_or_initialize_by_COMPANYCODE_and_DEVELOPMENTCODE_and_LOTNUMBER(row['COMPANYCODE'], row['DEVELOPMENTCODE'], row['LOTNUMBER'])
          batch << GlTrans2016Ods.new(:COMPANYCODE => row['COMPANYCODE'],
                                      :DEVELOPMENTCODE => row['DEVELOPMENTCODE'],
                                      :HOUSENUMBER => row['HOUSENUMBER'],
                                      :GLACCOUNT => row['GLACCOUNT'],
                                      :TRANSREMARK => row['TRANSREMARK'],
                                      :AMOUNT => row['AMOUNT']
                                      )
            if batch.size >= batch_size
              GlTrans2016Ods.import batch
              batch = [] 
            end
        end
        # GlTrans2016Ods.mass_insert(batch, :per_batch => 1000)
        GlTrans2016Ods.import batch
    end

    puts "Total no. of Loaded rows.........#{GlTrans2016Ods.count}"
    puts "********** 1B) Finished Loading GlTrans2016 Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_gltrans2016
		ActiveRecord::Base.connection.execute("TRUNCATE ODS_GLTRANS2016")
	end


end