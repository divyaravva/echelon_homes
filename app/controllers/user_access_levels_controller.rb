class UserAccessLevelsController < ApplicationController
	before_filter :authenticate
	before_filter :is_admin
    before_filter :get_product
 	before_filter :get_product_view
  
	def index			
		@main_menu_tabs = MenuOption.get_all_menu
		if request.xhr?
			unless params[:Group_ID].nil?
				@user_groups = Tblusergroup.find(params[:Group_ID])
			else
				@user_groups = Tblusergroup.first
			end
			render :partial => "views_list"
		else
			@user_groups = Tblusergroup.first	
		end
				
	end

	def change_group_view
		# @main_menu_tabs = Menu.all
		@main_menu_tabs = MenuOption.get_all_menu
		@user_groups = Tblusergroup.find(params[:Group_ID])
		render :partial => "views_sub_view_list"					
	end


	def update_info	
		# @main_menu_tabs = Menu.all
		@main_menu_tabs = MenuOption.get_all_menu
		@user_groups = Tblusergroup.find(params[:Group_ID]) unless params[:Group_ID].nil?
		unless params[:menu_details].nil?			
				access_list = []
				# params[:menu_details].each do |id,value,menu_option_name, access_level|
				# params[:Access_id].each do |menu_option_name, access_level|
				params[:menu_details].each do |d|
					menu_option_name = d.keys[1]
					menu_id = d.keys[0]
					access_list.push({:Level=>2, :ID => d[menu_id],:Menu_Option_Name => menu_option_name, :Access_id => d[menu_option_name] })
				end			
			@user_groups.save_group_access(access_list)
			@user_groups.update_info(params[:Group_Name],params[:Description]) 		  		  
		end	
		render :partial => "views_list"
	end

	def create_user_group	
		@all_groups = Tblusergroup.fetch_user_group_names
		@main_menu_tabs = MenuOption.get_all_menu
		unless params[:Access_id].nil? 
			@user_groups = Tblusergroup.create(:Group_Name => params[:Group_Name], :Description => params[:Description]) unless params[:Group_Name].nil?				
				access_list = []
				params[:Access_id].each do |menu_option_name, access_level|
					access_list.push({:Level=>2, :Menu_Option_Name => menu_option_name, :Access_id => access_level})
				end		
			@user_groups.save_group_access(access_list)			
			render :partial => "views_list"		  		  
		end		
	end
	
	def create_user_group_view		
		@main_menu_tabs = MenuOption.get_all_menu
		render :partial => "create_view"
	end

	def edit_user_group_view		
		@main_menu_tabs = MenuOption.get_all_menu
		@user_groups = Tblusergroup.find(params[:Group_ID]) unless params[:Group_ID].nil?
		@all_user_groups = Tblusergroup.fetch_user_group_names.reject{|group_name| group_name==@user_groups.Group_Name}
		render :partial => "edit_view"
	end

	def delete_user_group		
		# @main_menu_tabs = Menu.all
		@main_menu_tabs = MenuOption.get_all_menu
		@user_groups = Tblusergroup.find(params[:Group_ID]) unless params[:Group_ID].nil?		
		@user_groups.role_access.destroy_all unless @user_groups.nil?
		@user_groups.destroy unless @user_groups.nil?		
		@user_groups = Tblusergroup.first
		render :partial => "views_list"	
	end
end