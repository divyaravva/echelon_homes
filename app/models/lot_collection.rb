class LotCollection < ActiveRecord::Base
  attr_protected
  self.table_name = "lot_collections"     
  after_create :assign_lot_collection_to_admin_role   

  has_many :lot_collection_lots,:dependent => :destroy,:foreign_key => "Lot_Collection_Id"  
  has_many :lots ,:through => :lot_collection_lots
  has_many :rds_lot_collections, :dependent => :destroy,:foreign_key => "Lot_Collection_Id"  
  has_many :menu_options, :through => :rds_lot_collections 

  validates_uniqueness_of :collection_name, :message => "Lot Collection Name has already taken." 
  validates_presence_of   :collection_name, :message => "Lot Collection Name is blank." 

  def self.find_lots_for_create_collection(community,phase)
    LotCollection.find_by_sql("select lot_info.Phase_Name,lot_info.Community,colc.Lot_Collection_Id,
    	lot_info.Lot_Number from GIS_LotsInfo as lot_info left outer join lot_collection_lots 
    	as colc on lot_info.Community=colc.Community and lot_info.Phase_Name=colc.Phase and 
    	lot_info.Lot_Number=colc.Lot_Number WHERE lot_info.Community='#{community}' 
    	and lot_info.Phase_Name='#{phase}' and colc.Lot_Collection_Id is NULL")
  end

  def self.find_lots_for_collection_creation(community)    
  	LotCollection.find_by_collection_name(community+"-ALL-LOTS").lots.where("lot_collection_lots.Community" => community)
  end	

  def assign_lot_collection_to_admin_role    
    role = RoleDefinition.find_by_Role_Definition("Admin")    
    rds_lot_collection = self.rds_lot_collections.create(:Role_Definition_Id => role.id,
           :Lot_Collection_Id => self.id, :Product_Type_Id => "1,2")
    MenuOption.all.each do |menu_option|        
        rds_lot_collection_menu_option = RdsLotCollectionsMenuOption.create(:Rds_Lot_Collection_Id => rds_lot_collection.id, 
              :Menu_Option_Id => menu_option.Id)                
        role_access = RdsLotCollectionsMenuOptionRoleAccess.create(:Rds_Lot_Collections_Menu_Options_Id => rds_lot_collection_menu_option.id,
              :Role_Access_Id => Access.first.ID)
    end    
  end 

  def self.get_associated_object
    includes([:lot_collection_lots])
  end

  def self.create_existing_collections    
    lot_groups = Tbldatum.select("Community,Builder,Unique_Id,Phase_Name,House_Type").where("GIS_LotsInfo.Builder is not null").group(:Builder,:House_Type)    
    lot_groups.each do |lot_group|
      lc_name = "#{lot_group.Builder}-#{lot_group.House_Type}"      
      b_id = ::Builder.find_by_Builder_Name(lot_group.Builder).id
      lc=LotCollection.find_or_create_by_collection_name_and_builder_id(lc_name, b_id)      
      lots = Tbldatum.select("Community,Builder,Unique_Id,Phase_Name,House_Type,Floor_Type_Id").where("GIS_LotsInfo.Builder='#{lot_group.Builder}' and GIS_LotsInfo.House_Type='#{lot_group.House_Type}'")
      lots.each do |lot|
        lc.lot_collection_lots.create(
          :Community => lot.Community,
          :Phase => lot.Phase_Name,
          :Lot_Number => lot.Unique_Id,
         :floor_type_id => (lot.House_Type.nil?)?nil:FloorType.find_by_floor_type(lot.House_Type).id 
          )
        end
    end      
  end  

  def self.assign_plans_for_lc
    lcs=self.all.drop(1)
    lcs.each do|lc|
        fps=FloorPlan.find_all_by_builder_id_and_Product_Type_Id(lc.builder_id,lc.lot_collection_lots.first.floor_type_id).map{|fp| fp.id}
        lc.floor_plan_ids = fps      
    end
  end


  def self.create_default_lot_collections
      ActiveRecord::Base.connection.execute("TRUNCATE lot_collections")  
      ActiveRecord::Base.connection.execute("TRUNCATE lot_collection_lots")  
      # ActiveRecord::Base.connection.execute("TRUNCATE Builder_Plan_Lot_collections")  
      ActiveRecord::Base.connection.execute("TRUNCATE Role_Definitions")  
      ActiveRecord::Base.connection.execute("TRUNCATE Rds_Lot_Collections")  
      ActiveRecord::Base.connection.execute("TRUNCATE Rds_Lot_Collections_Menu_Options")  
      ActiveRecord::Base.connection.execute("TRUNCATE Rds_Lot_Collections_Menu_Options_Role_Access")  
      RoleDefinition.create(:Role_Definition => "Admin")
      # LotCollection.create_unassigned_lot_collection
      # LotCollection.create_existing_collections
      LotCollection.assign_lot_collections_to_admin_role
      # LotCollection.assign_plans_for_lc
  end

  def get_floor_plans
    Rails.cache.fetch("echelon_floor_plans_of_lot_collection_#{self.id}") do
      self.floor_plans
    end
  end

  def get_builder
    Rails.cache.fetch("echelon_builder_of_lot_collection_#{self.id}") do
      self.builder
    end
  end


 def self.create_default_lot_collection_based_on_lotsinfo    
    lots = Tbldatum.select("Community,Unique_ID,Phase_Name").where("GIS_LotsInfo.Community='North Fork'")      
    lots.each do |lot|
      LotCollectionLot.create(
        :Community => lot.Community,
        :Phase => lot.Phase_Name,
        :Lot_Number => lot.Unique_ID,
        :Lot_Collection_Id => 1
        )
    end
  end

  def self.total_lots_in_all_collec
    @total_lots = 0
    self.all.each do |lc|
      @total_lots += lc.lot_collection_lots.count
    end
    @total_lots
  end

  def self.assign_lot_collection_to_admin_role_new_menus    
    role = RoleDefinition.find_by_Role_Definition("Admin")    
    # rds_lot_collection = self.rds_lot_collections.find_or_create_by(:Role_Definition_Id => role.id,
           # :Lot_Collection_Id => self.id, :Product_Type_Id => "1,2")
    rds_lot_collection = RdsLotCollection.all
    rds_lot_collection.each do |rds|
      MenuOption.all.each do |menu_option|        
          rds_lot_collection_menu_option = RdsLotCollectionsMenuOption.find_or_initialize_by_Rds_Lot_Collection_Id_and_Menu_Option_Id(rds.id, menu_option.Id)
          rds_lot_collection_menu_option.update_attributes(:Menu_Option_Id => menu_option.Id)
          role_access = RdsLotCollectionsMenuOptionRoleAccess.find_or_initialize_by_Rds_Lot_Collections_Menu_Options_Id_and_Role_Access_Id(rds_lot_collection_menu_option.id, Access.first.ID)
          role_access.update_attributes(:Role_Access_Id => Access.first.ID)
      end 
    end   
  end 

  def self.update_lot_collection_lots
    ActiveRecord::Base.connection.execute("TRUNCATE lot_collection_lots")   
    lot_groups = Tbldatum.group(:Community).where("Community IS NOT NULL")
    lot_groups.each do |group|     
      lc=LotCollection.find_or_create_by_collection_name("#{group.Community}-ALL-LOTS") 
      # lots=Tbldatum.where("Community=? and SVG_ID is not",group.Community, null)
      lots = Tbldatum.where("Community='#{group.Community}' and SVG_ID is not null")
      lots.each do |lot|
        rec_presence = LotCollectionLot.find_by_Lot_Number(lot.Unique_ID)
        if rec_presence.blank?
          lc.lot_collection_lots.create(        
            :Community => lot.Community, 
            :Phase =>lot.Phase_Name, 
            :Lot_Number => lot.Unique_ID,
            :Lot_Collection_Id => lc.id
          )
        end
      end     
    end
  end

  def self.assign_lot_collections_to_admin_role    
    role = RoleDefinition.find_or_create_by_Role_Definition("Admin")
    LotCollection.all.each do |lc|
      rds_lot_collection = role.rds_lot_collections.find_or_initialize_by_Role_Definition_Id_and_Lot_Collection_Id(role.id,lc.id)
      rds_lot_collection.update_attributes(:Product_Type_Id => "1,2")
      MenuOption.all.each do |menu_option|        
        rds_lot_collection_menu_option = RdsLotCollectionsMenuOption.find_or_create_by_Rds_Lot_Collection_Id_and_Menu_Option_Id(rds_lot_collection.id, menu_option.Id)
        role_access = RdsLotCollectionsMenuOptionRoleAccess.find_or_create_by_Rds_Lot_Collections_Menu_Options_Id_and_Role_Access_Id(rds_lot_collection_menu_option.id, Access.first.ID)
      end
    end
  end

  def self.update_lc_tbls
    # run svg_id method after checking the method
    Tbldatum.update_SVG_ID
    self.update_lot_collection_lots
    self.assign_lot_collections_to_admin_role
  end

end
