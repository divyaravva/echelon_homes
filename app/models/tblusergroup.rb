class Tblusergroup < ActiveRecord::Base
  attr_accessible :Group_Name, :ID
  self.table_name = "tblaccessgroup"
  self.primary_key ="ID"
  has_many :role_access, :class_name => 'Tblusergroupaccess', :foreign_key => 'Group_id',:dependent => :destroy
  has_many :users, :class_name => 'Tblusers', :primary_key => 'ID', :foreign_key => 'Access_Group'
  attr_protected

  def menu_items  
     # menu_option = self.role_access.where(:Access_id=>Access.get_ids_except_no_access)
     # .includes(:menu_option).map{|x|x.menu_option.MenuOptionName}.uniq 
      # Menu.find_all_by_MenuName(menu_items)   
      MenuOption.get_menu
         
  end

  def menu_items_all
    self.role_access.includes(:tab).where("tabs.parent_tab_id is NULL")    
  end    

  def self.sub_menu_items_of_menu(menu_name)
      # self.role_access.where(:Access_id=>Access.get_ids_except_no_access).joins(:menu_option).where("GIS_MenuOption.MenuName=?", menu_name).where( "GIS_MenuOption.P_Id=?",ProductType.get_ids_of_product)
     self.find_by_sql("select Distinct(MenuOptionName),td.Access_id from GIS_MenuOption as mo, 
      MenuOption_ProjectType as mp, tbldatafrmfields as td where mp.MenuOptionId = mo.Id and 
      td.Access_id In (select ID from accesses where name!='No Access') and mo.MenuName='"+menu_name+"' and mp.ProjectId=1"+ProductType.get_ids_of_product+" and td.Menu_Option_Name= mo.MenuOptionName and td.Menu_Option_Name = mo.MenuOptionName")

  end

  def all_sub_menu_items_of_menu(id)
     self.role_access.includes(:tab).where("tabs.parent_tab_id = #{id}")
  end


  def sub_menu_items
     self.role_access.where(:level => 2)
  end

  def sub_accessible_roles
     self.role_access.where(:Access_id=>1).includes(:tab).map{|ra| [ra.tab.ID, ra.tab.name]}                        
  end 

 def self.fetch_user_groups
    self.select("Group_Name,ID").map{|g| [g.Group_Name, g.ID]}
 end

 def self.fetch_user_group_names
    self.select("Group_Name").map{|g| g.Group_Name}    
 end 

 def save_group_access(access_list)  
    access_list.each do |view_access|
      x=self.role_access.find_or_initialize_by_Menu_Option_Name(view_access[:Menu_Option_Name])
      x.update_attributes(view_access)
    end
 end

 def fetch_by_menu_option_name(menu_option_name)
   # self.role_access.find_by_Menu_Option_Name(menu_option_name)
   self.role_access.find(menu_option_name)
 end

 def update_info(group_name, description)
  unless group_name.nil?
    attrs={:Group_Name => group_name, :Description => description}
  else
    attrs={:Description => description}
  end
  self.update_attributes(attrs)
 end

 

end
