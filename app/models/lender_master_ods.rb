class LenderMasterOds < ActiveRecord::Base
	attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_LENDERMASTER"

  def self.load_ods_lender_master(windows_db_nm)
    #ActiveRecord::Base.transaction do
      truncate_ods_lender_master

      puts "********** 1A) Started Loading LenderMasterOds Table contents at..#{Time.now} ***********"
      client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
        if (client.nil? == true)
          #message = "Inside the Lots info Load Method"
          #Mailer.notify_user_on_client_failure(message).deliver
        elsif (client.nil? == false && client.active? == true)
          result = client.execute("select * from "+windows_db_nm+".dbo.LENDERMASTER");
            puts "............................................."
          	puts "............................................."

            batch=[]
            batch_size = 1000

            result.each do |row|
             batch << LenderMasterOds.new(:COMPANYCODE => row['COMPANYCODE'],
                        										:LENDERCODE => row['LENDERCODE'],
                        										:NAME => row['NAME'],
                        										:ADDRESS1 => row['ADDRESS1'],
                        										:ADDRESS2 => row['ADDRESS2'],
                        										:ADDRESS3 => row['ADDRESS3'],
                        										:PHONENUMBER => row['PHONENUMBER'],
                        										:LOANOFFICER => row['LOANOFFICER'],
                        										:LOANPROCESSOR => row['LOANPROCESSOR'],
                                            :OFFICEREMAIL => row['OFFICEREMAIL'],
                                            :PROCESSOREMAIL => row['PROCESSOREMAIL'],
                        										:TERMINATOR => row['TERMINATOR']
    								                        )
            
              if batch.size >= batch_size 
                LenderMasterOds.import batch
                batch = [] 
              end
            end
            LenderMasterOds.import batch
           end
    #end   

        puts "Total no. of Loaded rows.........#{LenderMasterOds.count}"
        puts "********** 1B) Finished Loading LenderMasterOds Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_lender_master
		ActiveRecord::Base.connection.execute("TRUNCATE ODS_LENDERMASTER")
	end
  
end