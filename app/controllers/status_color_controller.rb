class StatusColorController < ApplicationController
  before_filter :authenticate
  before_filter :has_roles  
  before_filter :get_product
  before_filter :get_product_view
  before_filter :get_comm_city_view
  before_filter :get_community_city 
  before_filter :get_communities  
  before_filter :get_communities_view    
  before_filter :get_parcels    
  before_filter :get_parcels_view
  
	def index
    @type = params[:product_type]
    @menu = params[:menu_name]
    @menu_option = params[:menu_option]
    @community = params[:community]
    @legend_type = params[:legend_type]
		@status_color = StatusColor.lot_status_details
	end
  
	def edit 
    @status_color_details = StatusColor.find(params[:id]) unless params[:id].nil?
    @status_name = @status_color_details.StatusKey unless @status_color_details.nil?
    @color = StatusColor.all.select{|x| x.StatusKey!=@status_name} 
    @type = params[:product_type]
    @community = params[:community]
    @menu_name = params[:menu]
    @menu_option_name = params[:menu_option]
    @legend_type = params[:filter]
   end

   def update
    
    @type = params[:product_type]
    @menu = params[:menu]
    @menu_option = params[:menu_option]
    @community = params[:community]
    @legend_type = params[:legend_type]
    all_status_key_recs = StatusDetail.find_all_by_LegendType_and_StatusKey(params[:legend_type],params[:status_key])
    all_status_key_recs.each do |each_rec|
      each_rec.update_attributes(:ColorCode => "#"+params[:status_color])
    end
    Tbldatum.invalidate_cache
    redirect_to :controller => 'status_color', :action => 'index', :product_type => @type, :legend_type => @legend_type
   end

   def lot_color_status
      status_color_details = StatusColor.find(params[:id]) unless params[:id].nil?   
      status_name = StatusColor.find_all_by_StatusKey(status_color_details.StatusKey)
      status_name.each do |details|
      if params[:status] == "archive" 
        details.update_attributes({:Is_Active => 0}) unless status_color_details.nil?
      else
        details.update_attributes({:Is_Active => 1}) unless status_color_details.nil?
       end
      end
      @status_color = StatusColor.lot_status_details
       render :partial => "status_color_list"
   end

  def get_menu
    @menu_from_edit = params[:menu_from_edit]
    @menu_option_from_edit = params[:menu_option_from_edit]
    @type= params[:product_type]
    @community = params[:community]
    @menus = MenuOption.get_menus(params[:product_type],@udata, @community )
    render :partial => "menu_category"
   end

  def get_sub_menus
   @menu_option_from_edit = params[:menu_option_from_edit]
   @type = params[:product_type]
   @menu = params[:menu_name]
   @community = params[:community]
   @sub_menus = MenuOption.get_sub_menus(params[:menu_name])
   render :partial => "sub_menu_list"
  end

  def get_lot_status
    @type = params[:product_type]
    @menu = params[:menu_name]
    @legend_type = params[:legend_type]
    @community = params[:community]
    @status_color = MenuOption.get_lot_status(params[:legend_type])
    render :partial => "status_color_list"
  end

  def get_community
    @type = params[:product_type]
    @community_from_edit = params[:community_from_edit]
    @menu_from_edit = params[:menu_from_edit]
    @menu_option_from_edit = params[:menu_option_from_edit]
    @legend_type_from_edit = params[:legend_type_from_edit]
    @community = MenuOption.get_communities_list
    render :partial => "communities_list"
  end

  def get_filter_values
    @type = params[:product_type]
    @community = params[:community]
    @community_from_edit = params[:community_from_edit]
    @legend_type_from_edit = params[:legend_type_from_edit]
    @filter_values = MenuOption.filter_values(@type)
    render :partial => "legend_list"
  end
end