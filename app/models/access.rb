class Access < ActiveRecord::Base
  attr_accessible :name
    self.table_name = "accesses"

 def self.fetch_access
  self.order("ID desc").select("name,ID").map{|g| [g.name, g.ID]}
 end

 def self.get_ids_except_no_access
 	self.all.map{|a|a.ID unless a.name=="No Access"}.compact
 end

 def self.get_name_by_id(id)
 	self.find(id).alias_name unless id.nil?
 end
     
end
