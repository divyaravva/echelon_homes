class Report < ActiveRecord::Base
	
  attr_accessible :document, :user_id, :description, :document_file_name, :display_document_file_name, :report_type
  has_attached_file :document,
                    :url => "/images/:class/:id/:style/:basename.:extension",
                    :path => DOCUMENT_PATH+"shared/images/:class/:id/:style/:basename.:extension"


  validates_attachment_presence :document, :message => "Please a upload Document file"
  has_many :rds_lot_collections_report_role_accesses,:dependent => :destroy, :foreign_key => "Report_Id" 


def self.get_uploaded_reports
  self.find(:all, :conditions => ["report_type =?", "Uploaded"])
end

def self.get_generated_reports
  self.find(:all, :conditions => ["report_type =?", "Generated"])
end

def self.get_the_report_based_on_current_tab(user)
  Tblusers.get_uploaded_reports_with_view_access(user)
end

def self.get_access_permission_for_report(report, role_id, lot_collection_id)
   role_acc_rec = report.rds_lot_collections_report_role_accesses
    role_acc_rec_for_role = role_acc_rec.where("Role_Id =? and Lot_Collection_Id=?", role_id, lot_collection_id) unless role_acc_rec.blank?
   unless role_id.blank? || role_acc_rec_for_role.blank?    
    role_acce_id = role_acc_rec_for_role.first.Role_Access_Id
   else
    role_acce_id = ''
   end
end

end
