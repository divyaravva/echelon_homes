class SchedActivitiesOds < ActiveRecord::Base
	attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_SCHEDACTIVITIES"
  self.primary_key="ACTIVITYCODE"

  def self.load_ods_sched_activities(windows_db_nm)

  	truncate_ods_sched_activities

    puts "********** 1A) Started Loading SCHEDACTIVITIES Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
      #message = "Inside the Lots info Load Method"
      #Mailer.notify_user_on_client_failure(message).deliver
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from "+windows_db_nm+".dbo.SCHEDACTIVITIES");
      	puts "............................................."
      	puts "............................................."

        batch=[]
        batch_size = 1000

        result.each do |row|
          #SchedActivities.find_or_initialize_by_COMPANYCODE_and_DEVELOPMENTCODE_and_LOTNUMBER(row['COMPANYCODE'], row['DEVELOPMENTCODE'], row['LOTNUMBER'])
          # batch << SchedActivitiesOds.new(row)
           batch << SchedActivitiesOds.new(:COMPANYCODE => row['COMPANYCODE'],
                                          :ACTIVITYCODE => row['ACTIVITYCODE'],
                                          :DESCRIPTION => row['DESCRIPTION'],
                                          :AMOUNT1 => row['AMOUNT1'],
                                          :AMOUNT2 => row['AMOUNT2'],
                                          :SUSPENDABLE => row['SUSPENDABLE']
                                            )
            if batch.size >= batch_size
              SchedActivitiesOds.import batch
              batch = [] 
            end  
        end
        SchedActivitiesOds.import batch
        # SchedActivitiesOds.mass_insert(batch, :per_batch => 1000)
    end

    puts "Total no. of Loaded rows.........#{SchedActivitiesOds.count}"
    puts "********** 1B) Finished Loading SchedActivities Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_sched_activities
		ActiveRecord::Base.connection.execute("TRUNCATE ODS_SCHEDACTIVITIES")
	end


end