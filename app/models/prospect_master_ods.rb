class ProspectMasterOds < ActiveRecord::Base
	attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_PROSPECTMASTER"
  belongs_to :house_master, :class_name => "HouseMasterOds" 
  has_one :sales_person1, :class_name =>"SalesPersonMasterOds",:foreign_key => [:COMPANYCODE, :SALESPERSONCODE], :primary_key => [:COMPANYCODE, :SALESPERSONCODE]
  has_one :sales_person2, :class_name =>"SalesPersonMasterOds",:foreign_key => [:COMPANYCODE, :SALESPERSONCODE], :primary_key => [:COMPANYCODE, :SALESPERSONCODE2]
  has_one :sales_person3, :class_name =>"SalesPersonMasterOds",:foreign_key => [:COMPANYCODE, :SALESPERSONCODE], :primary_key => [:COMPANYCODE, :SALESPERSONCODE3]
  has_one :agent_record, :class_name =>"AgentMasterOds",:foreign_key => [:AGENTCODE], :primary_key => [:AGENTCODE]
  has_one :broker_record, :class_name =>"BrokerMasterOds",:foreign_key => [:BROKERCODE], :primary_key => [:BROKERCODE]
  
  def self.load_ods_prospect_master(dev_codes,windows_db_nm)

  	truncate_ods_prospect_master

    puts "********** 1A) Started Loading ProspectMasterOds Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
      #message = "Inside the Lots info Load Method"
      #Mailer.notify_user_on_client_failure(message).deliver
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from "+windows_db_nm+".dbo.PROSPECTMASTER where DEVELOPMENTCODE in ("+dev_codes+")  and HOUSENUMBER != '00000000'");
      	puts "............................................."
      	puts "............................................."

      	batch=[]
        batch_size = 1000

        result.each do |row|
          batch << ProspectMasterOds.new(:CASENUMBER => row['CASENUMBER'],
										:COMPANYCODE => row['COMPANYCODE'],
										:DEVELOPMENTCODE => row['DEVELOPMENTCODE'],
										:LASTNAME => row['LASTNAME'],
										:FIRSTNAME => row['FIRSTNAME'],
                    :MIDDLEINITIAL => row['MIDDLEINITIAL'], 
                    :SALUTATION => row['SALUTATION'], 
                    :STREETADDRESS1 => row['STREETADDRESS1'], 
										:STREETADDRESS2 => row['STREETADDRESS2'],	
										:CITY => row['CITY'],
										:STATE => row['STATE'],
										:ZIP => row['ZIP'],
                    :HOMEPHONE1 => row['HOMEPHONE1'],
                    :HOMEPHONE2 => row['HOMEPHONE2'],
                    :WORKPHONE1 => row['WORKPHONE1'],
                    :WORKPHONE2 => row['WORKPHONE2'],
                    :OTHERPHONE1 => row['OTHERPHONE1'],
                    :OTHERPHONE2 => row['OTHERPHONE2'],
                    :STATUSCODE => row['STATUSCODE'],
                    :TRAFFICDATE1 => row['TRAFFICDATE1'],
                    :TRAFFICDATE2 => row['TRAFFICDATE2'],
                    :TRAFFICDATE3 => row['TRAFFICDATE3'],
                    :TRAFFICDATE4 => row['TRAFFICDATE4'],
										:SETTLEMENTDATE => row['SETTLEMENTDATE'],
										:CONTRACTDATE => row['CONTRACTDATE'],
										:RATIFIEDDATE => row['RATIFIEDDATE'],
										:MODELCODE => row['MODELCODE'],
										:ELEVATIONCODE => row['ELEVATIONCODE'],
                    :SALESPERSONCODE => row['SALESPERSONCODE'],
                    :HOUSENUMBER => row['HOUSENUMBER'], 
                    :SELECTSCHDDATE => row['SELECTSCHDDATE'],
                    :SELECTCMPLDATE => row['SELECTCMPLDATE'],
                    :SELECTCUTOFFDATE => row['SELECTCUTOFFDATE'],
                    :STARTDATE => row['STARTDATE'],
                    :ESTCOMPDATE => row['ESTCOMPDATE'],
                    :OTHERAMT1DESC => row['OTHERAMT1DESC'],
                    :OTHERAMT2DESC => row['OTHERAMT2DESC'],
                    :OTHERAMT3DESC => row['OTHERAMT3DESC'],
                    :AGENTCODE => row['AGENTCODE'],
                    :BROKERCODE => row['BROKERCODE'],
                    :CUSTSIGNEDDATE => row['CUSTSIGNEDDATE'],
                    :SALESPERSONCODE2 => row['SALESPERSONCODE2'],
                    :SALESPERSONCODE3 => row['SALESPERSONCODE3'],
                    :CROSSREFERENCE => row['CROSSREFERENCE'],
                    :SPECHOUSEFLAG => row['SPECHOUSEFLAG'],
                    :SOURCE => row['SOURCE'],
                    :EMAIL => row['EMAIL'],
                    :TIMECREATED => row['TIMECREATED'],
                    :DATECREATED => row['DATECREATED'],
                    :USERID => row['USERID'],
                    :UNUSED => row['UNUSED'],
                    :BASEPRICE => row['BASEPRICE'],
                    :LOTPREMIUM => row['LOTPREMIUM'],
                    :OPTIONSPRICE => row['OPTIONSPRICE'],
                    :UPGRADESPRICE => row['UPGRADESPRICE'],
                    :BASEDEPOSIT => row['BASEDEPOSIT'],
                    :OPTIONDEPOSIT => row['OPTIONDEPOSIT'],
                    :OTHERAMT1 => row['OTHERAMT1'],
                    :OTHERAMT2 => row['OTHERAMT2'],
                    :OTHERAMT3 => row['OTHERAMT3'],
                    :AGENTAMOUNT => row['AGENTAMOUNT'],
                    :BROKERAMOUNT => row['BROKERAMOUNT']
								)
								if batch.size >= batch_size 
                  ProspectMasterOds.import batch
                  batch = [] 
                end
        end
        		ProspectMasterOds.import batch
    end

    puts "Total no. of Loaded rows.........#{ProspectMasterOds.count}"
    puts "********** 1B) Finished Loading ProspectMasterOds Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_prospect_master
		ActiveRecord::Base.connection.execute("TRUNCATE ODS_PROSPECTMASTER")
	end


end