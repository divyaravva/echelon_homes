class LotSalesStatus < ActiveRecord::Migration
  def change
    create_table :lot_sales_statuses ,:id=>false do |t|
      t.primary_key :ID
      t.string :Lot_Sales_Status
      t.string :Color

      t.timestamps
    end
  end
end
