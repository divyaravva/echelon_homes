class DataRefreshStatus < ActiveRecord::Base
	require 'net/ssh'
	require 'tiny_tds'

	self.table_name="DataRefreshStatus"
	attr_protected

	def self.execute_jar_file(refresh_type, client)
		if SITE_ROOT_DOMAIN_NAME == "http://localhost:3000"
			environment = "Development"
		# elsif SITE_ROOT_DOMAIN_NAME == "http://ihmsdemosandbox.stg.lotvue.com" 
	    elsif SITE_ROOT_DOMAIN_NAME.include?".stg."
			environment = "Staging"
		elsif !SITE_ROOT_DOMAIN_NAME.include?".stg." 
			environment = "Production"
		end
			
		if(client.nil? == false && client.active? == true)
			res=client.execute("insert into [echelon_homes].[dbo].[DataLoadStatus] (StartDate, Jar_Status, Total_Job_Status, Environment, Refreshed_Mode) values('#{Time.now.strftime("%Y-%m-%d %H:%M:%S")}', 'Running', 'Running', '#{environment}', '#{refresh_type}')")
			res.cancel
			# @hostname = "52.6.200.70"
			@hostname = "52.5.186.173"
			@username = "lotvue"
			@password = "lotvue"
			#  '\' extra slash works like a escape character
			@cmd = "D:\\lotvue\\echelon_homes\\echelon_homes_DataDumpingJarV0.1\\Launch.bat"

			begin
			    ssh = Net::SSH.start(@hostname, @username, :password => @password,  :paranoid => false)
			    #ssh = Net::SSH.start("52.6.200.70", "lotvue", :password => "lotvue")
			    res = ssh.exec!(@cmd)
			    ssh.close
			    puts res
			    res1=client.execute("update [echelon_homes].[dbo].[DataLoadStatus] set Jar_Status='Succeeded' where id=(select top 1 id from [echelon_homes].[dbo].[DataLoadStatus] order by id desc)");		    
			    res1.cancel
			    Mailer.notify_user_on_successful_jar_execute("Successful").deliver
		 	rescue Exception => e
			  	res=client.execute("update [echelon_homes].[dbo].[DataLoadStatus] set EndDate='#{Time.now.strftime("%Y-%m-%d %H:%M:%S")}', Jar_Status='Failed', Total_Job_Status='Failed' where id=(select top 1 id from [echelon_homes].[dbo].[DataLoadStatus] order by id desc)");
			  	res.cancel
			  	Mailer.notify_user_on_failure_jar_execute(e.message).deliver		  	
		  	end
		end
	end

	def self.load_all_ods_tables
		puts "Loading all table started at......#{Time.now}"
		dev_codes = ['CW'].map(&:inspect).join(', ')
        windows_db_nm = 'echelon_homes'
		ActiveRecord::Base.transaction do

			ContractMasterOds.load_ods_contract_master(dev_codes,windows_db_nm)
			ElevationMasterOds.load_ods_elevation_master(dev_codes,windows_db_nm)
			HouseCostDetailOds.load_ods_house_cost_detail(dev_codes,windows_db_nm)
			HouseCostSummaryOds.load_ods_house_cost_summary(dev_codes,windows_db_nm)
			HouseMasterOds.load_ods_house_master(dev_codes,windows_db_nm)
			OptionLotMasterOds.load_ods_option_lot_master(dev_codes,windows_db_nm)
			ProspectMasterOds.load_ods_prospect_master(dev_codes,windows_db_nm)
			SchedhouseDetailOds.load_ods_schedhouse_detail(dev_codes,windows_db_nm)
			WarrantyMasterOds.load_ods_warranty_master(dev_codes,windows_db_nm)
			CustDepositDtlOds.load_ods_cust_deposit(dev_codes,windows_db_nm)
			GlTrans2013Ods.load_ods_gltrans2013(dev_codes,windows_db_nm)
			GlTrans2014Ods.load_ods_gltrans2014(dev_codes,windows_db_nm)
			GlTrans2015Ods.load_ods_gltrans2015(dev_codes,windows_db_nm)
			GlTrans2016Ods.load_ods_gltrans2016(dev_codes,windows_db_nm)
			SalesSubsidiaryOds.load_ods_sales_subsidiary(dev_codes,windows_db_nm)
			HouseOptionsOds.load_ods_house_options(dev_codes,windows_db_nm)

			JlLoanDetailOds.load_ods_jl_loan_detail(windows_db_nm)
			JlloanMasterOds.load_ods_jlloan_master(windows_db_nm)
			ModelMasterOds.load_ods_model_master(windows_db_nm)
			JlHousesOds.load_ods_jlhouses(windows_db_nm)
			PodistOds.load_ods_podist(windows_db_nm)
			ProspectContingOds.load_ods_prospect_conting(windows_db_nm)
			ProspectFinanceOds.load_ods_prospect_finance(windows_db_nm)			
			SalesPersonMasterOds.load_ods_sales_person_master(windows_db_nm)
			AgentMasterOds.load_ods_agent_master(windows_db_nm)
			BrokerMasterOds.load_ods_broker_master(windows_db_nm)
			DevMasterOds.load_ods_devmaster(windows_db_nm)
			LenderMasterOds.load_ods_lender_master(windows_db_nm)
			JlBankMasterFileOds.load_ods_jlbankmasterfile(windows_db_nm)
			SchedActivitiesOds.load_ods_sched_activities(windows_db_nm)
			SchedHouseHeaderOds.load_ods_sched_house_header(windows_db_nm)

		end
		puts "Loading all table Finished at......#{Time.now}"
	end

end