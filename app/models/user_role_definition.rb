class UserRoleDefinition < ActiveRecord::Base
  attr_protected
  self.table_name = "User_Role_Definitions"  
  belongs_to :user, :foreign_key => "User_Id", :class_name => "Tblusers"
  belongs_to :role, :foreign_key => "Role_Definition_Id",:class_name => "RoleDefinition"  
end