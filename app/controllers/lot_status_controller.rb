class LotStatusController < ApplicationController
  before_filter :authenticate
   before_filter :is_admin
   before_filter :get_product
   before_filter :get_product_view    
   before_filter :get_comm_city_view
   before_filter :get_community_city   
   before_filter :get_communities   
   before_filter :get_communities_view
   before_filter :get_parcels
   before_filter :get_parcels_view
	
	def index
    @lot_status = StatusDetail.find_by_sql("select distinct(StatusKey) , Status_Display_Name , Is_Active from GIS_Status_Details where LegendType='Sales Status' order by LegendOrder")
	end

  def edit
     @legend_details = StatusDetail.find(params[:id]) unless params[:id].nil?
  end

  def update
    status_details = StatusDetail.find(params[:id]) unless params[:id].nil? 
    status_details.update_attributes(:Status_Display_Name => params[:display_status_name])
    redirect_to "/lot_status"
  end

  def status_update
    
    params[:lot_status].each do |hashes|
      lot_status = StatusDetail.find_all_by_LegendType_and_StatusKey("Sales Status", hashes["status_key"])
          unless lot_status.blank?
            lot_status.each do |each_rec|
               if hashes["active"].present?
                each_rec.update_attribute(:Is_Active, 1)
               else
                each_rec.update_attribute(:Is_Active, 0)
               end
           end
          end
    end
     @lot_status = StatusDetail.find_by_sql("select distinct(StatusKey) , Status_Display_Name , Is_Active from GIS_Status_Details where LegendType='Sales Status' order by LegendOrder")
    render :partial => "lot_status_list"
  end

  def display_names_for_status
   @all_stkey_recs = StatusDetail.find_by_sql("select distinct(StatusKey) , Status_Display_Name , Is_Active from GIS_Status_Details where LegendType='Sales Status' order by LegendOrder")
  end

  def display_names
    params[:lot_status].each do |hashes|
      lot_status = StatusDetail.find_all_by_LegendType_and_StatusKey("Sales Status", hashes["status_key"])
        unless lot_status.blank?
            lot_status.each do |each_rec|
               if hashes["display_name"].present?
                each_rec.update_attribute(:Status_Display_Name, hashes["display_name"])
               end
            end
        end
    end     
      redirect_to "/lot_status"
  end
end