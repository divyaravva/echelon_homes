class GalleryImagesToLot < ActiveRecord::Base
  # attr_accessible :image_gallery_id, :lot_info_id, :tbldatum_id, :product_type
  before_create :increment_upload_counter
  before_destroy :decrement_upload_counter
  attr_protected
  belongs_to :image_gallery
  belongs_to :lot ,:foreign_key => "tbldatum_id", :class_name => "Tbldatum"
 
def is_key_image_lot
   self.key_image==0
end 
private 

def increment_upload_counter  
	if product_type == "Lot Buzz"
		Tbldatum.increment_counter(:pic_count_lotbuzz, self.tbldatum_id)
	elsif product_type == "Lot Insight"
    Tbldatum.increment_counter(:pic_count_lotinsight, self.tbldatum_id)
  else
    Tbldatum.increment_counter(:pic_count_lotbuzz, self.tbldatum_id)
    Tbldatum.increment_counter(:pic_count_lotinsight, self.tbldatum_id)
  end
	
end


def decrement_upload_counter
  if product_type == "Lot Buzz"
		Tbldatum.decrement_counter(:pic_count_lotbuzz, self.tbldatum_id)
	elsif product_type == "Lot Insight"
    Tbldatum.decrement_counter(:pic_count_lotinsight, self.tbldatum_id)
  else
    Tbldatum.decrement_counter(:pic_count_lotbuzz, self.tbldatum_id)
    Tbldatum.decrement_counter(:pic_count_lotinsight, self.tbldatum_id)
  end

end

def self.get_lot_ids_array(image_id, phase_name)
  lot_ids_array = self.find_by_sql("select DISTINCT(tbldatum_id) from gallery_images_to_lots where image_gallery_id='#{image_id}' ORDER BY tbldatum_id").map{|x| x.tbldatum_id}
      housenum_array = []
      unless lot_ids_array.blank?        
        lot_ids_array.each do |each_lot_id|
          if phase_name.blank?
            lot_nbr = Tbldatum.find_by_Unique_ID(each_lot_id)
          else
            lot_nbr = Tbldatum.find_by_Unique_ID_and_Phase_Name(each_lot_id, phase_name)
          end    

          unless lot_nbr.blank?
            housenum_hash={}      
            housenum_hash["lot_nbr"]= lot_nbr.Lot_Number
            housenum_hash["phase_name"]= lot_nbr.Phase_Name
            housenum_array <<  housenum_hash
          end
        end
         return housenum_array.uniq
      end      
end

def self.get_filings_for_ltnbrs(lots_array,lot_nbr)
  resultant_array = []
  lots_array.find_all{|k,v| k["lot_nbr"]==lot_nbr}.map{|x|x["phase_name"]}.each do |each_hash|
      resultant_array << each_hash
  end
  resultant_array.join(", ")
end

def self.update_keyimage_if_lot_having_one_image(lot_id, community, product_type)
  images = Tbldatum.find(lot_id).image_galleries.where("gallery_images_to_lots.community"=> community).where("gallery_images_to_lots.product_type LIKE ?", "%#{product_type}%")  
  
  key_image_url = Tbldatum.find(lot_id).Key_Image_Url
  if key_image_url.blank?
     if images.count == 1   
        # gallery_rec = GalleryImagesToLot.find_by_tbldatum_id_and_community_and_product_type(lot_id, community, product_type)
        gallery_rec = GalleryImagesToLot.where("tbldatum_id =? and community =? and product_type like ? ", lot_id, community, "%#{product_type}%" )[0]
        gallery_rec.update_attributes(:key_image => 1, :updated_at => Time.now.utc)
        image = Tbldatum.find(lot_id).image_galleries.where("gallery_images_to_lots.product_type like ? and key_image = ? ","%#{product_type}%", 1)
        image_url = image[0].pics(:thumb_add_images) unless image.blank?
        db_row = Tbldatum.find(lot_id)
        db_row.update_attributes(:Key_Image_Url => image_url, :Key_Image_Id =>image[0].id )

     else
      all_gallery_recs = GalleryImagesToLot.find(:all, :conditions => ["tbldatum_id =? and community=? and product_type  like ? and key_image=?", lot_id, community, "%#{product_type}%", 1])       
      if all_gallery_recs.blank?
         # latest_gallry_img_to_lot_rec = GalleryImagesToLot.find_all_by_tbldatum_id_and_community_and_product_type(lot_id, community, product_type).sort_by(&:created_at).last
         latest_gallry_img_to_lot_rec = GalleryImagesToLot.where("tbldatum_id =? and community =? and product_type like ? ", lot_id, community, "%#{product_type}%" ).sort_by(&:created_at).last
         latest_gallry_img_to_lot_rec.update_attributes(:key_image => 1) unless latest_gallry_img_to_lot_rec.blank?
         image = Tbldatum.find(lot_id).image_galleries.where("gallery_images_to_lots.product_type like ? and key_image = ?", "%#{product_type}%", 1)  unless latest_gallry_img_to_lot_rec.blank?
         image_url = image[0].pics(:thumb_add_images) unless image.blank?
         db_row = Tbldatum.find(lot_id)
         db_row.update_attributes(:Key_Image_Url => image_url, :Key_Image_Id =>image[0].id) unless image_url.blank?
      end
    end  
  end  
end

def self.get_lot_ids_array_for_plans(plan_id, phase_name)
  lot_nmbrs_array = []
  if phase_name.blank?
    all_lots_with_plans = Tbldatum.find_all_by_Floor_Plan_Id(plan_id)
  else
    all_lots_with_plans = Tbldatum.find_all_by_Floor_Plan_Id_and_Phase_Name(plan_id, phase_name)
  end
  
  unless all_lots_with_plans.blank?    
    all_lots_with_plans.each do |each_rec|
    housenum_hash_for_plans = {}
      housenum_hash_for_plans["lot_nbr"] = each_rec.Lot_Number
      housenum_hash_for_plans["phase_name"] = each_rec.Phase_Name
      lot_nmbrs_array << housenum_hash_for_plans
    end
  end
  return lot_nmbrs_array.uniq
end
  
end
