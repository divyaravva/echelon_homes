class SetTimeZoneController < ApplicationController
   
   before_filter :authenticate  
   before_filter :get_product
   before_filter :get_product_view
   before_filter :get_comm_city_view
   before_filter :get_community_city       
   before_filter :get_communities   
   before_filter :get_communities_view
   before_filter :get_parcels
   before_filter :get_parcels_view
  
   def index     
    
   end


   def get_zones
    render :json => { :time_zones => ActiveSupport::TimeZone.us_zones.map{|t| [t.to_s, t.name] } , :saved_time_zone => SetTimeZone.get_saved_time_zone }
   end

   def save
    Rails.cache.delete("echelon_timezone")
    SetTimeZone.save(params["time_zone_new"],@udata)
    render :json => { :time_zones => ActiveSupport::TimeZone.us_zones.map{|t| [t.to_s, t.name] } , :saved_time_zone => SetTimeZone.get_saved_time_zone }
   end

end
