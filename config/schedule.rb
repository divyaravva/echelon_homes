# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#

    def set_time_based_on_time_zone_for_jar_execution
	      # if Time.now.zone == "IST"
	      #     '12:35 pm'
	    if Time.now.zone == "UTC"
	        '5:30 am'
	    end
  end

if "#{environment}" == "development"
  SITE_ROOT_DOMAIN_NAME ="http://localhost:3000"
elsif "#{environment}"=="production"
  SITE_ROOT_DOMAIN_NAME ="http://echelonhomes.stg.lotvue.com"
end

set :output, { error: 'error.log', standard: 'cron.log' }
# set :bundler, "~/.rvm/wrappers/ruby-1.9.3-p362@day-break/bundle"

job_type :gp_rake, "cd :path && RAILS_ENV=:environment bundle exec rake :task --silent :output"

if SITE_ROOT_DOMAIN_NAME == "http://echelonhomes.stg.lotvue.com"
  # every 2.hours do      
  #   rake "run_jar_file[Automatic]"
  # end
  	every 1.day, :at => set_time_based_on_time_zone_for_jar_execution do  
	 	rake "run_jar_file[Automatic]"
	end
end