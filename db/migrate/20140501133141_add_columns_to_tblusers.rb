class AddColumnsToTblusers < ActiveRecord::Migration
  def change
    add_column :tblusers, :ConfirmPassword, :string
    add_column :tblusers, :Is_Active, :string, :default => '0'
    add_column :tblusers, :Is_Confirmed, :string, :default => '0'
    add_column :tblusers, :token, :string
  end
end
