class UdOptLotMasterOds < ActiveRecord::Base
  attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_UDOPTLOTMASTER"
  self.primary_keys = :DEVELOPMENTCODE, :LOTNUMBER
 

  def self.load_ods_udopt_lot_master(windows_db_nm)

    truncate_ods_option_lot_master

    puts "********** 1A) Started Loading UDOPTLOTMASTER Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from "+windows_db_nm+".dbo.UDOPTLOTMASTER");
        puts "Total no. of ODS rows.........#{result.count}"
        puts "............................................."
        puts "............................................."

        batch=[]
        batch_size = 1000

        result.each do |row|
          #ModelMasterOds.find_or_initialize_by_COMPANYCODE_and_DEVELOPMENTCODE_and_LOTNUMBER(row['COMPANYCODE'], row['DEVELOPMENTCODE'], row['LOTNUMBER'])
          batch << UdOptLotMasterOds.new(:COMPANYCODE => row['COMPANYCODE'],
                                      :DEVELOPMENTCODE => row['DEVELOPMENTCODE'],
                                      :LOTNUMBER => row['LOTNUMBER'],
                                      :UNITNUMBER => row['UNITNUMBER'],
                )
            if batch.size >= batch_size 
              UdOptLotMasterOds.import batch
              batch = [] 
            end
        end
        UdOptLotMasterOds.import batch
    end

    puts "Total no. of Loaded rows.........#{UdOptLotMasterOds.count}"
    puts "********** 1B) Finished Loading UdOptLotMasterOds Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_option_lot_master
    ActiveRecord::Base.connection.execute("TRUNCATE ODS_UDOPTLOTMASTER")
  end
end