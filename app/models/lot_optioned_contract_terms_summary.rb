class LotOptionedContractTermsSummary < ActiveRecord::Base
  include ActionView::Helpers::NumberHelper
  attr_protected
  self.table_name = "Lot_Optioned_Contract_Terms_Summary"


   def generate_report_data_json
	  {
	  	:id => self.id,
	  	:Community => self.Community, 
	  	:Contract_Description => self.Contract_Description, 
	  	:No_Of_Lots => self.No_Of_Lots, 
	  	:Deposit_Refund_Terms => self.Deposit_Refund_Terms, 
	  	:Avg_Refund_Per_Lot => self.Avg_Refund_Per_Lot, 
	  	:Take_Down_Terms => self.Take_Down_Terms, 
	  	:Contract_Lot_Cost_Ratio => self.Contract_Lot_Cost_Ratio 
	  }
	 end

end

