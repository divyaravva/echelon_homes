class ManageClickFieldsController < ApplicationController

   before_filter :authenticate
   before_filter :is_admin
   before_filter :get_product
   before_filter :get_product_view  
   before_filter :get_comm_city_view
   before_filter :get_community_city     
   before_filter :get_communities   
   before_filter :get_communities_view
   before_filter :get_parcels
   before_filter :get_parcels_view

  def index
    @lot_click_fields = LotFieldDisplayName.all
  end

  def update
    params[:lot_nb_hver_status] = "on"  unless params[:lot_nb_hver_status].blank? == false
    params[:lot_nb_clk_status] = "on"  unless params[:lot_nb_clk_status].blank? == false
    params[:hver_status] = "off"  unless params[:hver_status].blank? == false
    params[:clk_status] = "off"  unless params[:clk_status].blank? == false
    params[:price_hver_status] = "off"  unless params[:price_hver_status].blank? == false
    params[:price_clk_status] = "off"  unless params[:price_clk_status].blank? == false
    params[:date_hver_status] = "off"  unless params[:date_hver_status].blank? == false
    params[:date_clk_status] = "off"  unless params[:date_clk_status].blank? == false
    params[:size_hver_status] = "off"  unless params[:size_hver_status].blank? == false
    params[:size_clk_status] = "off"  unless params[:size_clk_status].blank? == false
    params[:desc_hver_status] = "off"  unless params[:desc_hver_status].blank? == false
    params[:desc_clk_status] = "off"  unless params[:desc_clk_status].blank? == false
    params[:lt_type_hver_status] = "off"  unless params[:lt_type_hver_status].blank? == false
    params[:lt_type_clk_status] = "off"  unless params[:lt_type_clk_status].blank? == false
    params[:avbl_hver_status] = "off"  unless params[:avbl_hver_status].blank? == false
    params[:avbl_clk_status] = "off"  unless params[:avbl_clk_status].blank? == false
    params[:addr_clk_status] = "off"  unless params[:addr_clk_status].blank? == false
    params[:addr_hver_status] = "off"  unless params[:addr_hver_status].blank? == false
    params[:lot_premium_clk_status] = "off"  unless params[:lot_premium_clk_status].blank? == false
    params[:lot_premium_hver_status] = "off"  unless params[:lot_premium_hver_status].blank? == false
    params[:elevation_hver_status] = "off"  unless params[:elevation_hver_status].blank? == false
    params[:elevation_clk_status] = "off"  unless params[:elevation_clk_status].blank? == false
    params[:sec_r_block_hver_status] = "off"  unless params[:sec_r_block_hver_status].blank? == false
    params[:sec_r_block_clk_status] = "off"  unless params[:sec_r_block_clk_status].blank? == false


  
    lot_fields_details=LotFieldDisplayName.find(1)
    lot_fields_details.update_attributes({:Lot_Number_Hover_Status => params[:lot_nb_hver_status], :Lot_Number_Click_Status => params[:lot_nb_clk_status],
      :Hover_Status => params[:hver_status], :Click_Status => params[:clk_status],
      :Price_Hvr_Status => params[:price_hver_status], :Price_Clk_Status => params[:price_clk_status], 
      :Date_Hvr_Status => params[:date_hver_status], :Date_Clk_Status => params[:date_clk_status],
      :Size_Hvr_Status => params[:size_hver_status], :Size_Clk_Status => params[:size_clk_status],
      :Desc_Hvr_Status => params[:desc_hver_status], :Desc_Clk_Status => params[:desc_clk_status],
      :Lt_Type_Hvr_Status=> params[:lt_type_hver_status], :Lt_Type_Clk_Status => params[:lt_type_clk_status],
      :Avble_Hvr_Status => params[:avbl_hver_status], :Avble_Clk_Status => params[:avbl_clk_status], 
      :Address_Hvr_Status => params[:addr_hver_status], :Address_Clk_Status => params[:addr_clk_status],
      :Lot_Premium_Hvr_Status => params[:lot_premium_hver_status], :Lot_Premium_Clk_Status => params[:lot_premium_clk_status],
      :Elevation_Hvr_Status => params[:elevation_hver_status], :Elevation_Clk_Status => params[:elevation_clk_status],
      :sec_r_block_Hvr_Status => params[:sec_r_block_hver_status], :sec_r_block_Clk_Status => params[:sec_r_block_clk_status],})
    @lot_click_fields = LotFieldDisplayName.all
    redirect_to "/manage_click_fields"
  end


def display_names_for_lot
 
end

 def update_display_names
    display_names = LotFieldDisplayName.find(1)
    params[:lot_number] = "Lot Number"  unless params[:lot_number].blank? == false
    params[:lot_status] = "Lot Status"  unless params[:lot_status].blank? == false
    params[:lot_area] = "Lot Area"  unless params[:lot_area].blank? == false
    params[:sale_price] = "Sale Price"  unless params[:sale_price].blank? == false
    params[:est_com_date] = "Estimated Completion Date"  unless params[:est_com_date].blank? == false
    params[:lot_size] = "Lot Size"  unless params[:lot_size].blank? == false
    params[:description] = "Description"  unless params[:description].blank? == false
    params[:lot_type] = "Lot Type"  unless params[:lot_type].blank? == false
    params[:available_plans] = "Available Plans"  unless params[:available_plans].blank? == false
    params[:address] = "Address"  unless params[:address].blank? == false
    
    display_names.update_attributes(:Lot_Number => params[:lot_number], :Lot_Status => params[:lot_status], :Lot_Area => params[:lot_area],
    :Sale_Price => params[:sale_price], :Estimated_Completion_Date => params[:est_com_date], :Lot_Size => params[:lot_size], :Description => params[:description], 
    :Lot_Type => params[:lot_type], :Available_Plans => params[:available_plans], :Address => params[:address])
    redirect_to "/manage_click_fields"
  end

end