class LotbuzzController < ApplicationController
  def index
    
  end  

  def  render_communitylegend_view 
      @community_name = params[:community]
      @legend_keys = params[:legend_keys]
      @legend_type = params[:legendtype]
      @lotbuzz_site_url = params[:lotbuzz_site_url]
      @udata = Tblusers.find(2)
      @menu_option_id = MenuOption.find_by_MenuOptionName("Sales Status")
     
      @filter_type = params[:filter_type]
      @filter_flag = params[:filter_flag]
      @is_touch = params[:is_touch]
      if @legend_keys.blank?
        @all_lots = @udata.get_lots(@community_name, "",@menu_option_id)
      else
        @all_lots = @udata.get_lots_for_hover(@community_name, "", @legend_keys, "Sales Status", @udata, @menu_option_id.Id,@filter_type, "", "", "").uniq
      end
    @lot_details = StatusDetail.get_legend_values(@legend_type, @community_name, @menu_option_id)
      render :partial => "lotbuzz/legend" 
  end

  def secureview    
    @lots = LotInfo.all  
    @lot_statuses = LotStatus.all
    if params[:from_admin_page] == "from_admin_page"
    render "image_gallery/secureview.js.erb"
    end
  end

  def demo
     @community = params[:community].delete("'")
     @legend_keys = params[:legend_keys]
    render layout: false
  end

  def community_view_index
    @community_name = params[:community]
    @lotbuzz_site_url = params[:lotbuzz_site_url]
    render layout: "layout_remove_header" 
  end

  def lotbuzz_view 
      @community_name = params[:community]
      @lotbuzz_site_url = params[:lotbuzz_site_url]
      @filter = (params[:filter].nil? || params[:filter].blank?)? nil : params[:filter]
      @legend_keys = params[:legend_keys]
      @filter_type = params[:filter_type]
      @udata = Tblusers.find(2)
      @menu_option_id = MenuOption.find_by_MenuOptionName("Sales Status").Id
      @is_touch = params[:is_touch] 
      if @filter.nil?  
        @click_data = @udata.get_lots(@community_name,"", @menu_option_id)
        @hover_data = @click_data 
      else      
         @click_data = @udata.get_lots(@community_name,"", @menu_option_id)
         @all_lots = @hover_data =@udata.get_lots_for_hover(@community_name, "", @legend_keys, "Sales Status", @udata, @menu_option_id,@filter_type, "", "", "").uniq
      end
      if @is_touch == "1"
        render :partial => "lotbuzz/lotbuzz_view_touch"
      else
        render :partial => "lotbuzz/lotbuzz_view"
      end
  end

  def lead_request
    if params[:lot_number].blank?
      params[:lot_number] = ""
    else
      params[:lot_number]= params[:lot_number]
    end
    @community_name = params[:community]
    @parcel = params[:community_parcel]
    lead_details = LeadDetails.create({:Name => params[:name], :Email_Address => params[:email_address], 
      :Contact_Num => params[:phone_number], :Lot_Number => params[:lot_number], 
      :Phase_Name => params[:community_parcel], :Community_Name => @community_name})
    Mailer.lead_request_in_community_web(lead_details).deliver
    render :json=> {:status=>"success"}
  end

  def test
    render :layout => false
  end

end
