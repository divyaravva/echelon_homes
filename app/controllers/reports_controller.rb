class ReportsController < ApplicationController
    include ReportsHelper 
    before_filter :authenticate
    before_filter :has_roles 
    before_filter :get_product
    before_filter :get_product_view      
    before_filter :get_communities   
    before_filter :get_communities_view
    before_filter :get_parcels
    before_filter :get_parcels_view
    before_filter :set_current_user # To access current user in models
    
  # GET /reports
  # GET /reports.json

  def get_logs      
      # render :json => {all_lots_data: Tbldatum.fetch_Rp_SPECS_and_MODELS.map{|lot| lot.generate_report_spec_aging_json}, :distinct_dev_codes => Tbldatum.get_distinct_dev_codes, :distinct_filings => Tbldatum.get_distinct_filing, :distinct_housenumbers => Tbldatum.get_distinct_housenumbers, all_lots_data_by_community: Tbldatum.get_spec_report_community_view ,all_lots_data_by_plan: Tbldatum.get_spec_report_plan_view }
      render :json => Tbldatum.find_by_sql("SELECT * from GIS_LotsInfo where SVG_ID != ''")
   end

  def get_revenue_records        
    render :json => Tbldatum.revenue_records    
  end

   
  def index
   
  end

  # GET /reports/1
  # GET /reports/1.json
  def show
    @report = Report.find(params[:id])
    send_file @report.document.path,
    :filename => @report.document_file_name,
    :type => @report.document_content_type,
    :disposition => 'attachment'

  end

  # GET /reports/new
  # GET /reports/new.json
  def new
   
  end

  # GET /reports/1/edit
  def edit   
  end

  # POST /reports
  # POST /reports.json
  def create 
    current_view=Tab.find_by_name(params["tab_name"]) unless params["tab_name"].nil?         
    unless params[:reports].nil?
      params[:reports].each do |p|
        if p["document"].present?
          current_view.reports.create({:document => p["document"], :user_id => @udata.ID, :display_document_file_name => p["document"].original_filename, :report_type=> params[:report_type]})
          
          # To show the new uploaded report visible to all roles that has uploaded report menu enabled 
          report_id = current_view.reports.last.id
          role_ids = RoleDefinition.get_roles_access_to_uploaded_reports
          unless role_ids.blank?
            role_ids.each do |each_role_id|
              RdsLotCollectionsReportRoleAccess.create(:Role_Id =>each_role_id, :Report_Id =>report_id, :Role_Access_Id=>2, :Report_Type=>"Uploaded")
            end
          end

        else 
          @report_blank_message = true
      end
      end
    end

    @reports=Report.get_the_report_based_on_current_tab(@udata)
    @tab_name = params["tab_name"]
    @access_permission=params["access_permission"]
    render :template => "reports/view", :layout => false 
  end

  # PUT /reports/1
  # PUT /reports/1.json
  def update
  
  end

  # DELETE /reports/1
  # DELETE /reports/1.json
  def destroy    
  
  end

  def delete 
    @report = Report.find(params[:id])
    @report.destroy
    current_view=Tab.find_by_name(params["tab_name"]) unless params["tab_name"].nil?
    # @reports=Report.get_the_report_based_on_current_tab(current_view, @udata)
    @reports=Report.get_the_report_based_on_current_tab(@udata)
    @tab_name = params["tab_name"]
    @access_permission=params["access_permission"]
    render :template => "reports/view", :layout => false 
  end

  def print_report    
    report_type = params[:report_type]  
    dev_code = params[:dev_code]      
    status = params[:status]
    lots = []    
    if report_type=="Sales Status"
        lots = Tbldatum.get_objects_as_json(report_type,status, dev_code)
        sales_count = lots.count
    end    
    generate_excel_report(lots,report_type)

  end

  def activity_log
    @logs=ActivityLogger.find(:all, :order => "id DESC")
  end

  def update_uploaded_reports_info
    report_details = Report.find(params[:id])
    report_details.update_attributes(:description => params[:description], :display_document_file_name => params[:file_name])
    current_view=Tab.find_by_name(params["tab_name"]) unless params["tab_name"].nil?
    @reports=Report.get_the_report_based_on_current_tab(current_view, @udata)
    @tab_name = params["tab_name"]
    render :template => "reports/view", :layout => false 
  end

  def fetch_spec_aging_lots
    render :json => SpecAgingLot.all
  end

  def save_spec_aging_rec
    params["spec_aging_rec"]["Rp_Aging"]=( Time.now.to_date-DateTime.strptime( params["spec_aging_rec"]["Rp_QC_Stake_Date"],"%m-%d-%Y")).to_i
    params["spec_aging_rec"]["Rp_QC_Stake_Date"]=DateTime.strptime(params["spec_aging_rec"]["Rp_QC_Stake_Date"],"%m-%d-%Y")
    SpecAgingLot.create(params["spec_aging_rec"])
    render :json =>{all_lots_data: Tbldatum.fetch_Rp_SPECS_and_MODELS.map{|lot| lot.generate_report_spec_aging_json}, all_lots_data_by_community: Tbldatum.get_spec_report_community_view ,all_lots_data_by_plan: Tbldatum.get_spec_report_plan_view}
  end

  def update_spec_aging_lots
    spec_lot_attr=params["object_to_update"]["spec_lots_info"]
    spec_aging_rec = SpecAgingLot.find(spec_lot_attr["Id"]) unless spec_lot_attr["Id"].nil?
    if spec_aging_rec.Rp_QC_Stake_Date.strftime("%m-%d-%Y") != spec_lot_attr["Rp_QC_Stake_Date"]
      spec_lot_attr["Rp_Aging"]=( Time.now.to_date-DateTime.strptime(spec_lot_attr["Rp_QC_Stake_Date"],"%m-%d-%Y")).to_i
      spec_lot_attr["Rp_QC_Stake_Date"]=DateTime.strptime(spec_lot_attr["Rp_QC_Stake_Date"],"%m-%d-%Y")
    else    
      spec_lot_attr["Rp_QC_Stake_Date"]=DateTime.strptime(spec_lot_attr["Rp_QC_Stake_Date"],"%m-%d-%Y")
    end
    spec_aging_rec.update_attributes(spec_lot_attr.except("Id")) unless spec_aging_rec.nil?
    render :json =>{all_lots_data: Tbldatum.fetch_Rp_SPECS_and_MODELS.map{|lot| lot.generate_report_spec_aging_json}, all_lots_data_by_community: Tbldatum.get_spec_report_community_view ,all_lots_data_by_plan: Tbldatum.get_spec_report_plan_view}
  end

  def delete_spec_aging_rec
    SpecAgingLot.find(params["Id"]).destroy rescue nil
    render :json =>{all_lots_data: Tbldatum.fetch_Rp_SPECS_and_MODELS.map{|lot| lot.generate_report_spec_aging_json}, all_lots_data_by_community: Tbldatum.get_spec_report_community_view ,all_lots_data_by_plan: Tbldatum.get_spec_report_plan_view}
  end
  
  def get_generated_reports
   render :json => Tblusers.get_generated_reports_with_view_and_edit_access(@udata)
  end

  def get_generated_reports_for_edit_access
   render :json => Tblusers.get_generated_reports_for_edit_access(@udata)
  end

  def update_lot_new_stake_date_owned
      lot=Tbldatum.find(params["Id"]) rescue nil
      lot.update_lot_new_stake_date(params["new_start_stake_date"]) unless lot.nil?
      render :json =>{
          :lots_owned => Tbldatum.ownerhship("Lot Owned").map{|lot| lot.generate_owned_optioned_json},
          :lots_optioned => Tbldatum.ownerhship("Lot Optioned").map{|lot| lot.generate_owned_optioned_json},
          :lots_owned_summary => Tbldatum.get_lot_aging_dashboard_owned_summary , 
          :lots_optioned_summary => Tbldatum.get_lot_aging_dashboard_optioned_summary, 
          :loan_lender_summary_columns => Tbldatum.get_columns_for_lot_aging_dashboard_loan_by_lender, 
          :loan_lender_summary => Tbldatum.get_lot_aging_lots_owned_loan_amount_by_lender_summary,
          :contract_terms_summary => Tbldatum.get_contract_terms_summary.map{ |lot| lot.generate_report_data_json }
        }
  end

  def update_lot_new_stake_date_optioned
      lot=Tbldatum.find(params["Id"]) rescue nil
      lot.update_lot_new_stake_date(params["new_start_stake_date"]) unless lot.nil?
       render :json =>{
          :lots_owned => Tbldatum.ownerhship("Lot Owned").map{|lot| lot.generate_owned_optioned_json},
          :lots_optioned => Tbldatum.ownerhship("Lot Optioned").map{|lot| lot.generate_owned_optioned_json},
          :lots_owned_summary => Tbldatum.get_lot_aging_dashboard_owned_summary , 
          :lots_optioned_summary => Tbldatum.get_lot_aging_dashboard_optioned_summary, 
          :loan_lender_summary_columns => Tbldatum.get_columns_for_lot_aging_dashboard_loan_by_lender, 
          :loan_lender_summary => Tbldatum.get_lot_aging_lots_owned_loan_amount_by_lender_summary,
          :contract_terms_summary => Tbldatum.get_contract_terms_summary.map{ |lot| lot.generate_report_data_json }
        }
  end

  def fetch_lot_aging_data
    render :json =>{
      :lots_owned => Tbldatum.ownerhship("Lot Owned").map{|lot| lot.generate_owned_optioned_json},
      :lots_optioned => Tbldatum.ownerhship("Lot Optioned").map{|lot| lot.generate_owned_optioned_json},
      :lots_owned_summary => Tbldatum.get_lot_aging_dashboard_owned_summary , 
      :lots_optioned_summary => Tbldatum.get_lot_aging_dashboard_optioned_summary, 
      :loan_lender_summary_columns => Tbldatum.get_columns_for_lot_aging_dashboard_loan_by_lender, 
      :loan_lender_summary => Tbldatum.get_lot_aging_lots_owned_loan_amount_by_lender_summary,
      :contract_terms_summary => Tbldatum.get_contract_terms_summary.map{ |lot| lot.generate_report_data_json},
      :distinct_community_desc => Tbldatum.get_distinct_community_desc
    }
  end

  def save_lot_aging_contract_terms_summary    
    added_contract_summary=LotOptionedContractTermsSummary.create(params["contract_terms_summary"])
    
    unless added_contract_summary.blank?
      development_code = Tbldatum.get_community_from_community_description(added_contract_summary.Community)      
      added_contract_summary.update_attributes(:Development_Code => development_code)
    end

    render :json =>{
      :lots_owned => Tbldatum.ownerhship("Lot Owned").map{|lot| lot.generate_owned_optioned_json},
      :lots_optioned => Tbldatum.ownerhship("Lot Optioned").map{|lot| lot.generate_owned_optioned_json},
      :lots_owned_summary => Tbldatum.get_lot_aging_dashboard_owned_summary , 
      :lots_optioned_summary => Tbldatum.get_lot_aging_dashboard_optioned_summary, 
      :loan_lender_summary_columns => Tbldatum.get_columns_for_lot_aging_dashboard_loan_by_lender, 
      :loan_lender_summary => Tbldatum.get_lot_aging_lots_owned_loan_amount_by_lender_summary,
      :contract_terms_summary => Tbldatum.get_contract_terms_summary.map{ |lot| lot.generate_report_data_json }
    }
  end

  def update_lot_aging_contract_terms_summary
     lot=LotOptionedContractTermsSummary.find(params["contract_terms_summary"]["id"]) rescue nil
     development_code = Tbldatum.get_community_from_community_description(params["contract_terms_summary"]["Community"])
     lot.update_attributes(params["contract_terms_summary"]) unless lot.nil?
     lot.update_attributes(:Development_Code => development_code) unless lot.nil?
     render :json =>{
      :lots_owned => Tbldatum.ownerhship("Lot Owned").map{|lot| lot.generate_owned_optioned_json},
      :lots_optioned => Tbldatum.ownerhship("Lot Optioned").map{|lot| lot.generate_owned_optioned_json},
      :lots_owned_summary => Tbldatum.get_lot_aging_dashboard_owned_summary , 
      :lots_optioned_summary => Tbldatum.get_lot_aging_dashboard_optioned_summary, 
      :loan_lender_summary_columns => Tbldatum.get_columns_for_lot_aging_dashboard_loan_by_lender, 
      :loan_lender_summary => Tbldatum.get_lot_aging_lots_owned_loan_amount_by_lender_summary,
      :contract_terms_summary => Tbldatum.get_contract_terms_summary.map{ |lot| lot.generate_report_data_json }
    }

  end

  def delete_contract_term_summary
    LotOptionedContractTermsSummary.find(params["id"]).destroy rescue nil
    render :json =>{
      :lots_owned => Tbldatum.ownerhship("Lot Owned").map{|lot| lot.generate_owned_optioned_json},
      :lots_optioned => Tbldatum.ownerhship("Lot Optioned").map{|lot| lot.generate_owned_optioned_json},
      :lots_owned_summary => Tbldatum.get_lot_aging_dashboard_owned_summary , 
      :lots_optioned_summary => Tbldatum.get_lot_aging_dashboard_optioned_summary, 
      :loan_lender_summary_columns => Tbldatum.get_columns_for_lot_aging_dashboard_loan_by_lender, 
      :loan_lender_summary => Tbldatum.get_lot_aging_lots_owned_loan_amount_by_lender_summary,
      :contract_terms_summary => Tbldatum.get_contract_terms_summary.map{ |lot| lot.generate_report_data_json }
    }
  end

end
