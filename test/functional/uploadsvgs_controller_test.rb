require 'test_helper'

class UploadsvgsControllerTest < ActionController::TestCase
  setup do
    @uploadsvg = uploadsvgs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:uploadsvgs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create uploadsvg" do
    assert_difference('Uploadsvg.count') do
      post :create, uploadsvg: {  }
    end

    assert_redirected_to uploadsvg_path(assigns(:uploadsvg))
  end

  test "should show uploadsvg" do
    get :show, id: @uploadsvg
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @uploadsvg
    assert_response :success
  end

  test "should update uploadsvg" do
    put :update, id: @uploadsvg, uploadsvg: {  }
    assert_redirected_to uploadsvg_path(assigns(:uploadsvg))
  end

  test "should destroy uploadsvg" do
    assert_difference('Uploadsvg.count', -1) do
      delete :destroy, id: @uploadsvg
    end

    assert_redirected_to uploadsvgs_path
  end
end
