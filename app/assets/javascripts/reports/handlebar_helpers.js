//handlebars helper for math expression divided by 0
Handlebars.registerHelper("divbyzero", function(arr_index, options){
return arr_index%2==0 ? options.fn() : options.inverse()  
});

//handlebars custom block helper for displaying links with page numbers
Handlebars.registerHelper("each_upto", function(collection, options){
  var res=[]
  var pages=Math.floor(collection.length/perPage);
  if(pages==0)
    return res  
  var lastpage=collection.length-(pages*perPage);
  if(lastpage>0){
    pages=pages+1;
  }  
  for(var i=1; i<=pages ;i++){
    res.push(options.fn(i))    
  }  
  return res.join("");
});

//handlebars helper for appending css class
Handlebars.registerHelper("tbldata_pager", function(index,current_page_index, options){  
return index==current_page_index ? options.fn() : options.inverse()  
});

//handlebars helper for next page hiding and showing
Handlebars.registerHelper("next_pager", function(collection,current_page_index, options){ 
  var pages=Math.floor(collection.length/perPage);  
  var lastpage=collection.length-(pages*perPage);
  if(lastpage>0){
    pages=pages+1;
  } 
  if(pages==0){
    pages=1
  }   
  return current_page_index==pages ? options.inverse() : options.fn()
});

//handlebars helper for previous page hiding and showing
Handlebars.registerHelper("previous_pager", function(collection,current_page_index, options){  
return current_page_index==1 ? options.inverse() : options.fn()  
});
Handlebars.registerHelper("ifgreaterthanzero", function(colleection_length, options){
return colleection_length>0 ? options.fn() : options.inverse()  
});

Handlebars.registerHelper("isSalesCancellation", function(report_type, options){    
  if(report_type =="Home Sale Cancellations" || report_type =="Home Sales and Cancellations"){
    return options.fn()  
  }    
});

Handlebars.registerHelper("isSales", function(report_type, options){    
  if(report_type =="Home Sales" || report_type =="Home Sale Cancellations" || report_type =="Home Sales and Cancellations"){
    return options.fn()  
  }    
});

Handlebars.registerHelper("isSalesByOutsideRealtor", function(report_type, options){    
  if(report_type =="Home Sales by Outside Realtor"){
    return options.fn()  
  }    
});

Handlebars.registerHelper("isSalesByReferrer", function(report_type, options){    
  if(report_type =="Home Sales by Referrer"){
    return options.fn()  
  }    
});
