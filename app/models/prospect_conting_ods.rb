class ProspectContingOds < ActiveRecord::Base
	attr_protected
  require 'tiny_tds'

  self.table_name = "ODS_PROSPECTCONTING"

  def self.load_ods_prospect_conting(windows_db_nm)

  	truncate_ods_prospect_conting

    puts "********** 1A) Started Loading ProspectContingOds Table contents at..#{Time.now} ***********"
    client = TinyTds::Client.new(:dataserver => 'EH_SERVER', :username => 'sa', :password => 'Matter2')
    if (client.nil? == true)
    elsif (client.nil? == false && client.active? == true)
      result = client.execute("select * from "+windows_db_nm+".dbo.PROSPECTCONTING");
      	puts "............................................."
      	puts "............................................."

        batch=[]
        batch_size = 1000

        result.each do |row|
          batch << ProspectContingOds.new(:CASENUMBER => row['CASENUMBER'],
                                      :CONTINGENCYNUM => row['CONTINGENCYNUM'],
                                      :DUEDATE => row['DUEDATE'],
                                      :SATISFIEDDATE => row['SATISFIEDDATE'],
                                      :DESCRIPTION => row['DESCRIPTION'],
                                      :AMOUNT => row['AMOUNT']
								)
            if batch.size >= batch_size 
              ProspectContingOds.import batch
              batch = [] 
            end
        end
        ProspectContingOds.import batch
    end

    puts "Total no. of Loaded rows.........#{ProspectContingOds.count}"
    puts "********** 1B) Finished Loading ProspectContingOds Table contents...#{Time.now} ***********"
  end

  def self.truncate_ods_prospect_conting
		ActiveRecord::Base.connection.execute("TRUNCATE ODS_PROSPECTCONTING")
	end

 end