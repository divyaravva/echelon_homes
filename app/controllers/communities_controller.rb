class CommunitiesController < ApplicationController
  before_filter :authenticate
  before_filter :has_roles  
  before_filter :get_product
  before_filter :get_product_view
  before_filter :get_comm_city_view
  before_filter :get_community_city 
  before_filter :get_communities  
  before_filter :get_communities_view    
  before_filter :get_parcels    
  # before_filter :get_parcels_view
    
  def index  
  	  	      
  end

  def render_community_view
    @filter = (params[:filter].nil? || params[:filter].blank?)? nil : params[:filter]
    @legend_keys = params[:legend_keys]
    @colurize_data = ProductSalesContractParcel.fetch_color_info
    @is_touch = params[:is_touch]
    @all_parcels = 
    if @filter.nil?   
      @hover_data = ProductSalesContractParcel.all
    else
      @hover_data = ProductSalesContractParcel.fetch_parcel_info_filter(@legend_keys)
    end  	
    if @is_touch == "1"
      render :partial => "communities/community_view_touch"
    else
      render :partial => "communities/community_view"
    end
  end

  def render_community_legend_view
  	@legend_data=  ParcelStatus.all
	  render :partial => "communities/commonlegend"
  end

  def show_community 
      
  end


end
