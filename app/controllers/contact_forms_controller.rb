class ContactFormsController < ApplicationController
   before_filter :authenticate
  before_filter :has_roles  
  before_filter :get_product
  before_filter :get_product_view
  before_filter :get_comm_city_view
  before_filter :get_community_city
  before_filter :get_communities  
  before_filter :get_communities_view    
  before_filter :get_parcels    
  before_filter :get_parcels_view

  def index
    if params[:city].blank?
  	 @contact_info = ContactForm.all 
    else
      @contact_info = ContactForm.where(:city_name => params[:city])
    end
    @city_names = ContactForm.pluck(:city_name)
    if ContactForm.count == LotCollection.pluck(:community_city).uniq.count
      @matched_cnt = "yes"
    else
      @matched_cnt = "no"
    end
    respond_to do |format|
      format.html
      format.js
    end
  end

  def new
    @contact = ContactForm.new
    @city_names_select = LotCollection.pluck(:community_city).uniq - ContactForm.pluck(:city_name).uniq
  end

  def create
    @contact = ContactForm.create(params[:contact_form])
    redirect_to contact_forms_path
  end

  def edit
  	@contact =  @contact_info = ContactForm.find(params[:id])
  end

  def update
    contact_info_details = ContactForm.find(params[:id])
    contact_info_details.update_attributes(params[:contact_form])
    redirect_to "/contact_forms"
  end

end